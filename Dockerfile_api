# Dockerfile for the LMS API

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

COPY *.sln .

# Copy all needed projects
COPY CreditBook.LMS.Common CreditBook.LMS.Common/
COPY CreditBook.LMS.API.Core CreditBook.LMS.API.Core/
COPY CreditBook.LMS.Data.Core CreditBook.LMS.Data.Core/
COPY CreditBook.LMS.Domain.Core CreditBook.LMS.Domain.Core/
COPY CreditBook.LMS.External.Core CreditBook.LMS.External.Core/
COPY CreditBook.LMS.Repository.Core CreditBook.LMS.Repository.Core/
COPY CreditBook.LMS.Service.Core CreditBook.LMS.Service.Core/

WORKDIR /app/CreditBook.LMS.API.Core
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 as runtime
WORKDIR /app
COPY --from=build /app/CreditBook.LMS.API.Core/out ./
ENTRYPOINT ["dotnet", "CreditBook.LMS.API.Core.dll"]