﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace CreditBook.LMS.Common.Utilities
{
    public class EmailManager
    {
        //public static async Task SendIBVConfirmation(string emailContent, string sendGridApiKey, List<string> emails)
        //{
        //    try
        //    {
        //        foreach (var email in emails)
        //        {
        //            var apiKey = sendGridApiKey; //Environment.GetEnvironmentVariable(sendGridApiKey);
        //            var client = new SendGridClient(apiKey);
        //            var from = new EmailAddress("info@creditbook.ca", "Credit Book LMS");
        //            var subject = "Nouvelle demande IBV";
        //            var to = new EmailAddress(email);
        //            var plainTextContent = emailContent;
        //            //var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
        //            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, null);
        //            await client.SendEmailAsync(msg);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static async Task SendForgotPasswordConfirmation(string newPassword, string sendGridApiKey, List<string> emails)
        {
            try
            {
                foreach (var email in emails)
                {
                    var apiKey = sendGridApiKey; //Environment.GetEnvironmentVariable(sendGridApiKey);
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress("info@creditbook.ca", "Credit Book LMS");
                    var subject = "Nouvelle demande IBV";
                    var to = new EmailAddress(email);
                    var plainTextContent = "Voici votre mot de passe temporaire. Vous pourrez le modifier une fois connecté. " + newPassword;
                    //var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
                    var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, null);
                    await client.SendEmailAsync(msg);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task SendPerceptechErrorReport(string reportPath, string reportName, string sendGridApiKey, List<string> emails, string companyName, string language)
        {
            try
            {
                foreach (var email in emails)
                {
                    var apiKey = sendGridApiKey;
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress("info@creditbook.ca", "Credit Book LMS");
                    var subject = language == "FR" ? "Perceptech Rapport d'erreur" : "Perceptech Error Report";
                    var to = new EmailAddress(email);
                    var date = DateTime.Today;
                    var plainTextContent = companyName + " Date: " + date.Year + "-" + date.Month.ToString("d2") + "-" + date.Day.ToString("d2");
                    var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, null);

                    var bytes = File.ReadAllBytes(reportPath + reportName);
                    var file = Convert.ToBase64String(bytes);
                    msg.AddAttachment(reportName, file);

                    var response = await client.SendEmailAsync(msg);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task SendUploadError(string sendGridApiKey, List<string> emails, string language, string uploadErrorPath, string uploadFileName, string companyName)
        {
            try
            {
                var emailList = new List<EmailAddress>();
                foreach (var email in emails)
                {
                    emailList.Add(new EmailAddress() { Email = email });
                }

                var apiKey = sendGridApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("info@creditbook.ca", "Credit Book LMS");
                var subject = language == "FR" ? "Perceptech Erreur de téléchargement du fichier" : "Perceptech Upload File Error";
                var date = DateTime.Today;
                var plainTextContent = companyName + " Date: " + date.Year + "-" + date.Month.ToString("d2") + "-" + date.Day.ToString("d2");
                var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, emailList, subject, plainTextContent, null);

                var bytes = File.ReadAllBytes(uploadErrorPath + uploadFileName);
                var file = Convert.ToBase64String(bytes);
                msg.AddAttachment(uploadFileName, file);

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static async Task SendDailyRejecttionError(string sendGridApiKey, List<string> emails, string companyName)
        {
            try
            {
                var emailList = new List<EmailAddress>();
                foreach (var email in emails)
                {
                    emailList.Add(new EmailAddress() { Email = email });
                }

                var apiKey = sendGridApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("info@creditbook.ca", "Credit Book LMS");
                var subject = "Daily Rejection Error";
                var date = DateTime.Today.AddDays(-1);
                var plainTextContent = companyName + " Date: " + date.Year + "-" + date.Month.ToString("d2") + "-" + date.Day.ToString("d2");
                var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, emailList, subject, plainTextContent, null);

                await client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
