using System.Net.Mail;
using System.Threading.Tasks;

namespace CreditBook.LMS.External.Core
{
    public interface ISendgridClient
    {
        Task SendEmail(MailAddress emailFrom, MailAddress emailTo, string subject, string body, string filePath = null, string fileName = null);
    }
}