using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace CreditBook.LMS.External.Core
{
    public class SendgridClient : ISendgridClient
    {
        private readonly string _apiToken;
        private readonly SendGridClient _sendgridClient;

        public SendgridClient(string apiToken)
        {
            _apiToken = apiToken;
            _sendgridClient = new SendGridClient(_apiToken);
        }

        public async Task SendEmail(MailAddress emailFrom, MailAddress emailTo, string subject, string body, string filePath, string fileName)
        {
            var from = new EmailAddress(emailFrom.Address, emailFrom.DisplayName);
            var to = new EmailAddress(emailTo.Address, emailTo.DisplayName);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, null, body);

            if(!string.IsNullOrEmpty(fileName))
            {
                var bytes = File.ReadAllBytes(filePath + fileName);
                var file = Convert.ToBase64String(bytes);
                msg.AddAttachment(fileName, file);
            }

            var response = await _sendgridClient.SendEmailAsync(msg);
        }
    }
}