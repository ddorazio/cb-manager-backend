using System;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace CreditBook.LMS.External.Core
{
    public class TwilioClient : ITwilioClient
    {
        private readonly string _accountSid;
        private readonly string _authToken;
        private readonly string _phoneNumber;

        public TwilioClient(string accountSid, string authToken, string phoneNumber)
        {
            _accountSid = accountSid;
            _authToken = authToken;
            _phoneNumber = phoneNumber;
        }

        public void Initialize()
        {
            Twilio.TwilioClient.Init(_accountSid, _authToken);
        }

        public async Task SendSms(string numberTo, string body)
        {
            try
            {
                var message = await MessageResource.CreateAsync(
                    body: body,
                    from: new Twilio.Types.PhoneNumber(_phoneNumber),
                    to: new Twilio.Types.PhoneNumber(numberTo)
                );
            }
            catch (Exception ex) { } // Catch error for invalid phone number but continue execution

        }
    }
}