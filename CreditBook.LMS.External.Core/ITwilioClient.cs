﻿using System;
using System.Threading.Tasks;

namespace CreditBook.LMS.External.Core
{
    public interface ITwilioClient
    {
        Task SendSms(string numberTo, string body);
    }
}
