using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CreditBook.LMS.External.Core
{
    public class DataHubClient : IDataHubClient
    {
        private readonly string _url;

        private readonly HttpClient _httpClient;

        public DataHubClient(string url, string apiKey)
        {
            _url = url;
            // Configure HTTP client immediately
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(url);
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("API-KEY", apiKey);
        }

        public async Task PostAsync(string path, object payload)
        {
            var body = ObjectToStringContent(payload);
            var result = await _httpClient.PostAsync(path, body);
            if (!result.IsSuccessStatusCode)
            {
                var responseBody = await result.Content.ReadAsStringAsync();
                throw new Exception($"Non-success response from DataHub.\nRequest body: {body}\nResponse body: {responseBody}");
            }
        }

        public async Task PostAsync_Custom(string path, object payload)
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("API-KEY", "iwds6oYY.gk7n5P7qagVsVcZDGyVfJVD23a7iaoKS");
            var body = ObjectToStringContent(payload);
            var result = await _httpClient.PostAsync(path, body);
            if (!result.IsSuccessStatusCode)
            {
                var responseBody = await result.Content.ReadAsStringAsync();
                throw new Exception($"Non-success response from DataHub.\nRequest body: {body}\nResponse body: {responseBody}");
            }
        }

        private StringContent ObjectToStringContent(object payload)
            => new StringContent
            (
                JsonConvert.SerializeObject(payload),
                Encoding.UTF8,
                "application/json"
            );
    }
}