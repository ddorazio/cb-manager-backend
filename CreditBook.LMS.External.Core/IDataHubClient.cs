using System.Threading.Tasks;

namespace CreditBook.LMS.External.Core
{
    public interface IDataHubClient
    {
        Task PostAsync(string path, object payload);
        Task PostAsync_Custom(string path, object payload);
    }
}