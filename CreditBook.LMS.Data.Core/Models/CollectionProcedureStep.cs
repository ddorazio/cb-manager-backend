﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class CollectionProcedureStep
    {
        public CollectionProcedureStep()
        {
            ClientLoanCollectionProcedureStep = new HashSet<ClientLoanCollectionProcedureStep>();
        }

        public int Id { get; set; }
        public string DescriptionEn { get; set; }
        public int? Order { get; set; }
        public string DescriptionFr { get; set; }

        public virtual ICollection<ClientLoanCollectionProcedureStep> ClientLoanCollectionProcedureStep { get; set; }
    }
}
