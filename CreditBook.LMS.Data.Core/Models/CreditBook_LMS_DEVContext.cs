﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class CreditBook_LMS_DEVContext : DbContext
    {
        public CreditBook_LMS_DEVContext()
        {
        }

        public CreditBook_LMS_DEVContext(DbContextOptions<CreditBook_LMS_DEVContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientAddress> ClientAddress { get; set; }
        public virtual DbSet<ClientBankDetails> ClientBankDetails { get; set; }
        public virtual DbSet<ClientComment> ClientComment { get; set; }
        public virtual DbSet<ClientCommentLabel> ClientCommentLabel { get; set; }
        public virtual DbSet<ClientDocument> ClientDocument { get; set; }
        public virtual DbSet<ClientEmailAddress> ClientEmailAddress { get; set; }
        public virtual DbSet<ClientEmploymentStatus> ClientEmploymentStatus { get; set; }
        public virtual DbSet<ClientFinancialStatus> ClientFinancialStatus { get; set; }
        public virtual DbSet<ClientHistory> ClientHistory { get; set; }
        public virtual DbSet<ClientHistoryType> ClientHistoryType { get; set; }
        public virtual DbSet<ClientIbv> ClientIbv { get; set; }
        public virtual DbSet<ClientIbvStatus> ClientIbvStatus { get; set; }
        public virtual DbSet<ClientLoan> ClientLoan { get; set; }
        public virtual DbSet<ClientLoanCollection> ClientLoanCollection { get; set; }
        public virtual DbSet<ClientLoanCollectionComment> ClientLoanCollectionComment { get; set; }
        public virtual DbSet<ClientLoanCollectionProcedureStep> ClientLoanCollectionProcedureStep { get; set; }
        public virtual DbSet<ClientLoanContractSignature> ClientLoanContractSignature { get; set; }
        public virtual DbSet<ClientLoanHistory> ClientLoanHistory { get; set; }
        public virtual DbSet<ClientLoanPayment> ClientLoanPayment { get; set; }
        public virtual DbSet<ClientLoanPaymentAgreement> ClientLoanPaymentAgreement { get; set; }
        public virtual DbSet<ClientLoanPaymentPerceptech> ClientLoanPaymentPerceptech { get; set; }
        public virtual DbSet<ClientMerge> ClientMerge { get; set; }
        public virtual DbSet<ClientReference> ClientReference { get; set; }
        public virtual DbSet<ClientRevenueSource> ClientRevenueSource { get; set; }
        public virtual DbSet<ClientSocial> ClientSocial { get; set; }
        public virtual DbSet<ClientStatus> ClientStatus { get; set; }
        public virtual DbSet<CollectionDegree> CollectionDegree { get; set; }
        public virtual DbSet<CollectionPaymentAgreement> CollectionPaymentAgreement { get; set; }
        public virtual DbSet<CollectionProcedureStep> CollectionProcedureStep { get; set; }
        public virtual DbSet<CollectionStatus> CollectionStatus { get; set; }
        public virtual DbSet<CommentLabel> CommentLabel { get; set; }
        public virtual DbSet<CreditBookCheckStatus> CreditBookCheckStatus { get; set; }
        public virtual DbSet<Deposit> Deposit { get; set; }
        public virtual DbSet<DepositStatus> DepositStatus { get; set; }
        public virtual DbSet<DepositType> DepositType { get; set; }
        public virtual DbSet<DocumentType> DocumentType { get; set; }
        public virtual DbSet<EmailConfiguration> EmailConfiguration { get; set; }
        public virtual DbSet<ExternaNonCompletedRequest> ExternaNonCompletedRequest { get; set; }
        public virtual DbSet<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
        public virtual DbSet<ExternalLoanWebRequestAutoDecline> ExternalLoanWebRequestAutoDecline { get; set; }
        public virtual DbSet<ExternalLoanWebRequestChecklist> ExternalLoanWebRequestChecklist { get; set; }
        public virtual DbSet<FeatureConfiguration> FeatureConfiguration { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<HousingStatus> HousingStatus { get; set; }
        public virtual DbSet<IncomeType> IncomeType { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<LoanAmount> LoanAmount { get; set; }
        public virtual DbSet<LoanChargeConfiguration> LoanChargeConfiguration { get; set; }
        public virtual DbSet<LoanChargeType> LoanChargeType { get; set; }
        public virtual DbSet<LoanConfiguration> LoanConfiguration { get; set; }
        public virtual DbSet<LoanFrequency> LoanFrequency { get; set; }
        public virtual DbSet<LoanPaymentType> LoanPaymentType { get; set; }
        public virtual DbSet<LoanRequestChecklist> LoanRequestChecklist { get; set; }
        public virtual DbSet<LoanRequestStatus> LoanRequestStatus { get; set; }
        public virtual DbSet<LoanStatus> LoanStatus { get; set; }
        public virtual DbSet<LoanerConfiguration> LoanerConfiguration { get; set; }
        public virtual DbSet<MessageTemplate> MessageTemplate { get; set; }
        public virtual DbSet<NonPaymentReason> NonPaymentReason { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<NotificationMessage> NotificationMessage { get; set; }
        public virtual DbSet<NotificationRead> NotificationRead { get; set; }
        public virtual DbSet<PayFrequency> PayFrequency { get; set; }
        public virtual DbSet<PaymentStatus> PaymentStatus { get; set; }
        public virtual DbSet<PerceptechErrorCode> PerceptechErrorCode { get; set; }
        public virtual DbSet<PerceptechRejectionLog> PerceptechRejectionLog { get; set; }
        public virtual DbSet<PerceptechSubmitLog> PerceptechSubmitLog { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<WebhookConfiguration> WebhookConfiguration { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CellPhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.HomePhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdclientStatus).HasColumnName("IDClientStatus");

                entity.Property(e => e.Idgender).HasColumnName("IDGender");

                entity.Property(e => e.Idlanguage).HasColumnName("IDLanguage");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.SocialInsuranceNumber)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientStatusNavigation)
                    .WithMany(p => p.Client)
                    .HasForeignKey(d => d.IdclientStatus)
                    .HasConstraintName("FK_Client_ClientStatus");

                entity.HasOne(d => d.IdgenderNavigation)
                    .WithMany(p => p.Client)
                    .HasForeignKey(d => d.Idgender)
                    .HasConstraintName("FK_Client_Gender");

                entity.HasOne(d => d.IdlanguageNavigation)
                    .WithMany(p => p.Client)
                    .HasForeignKey(d => d.Idlanguage)
                    .HasConstraintName("FK_Client_Language");
            });

            modelBuilder.Entity<ClientAddress>(entity =>
            {
                entity.ToTable("Client_Address");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Apartment).HasMaxLength(16);

                entity.Property(e => e.City).HasMaxLength(100);

                entity.Property(e => e.CivicNumber).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(100);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.Idprovince).HasColumnName("IDProvince");

                entity.Property(e => e.PostalCode).HasMaxLength(100);

                entity.Property(e => e.StartOfResidencyDate).HasColumnType("date");

                entity.Property(e => e.StreetName).HasMaxLength(100);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientAddress)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("Client_Address_Client_FK");

                entity.HasOne(d => d.IdprovinceNavigation)
                    .WithMany(p => p.ClientAddress)
                    .HasForeignKey(d => d.Idprovince)
                    .HasConstraintName("FK_Client_Address_Province");
            });

            modelBuilder.Entity<ClientBankDetails>(entity =>
            {
                entity.ToTable("Client_BankDetails");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BankruptEndDate).HasColumnType("date");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.InstitutionNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TransitNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientBankDetails)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("Client_BankDetails_FK");
            });

            modelBuilder.Entity<ClientComment>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientComment)
                    .HasForeignKey(d => d.Idclient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientComment_FK");
            });

            modelBuilder.Entity<ClientCommentLabel>(entity =>
            {
                entity.HasIndex(e => new { e.IdclientComment, e.IdcommentLabel })
                    .HasName("ClientCommentLabel_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdclientComment).HasColumnName("IDClientComment");

                entity.Property(e => e.IdcommentLabel).HasColumnName("IDCommentLabel");

                entity.HasOne(d => d.IdclientCommentNavigation)
                    .WithMany(p => p.ClientCommentLabel)
                    .HasForeignKey(d => d.IdclientComment)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientCommentLabel_FK");

                entity.HasOne(d => d.IdcommentLabelNavigation)
                    .WithMany(p => p.ClientCommentLabel)
                    .HasForeignKey(d => d.IdcommentLabel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientCommentLabel_CommentLabeL_FK");
            });

            modelBuilder.Entity<ClientDocument>(entity =>
            {
                entity.HasIndex(e => e.S3key)
                    .HasName("ClientDocument_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DeleteDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeleteUsername).HasMaxLength(100);

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdclientLoan).HasColumnName("IDClientLoan");

                entity.Property(e => e.IddocumentType).HasColumnName("IDDocumentType");

                entity.Property(e => e.Note).HasMaxLength(256);

                entity.Property(e => e.S3key)
                    .IsRequired()
                    .HasColumnName("S3Key")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UploadDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientDocument)
                    .HasForeignKey(d => d.Idclient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientDocument_Client_FK");

                entity.HasOne(d => d.IdclientLoanNavigation)
                    .WithMany(p => p.ClientDocument)
                    .HasForeignKey(d => d.IdclientLoan)
                    .HasConstraintName("ClientDocument_ClientLoan_FK");

                entity.HasOne(d => d.IddocumentTypeNavigation)
                    .WithMany(p => p.ClientDocument)
                    .HasForeignKey(d => d.IddocumentType)
                    .HasConstraintName("ClientDocument_FK");
            });

            modelBuilder.Entity<ClientEmailAddress>(entity =>
            {
                entity.ToTable("Client_EmailAddress");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientEmailAddress)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("Client_EmailAddress_Client_FK");
            });

            modelBuilder.Entity<ClientEmploymentStatus>(entity =>
            {
                entity.ToTable("Client_EmploymentStatus");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmployerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Extension)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExtensionHr)
                    .HasColumnName("ExtensionHR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HiringDate).HasColumnType("date");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdpayFrequency).HasColumnName("IDPayFrequency");

                entity.Property(e => e.NextPayDate).HasColumnType("date");

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumberHr)
                    .HasColumnName("PhoneNumberHR")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SupervisorName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientEmploymentStatus)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("Client_EmploymentStatus_FK");

                entity.HasOne(d => d.IdpayFrequencyNavigation)
                    .WithMany(p => p.ClientEmploymentStatus)
                    .HasForeignKey(d => d.IdpayFrequency)
                    .HasConstraintName("FK_Client_EmploymentStatus_PayFrequency");
            });

            modelBuilder.Entity<ClientFinancialStatus>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("ClientFinancialStatus_UN")
                    .IsUnique();

                entity.Property(e => e.GrossAnnualRevenue).HasColumnType("money");

                entity.Property(e => e.MonthlyCarPayment).HasColumnType("money");

                entity.Property(e => e.MonthlyHousingCost).HasColumnType("money");

                entity.Property(e => e.MonthlyOtherLoanPayment).HasColumnType("money");

                entity.Property(e => e.MonthlyUtilityCost).HasColumnType("money");

                entity.HasOne(d => d.Client)
                    .WithOne(p => p.ClientFinancialStatus)
                    .HasForeignKey<ClientFinancialStatus>(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientFinancialStatus_FK");

                entity.HasOne(d => d.HousingStatus)
                    .WithMany(p => p.ClientFinancialStatus)
                    .HasForeignKey(d => d.HousingStatusId)
                    .HasConstraintName("ClientFinancialStatus_HousingStatus_FK");
            });

            modelBuilder.Entity<ClientHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdclientHistoryType).HasColumnName("IDClientHistoryType");

                entity.Property(e => e.Username)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientHistory)
                    .HasForeignKey(d => d.Idclient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientHistory_FK");

                entity.HasOne(d => d.IdclientHistoryTypeNavigation)
                    .WithMany(p => p.ClientHistory)
                    .HasForeignKey(d => d.IdclientHistoryType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientHistory_ClientHistoryType_FK");
            });

            modelBuilder.Entity<ClientHistoryType>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("ClientHistoryType_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.DescriptionFr)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<ClientIbv>(entity =>
            {
                entity.ToTable("Client_IBV");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdclientIbvstatus).HasColumnName("IDClientIBVStatus");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientIbv)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("FK_Client_IBV_Client");

                entity.HasOne(d => d.IdclientIbvstatusNavigation)
                    .WithMany(p => p.ClientIbv)
                    .HasForeignKey(d => d.IdclientIbvstatus)
                    .HasConstraintName("FK_Client_IBV_Client_IBV_Status");
            });

            modelBuilder.Entity<ClientIbvStatus>(entity =>
            {
                entity.ToTable("Client_IBV_Status");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientLoan>(entity =>
            {
                entity.ToTable("Client_Loan");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BrokerageFees).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BrokerageRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ContractUrl)
                    .HasColumnName("ContractURL")
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Fees).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdloanAmount).HasColumnName("IDLoanAmount");

                entity.Property(e => e.IdloanFrequency).HasColumnName("IDLoanFrequency");

                entity.Property(e => e.IdloanStatus).HasColumnName("IDLoanStatus");

                entity.Property(e => e.Idrequest).HasColumnName("IDRequest");

                entity.Property(e => e.InterestAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InterestRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MaxInterestRate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.OriginalPaymentAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.OriginalSubFeesPrice).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Rebates).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StopLoanDate).HasColumnType("date");

                entity.Property(e => e.SubFees).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientLoan)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("FK_Client_Loan_Client");

                entity.HasOne(d => d.IdloanAmountNavigation)
                    .WithMany(p => p.ClientLoan)
                    .HasForeignKey(d => d.IdloanAmount)
                    .HasConstraintName("FK_Client_Loan_Loan_Amount");

                entity.HasOne(d => d.IdloanFrequencyNavigation)
                    .WithMany(p => p.ClientLoan)
                    .HasForeignKey(d => d.IdloanFrequency)
                    .HasConstraintName("FK_Client_Loan_Loan_Frequency");

                entity.HasOne(d => d.IdloanStatusNavigation)
                    .WithMany(p => p.ClientLoan)
                    .HasForeignKey(d => d.IdloanStatus)
                    .HasConstraintName("FK_Client_Loan_LoanStatus");
            });

            modelBuilder.Entity<ClientLoanCollection>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.FollowUpDate).HasColumnType("datetime");

                entity.Property(e => e.IdclientLoan).HasColumnName("IDClientLoan");

                entity.Property(e => e.IdcollectionDegree).HasColumnName("IDCollectionDegree");

                entity.Property(e => e.IdcollectionStatus).HasColumnName("IDCollectionStatus");

                entity.Property(e => e.IdnonPaymentReason).HasColumnName("IDNonPaymentReason");

                entity.Property(e => e.IdpaymentAgreement).HasColumnName("IDPaymentAgreement");

                entity.Property(e => e.InteracConfirmationEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InteracPlannedPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.InteracRealPaymentDate).HasColumnType("datetime");

                entity.HasOne(d => d.IdclientLoanNavigation)
                    .WithMany(p => p.ClientLoanCollection)
                    .HasForeignKey(d => d.IdclientLoan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanCollection_FK");

                entity.HasOne(d => d.IdcollectionDegreeNavigation)
                    .WithMany(p => p.ClientLoanCollection)
                    .HasForeignKey(d => d.IdcollectionDegree)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanCollection_CollectionDegree_FK");

                entity.HasOne(d => d.IdcollectionStatusNavigation)
                    .WithMany(p => p.ClientLoanCollection)
                    .HasForeignKey(d => d.IdcollectionStatus)
                    .HasConstraintName("ClientLoanCollection_FK_1");

                entity.HasOne(d => d.IdnonPaymentReasonNavigation)
                    .WithMany(p => p.ClientLoanCollection)
                    .HasForeignKey(d => d.IdnonPaymentReason)
                    .HasConstraintName("ClientLoanCollection_NonPaymentReason_FK");
            });

            modelBuilder.Entity<ClientLoanCollectionComment>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedDateTime).HasColumnType("datetime");

                entity.Property(e => e.IdclientLoanCollection).HasColumnName("IDClientLoanCollection");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.IdclientLoanCollectionNavigation)
                    .WithMany(p => p.ClientLoanCollectionComment)
                    .HasForeignKey(d => d.IdclientLoanCollection)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanCollectionComment_FK");
            });

            modelBuilder.Entity<ClientLoanCollectionProcedureStep>(entity =>
            {
                entity.HasIndex(e => new { e.IdclientLoanCollection, e.IdcollectionProcedureStep })
                    .HasName("ClientLoanCollectionProcedureStep_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdclientLoanCollection).HasColumnName("IDClientLoanCollection");

                entity.Property(e => e.IdcollectionProcedureStep).HasColumnName("IDCollectionProcedureStep");

                entity.HasOne(d => d.IdclientLoanCollectionNavigation)
                    .WithMany(p => p.ClientLoanCollectionProcedureStep)
                    .HasForeignKey(d => d.IdclientLoanCollection)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanCollectionProcedureStep_FK");

                entity.HasOne(d => d.IdcollectionProcedureStepNavigation)
                    .WithMany(p => p.ClientLoanCollectionProcedureStep)
                    .HasForeignKey(d => d.IdcollectionProcedureStep)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanCollectionProcedureStep_FK_1");
            });

            modelBuilder.Entity<ClientLoanContractSignature>(entity =>
            {
                entity.Property(e => e.BrowserName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Guid).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Ipaddress)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.SignatureUrl)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp).HasColumnType("datetime");

                entity.HasOne(d => d.ClientLoan)
                    .WithMany(p => p.ClientLoanContractSignature)
                    .HasForeignKey(d => d.ClientLoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientLoanContractSignature_FK");
            });

            modelBuilder.Entity<ClientLoanHistory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BrokerageFees).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Fees).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdloanAmount).HasColumnName("IDLoanAmount");

                entity.Property(e => e.IdloanFrequency).HasColumnName("IDLoanFrequency");

                entity.Property(e => e.Idrequest).HasColumnName("IDRequest");

                entity.Property(e => e.InterestAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.OriginalPaymentAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Rebates).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientLoanHistory)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("FK_ClientLoanHistory_Client");

                entity.HasOne(d => d.IdloanAmountNavigation)
                    .WithMany(p => p.ClientLoanHistory)
                    .HasForeignKey(d => d.IdloanAmount)
                    .HasConstraintName("FK_ClientLoanHistory_Loan_Amount");

                entity.HasOne(d => d.IdloanFrequencyNavigation)
                    .WithMany(p => p.ClientLoanHistory)
                    .HasForeignKey(d => d.IdloanFrequency)
                    .HasConstraintName("FK_ClientLoanHistory_Loan_Frequency");

                entity.HasOne(d => d.IdrequestNavigation)
                    .WithMany(p => p.ClientLoanHistory)
                    .HasForeignKey(d => d.Idrequest)
                    .HasConstraintName("FK_ClientLoanHistory_ExternalLoanWebRequest");
            });

            modelBuilder.Entity<ClientLoanPayment>(entity =>
            {
                entity.ToTable("Client_Loan_Payment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Balance).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Capital).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DeferredFee).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Fees).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IdclientLoan).HasColumnName("IDClientLoan");

                entity.Property(e => e.IdpaymentStatus).HasColumnName("IDPaymentStatus");

                entity.Property(e => e.IdperceptechErrorCode).HasColumnName("IDPerceptechErrorCode");

                entity.Property(e => e.Interest).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.IsAmountChanged).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsManual).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsNsf)
                    .HasColumnName("IsNSF")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsRebate).HasDefaultValueSql("((0))");

                entity.Property(e => e.MaxAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Nsffee)
                    .HasColumnName("NSFFee")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PaymentDate).HasColumnType("date");

                entity.Property(e => e.SubFeesPayment).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TransactionFee).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IdclientLoanNavigation)
                    .WithMany(p => p.ClientLoanPayment)
                    .HasForeignKey(d => d.IdclientLoan)
                    .HasConstraintName("FK_Client_Loan_Payment_Client_Loan");

                entity.HasOne(d => d.IdpaymentStatusNavigation)
                    .WithMany(p => p.ClientLoanPayment)
                    .HasForeignKey(d => d.IdpaymentStatus)
                    .HasConstraintName("FK_Client_Loan_Payment_PaymentStatus");

                entity.HasOne(d => d.IdperceptechErrorCodeNavigation)
                    .WithMany(p => p.ClientLoanPayment)
                    .HasForeignKey(d => d.IdperceptechErrorCode)
                    .HasConstraintName("FK_Client_Loan_Payment_PerceptechErrorCode");
            });

            modelBuilder.Entity<ClientLoanPaymentAgreement>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ConfirmedDate).HasColumnType("datetime");

                entity.Property(e => e.IdclientLoanPayment).HasColumnName("IDClientLoanPayment");

                entity.HasOne(d => d.IdclientLoanPaymentNavigation)
                    .WithMany(p => p.ClientLoanPaymentAgreement)
                    .HasForeignKey(d => d.IdclientLoanPayment)
                    .HasConstraintName("FK_ClientLoanPaymentAgreement_Client_Loan_Payment");
            });

            modelBuilder.Entity<ClientLoanPaymentPerceptech>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdclientLoanPayment).HasColumnName("IDClientLoanPayment");

                entity.Property(e => e.SubmittedDate).HasColumnType("datetime");

                entity.HasOne(d => d.IdclientLoanPaymentNavigation)
                    .WithMany(p => p.ClientLoanPaymentPerceptech)
                    .HasForeignKey(d => d.IdclientLoanPayment)
                    .HasConstraintName("FK_ClientLoanPaymentPerceptech_Client_Loan_Payment");
            });

            modelBuilder.Entity<ClientMerge>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdclientNew).HasColumnName("IDClientNew");

                entity.Property(e => e.IdclientOld).HasColumnName("IDClientOld");

                entity.Property(e => e.IdexternalLoanWebRequest).HasColumnName("IDExternalLoanWebRequest");

                entity.Property(e => e.TimeStamp).HasColumnType("datetime");

                entity.HasOne(d => d.IdclientNewNavigation)
                    .WithMany(p => p.ClientMergeIdclientNewNavigation)
                    .HasForeignKey(d => d.IdclientNew)
                    .HasConstraintName("FK_ClientMerge_ClientNew");

                entity.HasOne(d => d.IdclientOldNavigation)
                    .WithMany(p => p.ClientMergeIdclientOldNavigation)
                    .HasForeignKey(d => d.IdclientOld)
                    .HasConstraintName("FK_ClientMerge_ClientOld");

                entity.HasOne(d => d.IdexternalLoanWebRequestNavigation)
                    .WithMany(p => p.ClientMerge)
                    .HasForeignKey(d => d.IdexternalLoanWebRequest)
                    .HasConstraintName("FK_ClientMerge_ExternalLoanWebRequest");
            });

            modelBuilder.Entity<ClientReference>(entity =>
            {
                entity.ToTable("Client_Reference");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Relation)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientReference)
                    .HasForeignKey(d => d.Idclient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Client_Reference_ClientID_Client_ID_FK");
            });

            modelBuilder.Entity<ClientRevenueSource>(entity =>
            {
                entity.HasIndex(e => e.ClientId)
                    .HasName("ClientRevenueSource_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.EmploymentInsuranceNextPayDate).HasColumnType("datetime");

                entity.Property(e => e.EmploymentInsuranceStart).HasColumnType("datetime");

                entity.Property(e => e.EmploymentNextPay).HasColumnType("datetime");

                entity.Property(e => e.NextDepositDate).HasColumnType("datetime");

                entity.Property(e => e.NextPayDate).HasColumnType("datetime");

                entity.Property(e => e.SelfEmployedPhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SelfEmployedStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Client)
                    .WithOne(p => p.ClientRevenueSource)
                    .HasForeignKey<ClientRevenueSource>(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientRevenueSource_Client");

                entity.HasOne(d => d.IncomeType)
                    .WithMany(p => p.ClientRevenueSource)
                    .HasForeignKey(d => d.IncomeTypeId)
                    .HasConstraintName("ClientRevenueSource__IncomeType_FK");

                entity.HasOne(d => d.SelfEmployedPayFrequency)
                    .WithMany(p => p.ClientRevenueSource)
                    .HasForeignKey(d => d.SelfEmployedPayFrequencyId)
                    .HasConstraintName("ClientRevenueSource_PayFrequency_FK");
            });

            modelBuilder.Entity<ClientSocial>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FacebookId)
                    .HasColumnName("FacebookID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookPhoto).IsUnicode(false);

                entity.Property(e => e.GoogleId)
                    .HasColumnName("GoogleID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GooglePhoto).IsUnicode(false);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ClientSocial)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("FK_ClientSocial_Client");
            });

            modelBuilder.Entity<ClientStatus>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CollectionDegree>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("CollectionDegree_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CollectionPaymentAgreement>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("CollectionPaymentAgreement_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CollectionProcedureStep>(entity =>
            {
                entity.HasIndex(e => e.Order)
                    .HasName("CollectionProcedureStep_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.DescriptionFr).HasMaxLength(256);
            });

            modelBuilder.Entity<CollectionStatus>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("CollectionStatus_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CommentLabel>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Handle)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CreditBookCheckStatus>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Deposit>(entity =>
            {
                entity.Property(e => e.InteracConfirmationCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClientLoan)
                    .WithMany(p => p.Deposit)
                    .HasForeignKey(d => d.ClientLoanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Deposit_FK");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Deposit)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Deposit_DepositStatus");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Deposit)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_Deposit_DepositType");
            });

            modelBuilder.Entity<DepositStatus>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DepositType>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("DocumentType_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailConfiguration>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
            });

            modelBuilder.Entity<ExternaNonCompletedRequest>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CellPhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FacebookEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookFirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookId)
                    .HasColumnName("FacebookID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookLastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookPhoto).IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleFisrtName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleId)
                    .HasColumnName("GoogleID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleLastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GooglePhoto).IsUnicode(false);

                entity.Property(e => e.Idlanguage).HasColumnName("IDLanguage");

                entity.Property(e => e.LastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdlanguageNavigation)
                    .WithMany(p => p.ExternaNonCompletedRequest)
                    .HasForeignKey(d => d.Idlanguage)
                    .HasConstraintName("FK_ExternaNonCompletedRequest_Language");
            });

            modelBuilder.Entity<ExternalLoanWebRequest>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AmountApproved).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.AptNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Bankrupcy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CellPhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ChecklistCompleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CivicNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreditBookCheckRequestId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DebtRatio).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EmployerExtension)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerPhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmploymentInsuranceNextPayDate).HasColumnType("date");

                entity.Property(e => e.EmploymentInsuranceStartDate).HasColumnType("date");

                entity.Property(e => e.FacebookEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookFirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookId)
                    .HasColumnName("FacebookID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookLastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookPhoto).IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleFisrtName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleId)
                    .HasColumnName("GoogleID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleLastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GooglePhoto).IsUnicode(false);

                entity.Property(e => e.GrossAnnualSalary).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.HiringDate).HasColumnType("datetime");

                entity.Property(e => e.IbvrequestId)
                    .HasColumnName("IBVRequestId")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Idclient).HasColumnName("IDClient");

                entity.Property(e => e.IdexternalLoanWebRequestAutoDecline).HasColumnName("IDExternalLoanWebRequestAutoDecline");

                entity.Property(e => e.IdincomeType).HasColumnName("IDIncomeType");

                entity.Property(e => e.Idlanguage).HasColumnName("IDLanguage");

                entity.Property(e => e.IdloanRequestRefusalReason).HasColumnName("IDLoanRequestRefusalReason");

                entity.Property(e => e.IdpayFrequency).HasColumnName("IDPayFrequency");

                entity.Property(e => e.Ipaddress)
                    .HasColumnName("IPAddress")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MonthlyCarPayment).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MonthlyElectricalPayment).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MonthlyPaymenet).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MontlyAppliancePayment).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.NextDepositDate).HasColumnType("date");

                entity.Property(e => e.NextPayDate).HasColumnType("datetime");

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref1Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref1FirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ref1LastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ref1Relationship)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref1Telephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref2Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref2FirstName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ref2LastName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ref2Relationship)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref2Telephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedDate).HasColumnType("datetime");

                entity.Property(e => e.ResidenceStartDate).HasColumnType("date");

                entity.Property(e => e.SecondaryEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SelfEmployedStartDate).HasColumnType("date");

                entity.Property(e => e.SelfEmployedWorkTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SupervisorName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdclientNavigation)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.Idclient)
                    .HasConstraintName("FK_ExternalLoanWebRequest_Client");

                entity.HasOne(d => d.IdexternalLoanWebRequestAutoDeclineNavigation)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.IdexternalLoanWebRequestAutoDecline)
                    .HasConstraintName("FK_ExternalLoanWebRequest_ExternalLoanWebRequestAutoDecline");

                entity.HasOne(d => d.IdincomeTypeNavigation)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.IdincomeType)
                    .HasConstraintName("FK_ExternalLoanWebRequest_IncomeType");

                entity.HasOne(d => d.IdlanguageNavigation)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.Idlanguage)
                    .HasConstraintName("FK_ExternalLoanWebRequest_Language");

                entity.HasOne(d => d.IdpayFrequencyNavigation)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.IdpayFrequency)
                    .HasConstraintName("FK_ExternalLoanWebRequest_PayFrequency");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ExternalLoanWebRequest)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_ExternalLoanWebRequest_LoanRequestStatus");
            });

            modelBuilder.Entity<ExternalLoanWebRequestAutoDecline>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ExternalLoanWebRequestChecklist>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdexternalLoanWebRequest).HasColumnName("IDExternalLoanWebRequest");

                entity.Property(e => e.IdloanRequestChecklist).HasColumnName("IDLoanRequestChecklist");

                entity.HasOne(d => d.IdexternalLoanWebRequestNavigation)
                    .WithMany(p => p.ExternalLoanWebRequestChecklist)
                    .HasForeignKey(d => d.IdexternalLoanWebRequest)
                    .HasConstraintName("FK_ExternalLoanWebRequestChecklist_ExternalLoanWebRequest");

                entity.HasOne(d => d.IdloanRequestChecklistNavigation)
                    .WithMany(p => p.ExternalLoanWebRequestChecklist)
                    .HasForeignKey(d => d.IdloanRequestChecklist)
                    .HasConstraintName("FK_ExternalLoanWebRequestChecklist_LoanRequestChecklist");
            });

            modelBuilder.Entity<FeatureConfiguration>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.NameEn).HasMaxLength(100);

                entity.Property(e => e.NameFr).HasMaxLength(100);
            });

            modelBuilder.Entity<HousingStatus>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IncomeType>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("IncomeType_UN")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.Property(e => e.NameEn).HasMaxLength(100);

                entity.Property(e => e.NameFr).HasMaxLength(100);
            });

            modelBuilder.Entity<LoanAmount>(entity =>
            {
                entity.ToTable("Loan_Amount");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasColumnType("money");
            });

            modelBuilder.Entity<LoanChargeConfiguration>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AmountCharged).HasColumnType("money");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.IdloanChargeType).HasColumnName("IDLoanChargeType");

                entity.HasOne(d => d.IdloanChargeTypeNavigation)
                    .WithMany(p => p.LoanChargeConfiguration)
                    .HasForeignKey(d => d.IdloanChargeType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LoanChargeConfiguration_FK");
            });

            modelBuilder.Entity<LoanChargeType>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("LoanChargeType_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.NameFr)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<LoanConfiguration>(entity =>
            {
                entity.ToTable("Loan_Configuration");

                entity.HasIndex(e => new { e.IdloanFrequency, e.IdloanAmount, e.TotalPayments })
                    .HasName("Loan_Amount_Frequency_Payments_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdloanAmount).HasColumnName("IDLoanAmount");

                entity.Property(e => e.IdloanFrequency).HasColumnName("IDLoanFrequency");

                entity.HasOne(d => d.IdloanAmountNavigation)
                    .WithMany(p => p.LoanConfiguration)
                    .HasForeignKey(d => d.IdloanAmount)
                    .HasConstraintName("FK_Loan_Configuration_Loan_Amount");

                entity.HasOne(d => d.IdloanFrequencyNavigation)
                    .WithMany(p => p.LoanConfiguration)
                    .HasForeignKey(d => d.IdloanFrequency)
                    .HasConstraintName("FK_Loan_Configuration_Loan_Frequency");
            });

            modelBuilder.Entity<LoanFrequency>(entity =>
            {
                entity.ToTable("Loan_Frequency");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn).HasMaxLength(100);

                entity.Property(e => e.NameFr).HasMaxLength(100);
            });

            modelBuilder.Entity<LoanPaymentType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanRequestChecklist>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanRequestStatus>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanStatus>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LoanerConfiguration>(entity =>
            {
                entity.HasIndex(e => e.Key)
                    .HasName("LoanerConfiguration_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MessageTemplate>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.SubjectEn).HasMaxLength(256);

                entity.Property(e => e.SubjectFr).HasMaxLength(256);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<NonPaymentReason>(entity =>
            {
                entity.HasIndex(e => e.Code)
                    .HasName("CollectionNonPaymentReason_UN")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateRead).HasColumnType("datetime");

                entity.Property(e => e.Path).HasMaxLength(256);

                entity.Property(e => e.Read).HasColumnName("read");

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<NotificationMessage>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.MessageEn)
                    .HasColumnName("messageEn")
                    .HasMaxLength(256);

                entity.Property(e => e.MessageFr)
                    .HasColumnName("messageFr")
                    .HasMaxLength(256);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<NotificationRead>(entity =>
            {
                entity.Property(e => e.IdEntity).HasMaxLength(450);
            });

            modelBuilder.Entity<PayFrequency>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreditBookCheckCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PaymentStatus>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PerceptechErrorCode>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MessageEn)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MessageFr)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PerceptechRejectionLog>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateSubmitted).HasColumnType("datetime");

                entity.Property(e => e.RejectionDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PerceptechSubmitLog>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateSubmitted).HasColumnType("datetime");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NameEn)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameFr)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WebhookConfiguration>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Key)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
