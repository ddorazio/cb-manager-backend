﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanCollection
    {
        public ClientLoanCollection()
        {
            ClientLoanCollectionComment = new HashSet<ClientLoanCollectionComment>();
            ClientLoanCollectionProcedureStep = new HashSet<ClientLoanCollectionProcedureStep>();
        }

        public int Id { get; set; }
        public int IdclientLoan { get; set; }
        public int? IdcollectionStatus { get; set; }
        public int? IdpaymentAgreement { get; set; }
        public int? IdnonPaymentReason { get; set; }
        public DateTime? FollowUpDate { get; set; }
        public int IdcollectionDegree { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? InteracPlannedPaymentDate { get; set; }
        public DateTime? InteracRealPaymentDate { get; set; }
        public string InteracConfirmationEmail { get; set; }
        public bool? InteracSmsReminderOnPaymentDate { get; set; }

        public virtual ClientLoan IdclientLoanNavigation { get; set; }
        public virtual CollectionDegree IdcollectionDegreeNavigation { get; set; }
        public virtual CollectionStatus IdcollectionStatusNavigation { get; set; }
        public virtual NonPaymentReason IdnonPaymentReasonNavigation { get; set; }
        public virtual ICollection<ClientLoanCollectionComment> ClientLoanCollectionComment { get; set; }
        public virtual ICollection<ClientLoanCollectionProcedureStep> ClientLoanCollectionProcedureStep { get; set; }
    }
}
