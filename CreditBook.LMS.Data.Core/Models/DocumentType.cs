﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            ClientDocument = new HashSet<ClientDocument>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientDocument> ClientDocument { get; set; }
    }
}
