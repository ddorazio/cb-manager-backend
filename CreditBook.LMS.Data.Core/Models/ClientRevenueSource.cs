﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientRevenueSource
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? IncomeTypeId { get; set; }
        public DateTime? NextDepositDate { get; set; }
        public DateTime? EmploymentInsuranceNextPayDate { get; set; }
        public DateTime? NextPayDate { get; set; }
        public DateTime? EmploymentInsuranceStart { get; set; }
        public DateTime? EmploymentNextPay { get; set; }
        public bool? SelfEmployedDirectDeposit { get; set; }
        public string SelfEmployedPhoneNumber { get; set; }
        public int? SelfEmployedPayFrequencyId { get; set; }
        public DateTime? SelfEmployedStartDate { get; set; }

        public virtual Client Client { get; set; }
        public virtual IncomeType IncomeType { get; set; }
        public virtual PayFrequency SelfEmployedPayFrequency { get; set; }
    }
}
