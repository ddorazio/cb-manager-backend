﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanCollectionProcedureStep
    {
        public int Id { get; set; }
        public int IdclientLoanCollection { get; set; }
        public int IdcollectionProcedureStep { get; set; }
        public bool Checked { get; set; }

        public virtual ClientLoanCollection IdclientLoanCollectionNavigation { get; set; }
        public virtual CollectionProcedureStep IdcollectionProcedureStepNavigation { get; set; }
    }
}
