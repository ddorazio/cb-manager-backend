﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class CommentLabel
    {
        public CommentLabel()
        {
            ClientCommentLabel = new HashSet<ClientCommentLabel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Handle { get; set; }

        public virtual ICollection<ClientCommentLabel> ClientCommentLabel { get; set; }
    }
}
