﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientHistory
    {
        public int Id { get; set; }
        public int Idclient { get; set; }
        public DateTime DateTime { get; set; }
        public int IdclientHistoryType { get; set; }
        public string Username { get; set; }

        public virtual ClientHistoryType IdclientHistoryTypeNavigation { get; set; }
        public virtual Client IdclientNavigation { get; set; }
    }
}
