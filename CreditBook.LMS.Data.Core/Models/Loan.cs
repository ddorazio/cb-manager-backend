﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Loan
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? IdloanFrequency { get; set; }

        public virtual Client IdNavigation { get; set; }
    }
}
