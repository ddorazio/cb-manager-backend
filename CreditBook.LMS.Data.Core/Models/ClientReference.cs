﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientReference
    {
        public int Id { get; set; }
        public int Idclient { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Relation { get; set; }
        public string LastName { get; set; }

        public virtual Client IdclientNavigation { get; set; }
    }
}
