﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanChargeConfiguration
    {
        public int Id { get; set; }
        public int IdloanChargeType { get; set; }
        public decimal AmountCharged { get; set; }
        public DateTime EffectiveDate { get; set; }

        public virtual LoanChargeType IdloanChargeTypeNavigation { get; set; }
    }
}
