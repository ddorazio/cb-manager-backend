﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Deposit
    {
        public int Id { get; set; }
        public int ClientLoanId { get; set; }
        public int StatusId { get; set; }
        public int? TypeId { get; set; }
        public string InteracConfirmationCode { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ClientLoan ClientLoan { get; set; }
        public virtual DepositStatus Status { get; set; }
        public virtual DepositType Type { get; set; }
    }
}
