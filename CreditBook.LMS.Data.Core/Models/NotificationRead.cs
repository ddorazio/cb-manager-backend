﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class NotificationRead
    {
        public int Id { get; set; }
        public int IdNotif { get; set; }
        public string IdEntity { get; set; }
        public bool? Read { get; set; }
        public bool? Seen { get; set; }
    }
}
