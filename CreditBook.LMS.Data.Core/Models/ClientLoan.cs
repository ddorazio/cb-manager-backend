﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoan
    {
        public ClientLoan()
        {
            ClientDocument = new HashSet<ClientDocument>();
            ClientLoanCollection = new HashSet<ClientLoanCollection>();
            ClientLoanContractSignature = new HashSet<ClientLoanContractSignature>();
            ClientLoanPayment = new HashSet<ClientLoanPayment>();
            Deposit = new HashSet<Deposit>();
        }

        public int Id { get; set; }
        public int? Idclient { get; set; }
        public int? IdloanStatus { get; set; }
        public int? IdloanFrequency { get; set; }
        public int? IdloanAmount { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? LoanNumber { get; set; }
        public bool? IsPersonalized { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Fees { get; set; }
        public DateTime? CreatedDate { get; set; }
        public decimal? Rebates { get; set; }
        public decimal? OriginalPaymentAmount { get; set; }
        public decimal? BrokerageFees { get; set; }
        public int? BiMonthlyStartDay { get; set; }
        public int? BiMonthlyEndDay { get; set; }
        public DateTime? StopLoanDate { get; set; }
        public int? Idrequest { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsPerceptech { get; set; }
        public string ContractUrl { get; set; }
        public int? InitialNumberOfPayments { get; set; }
        public decimal? SubFees { get; set; }
        public decimal? OriginalSubFeesPrice { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? BrokerageRate { get; set; }
        public decimal? MaxInterestRate { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual LoanAmount IdloanAmountNavigation { get; set; }
        public virtual LoanFrequency IdloanFrequencyNavigation { get; set; }
        public virtual LoanStatus IdloanStatusNavigation { get; set; }
        public virtual ICollection<ClientDocument> ClientDocument { get; set; }
        public virtual ICollection<ClientLoanCollection> ClientLoanCollection { get; set; }
        public virtual ICollection<ClientLoanContractSignature> ClientLoanContractSignature { get; set; }
        public virtual ICollection<ClientLoanPayment> ClientLoanPayment { get; set; }
        public virtual ICollection<Deposit> Deposit { get; set; }
    }
}
