﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientDocument
    {
        public int Id { get; set; }
        public int Idclient { get; set; }
        public int? IdclientLoan { get; set; }
        public int? IddocumentType { get; set; }
        public Guid? Guid { get; set; }
        public string Note { get; set; }
        public string Filename { get; set; }
        public string S3key { get; set; }
        public bool IsDeleted { get; set; }
        public string DeleteUsername { get; set; }
        public DateTime? DeleteDateTime { get; set; }
        public DateTime UploadDateTime { get; set; }
        public string Title { get; set; }

        public virtual ClientLoan IdclientLoanNavigation { get; set; }
        public virtual Client IdclientNavigation { get; set; }
        public virtual DocumentType IddocumentTypeNavigation { get; set; }
    }
}
