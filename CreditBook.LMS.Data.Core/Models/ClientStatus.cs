﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientStatus
    {
        public ClientStatus()
        {
            Client = new HashSet<Client>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }

        public virtual ICollection<Client> Client { get; set; }
    }
}
