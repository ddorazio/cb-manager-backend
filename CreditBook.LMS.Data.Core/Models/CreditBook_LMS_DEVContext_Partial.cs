﻿using System;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class CreditBook_LMS_DEVContext : DbContext
    {
        public string _connectionString { get; set; }

        public CreditBook_LMS_DEVContext(string connectionString)
        {
            _connectionString = connectionString;
        }
    }
}
