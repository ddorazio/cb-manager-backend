﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanRequestChecklist
    {
        public LoanRequestChecklist()
        {
            ExternalLoanWebRequestChecklist = new HashSet<ExternalLoanWebRequestChecklist>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ExternalLoanWebRequestChecklist> ExternalLoanWebRequestChecklist { get; set; }
    }
}
