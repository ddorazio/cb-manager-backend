﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientEmailAddress
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public string EmailAddress { get; set; }
        public bool? IsPrimary { get; set; }

        public virtual Client IdclientNavigation { get; set; }
    }
}
