﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientMerge
    {
        public int Id { get; set; }
        public int? IdclientNew { get; set; }
        public int? IdclientOld { get; set; }
        public int? IdexternalLoanWebRequest { get; set; }
        public DateTime? TimeStamp { get; set; }

        public virtual Client IdclientNewNavigation { get; set; }
        public virtual Client IdclientOldNavigation { get; set; }
        public virtual ExternalLoanWebRequest IdexternalLoanWebRequestNavigation { get; set; }
    }
}
