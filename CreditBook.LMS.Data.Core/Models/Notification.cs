﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Notification
    {
        public int Id { get; set; }
        public int? IdClient { get; set; }
        public string Type { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Path { get; set; }
        public DateTime? DateRead { get; set; }
        public bool? Read { get; set; }
    }
}
