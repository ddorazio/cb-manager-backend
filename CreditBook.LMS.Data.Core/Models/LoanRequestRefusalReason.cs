﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanRequestRefusalReason
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
