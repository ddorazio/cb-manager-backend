﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanFrequency
    {
        public LoanFrequency()
        {
            ClientLoan = new HashSet<ClientLoan>();
            ClientLoanHistory = new HashSet<ClientLoanHistory>();
            LoanConfiguration = new HashSet<LoanConfiguration>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public int? Frequency { get; set; }
        public string Code { get; set; }
        public int? MinimumDaysBeforeDeposit { get; set; }

        public virtual ICollection<ClientLoan> ClientLoan { get; set; }
        public virtual ICollection<ClientLoanHistory> ClientLoanHistory { get; set; }
        public virtual ICollection<LoanConfiguration> LoanConfiguration { get; set; }
    }
}
