﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanPayment
    {
        public ClientLoanPayment()
        {
            ClientLoanPaymentAgreement = new HashSet<ClientLoanPaymentAgreement>();
            ClientLoanPaymentPerceptech = new HashSet<ClientLoanPaymentPerceptech>();
        }

        public int Id { get; set; }
        public int? IdclientLoan { get; set; }
        public int? IdpaymentStatus { get; set; }
        public int? IdperceptechErrorCode { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? Interest { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Balance { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Fees { get; set; }
        public bool? IsNsf { get; set; }
        public bool? IsManual { get; set; }
        public bool? IsRebate { get; set; }
        public bool? IsAmountChanged { get; set; }
        public bool? IsDateChanged { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsFrequencyChanged { get; set; }
        public int? Frequency { get; set; }
        public string Description { get; set; }
        public bool? IsDeferredFee { get; set; }
        public decimal? SubFeesPayment { get; set; }
        public decimal? Nsffee { get; set; }
        public decimal? TransactionFee { get; set; }
        public decimal? DeferredFee { get; set; }

        public virtual ClientLoan IdclientLoanNavigation { get; set; }
        public virtual PaymentStatus IdpaymentStatusNavigation { get; set; }
        public virtual PerceptechErrorCode IdperceptechErrorCodeNavigation { get; set; }
        public virtual ICollection<ClientLoanPaymentAgreement> ClientLoanPaymentAgreement { get; set; }
        public virtual ICollection<ClientLoanPaymentPerceptech> ClientLoanPaymentPerceptech { get; set; }
    }
}
