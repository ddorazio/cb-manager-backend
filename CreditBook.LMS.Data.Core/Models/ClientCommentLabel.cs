﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientCommentLabel
    {
        public int Id { get; set; }
        public int IdclientComment { get; set; }
        public int IdcommentLabel { get; set; }

        public virtual ClientComment IdclientCommentNavigation { get; set; }
        public virtual CommentLabel IdcommentLabelNavigation { get; set; }
    }
}
