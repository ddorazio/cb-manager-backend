﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class EmailConfiguration
    {
        public int Id { get; set; }
        public string HeaderContentEn { get; set; }
        public string HeaderContentFr { get; set; }
        public string FooterContentEn { get; set; }
        public string FooterContentFr { get; set; }
        public string LogoUrl { get; set; }
    }
}
