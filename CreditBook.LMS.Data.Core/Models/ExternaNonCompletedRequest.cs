﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ExternaNonCompletedRequest
    {
        public int Id { get; set; }
        public int? Idlanguage { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthday { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public bool? IsActive { get; set; }
        public string ReferenceNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string GoogleId { get; set; }
        public string GoogleEmail { get; set; }
        public string GooglePhoto { get; set; }
        public string GoogleFisrtName { get; set; }
        public string GoogleLastName { get; set; }
        public string FacebookId { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookFirstName { get; set; }
        public string FacebookLastName { get; set; }
        public string FacebookPhoto { get; set; }

        public virtual Language IdlanguageNavigation { get; set; }
    }
}
