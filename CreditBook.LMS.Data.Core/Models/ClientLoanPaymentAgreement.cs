﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanPaymentAgreement
    {
        public int Id { get; set; }
        public int? IdclientLoanPayment { get; set; }
        public bool? IsConfirmed { get; set; }
        public DateTime? ConfirmedDate { get; set; }

        public virtual ClientLoanPayment IdclientLoanPaymentNavigation { get; set; }
    }
}
