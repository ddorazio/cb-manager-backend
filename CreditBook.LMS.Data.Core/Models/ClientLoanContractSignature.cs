﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanContractSignature
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public int ClientLoanId { get; set; }
        public bool IsSigned { get; set; }
        public string Ipaddress { get; set; }
        public DateTime? TimeStamp { get; set; }
        public string SignatureUrl { get; set; }
        public DateTime? RequestedDate { get; set; }
        public string BrowserName { get; set; }
        public string Email { get; set; }

        public virtual ClientLoan ClientLoan { get; set; }
    }
}
