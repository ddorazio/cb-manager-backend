﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ExternalLoanWebRequestChecklist
    {
        public int Id { get; set; }
        public int? IdexternalLoanWebRequest { get; set; }
        public int? IdloanRequestChecklist { get; set; }
        public bool? Confirmed { get; set; }

        public virtual ExternalLoanWebRequest IdexternalLoanWebRequestNavigation { get; set; }
        public virtual LoanRequestChecklist IdloanRequestChecklistNavigation { get; set; }
    }
}
