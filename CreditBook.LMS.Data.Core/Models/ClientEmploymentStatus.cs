﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientEmploymentStatus
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public int? IdpayFrequency { get; set; }
        public string EmployerName { get; set; }
        public string SupervisorName { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string Occupation { get; set; }
        public DateTime? HiringDate { get; set; }
        public DateTime? NextPayDate { get; set; }
        public string PhoneNumberHr { get; set; }
        public string ExtensionHr { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual PayFrequency IdpayFrequencyNavigation { get; set; }
    }
}
