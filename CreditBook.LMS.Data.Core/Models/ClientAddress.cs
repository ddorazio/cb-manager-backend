﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientAddress
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public string CivicNumber { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public bool? IsPrimary { get; set; }
        public string StreetName { get; set; }
        public DateTime? StartOfResidencyDate { get; set; }
        public string Apartment { get; set; }
        public int? Idprovince { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual Province IdprovinceNavigation { get; set; }
    }
}
