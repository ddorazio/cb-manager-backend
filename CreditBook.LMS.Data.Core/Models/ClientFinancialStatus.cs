﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientFinancialStatus
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? HousingStatusId { get; set; }
        public decimal GrossAnnualRevenue { get; set; }
        public decimal MonthlyHousingCost { get; set; }
        public decimal MonthlyUtilityCost { get; set; }
        public decimal MonthlyCarPayment { get; set; }
        public decimal MonthlyOtherLoanPayment { get; set; }

        public virtual Client Client { get; set; }
        public virtual HousingStatus HousingStatus { get; set; }
    }
}
