﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientHistoryType
    {
        public ClientHistoryType()
        {
            ClientHistory = new HashSet<ClientHistory>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionFr { get; set; }

        public virtual ICollection<ClientHistory> ClientHistory { get; set; }
    }
}
