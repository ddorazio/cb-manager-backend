﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class CollectionPaymentAgreement
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
