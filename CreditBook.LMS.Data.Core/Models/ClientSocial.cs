﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientSocial
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public string GoogleId { get; set; }
        public string GooglePhoto { get; set; }
        public string FacebookId { get; set; }
        public string FacebookPhoto { get; set; }

        public virtual Client IdclientNavigation { get; set; }
    }
}
