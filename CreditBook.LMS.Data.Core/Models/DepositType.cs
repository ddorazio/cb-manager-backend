﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class DepositType
    {
        public DepositType()
        {
            Deposit = new HashSet<Deposit>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Deposit> Deposit { get; set; }
    }
}
