﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Language
    {
        public Language()
        {
            Client = new HashSet<Client>();
            ExternaNonCompletedRequest = new HashSet<ExternaNonCompletedRequest>();
            ExternalLoanWebRequest = new HashSet<ExternalLoanWebRequest>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Client> Client { get; set; }
        public virtual ICollection<ExternaNonCompletedRequest> ExternaNonCompletedRequest { get; set; }
        public virtual ICollection<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
    }
}
