﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanChargeType
    {
        public LoanChargeType()
        {
            LoanChargeConfiguration = new HashSet<LoanChargeConfiguration>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }

        public virtual ICollection<LoanChargeConfiguration> LoanChargeConfiguration { get; set; }
    }
}
