﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class NotificationMessage
    {
        public string Type { get; set; }
        public string MessageFr { get; set; }
        public string MessageEn { get; set; }
    }
}
