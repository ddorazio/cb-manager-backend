﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientIbv
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public int? IdclientIbvstatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Url { get; set; }
        public string Guid { get; set; }

        public virtual ClientIbvStatus IdclientIbvstatusNavigation { get; set; }
        public virtual Client IdclientNavigation { get; set; }
    }
}
