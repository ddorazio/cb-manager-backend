﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Client
    {
        public Client()
        {
            ClientAddress = new HashSet<ClientAddress>();
            ClientBankDetails = new HashSet<ClientBankDetails>();
            ClientComment = new HashSet<ClientComment>();
            ClientDocument = new HashSet<ClientDocument>();
            ClientEmailAddress = new HashSet<ClientEmailAddress>();
            ClientEmploymentStatus = new HashSet<ClientEmploymentStatus>();
            ClientHistory = new HashSet<ClientHistory>();
            ClientIbv = new HashSet<ClientIbv>();
            ClientLoan = new HashSet<ClientLoan>();
            ClientLoanHistory = new HashSet<ClientLoanHistory>();
            ClientMergeIdclientNewNavigation = new HashSet<ClientMerge>();
            ClientMergeIdclientOldNavigation = new HashSet<ClientMerge>();
            ClientReference = new HashSet<ClientReference>();
            ClientSocial = new HashSet<ClientSocial>();
            ExternalLoanWebRequest = new HashSet<ExternalLoanWebRequest>();
        }

        public int Id { get; set; }
        public int? Idgender { get; set; }
        public int? Idlanguage { get; set; }
        public bool? IsActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HomePhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string SocialInsuranceNumber { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CellPhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public bool IsDeleted { get; set; }
        public int? IdclientStatus { get; set; }
        public bool? PotentialMatch { get; set; }
        public bool? ProfileMerged { get; set; }

        public virtual ClientStatus IdclientStatusNavigation { get; set; }
        public virtual Gender IdgenderNavigation { get; set; }
        public virtual Language IdlanguageNavigation { get; set; }
        public virtual ClientFinancialStatus ClientFinancialStatus { get; set; }
        public virtual ClientRevenueSource ClientRevenueSource { get; set; }
        public virtual ICollection<ClientAddress> ClientAddress { get; set; }
        public virtual ICollection<ClientBankDetails> ClientBankDetails { get; set; }
        public virtual ICollection<ClientComment> ClientComment { get; set; }
        public virtual ICollection<ClientDocument> ClientDocument { get; set; }
        public virtual ICollection<ClientEmailAddress> ClientEmailAddress { get; set; }
        public virtual ICollection<ClientEmploymentStatus> ClientEmploymentStatus { get; set; }
        public virtual ICollection<ClientHistory> ClientHistory { get; set; }
        public virtual ICollection<ClientIbv> ClientIbv { get; set; }
        public virtual ICollection<ClientLoan> ClientLoan { get; set; }
        public virtual ICollection<ClientLoanHistory> ClientLoanHistory { get; set; }
        public virtual ICollection<ClientMerge> ClientMergeIdclientNewNavigation { get; set; }
        public virtual ICollection<ClientMerge> ClientMergeIdclientOldNavigation { get; set; }
        public virtual ICollection<ClientReference> ClientReference { get; set; }
        public virtual ICollection<ClientSocial> ClientSocial { get; set; }
        public virtual ICollection<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
    }
}
