﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ExternalLoanWebRequest
    {
        public ExternalLoanWebRequest()
        {
            ClientLoanHistory = new HashSet<ClientLoanHistory>();
            ClientMerge = new HashSet<ClientMerge>();
            ExternalLoanWebRequestChecklist = new HashSet<ExternalLoanWebRequestChecklist>();
        }

        public int Id { get; set; }
        public int? Idclient { get; set; }
        public int? Idlanguage { get; set; }
        public int? IdincomeType { get; set; }
        public int? IdpayFrequency { get; set; }
        public int? IdexternalLoanWebRequestAutoDecline { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AmountApproved { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthday { get; set; }
        public string CivicNumber { get; set; }
        public string StreetName { get; set; }
        public string AptNo { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime? ResidenceStartDate { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string Ref1FirstName { get; set; }
        public string Ref1LastName { get; set; }
        public string Ref1Telephone { get; set; }
        public string Ref1Email { get; set; }
        public string Ref1Relationship { get; set; }
        public string Ref2FirstName { get; set; }
        public string Ref2LastName { get; set; }
        public string Ref2Telephone { get; set; }
        public string Ref2Email { get; set; }
        public string Ref2Relationship { get; set; }
        public string Bankrupcy { get; set; }
        public bool? IsRenter { get; set; }
        public decimal? MonthlyPaymenet { get; set; }
        public decimal? MonthlyElectricalPayment { get; set; }
        public decimal? MonthlyCarPayment { get; set; }
        public decimal? MontlyAppliancePayment { get; set; }
        public decimal? GrossAnnualSalary { get; set; }
        public string EmployerName { get; set; }
        public string EmployerPhoneNumber { get; set; }
        public string EmployerExtension { get; set; }
        public string SupervisorName { get; set; }
        public string Occupation { get; set; }
        public DateTime? NextPayDate { get; set; }
        public DateTime? HiringDate { get; set; }
        public DateTime? EmploymentInsuranceStartDate { get; set; }
        public DateTime? EmploymentInsuranceNextPayDate { get; set; }
        public bool? DirectDeposit { get; set; }
        public DateTime? SelfEmployedStartDate { get; set; }
        public string SelfEmployedWorkTelephone { get; set; }
        public DateTime? NextDepositDate { get; set; }
        public bool? LoanWithoutDocuments { get; set; }
        public string Ipaddress { get; set; }
        public DateTime? RequestedDate { get; set; }
        public bool? CreditBookCheckSuccessfull { get; set; }
        public string CreditBookCheckRequestId { get; set; }
        public string IbvrequestId { get; set; }
        public int? IdloanRequestRefusalReason { get; set; }
        public int? StatusId { get; set; }
        public decimal? DebtRatio { get; set; }
        public string GoogleId { get; set; }
        public string GoogleEmail { get; set; }
        public string GooglePhoto { get; set; }
        public string GoogleFisrtName { get; set; }
        public string GoogleLastName { get; set; }
        public bool? DeclaringBankrupcy { get; set; }
        public string FacebookId { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookFirstName { get; set; }
        public string FacebookLastName { get; set; }
        public bool? ChecklistCompleted { get; set; }
        public bool? ClientMatched { get; set; }
        public string FacebookPhoto { get; set; }
        public bool? AutoDeclined { get; set; }
        public bool? Renewal { get; set; }
        public bool? AddressChanged { get; set; }
        public bool? JobChanged { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual ExternalLoanWebRequestAutoDecline IdexternalLoanWebRequestAutoDeclineNavigation { get; set; }
        public virtual IncomeType IdincomeTypeNavigation { get; set; }
        public virtual Language IdlanguageNavigation { get; set; }
        public virtual PayFrequency IdpayFrequencyNavigation { get; set; }
        public virtual LoanRequestStatus Status { get; set; }
        public virtual ICollection<ClientLoanHistory> ClientLoanHistory { get; set; }
        public virtual ICollection<ClientMerge> ClientMerge { get; set; }
        public virtual ICollection<ExternalLoanWebRequestChecklist> ExternalLoanWebRequestChecklist { get; set; }
    }
}
