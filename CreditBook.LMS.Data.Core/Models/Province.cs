﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class Province
    {
        public Province()
        {
            ClientAddress = new HashSet<ClientAddress>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientAddress> ClientAddress { get; set; }
    }
}
