﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class IncomeType
    {
        public IncomeType()
        {
            ClientRevenueSource = new HashSet<ClientRevenueSource>();
            ExternalLoanWebRequest = new HashSet<ExternalLoanWebRequest>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientRevenueSource> ClientRevenueSource { get; set; }
        public virtual ICollection<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
    }
}
