﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanHistory
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public int? IdloanFrequency { get; set; }
        public int? IdloanAmount { get; set; }
        public int? Idrequest { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? LoanNumber { get; set; }
        public bool? IsPersonalized { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Fees { get; set; }
        public DateTime? CreatedDate { get; set; }
        public decimal? Rebates { get; set; }
        public decimal? OriginalPaymentAmount { get; set; }
        public decimal? BrokerageFees { get; set; }
        public int? BiMonthlyStartDay { get; set; }
        public int? BiMonthlyEndDay { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual LoanAmount IdloanAmountNavigation { get; set; }
        public virtual LoanFrequency IdloanFrequencyNavigation { get; set; }
        public virtual ExternalLoanWebRequest IdrequestNavigation { get; set; }
    }
}
