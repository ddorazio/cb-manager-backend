﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class NonPaymentReason
    {
        public NonPaymentReason()
        {
            ClientLoanCollection = new HashSet<ClientLoanCollection>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientLoanCollection> ClientLoanCollection { get; set; }
    }
}
