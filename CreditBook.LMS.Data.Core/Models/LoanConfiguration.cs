﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanConfiguration
    {
        public int Id { get; set; }
        public int? IdloanFrequency { get; set; }
        public int? IdloanAmount { get; set; }
        public int? TotalPayments { get; set; }

        public virtual LoanAmount IdloanAmountNavigation { get; set; }
        public virtual LoanFrequency IdloanFrequencyNavigation { get; set; }
    }
}
