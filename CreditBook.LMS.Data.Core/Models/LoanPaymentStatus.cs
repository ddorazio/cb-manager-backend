﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanPaymentStatus
    {
        public LoanPaymentStatus()
        {
            ClientLoanPayment = new HashSet<ClientLoanPayment>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientLoanPayment> ClientLoanPayment { get; set; }
    }
}
