﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class PerceptechUploadErrors
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime LogDate { get; set; }
    }
}
