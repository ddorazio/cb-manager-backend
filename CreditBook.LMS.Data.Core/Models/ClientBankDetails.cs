﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientBankDetails
    {
        public int Id { get; set; }
        public int? Idclient { get; set; }
        public string TransitNumber { get; set; }
        public string InstitutionNumber { get; set; }
        public string AccountNumber { get; set; }
        public DateTime? BankruptEndDate { get; set; }
        public bool IsBankrupt { get; set; }

        public virtual Client IdclientNavigation { get; set; }
    }
}
