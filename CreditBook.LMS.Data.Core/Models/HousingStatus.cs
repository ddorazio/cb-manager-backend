﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class HousingStatus
    {
        public HousingStatus()
        {
            ClientFinancialStatus = new HashSet<ClientFinancialStatus>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientFinancialStatus> ClientFinancialStatus { get; set; }
    }
}
