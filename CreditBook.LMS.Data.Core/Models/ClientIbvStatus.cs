﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientIbvStatus
    {
        public ClientIbvStatus()
        {
            ClientIbv = new HashSet<ClientIbv>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientIbv> ClientIbv { get; set; }
    }
}
