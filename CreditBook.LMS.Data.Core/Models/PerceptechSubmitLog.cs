﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class PerceptechSubmitLog
    {
        public int Id { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public int? ReturnCode { get; set; }
    }
}
