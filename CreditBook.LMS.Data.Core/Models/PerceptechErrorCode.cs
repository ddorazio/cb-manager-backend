﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class PerceptechErrorCode
    {
        public PerceptechErrorCode()
        {
            ClientLoanPayment = new HashSet<ClientLoanPayment>();
        }

        public int Id { get; set; }
        public int? Code { get; set; }
        public string MessageEn { get; set; }
        public string MessageFr { get; set; }

        public virtual ICollection<ClientLoanPayment> ClientLoanPayment { get; set; }
    }
}
