﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class PayFrequency
    {
        public PayFrequency()
        {
            ClientEmploymentStatus = new HashSet<ClientEmploymentStatus>();
            ClientRevenueSource = new HashSet<ClientRevenueSource>();
            ExternalLoanWebRequest = new HashSet<ExternalLoanWebRequest>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }
        public string CreditBookCheckCode { get; set; }

        public virtual ICollection<ClientEmploymentStatus> ClientEmploymentStatus { get; set; }
        public virtual ICollection<ClientRevenueSource> ClientRevenueSource { get; set; }
        public virtual ICollection<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
    }
}
