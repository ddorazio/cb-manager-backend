﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientComment
    {
        public ClientComment()
        {
            ClientCommentLabel = new HashSet<ClientCommentLabel>();
        }

        public int Id { get; set; }
        public int Idclient { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDateTime { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public bool IsArchived { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public virtual Client IdclientNavigation { get; set; }
        public virtual ICollection<ClientCommentLabel> ClientCommentLabel { get; set; }
    }
}
