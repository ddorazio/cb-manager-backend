﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class NotificationTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SubjectEn { get; set; }
        public string SubjectFr { get; set; }
        public string EmailContentEn { get; set; }
        public string EmailContentFr { get; set; }
        public string SmsContentEn { get; set; }
        public string SmsContentFr { get; set; }
    }
}
