﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class ClientLoanCollectionComment
    {
        public int Id { get; set; }
        public int IdclientLoanCollection { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDateTime { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public bool IsArchived { get; set; }

        public virtual ClientLoanCollection IdclientLoanCollectionNavigation { get; set; }
    }
}
