﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanRequestStatus
    {
        public LoanRequestStatus()
        {
            ExternalLoanWebRequest = new HashSet<ExternalLoanWebRequest>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }

        public virtual ICollection<ExternalLoanWebRequest> ExternalLoanWebRequest { get; set; }
    }
}
