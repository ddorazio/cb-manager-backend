﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class PerceptechRejectionLog
    {
        public int Id { get; set; }
        public DateTime? RejectionDate { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public bool? Received { get; set; }
        public bool? Empty { get; set; }
    }
}
