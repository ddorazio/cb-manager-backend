﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Data.Core.Models
{
    public partial class LoanStatus
    {
        public LoanStatus()
        {
            ClientLoan = new HashSet<ClientLoan>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameFr { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ClientLoan> ClientLoan { get; set; }
    }
}
