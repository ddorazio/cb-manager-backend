﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.Logic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestSharp;
using RestSharp.Authenticators;

namespace CreditBook.LMS.Perceptech
{
    class Program
    {
        public static IConfiguration _configuration;
        public static ServiceProvider _serviceProvider;
        public static ServiceCollection serviceCollection;

        static void Main(string[] args)
        {
            try
            {
                serviceCollection = new ServiceCollection();
                ConfigureServices(serviceCollection);
                _serviceProvider = serviceCollection.BuildServiceProvider();

                //
                //ValidatePerceptechFile().Wait();

                //
                SubmitPerceptechFile().Wait();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public static async System.Threading.Tasks.Task ValidatePerceptechFile()
        //{
        //    try
        //    {
        //        string dailyReportUploadFolder = _configuration.GetSection("Perceptech")["DailyReportUploadFolder"];
        //        string dailyReportRemovedLinesFolder = _configuration.GetSection("Perceptech")["DailyReportRemovedLinesFolder"];
        //        string dailyReportUploadErrorFolder = _configuration.GetSection("Perceptech")["DailyReportUploadErrorFolder"];

        //        string uploadEndpoint = _configuration.GetSection("Perceptech")["UploadEndpoint"];
        //        string uploadCode = _configuration.GetSection("Perceptech")["UploadCode"];
        //        string uploadPassword = _configuration.GetSection("Perceptech")["UploadPassword"];

        //        string fileVersion = _configuration.GetSection("Perceptech")["FileVersion"];
        //        string clientNumber = _configuration.GetSection("Perceptech")["ClientNumber"];
        //        string processingCenter = _configuration.GetSection("Perceptech")["ProcessingCenter"];
        //        string company = _configuration.GetSection("Perceptech")["Company"];

        //        string sendGridAPIKey = _configuration.GetSection("SendGrid")["APIKey"];

        //        var service = _serviceProvider.GetService<IPerceptechService>();

        //        //
        //        await service.ValidateLoanPayments(sendGridAPIKey, dailyReportUploadFolder, dailyReportRemovedLinesFolder, clientNumber, company, fileVersion, processingCenter);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static async System.Threading.Tasks.Task SubmitPerceptechFile()
        {
            var service = _serviceProvider.GetService<IPerceptechService>();

            string dailyReportUploadFolder = _configuration.GetSection("Perceptech")["DailyReportUploadFolder"];
            string dailyReportRemovedLinesFolder = _configuration.GetSection("Perceptech")["DailyReportRemovedLinesFolder"];
            string dailyReportUploadErrorFolder = _configuration.GetSection("Perceptech")["DailyReportUploadErrorFolder"];

            string uploadEndpoint = _configuration.GetSection("Perceptech")["UploadEndpoint"];
            string uploadCode = _configuration.GetSection("Perceptech")["UploadCode"];
            string uploadPassword = _configuration.GetSection("Perceptech")["UploadPassword"];

            string fileVersion = _configuration.GetSection("Perceptech")["FileVersion"];
            string clientNumber = _configuration.GetSection("Perceptech")["ClientNumber"];
            string processingCenter = _configuration.GetSection("Perceptech")["ProcessingCenter"];
            string company = _configuration.GetSection("Perceptech")["Company"];

            string sendGridAPIKey = _configuration.GetSection("SendGrid")["APIKey"];

            try
            {
                //
                var upload = service.ProgramLoanPayments(sendGridAPIKey, dailyReportUploadFolder, dailyReportRemovedLinesFolder, clientNumber, company, fileVersion, processingCenter);

                // Send file to Perceptech if the File is not empty
                if (!upload.UploadEmpty)
                {
                    var client = new RestClient(uploadEndpoint);
                    client.Authenticator = new HttpBasicAuthenticator(uploadCode, uploadPassword);
                    client.Timeout = -1;

                    var request = new RestRequest(Method.POST);
                    request.AddFile("userfile", dailyReportUploadFolder + (DateTime.Today).ToString("yyyy-MM-dd") + "/" + upload.UploadFileName);

                    IRestResponse response = client.Execute(request);

                    // Check to see if we receive an error code
                    var result = response.Content;

                    await service.LogUploadErrors(result, upload.UploadFileName, sendGridAPIKey, dailyReportUploadErrorFolder);
                }
                else
                {
                    // Log submitted Date even if no payments are uploaded
                    await service.LogSubmittedDate();
                }
            }
            catch (Exception ex)
            {
                await service.LogUploadFailedUpload(sendGridAPIKey, dailyReportUploadErrorFolder);
                throw ex;
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            // Add IHttpClientFactory to DI
            services.AddHttpClient();

            // Build configuration
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            // Connection string
            var connectionString = _configuration.GetConnectionString("LMS");

            // Set up DbContextFactory singleton
            services.AddSingleton<IDbContextProvider>(new DbContextProvider(connectionString));

            // Set up services to Inject
            services.AddSingleton<IPerceptechService, PerceptechService>();
            //services.AddSingleton<IPerceptechDailyReturnService, PerceptechDailyReturnService>();
            services.AddSingleton<IMessageService, MessageService>();
            services.AddSingleton<IConfigurationService, ConfigurationService>();
            //services.AddSingleton<INotificationService, NotificationService>();

            // Configure SendGrid (email)
            services.AddSingleton<ISendgridClient>(new SendgridClient
            (
                _configuration.GetSection("SendGrid")["APIKey"]
            ));

            // Configure Twilio (SMS)
            try
            {
#warning Twilio config should be fetched from the SaaS manager app. appConfig.js is only temporary
                var twilioClient = new TwilioClient
                (
                    _configuration.GetSection("Twilio")["Key"],
                    _configuration.GetSection("Twilio")["Secret"],
                    _configuration.GetSection("Twilio")["PhoneNumber"]
                );
                twilioClient.Initialize();
                services.AddSingleton<ITwilioClient>(twilioClient);
            }
            catch (Exception e)
            {
                throw new Exception("Twilio configuration missing or invalid.", e);
            }

            // Configure Message service
            services.AddTransient<IMessageService>(serviceProvider => new MessageService
            (
                serviceProvider.GetService<IDbContextProvider>(),
                serviceProvider.GetService<ITwilioClient>(),
                serviceProvider.GetService<ISendgridClient>()
            ));

            // Add access to generic IConfiguration
            services.AddSingleton<IConfiguration>(_configuration);
        }
    }
}
