﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.Logic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestSharp;
using RestSharp.Authenticators;

namespace CreditBook.Perceptech.DailyRejections
{
    class Program
    {
        public static IConfiguration _configuration;
        public static ServiceProvider _serviceProvider;
        public static ServiceCollection serviceCollection;

        static void Main(string[] args)
        {
            try
            {
                serviceCollection = new ServiceCollection();
                ConfigureServices(serviceCollection);
                _serviceProvider = serviceCollection.BuildServiceProvider();

                // Do not run on weekends
                if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
                {
                    DownloadErrorReport().Wait();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ProgramPaymentNoOfDays(DateTime date) =>
        date.DayOfWeek switch
        {
            DayOfWeek.Monday => -3,
            DayOfWeek.Tuesday => -1,
            DayOfWeek.Wednesday => -1,
            DayOfWeek.Thursday => -1,
            DayOfWeek.Friday => -1,
            _ => throw new ArgumentException(message: "Invalid day of the week")
        };

        public static async Task DownloadErrorReport()
        {
            var service = _serviceProvider.GetService<IPerceptechDailyReturnService>();

            string dailyRejectionReportFolder = _configuration.GetSection("Perceptech")["DailyRejectionReportFolder"];

            string dailyRejectionReportEndpoint = _configuration.GetSection("Perceptech")["DailyRejectionReportEndpoint"];
            string reportCode = _configuration.GetSection("Perceptech")["UploadCode"];
            string reportPassword = _configuration.GetSection("Perceptech")["UploadPassword"];

            string sendGridAPIKey = _configuration.GetSection("SendGrid")["APIKey"];
            string lmsEndpoint = _configuration.GetSection("LMS")["Endpoint"];
            string clientNumber = _configuration.GetSection("Perceptech")["ClientNumber"];

            try
            {
                var rejectionDate = DateTime.Today.AddDays(ProgramPaymentNoOfDays(DateTime.Today));
                var dateFrom = rejectionDate;
                var dateTo = rejectionDate;

                // For production only
                var dateFormatFrom = dateFrom.Year + "-" + dateFrom.Month.ToString("d2") + "-" + dateFrom.Day.ToString("d2");
                var dateFormatTo = dateTo.Year + "-" + dateTo.Month.ToString("d2") + "-" + dateTo.Day.ToString("d2");

                // Get Preteur Language
                var language = service.GetLanguage();

                // Get Daily Report
                var url = dailyRejectionReportEndpoint + "?cie=" + clientNumber + "&rapport_type=900&format=CSV&de=" + dateFormatFrom + "&a=" + dateFormatTo + "&lang=" + language;
                var client = new RestClient(url);
                client.Authenticator = new HttpBasicAuthenticator(reportCode, reportPassword);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                var result = response.Content;
                //var result = "dd";

                // Validate if the Daily Rejections have already been received
                if(!await service.ValidateReportReceived(rejectionDate))
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        await service.UpdateLoanPayments(dailyRejectionReportFolder, result, language, lmsEndpoint);

                        // Report received
                        await service.UpdatePerecptechRejectionLog(rejectionDate, true, false);
                    }
                    else
                    {
                        // Report is empty
                        await service.UpdatePerecptechRejectionLog(rejectionDate, false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                service.SendDailyRejectionReporErrorEmail(sendGridAPIKey).Wait();
                throw ex;
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            // Add IHttpClientFactory to DI
            services.AddHttpClient();

            // Build configuration
            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            // Connection string
            var connectionString = _configuration.GetConnectionString("LMS");

            // Set up DbContextFactory singleton
            services.AddSingleton<IDbContextProvider>(new DbContextProvider(connectionString));

            // Set up services to Inject
            //services.AddSingleton<IPerceptechService, PerceptechService>();
            services.AddSingleton<IPerceptechDailyReturnService, PerceptechDailyReturnService>();
            services.AddSingleton<IConfigurationService, ConfigurationService>();
            services.AddSingleton<IMessageService, MessageService>();
            //services.AddSingleton<INotificationService, NotificationService>();

            // Configure SendGrid (email)
            services.AddSingleton<ISendgridClient>(new SendgridClient
            (
                _configuration.GetSection("SendGrid")["APIKey"]
            ));

            // Configure Twilio (SMS)
            try
            {
#warning Twilio config should be fetched from the SaaS manager app. appConfig.js is only temporary
                var twilioClient = new TwilioClient
                (
                    _configuration.GetSection("Twilio")["Key"],
                    _configuration.GetSection("Twilio")["Secret"],
                    _configuration.GetSection("Twilio")["PhoneNumber"]
                );
                twilioClient.Initialize();
                services.AddSingleton<ITwilioClient>(twilioClient);
            }
            catch (Exception e)
            {
                throw new Exception("Twilio configuration missing or invalid.", e);
            }

            // Configure Message service
            services.AddTransient<IMessageService>(serviceProvider => new MessageService
            (
                serviceProvider.GetService<IDbContextProvider>(),
                serviceProvider.GetService<ITwilioClient>(),
                serviceProvider.GetService<ISendgridClient>()
            ));

            // Add access to generic IConfiguration
            services.AddSingleton<IConfiguration>(_configuration);
        }
    }
}
