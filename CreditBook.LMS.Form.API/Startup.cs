using System;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.External;
using CreditBook.LMS.Service.Core.Logger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace CreditBook.LMS.Form.API
{
    public class Startup
    {
        public Startup(IHostEnvironment configuration)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(configuration.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
            //Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string AllowedOriginsPolicy = "AllowAnyOriginsPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure CORS
            services.AddCors(options =>
            {
                options.AddPolicy(AllowedOriginsPolicy,
                builder =>
                {
                    builder
                    //.WithOrigins(Configuration["BaseConfig:CORSSites"])
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            var connectionString = Configuration.GetConnectionString("LMS");

            // Set up DbContextFactory singleton
            services.AddSingleton<IDbContextProvider>(new DbContextProvider(connectionString));

            // Set up services to Inject
            services.AddTransient<IWebhookService_24HR, WebhookService_24HR>();
            services.AddTransient<IExternalLoanRequestService, ExternalLoanRequestService>();
            services.AddTransient<IClientHistoryService, ClientHistoryService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IConfigurationService, ConfigurationService>();
            services.AddTransient<IAPIHubClient_24HR, APIHubClient_24HR>();
            services.AddHttpContextAccessor();

            // Configure SendGrid (email)
            services.AddSingleton<ISendgridClient>(new SendgridClient
            (
                Configuration.GetSection("SendGrid")["Key"]
            ));

            // Configure logging service
            try
            {
                var loggingService = new ErrorService
                (
                    Configuration.GetSection("ErrorLogging")["Path"]
                );
                services.AddSingleton<IErrorService>(loggingService);
            }
            catch (Exception e)
            {
                throw new Exception("Error logger is missing or invalid", e);
            }

            // Configure Message service
            services.AddTransient<IMessageService>(serviceProvider => new MessageService
            (
                serviceProvider.GetService<IDbContextProvider>(),
                serviceProvider.GetService<ITwilioClient>(),
                serviceProvider.GetService<ISendgridClient>()
            ));

            // Configure Twilio (SMS)
            try
            {
                var twilioClient = new TwilioClient
                (
                    Configuration.GetSection("Twilio")["Key"],
                    Configuration.GetSection("Twilio")["Secret"],
                    Configuration.GetSection("Twilio")["PhoneNumber"]
                );
                twilioClient.Initialize();
                services.AddSingleton<ITwilioClient>(twilioClient);
            }
            catch (Exception e)
            {
                throw new Exception("Twilio configuration missing or invalid.", e);
            }

            // HTTP Requests
            services.AddHttpClient();

            // Set Default Date format
            object p = services.AddControllers().AddNewtonsoftJson(config =>
            {
                config.SerializerSettings.DateFormatString = "yyyy-MM-dd";
                config.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            // Allow CORS
            app.UseCors(AllowedOriginsPolicy);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
