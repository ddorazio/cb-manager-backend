﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models.External;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CreditBook.LMS.Form.API.Controllers
{
    [Route("api/FormRequest")]
    public class FormRequestController : Controller
    {
        private readonly IExternalLoanRequestService _loanRequestService;
        private IConfiguration _configuration { get; set; }
        private readonly IHttpClientFactory _clientFactory;
        private readonly IMessageService _messageService;
        private readonly IErrorService _errorService;

        private string CreditBookCheckGuid;

        public FormRequestController(IExternalLoanRequestService loanRequestService, IConfiguration configuration, IHttpClientFactory clientFactory, IMessageService messageService, IErrorService errorService)
        {
            _loanRequestService = loanRequestService;
            _configuration = configuration;
            _clientFactory = clientFactory;
            _messageService = messageService;
            _errorService = errorService;
        }

        [HttpPost("NewLoanRequest")]
        public async Task<ActionResult> NewLoanRequest([FromBody] NewLoanFormRequestModel model)
        {
            // Log incoming JSON for all requests
            _errorService.LoanRequestLogger(JsonConvert.SerializeObject(model).ToString());

            model.AutoDecline = false;
            bool success = false;
            var emailLanguage = model.LanguageCode == "E" ? LanguageEnum.English : LanguageEnum.French;

            // Check if we Auto Decline client
            if (await _loanRequestService.AutoDecline(model))
            {
                model.AutoDecline = true;
            }

            // CreditBook Check
            var okResponseCreditCheck = await PushNewLoanAsync(model) as OkObjectResult;

            if (okResponseCreditCheck != null)
            {
                model.CreditBookCheckSuccessfull = okResponseCreditCheck.StatusCode == 200 ? true : false;
                model.CreditBookCheckRequestId = CreditBookCheckGuid;
            }
            else
            {
                model.CreditBookCheckSuccessfull = false;
            }

            // Create IBV if request was not Auto-Declined
            NewIBVRequestResponseModel responseIBV = null;
            if (!model.AutoDecline)
            {
                var okResponseIBV = await CreateNewIBVRequest(new NewIBVRequestModel()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.CellPhoneNumber,
                    Email = model.PrimaryEmail,
                    IPAddress = model.IpAddress,
                    BrowserName = "Mozilla/etc"
                }) as OkObjectResult;
                responseIBV = okResponseIBV.Value as NewIBVRequestResponseModel;
                model.IBVRequestId = responseIBV.data.requestID;
                model.IBVUrl = responseIBV.data.url;
                model.IBVReportUrl = _configuration["IBV:IBVReportUrl"] + responseIBV.data.requestID;
            }

            // Create Client and LoanRequest in LMS
            success = await _loanRequestService.CreateLoanRequest(model);

            if (success)
            {
                // Send Auto decline email if Client was created
                if (model.AutoDecline)
                {
                    // Send Loan denied email
                    await _messageService.SendMessage_LoanRefused(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                    {
                        LoanRefused = new Domain.Core.Models.EmailTemplates.LoanRefusedModel()
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            LoanAmount = model.Amount,
                            EmailAddress = model.PrimaryEmail,
                            CellPhone = model.CellPhoneNumber
                        }
                    });
                }

                if (!model.AutoDecline)
                {
                    var ibvHtmlUrl = model.LanguageCode == "E"
                        ? "<a href='" + responseIBV.data.url + "'>IBV</a>"
                        : "<a href='" + responseIBV.data.url + "'>IBV</a>";

                    // Only send IBV email if the Client was successfully saved and was not Auto-Declined
                    if (model.LoanWithoutDocuments)
                    {
                        await _messageService.SendMessage_ApplicationWithoutDocuments(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                        {
                            WithoutDocuments = new Domain.Core.Models.EmailTemplates.RequestWithoutDocuments()
                            {
                                IBVHtmlURL = ibvHtmlUrl,
                                IBVUrl = responseIBV.data.url,
                                ClientEmail = model.PrimaryEmail,
                                ClientCell = model.CellPhoneNumber
                            }
                        });
                    }
                    else if (!model.LoanWithoutDocuments)
                    {
                        await _messageService.SendMessage_ApplicationWithDocuments(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                        {
                            WithDocuments = new Domain.Core.Models.EmailTemplates.RequestWithDocuments
                            {
                                IBVHtmlURL = ibvHtmlUrl,
                                ClientEmail = model.PrimaryEmail,
                                ClientCell = model.CellPhoneNumber
                            }
                        });
                    }
                }
            }

            if (model.AutoDecline)
            {
                return Ok(new { code = "DECLINED" });
            }

            if (success && !model.AutoDecline)
            {
                if (model.LoanWithoutDocuments)
                {
                    return Ok(new { code = "SUCCESS", data = new { url = model.IBVUrl } });
                }

                return Ok(new { code = "SUCCESS" });
            }

            return BadRequest();
        }

        [HttpPost("NonCompletedRequest")]
        public async Task<ActionResult> NonCompletedRequest([FromBody] NonCompletedRequestModel model)
        {
            // Create Client and LoanRequest in LMS
            var result = await _loanRequestService.NonCompletedRequest(model);

            return Ok(new { code = "SUCCESS", data = result });
        }

        // Internal
        public async Task<IActionResult> PushNewLoanAsync(NewLoanFormRequestModel model)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, _configuration["CreditBookCheck:Endpoint"] + "loanrequests/");
            request.Headers.Add("Authorization", _configuration["CreditBookCheck:APIKey"]);
            var client = _clientFactory.CreateClient();

            var payFrequencyMatched = "";

            switch (model.PayFrequencyCode)
            {
                case "W":
                    payFrequencyMatched = "weekly";
                    break;
                case "M":
                    payFrequencyMatched = "monthly";
                    break;
                case "2W":
                    payFrequencyMatched = "biweekly";
                    break;
                case "2M":
                    payFrequencyMatched = "semi-monthly";
                    break;
            }

            var creditBookCheckGuid = Guid.NewGuid().ToString().ToUpper();

            var content = new CreditBookCheckNewLoanModel()
            {
                requestId = creditBookCheckGuid,
                firstName = model.FirstName,
                lastName = model.LastName,
                dateOfBirth = String.Format("{0:yyyy-MM-dd}", model.Birthday),
                address = new CreditBookCheckAddress()
                {
                    streetAddressLine1 = model.CivicNumber + " " + model.StreetName,
                    city = model.City,
                    country = "CA",
                    stateProvince = model.Province,
                    zipPostalCode = model.PostalCode
                },
                //job = new CreditBookCheckEmployment()
                //{
                //    companyName = model.EmployerName,
                //    companyPhone = model.EmployerPhoneNumber,
                //    supervisorName = model.SupervisorName,
                //    jobTitle = model.Occupation,
                //    payFrequency = payFrequencyMatched, //model.PayFrequency,
                //    payNext = String.Format("{0:yyyy-MM-dd}", model.NextPayDate),
                //    startDate = model.IncomeTypeCode == "EMPLOYEE" || model.IncomeTypeCode == "SELF_EMPLOYED"
                //                    ? String.Format("{0:yyyy-MM-dd}", model.HiringDate) : null
                //},
                phone1 = model.CellPhoneNumber,
                email = model.PrimaryEmail,
                references = new List<CreditBookCheckReference>(),
                amount = Convert.ToDecimal(model.Amount),
                requestDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now),
                ipAddress = model.IpAddress,
                language = model.Language,
                status = "new"
            };

            // Validate Employment Status
            switch (model.IncomeTypeCode)
            {
                case "EMPLOYEE":
                    content.job = new CreditBookCheckEmployment()
                    {
                        companyName = model.EmployerName,
                        companyPhone = model.EmployerPhoneNumber,
                        supervisorName = model.SupervisorName,
                        jobTitle = model.Occupation,
                        payFrequency = payFrequencyMatched, //model.PayFrequency,
                        payNext = String.Format("{0:yyyy-MM-dd}", model.NextPayDate),
                        startDate = String.Format("{0:yyyy-MM-dd}", model.HiringDate)
                    };
                    break;


                case "SELF_EMPLOYED":
                    content.job = new CreditBookCheckEmployment()
                    {
                        companyName = model.LanguageCode == "F" ? "Travailleur autonome" : "Self-employed",
                        companyPhone = model.SelfEmployedWorkTelephone,
                        jobTitle = model.LanguageCode == "F" ? "Travailleur autonome" : "Self-employed",
                        payFrequency = payFrequencyMatched, //model.PayFrequency,
                        payNext = String.Format("{0:yyyy-MM-dd}", model.NextDepositDate),
                        startDate = String.Format("{0:yyyy-MM-dd}", model.SelfEmployedStartDate)
                    };
                    break;

                default:
                    break;
            }

            // Add reference 1
            content.references.Add(new CreditBookCheckReference()
            {
                firstName = model.Ref1FirstName,
                lastName = model.Ref1LastName,
                phone = model.Ref1Telephone,
                email = "",//model.Ref1Email,
                relationship = model.Ref1Relationship
            });

            // Add reference 2
            content.references.Add(new CreditBookCheckReference()
            {
                firstName = model.Ref2FirstName,
                lastName = model.Ref2LastName,
                phone = model.Ref2Telephone,
                email = "",//model.Ref2Email,
                relationship = model.Ref2Relationship
            });

            // Remove NULL objects before sending
            var json = JsonConvert.SerializeObject(content, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                request.Content = stringContent;
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    //var responseStream = await response.Content.ReadAsStringAsync();
                    //var responseData = JsonConvert.DeserializeObject<CreditBookCheckNewLoanResponseModel>(responseStream);

                    if (response.StatusCode == System.Net.HttpStatusCode.Created)
                    {
                        CreditBookCheckGuid = creditBookCheckGuid;
                        return Ok(new { status = "Success", CreditBookCheckGuid = creditBookCheckGuid });
                    }

                    if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                    {
                        return BadRequest();
                    }

                    return BadRequest();
                }
            }

            return BadRequest();
        }

        // Internal
        public async Task<IActionResult> CreateNewIBVRequest(NewIBVRequestModel model)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, _configuration["IBV:Endpoint"]);
            request.Headers.Add("APIKey", _configuration["IBV:APIKey"]);
            var client = _clientFactory.CreateClient();

            model.LMSReturnURL = _configuration["IBV:LMSReturnURL"];

            //var json = JsonConvert.SerializeObject(model);
            var json = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                request.Content = stringContent;
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var responseStream = await response.Content.ReadAsStringAsync();
                    var responseData = JsonConvert.DeserializeObject<NewIBVRequestResponseModel>(responseStream);

                    return Ok(responseData);
                }
            }

            return Ok(null);
        }
    }
}
