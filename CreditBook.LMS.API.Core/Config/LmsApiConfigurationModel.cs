namespace CreditBook.LMS.API.Core.Config
{
    // Dealer-specific LMS API configuration 
    internal class LmsApiConfigurationModel
    {
        public string JwtToken { get; set; }
        public string JwtIssuer { get; set; }

        public string LmsConnectionString { get; set; }

        public string IbvEndpoint { get; set; }
        public string IbvApiKey { get; set; }

        public string DataHubEndpoint { get; set; }
        public string DataHubApiKey { get; set; }

        public string AWSAccessKeyId { get; set; }
        public string AWSSecretKey { get; set; }
        public string S3BucketName { get; set; }

        public string TwilioKey { get; set; }
        public string TwilioSecret { get; set; }
        public string TwilioPhoneNumber { get; set; }

        public string SendGridKey { get; set; }

        public string NotificationEmailAddress { get; set; }
    }
}