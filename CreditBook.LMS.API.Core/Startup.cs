﻿using System;
using System.Net;
using System.Text;
using CreditBook.LMS.API.Core.Identity;
using CreditBook.LMS.API.Core.Identity.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.Hubs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace CreditBook.LMS.API.Core
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        readonly string AllowedOriginsPolicy = "AllowOriginsPolicy";

        public Startup(IWebHostEnvironment configuration)
        {
            // Load AppSettings JSON
            var builder = new ConfigurationBuilder()
                .SetBasePath(configuration.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)


                // Environment variables with overwrite everything else (ie appsettings.json, secret store)
                // The format is Prefix_Section__Key (__ replaces : for bash compatibility)
                // The prefix for this application is LMSAPI_
                // ex : Configuration.GetSection("DataHub")["Endpoint"] matches with LMSAPI_DataHub__Endpoint
                // More info : https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1
                .AddEnvironmentVariables(prefix: "LMSAPI_");

            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure CORS
            services.AddCors(options =>
            {
                options.AddPolicy(AllowedOriginsPolicy,
                builder =>
                {
                    builder
                    .WithOrigins(Configuration["BaseConfig:CORSSites"])
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
            });

            // Add Config so that it can be injected into Controllers
            //services.Configure<AppSettings>(Configuration.GetSection("PortalConfig"));

            var connectionString = Configuration.GetConnectionString("LMS");

            // Identity context
            services.AddDbContext<IdentityDbContext>(options =>
                options.UseSqlServer(connectionString));

            // Set up DbContextFactory singleton
            services.AddSingleton<IDbContextProvider>(new DbContextProvider(connectionString));

            // Set up services to Inject
            services.AddTransient<IWebhookService_24HR, WebhookService_24HR>();
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<ILoanService, LoanService>();
            services.AddTransient<INotificationService, NotificationService>();
            services.AddTransient<ILoanConfigurationService, LoanConfigurationService>();
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IClientCommentService, ClientCommentService>();
            services.AddTransient<IMessageTemplateService, MessageTemplateService>();
            services.AddTransient<IClientHistoryService, ClientHistoryService>();
            services.AddTransient<ICollectionService, CollectionService>();
            services.AddTransient<ILoanRequestService, LoanRequestService>();
            services.AddTransient<IContractService, ContractService>();
            services.AddTransient<IDepositService, DepositService>();
            services.AddTransient<IDataHubService, DataHubService>();
            services.AddTransient<IPaymentAgreement, PaymentAgreementService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IConfigurationService, ConfigurationService>();
            services.AddTransient<IAPIHubClient_24HR, APIHubClient_24HR>();

            services.AddHttpContextAccessor();

            // Configure Datahub API client
            try
            {
                // services.AddSingleton<IDataHubClient>(s => new DataHubClient
                // (
                //     Configuration.GetSection("CreditBookCheck")["Endpoint"],
                //     Configuration.GetSection("CreditBookCheck")["APIKey"],
                //     s.GetRequiredService<IConfigurationService>
                // ));

                var datahubClient = new DataHubClient
                (
                    Configuration.GetSection("CreditBookCheck")["Endpoint"],
                    Configuration.GetSection("CreditBookCheck")["APIKey"]
                );
                services.AddSingleton<IDataHubClient>(datahubClient);
            }
            catch (Exception e)
            {
                throw new Exception("CreditBookCheck API configuration missing or invalid.", e);
            }

            // Configure Twilio (SMS)
            try
            {
#warning Twilio config should be fetched from the SaaS manager app. appConfig.js is only temporary
                var twilioClient = new TwilioClient
                (
                    Configuration.GetSection("Twilio")["Key"],
                    Configuration.GetSection("Twilio")["Secret"],
                    Configuration.GetSection("Twilio")["PhoneNumber"]
                );
                twilioClient.Initialize();
                services.AddSingleton<ITwilioClient>(twilioClient);
            }
            catch (Exception e)
            {
                throw new Exception("Twilio configuration missing or invalid.", e);
            }

            // Configure SendGrid (email)
            services.AddSingleton<ISendgridClient>(new SendgridClient
            (
                Configuration.GetSection("SendGrid")["Key"]
            ));

            // Configure Message service
            services.AddTransient<IMessageService>(serviceProvider => new MessageService
            (
                serviceProvider.GetService<IDbContextProvider>(),
                serviceProvider.GetService<ITwilioClient>(),
                serviceProvider.GetService<ISendgridClient>()
            ));

            // HTTP Requests
            services.AddHttpClient();

            // Inject JWT Token into Controller
            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext>()
                .AddDefaultTokenProviders();

            // Identity create user settings
            services.Configure<IdentityOptions>(options =>
            {
                // Default Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                //options.Password.RequiredLength = 1;
                options.Password.RequiredUniqueChars = 1;
            });

            // Set up AWS clients
            var s3ClientProvider = new S3ClientProvider
            (
                Configuration["S3Config:AWSAccessKeyId"],
                Configuration["S3Config:AWSSecretKey"]
            );
            services.AddSingleton<IS3ClientProvider>(s3ClientProvider);
            services.AddSingleton<IS3StreamService>(provider => new S3StreamService
            (
                provider.GetService<IS3ClientProvider>(),
                Configuration["S3Config:BucketName"]
            ));

            // JWT Token
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["Tokens:Issuer"],
                        ValidAudience = Configuration["Tokens:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]))
                    };
                });

            // Set Default Date format
            services.AddControllers().AddNewtonsoftJson(config =>
            {
                config.SerializerSettings.DateFormatString = "yyyy-MM-dd";
                config.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Access files in wwwroot folder
            app.UseStaticFiles();

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    // Note : this lambda could be moved into its own file in case it grows more complex
                    // We might want to have more specific error messages eg in case the SQL server is down or something
                    context.Response.StatusCode = 500;
                    context.Response.ContentType = "application/json";

                    var model = new Error("UNHANDLED_EXCEPTION", "Unhandled exception.");
                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();
                    var error = exceptionHandlerPathFeature.Error;
                    if (env.EnvironmentName == Environments.Development && error != null)
                    {
                        model.Message = $"{error.Message}\n\n{error.StackTrace}";
                    }

                    var responseBody = JsonConvert.SerializeObject(model, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    await context.Response.WriteAsync(responseBody);
                });
            });

            if (env.EnvironmentName != Environments.Development)
            {
                app.UseHsts();
            }

            app.UseRouting();
            app.UseStaticFiles();

            // Allow CORS
            app.UseCors(AllowedOriginsPolicy);

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(config =>
            {
                config.MapControllers();
                config.MapHub<NotificationsHub>("/notificationsHub");
            });
        }
    }
}
