using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/ClientComment")]
    public class ClientCommentController : Controller
    {
        private readonly IClientCommentService _clientCommentService;
        private readonly IClientHistoryService _historyService;

        public ClientCommentController(IClientCommentService clientCommentService, IClientHistoryService historyService)
        {
            _clientCommentService = clientCommentService;
            _historyService = historyService;
        }

        [HttpGet("GetClientComments/{clientId}")]
        public async Task<IEnumerable<ClientCommentModel>> GetClientComments(int clientId)
        {
            return await _clientCommentService.GetClientComments(clientId);
        }

        [HttpGet("GetCommentLabels")]
        public async Task<IEnumerable<CommentLabelModel>> GetCommentLabels()
        {
            return await _clientCommentService.GetCommentLabels();
        }

        [HttpPost("CreateClientComment")]
        public async Task<ClientCommentModel> CreateClientComment([FromBody]ClientCommentModel data)
        {
            data.Username = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var result = await _clientCommentService.CreateClientComment(data);   
            //await _historyService.AddClientHistoryEntry(data.ClientId, $"{data.Username} added a comment.");
            return result;
        }

        [HttpPut("UpdateClientComment")]
        public async Task<ClientCommentModel> UpdateClientComment([FromBody]ClientCommentModel data)
        {
            return await _clientCommentService.UpdateClientComment(data);
        }

        [HttpDelete("DeleteClientComment/{clientCommentId}")]
        public async Task DeleteClientComment(int clientCommentId)
        {
            var username = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            await _clientCommentService.DeleteClientComment(clientCommentId, username);
        }

        [HttpPost("AddLabelToClientComment/{clientCommentId}/{commentLabelId}")]
        public async Task<CommentLabelModel> AddLabelToClientComment(int clientCommentId, int commentLabelId)
        {
            return await _clientCommentService.AddLabelToClientComment(clientCommentId, commentLabelId);
        }

        [HttpDelete("RemoveLabelFromClientComment/{clientCommentId}/{commentLabelId}")]
        public async Task RemoveLabelFromClientComment(int clientCommentId, int commentLabelId)
        {
            await _clientCommentService.RemoveLabelFromClientComment(clientCommentId, commentLabelId);
        }
    }
}