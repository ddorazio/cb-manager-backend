using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Deposit")]
    public class DepositController : Controller
    {
        private readonly IDepositService _depositService;

        public DepositController(IDepositService depositService)
        {
            _depositService = depositService;
        }

        [HttpGet("List")]
        public async Task<IEnumerable<DepositModel>> GetDeposits()
        {
            return await _depositService.GetDeposits();
        }

        [HttpPut("Update")]
        public async Task<ActionResult<DepositModel>> UpdateDeposit([FromBody] DepositModel model)
        {
            if (model.TypeCode == "" && string.IsNullOrEmpty(model.InteracConfirmationCode))
            {
                return BadRequest(new Error("INTERAC_CONFIRMATION_CODE_REQUIRED",
                                            "Interac deposits require an Interac confirmation code."));
            }
            return await _depositService.UpdateDeposit(model);
        }
    }
}