﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.API.Core.Identity.Models;
using CreditBook.LMS.Common.Utilities;
using CreditBook.LMS.Domain.Core.Models.API;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Route("api/Identity")]
    public class IdentityController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        private string _connectionString { get; set; }


        public IdentityController(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;

            _connectionString = _configuration["ConnectionStrings:LMS"];
        }

        [Route("Authenticate")]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] CreateUserModel model)
        {
            //GetEnvironment();
            var loginFailedResponse = new Error("LOGIN_FAILED", "Your login attempt failed.");

            var user = await _userManager.FindByEmailAsync(model.UserName);

            if (user == null || !await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return Unauthorized(loginFailedResponse);
            }

            if (!user.IsEnabled)
            {
                var disabledResponse = new Error("ACCOUNT_DISABLED", "Your account has been disabled.");
                return Unauthorized(disabledResponse);
            }

            var claims = new[]
            {
                    new Claim(JwtRegisteredClaimNames.Sub, model.UserName)
                };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token");

            // Add user Role to the JWT Token
            var roles = await _userManager.GetRolesAsync(user);
            claimsIdentity.AddClaims(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Tokens:Issuer"],
                                                _configuration["Tokens:Issuer"],
                                                claimsIdentity.Claims,
                                                expires: DateTime.Now.AddMinutes(60),
                                                signingCredentials: creds);

            var tokenValue = new JwtSecurityTokenHandler().WriteToken(token);

            // Validate user role
            if (await _userManager.IsInRoleAsync(user, "Admin"))
            {
                return Ok(new { status = "Success", message = "Login successfull", token = tokenValue });
            }

            if (await _userManager.IsInRoleAsync(user, "Agent"))
            {
                return Ok(new { status = "Success", message = "Login successfull", token = tokenValue });
            }

            return Unauthorized(loginFailedResponse);

        }

        [Authorize(Roles = "Admin")]
        [Route("ForgotPassword/{email}")]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            //GetEnvironment();

            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                var response = new Error("USER_NOT_FOUND", "Username does not exist.");
                return NotFound(response);
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            #warning Password reset feature is not fully implemented
            await _userManager.ResetPasswordAsync(user, token, "MB6PpmCw");

            List<string> emailList = new List<string>();
            emailList.Add(email);

            await EmailManager.SendForgotPasswordConfirmation("Na2BCKD!", "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", emailList);

            return Ok(new { status = "Success", message = "Reset successfull" });
        }

        [Authorize(Roles = "Admin")]
        [Route("ChangePassword")]
        [HttpPut]
        public async Task<IActionResult> ChangePassword([FromBody] string password)
        {
            //GetEnvironment();

            var user = await _userManager.FindByEmailAsync(this.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            if (user == null)
            {
                var response = new Error("USER_NOT_FOUND", "Username does not exist.");
                return NotFound(response);
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            await _userManager.ResetPasswordAsync(user, token, password);

            return Ok(new { status = "Success", message = "Change successfull" });
        }

        [Authorize(Roles = "Admin")]
        [Route("CreateAspUser")]
        [HttpPost]
        public async Task<IActionResult> CreateAspUser([FromBody] CreateUserModel model)
        {

            var user = await _userManager.FindByEmailAsync(model.UserName);

            if (user != null)
            {
                // Check if the Email that was found is
                return Conflict(new Error("EMAIL_EXISTS", "This email address is already in use."));
            }

            var newUser = new AppUser { UserName = model.UserName, Email = model.UserName, IsEnabled = model.IsEnabled };
            var result = await _userManager.CreateAsync(newUser, model.Password);

            if (result.Succeeded)
            {
                var roleExists = await _roleManager.RoleExistsAsync(model.Role);

                if (!roleExists)
                {
                    var newRole = new IdentityRole();
                    newRole.Name = model.Role;
                    await _roleManager.CreateAsync(newRole);
                }

                await _userManager.AddToRoleAsync(newUser, model.Role);
                return Ok(new { status = "Success", message = "Account created" });
            }

            throw new Exception("Could not process request.");
        }

        [Authorize(Roles = "Admin")]
        [Route("UpdateAspNetUserStatus")]
        [HttpPost]
        public async Task<IActionResult> UpdateAspNetUserStatus([FromBody] CreateUserModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.UserName);

            user.IsEnabled = model.IsEnabled;
            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return Ok(new { status = "Success", message = "Account deactivated" });
            }

            // Controller base has no method for "internal server error" but we can throw an exception
            // and let the middleware handle it
            throw new Exception("The account was not deactivated.");
        }
    }
}
