using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Collection")]
    public class CollectionController : Controller
    {
        private readonly ICollectionService _collectionService;

        public CollectionController(ICollectionService collectionService)
        {
            _collectionService = collectionService;
        }

        [HttpGet("RejectionsOfTheDay")]
        public async Task<ActionResult<IEnumerable<CollectionSummaryModel>>> GetRejectionsOfTheDay()
        {
            return Ok(await _collectionService.GetCollectionList(CollectionStatusEnum.RejectionsOfTheDay));
        }

        [HttpGet("WithFollowUp")]
        public async Task<IEnumerable<CollectionSummaryModel>> GetCollectionsWithFollowUp()
        {
            return await _collectionService.GetCollectionList(CollectionStatusEnum.NeedFollowUp);
        }

        [HttpGet("TransfersReceivable")]
        public async Task<IEnumerable<CollectionSummaryModel>> GetTransfersReceivableToDate()
        {
            return await _collectionService.GetCollectionList(CollectionStatusEnum.InteracETransfer);
        }

        [HttpGet("AwaitingResponse")]
        public async Task<IEnumerable<CollectionSummaryModel>> GetCollectionsAwaitingResponse()
        {
            return await _collectionService.GetCollectionList(CollectionStatusEnum.WaitingForResponse);
        }

        [HttpGet("{collectionId}")]
        public async Task<CollectionModel> GetCollection(int collectionId)
        {
            return await _collectionService.GetCollection(collectionId);
        }

        [HttpPut]
        public async Task<CollectionModel> UpdateCollection([FromBody] CollectionModel data)
        {
            return await _collectionService.UpdateCollection(data);
        }

        [HttpGet("GetComments/{collectionId}")]
        public async Task<IEnumerable<CollectionCommentModel>> GetComments(int collectionId)
        {
            return await _collectionService.GetComments(collectionId);
        }

        [HttpPost("SaveComment")]
        public async Task<CollectionCommentModel> SaveComment([FromBody] CollectionCommentModel data)
        {
            var username = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            data.Username = username;
            return await _collectionService.SaveComment(data);
        }

        [HttpDelete("DeleteComment/{collectionCommentId}")]
        public async Task DeleteComment(int collectionCommentId)
        {
            await _collectionService.DeleteComment(collectionCommentId);
        }

        [HttpPut("ToggleProcedureStep")]
        public async Task<CollectionProcedureStepModel> ToggleProcedureStep([FromBody]CollectionProcedureStepModel step)
        {
            return await _collectionService.ToggleProcedureStep(step);
        }

        [HttpPost("SendInteracCollectionReminders")]
        public async Task SendInteracCollectionReminders([FromBody]IEnumerable<int> collectionIds)
        {
            await _collectionService.SendCollectionReminders(collectionIds);
        }
    }
}