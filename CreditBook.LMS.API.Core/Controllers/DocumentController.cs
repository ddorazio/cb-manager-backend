using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Document")]
    public class DocumentController : Controller
    {
        private readonly IS3StreamService _s3StreamService;
        private readonly IDocumentService _documentService;

        public DocumentController(IS3StreamService s3StreamService, IDocumentService documentService)
        {
            _s3StreamService = s3StreamService;
            _documentService = documentService;
        }

        [HttpPost("UploadDocument")]
        public async Task<DocumentModel> UploadDocument([FromForm]DocumentModel document)
        {
            document.Filename = document.File.FileName;
            document.S3Key = $"{document.ClientId}/{document.Filename}_{DateTime.UtcNow.ToString("yyyyMMddHHmmss")}";
            using var stream = new MemoryStream();
            await document.File.CopyToAsync(stream);
            await _s3StreamService.UploadAsync(document.S3Key, stream);

            var doc = await _documentService.CreateDocument(document);
            return doc;
        }

        [HttpPut("UpdateDocument")]
        public async Task<DocumentModel> Update([FromBody]DocumentModel data)
        {
            return await _documentService.UpdateDocument(data);
        }

        [HttpGet("GetClientDocuments/{clientId}")]
        public async Task<IEnumerable<DocumentModel>> GetClientDocuments(int clientId)
        {
            return await _documentService.GetDocuments(clientId);
        }

        [HttpGet("Download/{documentId}")]
        public async Task<ActionResult> Download(Guid documentId)
        {
            var doc = await _documentService.GetDocument(documentId);
            if (doc == null) 
            { 
                var response = new Error("DOCUMENT_NOT_FOUND", "Found no document with this identifier.");
                return NotFound(response);
            }

            var extension = Path.GetExtension(doc.Filename);
            var contentType = extension switch 
            {
                ".pdf" => "application/pdf",
                _ => "application/octet-stream"
            };

            // These headers will make the browser attempt to render the file if it can
            Response.Headers.Add("Content-Disposition", $"inline; filename=\"{doc.Filename}\"");

            // ASP.NET might still be reading from the stream when this function exists scope
            // So we can't use a "using" statement            
            var stream = await _s3StreamService.GetDownloadStreamAsync(doc.S3Key);
            return File(stream, contentType);
        }

        [HttpDelete("DeleteDocument/{documentId}")]
        public async Task<ActionResult> DeleteDocument(Guid documentId)
        {
            var username = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            await _documentService.DeleteDocument(documentId, "N/A");
            return Ok();
        }
    }
}