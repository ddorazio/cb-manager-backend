using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Configuration")]
    public class ConfigurationController : Controller
    {
        private readonly ILoanConfigurationService _configService;
        private readonly IMessageTemplateService _templateService;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ConfigurationController(ILoanConfigurationService configService, IMessageTemplateService templateService, IWebHostEnvironment hostingEnvironment)
        {
            _configService = configService;
            _templateService = templateService;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("GetLoanAmounts")]
        public async Task<IEnumerable<LoanAmount>> GetLoanAmounts()
        {
            return await _configService.GetLoanAmounts();
        }

        [HttpPost("CreateLoanAmount")]
        public async Task<LoanAmount> CreateLoanAmount([FromBody] LoanAmount data)
        {
            return await _configService.CreateLoanAmount(data);
        }

        [HttpDelete("DeleteLoanAmount/{loanAmountId}")]
        public async Task DeleteLoanAmount(int loanAmountId)
        {
            await _configService.DeleteLoanAmount(loanAmountId);
        }

        [HttpGet("GetLoanFrequencies")]
        public async Task<IEnumerable<LoanFrequencyModel>> GetLoanFrequencies()
        {
            return await _configService.GetLoanFrequencies();
        }

        [HttpGet("GetLoanConfigurations")]
        public async Task<IEnumerable<LoanConfigurationModel>> GetLoanConfigurations()
        {
            var storedConfigs = await _configService.GetLoanConfigurations();
            var frequencies = await _configService.GetLoanFrequencies();
            var amounts = await _configService.GetLoanAmounts();
            // The apps need to show all possible permutations but we only store the ones the client actually configured
            List<LoanConfigurationModel> models = new List<LoanConfigurationModel>();
            foreach (var amount in amounts)
            {
                foreach (var frequency in frequencies)
                {
                    var configs = storedConfigs.Where(
                        c => c.IdloanAmount == amount.Id && c.IdloanFrequency == frequency.Id);
                    if (configs.Any())
                    {
                        foreach (var config in configs)
                        {
                            models.Add(new LoanConfigurationModel
                            {
                                Id = config.Id,
                                LoanFrequencyCode = frequency.Code,
                                LoanAmount = amount.Amount,
                                TotalPayments = config.TotalPayments
                            });
                        }
                    }
                    else
                    {
                        // If there is no config for this amount/freq combination we create one
                        // So the user can fill it out
                        models.Add(new LoanConfigurationModel
                        {
                            LoanFrequencyCode = frequency.Code,
                            LoanAmount = amount.Amount
                        });
                    }
                }
            }
            return models;
        }

        [HttpPost("CreateLoanConfiguration")]
        public async Task<LoanConfigurationModel> CreateLoanConfiguration([FromBody] LoanConfigurationModel data)
        {
            return await _configService.CreateLoanConfiguration(data);
        }

        [HttpPut("UpdateLoanConfiguration")]
        public async Task<LoanConfigurationModel> UpdateLoanConfiguration([FromBody] LoanConfigurationModel data)
        {
            return await _configService.UpdateLoanConfiguration(data);
        }

        [HttpGet("GetLoanChargeConfigurations")]
        public async Task<IEnumerable<LoanChargeConfigurationModel>> GetLoanChargeConfigurations()
        {
            return await _configService.GetLoanChargeConfigurations();
        }

        [HttpPost("CreateLoanChargeConfiguration")]
        public async Task<LoanChargeConfigurationModel> CreateLoanChargeConfiguration([FromBody] LoanChargeConfigurationModel data)
        {
            return await _configService.CreateLoanChargeConfiguration(data);
        }

        [HttpPut("UpdateLoanChargeConfiguration")]
        public async Task<LoanChargeConfigurationModel> UpdateLoanChargeConfiguration([FromBody] LoanChargeConfigurationModel data)
        {
            return await _configService.UpdateLoanChargeConfiguration(data);
        }

        [HttpDelete("DeleteLoanChargeConfiguration/{loanChargeConfigurationId}")]
        public async Task DeleteLoanChargeConfiguration(int loanChargeConfigurationId)
        {
            await _configService.DeleteLoanChargeConfiguration(loanChargeConfigurationId);
        }

        [HttpPut("UpdateLoanFrequency")]
        public async Task<LoanFrequencyModel> UpdateLoanFrequency([FromBody] LoanFrequencyModel data)
        {
            return await _configService.UpdateLoanFrequency(data);
        }

        [HttpDelete("DeleteLoanConfiguration/{loanConfigurationId}")]
        public async Task DeleteLoanFrequency(int loanConfigurationId)
        {
            await _configService.DeleteLoanConfiguration(loanConfigurationId);
        }

        [HttpGet("GetMessageTemplateList")]
        public async Task<IEnumerable<MessageTemplateModel>> GetMessageTemplateList()
        {
            return await _templateService.GetMessageTemplateList();
        }

        [HttpGet("GetMessageTemplate/{messageTemplateId}")]
        public async Task<ActionResult> GetMessageTemplate(int messageTemplateId)
        {
            var result = await _templateService.GetMessageTemplate(messageTemplateId);
            if (result == null)
            {
                var response = new Error("TEMPLATE_NOT_FOUND", "No message template found with this identifier.");
                return NotFound(response);
            }
            return Ok(result);
        }

        [HttpPut("UpdateMessageTemplate")]
        public async Task<MessageTemplateModel> UpdateMessageTemplate([FromBody] MessageTemplateModel data)
        {
            return await _templateService.UpdateMessageTemplate(data);
        }

        [HttpGet("GetEmailConfiguration")]
        public async Task<EmailConfigurationModel> GetEmailConfiguration()
        {
            return await _templateService.GetEmailConfiguration();
        }

        [HttpPut("SaveEmailConfiguration")]
        public async Task<EmailConfigurationModel> SaveEmailConfiguration([FromBody] EmailConfigurationModel data)
        {
            await _templateService.SaveEmailConfiguration(data);
            return data;
        }

        [HttpPut("SaveLogo")]
        public async Task<ActionResult<string>> SaveLogo(IFormFile file)
        {
            var supportedExtensions = new string[] { ".png", ".jpg", ".jpeg", ".gif" };
            var ext = Path.GetExtension(file.FileName);
            if (!supportedExtensions.Contains(ext))
            {
                var response = new Error("UNSUPPORTED_FILE_TYPE", "Unsupported file type.");
                return BadRequest(response);
            }

            var filename = "logo" + ext;
            var filepath = Path.Combine(_hostingEnvironment.WebRootPath, filename);

            using var fs = new FileStream(filepath, FileMode.Create);
            await file.CopyToAsync(fs);

            return Ok($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/{filename}");
        }
    }
}