﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Domain.Core.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using CreditBook.LMS.API.Core.Identity.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Route("api/Notification")]
    public class NotificationController : Controller
    {

        private string _language { get; set; }

        private readonly INotificationService _notificationService;




        public NotificationController(INotificationService notificationService)
        {

            _notificationService = notificationService;

        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [Authorize]
        [HttpPost("NewNotification")]
        public async Task<IActionResult> NewNotification([FromBody] NotificationModel model)
        {
            await _notificationService.NewNotification(model);
            return Ok(new { status = "Success" });

        }

        [Authorize]
        [HttpGet("GetNotifications")]
        public async Task<IActionResult> GetNotifications(string type)
        {
            GetRequestHeaders();
            var data = await _notificationService.GetNotifications(type, _language);

            return Ok(new { status = "Success", data = data });
        }

        [Authorize]
        [HttpPut("setNotificationRead")]
        public async Task<IActionResult> setNotificationRead(int id)
        {
            GetRequestHeaders();
            await _notificationService.SetNotificationRead(id);
            return Ok(new { status = "Success" });
        }

        [Route("NotifyDailyRejections/{loanId}")]
        [HttpPut]
        public async Task<IActionResult> NotifyDailyRejections(int loanId)
        {
            try
            {
                var model = await _notificationService.CreateNotificationRejectModel(new LoanGenericPaymentModel() { LoanId = loanId });
                await _notificationService.NewNotification(model);
                return Ok(new { status = "Success", message = "Daily Rejection notification sent" });
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }

        [Route("NotifyContractSigned/{documentId}")]
        [HttpPut]
        public async Task<IActionResult> NotifyContractSigned(Guid documentId)
        {
            try
            {
                var model = await _notificationService.CreateNotificationContractSignedModel(documentId);
                await _notificationService.NewNotification(model);
                return Ok(new { status = "Success", message = "Contratc signed notification sent" });
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }
    }
}
