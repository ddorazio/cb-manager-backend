using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.Domain.Core.Models.CreditBookCheck;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Agreement")]
    public class PaymentAgreementController : Controller
    {
        private string _language { get; set; }
        //private string _connectionString { get; set; }

        private readonly IConfiguration _configuration;
        private readonly IPaymentAgreement _paymentAgreement;

        public PaymentAgreementController(IPaymentAgreement paymentAgreement, IConfiguration configuration)
        {
            _paymentAgreement = paymentAgreement;
            _configuration = configuration;
        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [HttpGet("List")]
        public async Task<IActionResult> InteracPaymentAgreements()
        {
            //GetRequestHeaders();
            var result = await _paymentAgreement.InteracPaymentAgreement();
            return Ok(result);
        }


        [HttpPut("Update")]
        public async Task<IActionResult> UpdateInteracPaymentAgreements([FromBody] InteracPaymentAgreementModel model)
        {
            //GetRequestHeaders();
            var result = await _paymentAgreement.UpdateAgreement(model);
            return Ok(result);
        }
    }
}
