using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.External.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/LenderConfiguration")]
    public class LenderConfigurationontroller : Controller
    {
        private readonly IConfigurationService _configurationService;

        public LenderConfigurationontroller(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        [HttpGet("GetLenderLoanConfiguration")]
        public async Task<LenderLoanConfigurationModel> GetLenderLoanConfiguration()
        {
            return await _configurationService.LoadLenderLoanConfiguration();
        }
    }
}