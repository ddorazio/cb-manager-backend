using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.API;
using CreditBook.LMS.Domain.Core.Models.CreditBookCheck;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Client")]
    public class ClientController : Controller
    {
        private string _language { get; set; }
        //private string _connectionString { get; set; }

        private readonly IConfiguration _configuration;
        private readonly IClientService _clientService;
        private readonly IClientHistoryService _historyService;
        private readonly IHttpClientFactory _clientFactory;

        public ClientController(IClientService clientService, IClientHistoryService historyService, IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientService = clientService;
            _historyService = historyService;
            _clientFactory = clientFactory;
            _configuration = configuration;
        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [HttpGet("NewClientData")]
        public IActionResult NewClientData()
        {
            GetRequestHeaders();
            var result = _clientService.NewClientData(_language);
            return Ok(result);
        }

        [HttpGet("List")]
        public IActionResult GetClientSummaryList()
        {
            GetRequestHeaders();
            var result = _clientService.GetClientSummaryList(_language);
            return Ok(result);
        }

        [HttpPost("Save")]
        public async Task<IActionResult> CreateNewClient([FromBody] ClientModel data)
        {
            var client = _clientService.SaveNewClient(data);

            // Add history entry
            await _historyService.AddClientHistoryEntry(client.Id, ClientHistoryTypeEnum.ClientCreated);

            return Ok(client);
        }

        [HttpGet("{clientId}")]
        public async Task<IActionResult> GetClient(int clientId)
        {
            GetRequestHeaders();
            var result = await _clientService.GetClient(clientId, _language, _configuration["Contract:DigitalBaseURL"]);
            if (result == null)
            {
                var response = new Error("USER_NOT_FOUND", "Found no user with this identifier.");
                return NotFound(response);
            }
            return Ok(result);
        }

        [HttpGet("{clientId}/References")]
        public async Task<ActionResult<IEnumerable<ClientReferenceModel>>> GetReferences(int clientId)
        {
            var result = await _clientService.GetClientReferences(clientId);
            return Ok(result);
        }

        [HttpPost("UpdateReference")]
        public async Task<ActionResult<ClientReferenceModel>> UpdateReference([FromBody] ClientReferenceModel reference)
        {
            var result = await _clientService.UpdateClientReference(reference);
            return Ok(result);
        }

        [HttpPost("CreateReference")]
        public async Task<ActionResult<ClientReferenceModel>> CreateReference([FromBody] ClientReferenceModel reference)
        {
            var result = await _clientService.CreateClientReference(reference);
            return Ok(result);
        }

        [HttpDelete("DeleteReference/{refId}")]
        public async Task<ActionResult> DeleteReference(int refId)
        {
            await _clientService.DeleteClientReference(refId);
            return Ok();
        }

        [HttpPost("Update")]
        public async Task<ActionResult<ClientModel>> UpdateClient([FromBody] ClientModel data)
        {
            var result = await _clientService.UpdateClient(data);

            return Ok(result);
        }

        [HttpPost("Delete/{id}")]
        public ActionResult DeleteClient(int id)
        {
            try
            {
                if (_clientService.DeleteClient(id))
                {
                    return Ok(new { status = "Success", message = "Client deleted" });
                }

                return Ok(new { status = "Failed", message = "Client not deleted" });

            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("GetClientLoanNumbers/{clientId}")]
        public async Task<IEnumerable<dynamic>> GetClientLoanNumbers(int clientId)
        {
            var result = await _clientService.GetClientLoans(clientId);
            return result.Select(cl => new
            {
                Id = cl.Id,
                LoanNumber = cl.LoanNumber
            });
        }

        [HttpGet("GetClientHistory/{clientId}")]
        public async Task<IEnumerable<ClientHistoryModel>> GetClientHistory(int clientId)
        {
            return await _historyService.GetClientHistory(clientId);
        }

        [HttpPost("SearchCreditBookCheck")]
        public async Task<IActionResult> SearchCreditBookCheck([FromBody] SearchModel model)
        {
            GetRequestHeaders();

            var queryString = "?firstName=" + model.firstName
                                + "&lastName=" + model.lastName
                                + "&email=" + model.email
                                + "&phone=" + model.phone
                                + "&accuracy=2";

            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(_configuration["CreditBookCheck:Endpoint"]);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("API-KEY", _configuration["CreditBookCheck:APIKey"]);

            var response = await httpClient.GetAsync("search/loanrequests/" + queryString);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                var responseData = JsonConvert.DeserializeObject<ResponseSearchModel>(responseStream);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var data = await _clientService.FormatCreditBookSearchResults(responseData, _language);

                    return Ok(new { status = "Success", data = data });
                }
            }

            return BadRequest();
        }

        [HttpPost("MergeClientProfile/{oldClientId}/{newClientId}")]
        public async Task<ActionResult> MergeClientProfile(int oldClientId, int newClientId)
        {
            var result = await _clientService.MergeClientProfile(oldClientId, newClientId);
            return Ok(result);
        }
    }
}
