﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/Contract")]
    public class ContractController : Controller
    {
        private string _language { get; set; }
        private readonly IContractService _contractService;
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;
        private readonly IMessageService _messageService;

        public ContractController(IContractService contractService, IWebHostEnvironment environment, IConfiguration configuration, IMessageService messageService)
        {
            _contractService = contractService;
            _environment = environment;
            _configuration = configuration;
            _messageService = messageService;
        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [Route("GenerateLoanContract")]
        [HttpPost]
        public async Task<IActionResult> GenerateLoanContract([FromBody] ContractModel model)
        {
            if (await _contractService.VerifyContractSigned(_configuration["Contract:Folder"], _configuration["Contract:DigitalBaseURL"], model.LoanId, model.EmailAddress))
            {
                var contract = await _contractService.GenerateLoanContract(_configuration["Contract:Folder"], _configuration["Contract:BaseURL"], _configuration["Contract:DigitalBaseURL"], _environment.WebRootPath, model.LoanId);
                contract.EmailAddress = model.EmailAddress;

                //Send Contract by Email
                await _messageService.SendMessage_Contract(new EmailTemplateModel()
                {
                    Contract = contract
                });
            }
            return Ok();
        }
    }
}
