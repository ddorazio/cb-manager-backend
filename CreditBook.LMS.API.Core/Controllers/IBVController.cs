﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Common.Utilities;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Route("api/IBV")]
    public class IBVController : Controller
    {
        private string _language { get; set; }
        private readonly IClientService _clientService;
        private readonly IHttpClientFactory _clientFactory;
        private readonly IConfiguration _configuration;
        private readonly IMessageService _messageService;
        private readonly INotificationService _notificationService;

        public IBVController(IClientService clientService, IHttpClientFactory clientFactory, IConfiguration configuration, IMessageService messageService, INotificationService notificationService)
        {
            _clientService = clientService;
            _clientFactory = clientFactory;
            _configuration = configuration;
            _messageService = messageService;
            _notificationService = notificationService;
        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [Authorize]
        [Route("NewIBVRequest")]
        [HttpPost]
        public async Task<IActionResult> NewIBVRequest([FromBody] IBVModel model)
        {
            try
            {
                GetRequestHeaders();

                //var apiKey = HttpContext.Request.Headers["APIKey"];
                var apiKey = _configuration["IBV:APIKey"];

                var request = new HttpRequestMessage(HttpMethod.Post, _configuration["IBV:Endpoint"] + "CreateRequest");
                request.Headers.Add("APIKey", apiKey);

                var client = _clientFactory.CreateClient();

                //
                model.LMSReturnURL = _configuration["IBV:LMSReturnURL"];
                model.PhoneNumber = model.PhoneNumber.Replace("(", "")
                                                        .Replace(")", "")
                                                        .Replace("-", "")
                                                        .Replace(" ", "");

                var content = model;

                var json = JsonConvert.SerializeObject(content);

                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    var response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseStream = await response.Content.ReadAsStringAsync();
                        var responseData = JsonConvert.DeserializeObject<IBVReturnModel>(responseStream);

                        // Save to IBV table
                        var returnModel = _clientService.SaveNewIBV(model.ClientId, responseData.data.requestID, responseData.data.url, _language);
                        var clientModel = await _clientService.GetClientInfo(model.ClientId); //_clientService.GetClient(model.ClientId, _language);

                        if (returnModel != null)
                        {
                            List<string> emailList = new List<string>();
                            emailList.Add(model.Email);

                            // Send IBV email to User
                            var emailLanguage = clientModel.BasicInfo.PersonalInfo.LanguageCode == "E" ? LanguageEnum.English : LanguageEnum.French;

                            // TEMP SOLUTION
                            var ibvHtmlUrl = clientModel.BasicInfo.PersonalInfo.LanguageCode == "E"
                                                    ? "<a href='" + responseData.data.url + "'>IBV</a>"
                                                    : "<a href='" + responseData.data.url + "'>IBV</a>";

                            await _messageService.SendMessage_IbvApplication(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                            {
                                IBV = new Domain.Core.Models.EmailTemplates.IBVMessageModel()
                                {
                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    EmailAddress = model.Email,
                                    CellPhone = model.PhoneNumber,
                                    IBVHtmlURL = ibvHtmlUrl,
                                    IBVUrl = responseData.data.url
                                }
                            });

                            //await EmailManager.SendIBVConfirmation(responseData.data.url, "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", emailList);
                            return Ok(new { status = "Success", message = "IBV requested", data = returnModel });
                        }

                        return Ok(new { status = "Failed", message = "IBV requested failed" });
                    }
                }

                return Ok(new { status = "Failed", message = "IBV requested failed" });
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }

        [Authorize]
        [HttpPost("Resend/{ibvId}")]
        public async Task ResendIbvConfirmation(int ibvId)
        {
            var ibv = await _clientService.GetIBV(ibvId);
            if (ibv != null && ibv.IsCompleted == false)
            {
                var emailLanguage = ibv.LanguageCode == "E" ? LanguageEnum.English : LanguageEnum.French;
                await _messageService.SendMessage_IbvApplication(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                {
                    IBV = new Domain.Core.Models.EmailTemplates.IBVMessageModel()
                    {
                        FirstName = ibv.FirstName,
                        LastName = ibv.LastName,
                        EmailAddress = ibv.Email,
                        CellPhone = ibv.PhoneNumber,
                        IBVHtmlURL = $"<a href='{ibv.Url.Replace("Report", "IBV")}'>IBV</a>",
                        IBVUrl = ibv.Url
                    }
                });
            }
        }

        [HttpGet("ResendAll")]
        public async Task ResendAllIbvConfirmation()
        {
            var listIBV = await _clientService.GetAllIBV();
            foreach (var ibv in listIBV)
            {
                if (ibv.Url != null)
                //if (ibv != null && ibv.IsCompleted == false && ibv.Url != null)
                {
                    var emailLanguage = ibv.LanguageCode == "E" ? LanguageEnum.English : LanguageEnum.French;
                    await _messageService.SendMessage_IbvReminder(emailLanguage, new Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                    {
                        IBVReminder = new Domain.Core.Models.EmailTemplates.IBVReminderModel()
                        {
                            IBVHtmlURL = $"<a href='{ibv.Url.Replace("Report", "IBV")}'>IBV</a>",
                            IBVUrl = ibv.Url,
                            EmailAddress = ibv.Email,
                            CellPhone = ibv.PhoneNumber
                        }
                    });
                }
            }
        }

        #region External

        [Route("UpdateIBVStatusStart/{requestId}")]
        [HttpPut]
        public async Task<IActionResult> UpdateIBVStatusStart(string requestId)
        {
            try
            {
                if (await _clientService.UpdateIBVStatusStart(requestId))
                {
                    return Ok(new { status = "Success", message = "IBV status updated" });
                }

                return BadRequest(new { status = "Failed", message = "IBV status not updated" });
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }

        [Route("UpdateIBVStatusCompleted/{requestId}")]
        [HttpPut]
        public async Task<IActionResult> UpdateIBVStatusCompleted(string requestId)
        {
            try
            {
                if (await _clientService.UpdateIBVStatusCompleted(requestId))
                {
                    var model = await _notificationService.CreateNotificationIBVModel(requestId);
                    await _notificationService.NewNotification(model);
                    return Ok(new { status = "Success", message = "IBV status updated" });
                }

                return Ok(new { status = "Failed", message = "IBV status not updated" });
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }

        #endregion

    }
}
