using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditBook.LMS.API.Core.Controllers
{
    [Authorize]
    [Route("api/LoanRequest")]
    public class LoanRequestController : Controller
    {
        private readonly ILoanRequestService _loanRequestService;
        private readonly IDataHubService _dataHubService;
        private string _language { get; set; }

        public LoanRequestController(ILoanRequestService loanRequestService, IDataHubService dataHubService)
        {
            _loanRequestService = loanRequestService;
            _dataHubService = dataHubService;
        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [HttpGet("List")]
        public async Task<IEnumerable<LoanRequestModel>> GetLoanRequestList(int? clientId = null, string statusId = null)
        {
            GetRequestHeaders();
            return await _loanRequestService.GetLoanRequestList(clientId, statusId);
        }


        [HttpGet("ListProfile")]
        public async Task<IEnumerable<LoanRequestModel>> GetLoanRequestClientList(int clientId)
        {
            GetRequestHeaders();
            var language = _language == "EN" ? LanguageEnum.English : LanguageEnum.French;
            return await _loanRequestService.GetLoanRequestClientList(clientId, language);
        }

        [HttpGet("{requestId}")]
        public async Task<LoanRequestModel> GetLoanRequest(int requestId)
        {
            return await _loanRequestService.GetLoanRequest(requestId);
        }

        [HttpPut("Update")]
        public async Task<LoanRequestModel> UpdateLoanRequest([FromBody]LoanRequestModel model)
        {
            var result = await _loanRequestService.UpdateLoanRequest(model);

            //Moved to LoanRequestService to validate when to send update
            //await _dataHubService.UpdateLoanRequestStatus(result);
            return result;
        }

        [HttpPut("UpdateChecklist")]
        public async Task<LoanRequestModel> UpdateRequestChecklist([FromBody]LoanRequestModel model)
        {
            var result = await _loanRequestService.UpdateRequestChecklist(model);
            return result;
        }
    }
}