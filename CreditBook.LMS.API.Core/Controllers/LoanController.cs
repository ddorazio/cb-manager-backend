﻿using System;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Core;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CreditBook.LMS.API.Core.Controllers
{
    //[Authorize]
    [Route("api/Loan")]
    public class LoanController : Controller
    {
        private string _language { get; set; }

        private readonly ILoanService _loanService;
        private readonly IConfiguration _configuration;
        private readonly IMessageService _messageService;
        private readonly INotificationService _notificationService;

        public LoanController(ILoanService loanService, IConfiguration configuration, IMessageService messageService, INotificationService notificationService)
        {
            _loanService = loanService;
            _configuration = configuration;
            _messageService = messageService;
            _notificationService = notificationService;

        }

        private void GetRequestHeaders()
        {
            if (Request.Headers["Language"].Count != 0)
            {
                _language = Request.Headers["Language"][0].ToUpper();
            }
        }

        [HttpPost("NewLoan")]
        public ActionResult NewLoan([FromBody] NewLoanModel model)
        {
            GetRequestHeaders();
            var data = _loanService.NewLoan(model, _language);
            return Ok(new { status = "Success", message = "Loan created", data = data });
        }

        [HttpPost("LoanManualNSFPayment/")]
        public ActionResult LoanManualNSFPayment([FromBody] LoanGenericPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanManualNSFPayment(model, _language, false);
            //NotificationModel modelNotif = await _notificationService.CreateNotificationRejectModel(model);
            //await _notificationService.NewNotification(modelNotif);
            return Ok(new { status = "Success", message = "NSF added", data = data });
        }

        [HttpPost("LoanRemoveManualNSFPayment")]
        public ActionResult LoanRemoveManualNSFPayment([FromBody] LoanGenericPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanRemoveManualNSFPayment(model, _language);
            return Ok(new { status = "Success", message = "NSF removed", data = data });
        }

        [HttpPost("LoanAddRebate/")]
        public ActionResult LoanAddRebate([FromBody] LoanRebateModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanAddRebate(model, _language);
            return Ok(new { status = "Success", message = "Rebate added", data = data });
        }

        [HttpPost("LoanManualPayment/")]
        public ActionResult LoanManualPayment([FromBody] LoanPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanManualPayment(model, _language);
            return Ok(new { status = "Success", message = "Manual payment added", data = data });
        }

        [HttpPost("LoanRemoveRebateAndManualPayment/")]
        public ActionResult LoanRemoveRebateAndManualPayment([FromBody] LoanGenericPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanRemoveRebateAndManualPayment(model, _language);
            return Ok(new { status = "Success", message = "Rebate removed", data = data });
        }

        [HttpPost("LoanModifyPayment")]
        public ActionResult LoanModifyPayment([FromBody] LoanModifyPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.LoanModifyPayment(model, true, _language);
            return Ok(new { status = "Success", message = "Loan payment modified", data = data });
        }


        [HttpPost("CalculateTotalAmountRemaing")]
        public ActionResult CalculateTotalAmountRemaing([FromBody] LoanModifyPaymentModel model)
        {
            GetRequestHeaders();
            var data = _loanService.CalculateTotalAmountRemaining(model, _language);
            return Ok(new { status = "Success", message = "Loan modified", data = data });
        }

        [HttpPost("ModifyLoan")]
        public ActionResult ModifyLoan([FromBody] LoanModifyModel model)
        {
            GetRequestHeaders();
            var data = _loanService.ModifyLoan(model, _language);
            return Ok(new { status = "Success", message = "Loan modified", data = data });
        }

        [HttpPost("RestartLoan")]
        public ActionResult RestartLoan([FromBody] LoanRestartModel model)
        {
            GetRequestHeaders();
            var data = _loanService.RestartLoan(model, _language);
            return Ok(new { status = "Success", message = "Loan restarted", data = data });
        }

        [HttpPost("DeferPayment")]
        public async Task<ActionResult> DeferPayment([FromBody] LoanDelayPaymentModel model)
        {
            GetRequestHeaders();
            var data = await _loanService.DeferPayment(model, _language);
            return Ok(new { status = "Success", message = "Payment delayed", data = data });
        }

        [HttpDelete("DeleteLoan/{loanId}")]
        public async Task<ActionResult> DeleteLoan(int loanId)
        {
            var data = await _loanService.DeleteLoan(loanId, _language);
            return Ok(new { status = "Success", message = "Loan delete", data = data });
        }

        [HttpGet("GetAccountBalance/{loanId}")]
        public async Task<ActionResult> GetAccountBalance(int loanId)
        {
            GetRequestHeaders();
            var data = await _loanService.GetAccountBalance(loanId, _language);
            return Ok(new { status = "Success", data = data, message = "Account Balance" });
        }

        [HttpPost("SendLoanPDF")]
        public async Task<ActionResult> SendLoanPDF([FromBody] PDFEmailModel model)
        {
            GetRequestHeaders();
            var email = _loanService.SendLoanPDF(model, _configuration["LoanPDF:Folder"], _configuration["LoanPDF:Serial_no"]);
            await _messageService.SendMessage_Loan(email);
            return Ok(new { status = "Success", message = "PDF sent" });
        }

        [HttpPost("StoppedLoanPayment")]
        public async Task<ActionResult> StoppedLoanPayment([FromBody] StoppedLoanPaymentModel model)
        {
            GetRequestHeaders();
            var data = await _loanService.StoppedLoanPayment(model, _language);
            return Ok(new { status = "Success", message = "Payment added", data = data });
        }

        // This is not used but commented in case client changes their mind
        // [HttpPost("UpdateLoanStatus/{loanId}")]
        // public ActionResult UpdateLoanStatus(int loanId)
        // {
        //     GetRequestHeaders();
        //     var data = _loanService.UpdateLoanStatus(loanId, _language);
        //     return Ok(new { status = "Success", message = "Loan status updated", data = data });
        // }
    }
}
