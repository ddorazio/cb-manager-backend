﻿using System;
namespace CreditBook.LMS.API.Core.Identity.Models
{
    public class CreateUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool IsEnabled { get; set; }
    }
}
