﻿using System;
using Microsoft.AspNetCore.Identity;

namespace CreditBook.LMS.API.Core.Identity.Models
{
    public class AppUser : IdentityUser
    {
        public bool IsEnabled { get; set; }
    }
}
