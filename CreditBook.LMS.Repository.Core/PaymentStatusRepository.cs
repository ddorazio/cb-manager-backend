﻿using System;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core
{
    public class PaymentStatusRepository : Base.BaseRepository<PaymentStatus>
    {
        public PaymentStatusRepository(CreditBook_LMS_DEVContext dataContext) : base(dataContext)
        {
        }
    }
}
