﻿using System;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core
{
    public class ClientLoanPaymentRepository : Base.BaseRepository<ClientLoanPayment>
    {
        public ClientLoanPaymentRepository(CreditBook_LMS_DEVContext dataContext) : base(dataContext)
        {
        }
    }
}
