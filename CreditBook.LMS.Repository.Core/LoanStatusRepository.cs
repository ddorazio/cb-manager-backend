﻿using System;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core
{
    public class LoanStatusRepository : Base.BaseRepository<LoanStatus>
    {
        public LoanStatusRepository(CreditBook_LMS_DEVContext dataContext) : base(dataContext)
        {
        }
    }
}
