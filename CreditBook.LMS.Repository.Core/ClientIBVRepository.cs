﻿using System;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core
{

    public class ClientIBVRepository : Base.BaseRepository<ClientIbv>
    {
        public ClientIBVRepository(CreditBook_LMS_DEVContext dataContext) : base(dataContext)
        {
        }

    }

}
