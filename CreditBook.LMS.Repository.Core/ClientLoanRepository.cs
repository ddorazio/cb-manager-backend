﻿using System;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core
{
    public class ClientLoanRepository : Base.BaseRepository<ClientLoan>
    {
        public ClientLoanRepository(CreditBook_LMS_DEVContext dataContext) : base(dataContext)
        {
        }
    }
}
