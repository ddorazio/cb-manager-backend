﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CreditBook.LMS.Data.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Repository.Core.Base
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        #region
        private readonly DbContext _dataContext;
        #endregion

        public BaseRepository(CreditBook_LMS_DEVContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<T> GetAll()
        {
            return _dataContext.Set<T>();
        }

        public IQueryable<T> Search(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return _dataContext.Set<T>().Where(predicate);
        }

        public void Add(T entity)
        {
            _dataContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
        }

        public void Edit(T entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public IQueryable<T> SearchCopy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return _dataContext.Set<T>().Where(predicate).AsNoTracking();
        }

        public void ExecuteQuery(string query, int? ID)
        {
            if (ID != null)
            {
                _dataContext.Database.ExecuteSqlCommand(query + ID.ToString());
            }
            else
            {
                _dataContext.Database.ExecuteSqlCommand(query);
            }
        }
    }
}
