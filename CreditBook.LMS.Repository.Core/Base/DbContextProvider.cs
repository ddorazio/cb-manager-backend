using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Repository.Core.Base
{
    public class DbContextProvider : IDbContextProvider
    {
        private string _connectionString { get; set; }

        public DbContextProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CreditBook_LMS_DEVContext GetContext() 
            => new CreditBook_LMS_DEVContext(_connectionString);
    }
}