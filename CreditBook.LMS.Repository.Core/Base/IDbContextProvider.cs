using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Repository.Core.Custom;

namespace CreditBook.LMS.Repository.Core.Base
{
    public interface IDbContextProvider
    {
        CreditBook_LMS_DEVContext GetContext();
    }
}