using System;
using CreditBook.LMS.Data.Core.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace CreditBook.LMS.Repository.Core.Base
{
    // Transaction scope object
    public class UnitOfWork : IDisposable
    {
        private bool _finished = false;
        public readonly CreditBook_LMS_DEVContext _context;
        private IDbContextTransaction _trx;

        public UnitOfWork(IDbContextProvider dbContextProvider)
        {
            _context = dbContextProvider.GetContext();
        }

        public IRepository<U> StartFor<U>() where U : class
        {
            _trx = _context.Database.BeginTransaction();
            _finished = false;

            IRepository<U> repo = null;

            // This would be better with pattern matching
            if (typeof(U) == typeof(Client))
                repo = new ClientRepository(_context) as IRepository<U>;

            if (typeof(U) == typeof(ClientIbv))
                repo = new ClientIBVRepository(_context) as IRepository<U>;

            return repo;
        }

        public void Rollback()
        {
            _trx.Rollback();
            _finished = true;
        }

        public void Finish()
        {
            _context.SaveChanges();
            _trx.Commit();
            _finished = true;
        }

        public void Dispose()
        {
            if (!_finished) Finish();
        }

        #region Independant Functions

        public void StartTransaction()
        {
            _trx = _context.Database.BeginTransaction();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Commit()
        {
            _trx.Commit();
        }

        #endregion

        #region Repositories

        ClientLoanRepository repClientLoan = null;

        public ClientLoanRepository UClientLoanRepository
        {
            get
            {
                if (repClientLoan == null)
                {
                    repClientLoan = new ClientLoanRepository(_context);
                }

                return repClientLoan;
            }
        }

        ClientLoanPaymentRepository repClientLoanPayment = null;

        public ClientLoanPaymentRepository UClientLoanPaymentRepository
        {
            get
            {
                if (repClientLoanPayment == null)
                {
                    repClientLoanPayment = new ClientLoanPaymentRepository(_context);
                }

                return repClientLoanPayment;
            }
        }

        PaymentStatusRepository repPaymentStatus = null;

        public PaymentStatusRepository UPaymentStatusRepository
        {
            get
            {
                if (repPaymentStatus == null)
                {
                    repPaymentStatus = new PaymentStatusRepository(_context);
                }

                return repPaymentStatus;
            }
        }

        LoanStatusRepository repLoanStatus = null;

        public LoanStatusRepository ULoanStatusRepository
        {
            get
            {
                if (repLoanStatus == null)
                {
                    repLoanStatus = new LoanStatusRepository(_context);
                }

                return repLoanStatus;
            }
        }

        #endregion



    }
}