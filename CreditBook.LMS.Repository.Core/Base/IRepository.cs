﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace CreditBook.LMS.Repository.Core.Base
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> Search(Expression<Func<T, bool>> predicate);
        IQueryable<T> SearchCopy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
        //void Save();
        void ExecuteQuery(string query, int? ID);
    }
}
