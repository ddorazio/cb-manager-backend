﻿using System.Collections.Generic;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Repository.Core.Base;

namespace CreditBook.LMS.Repository.Core.Custom
{
    public interface IClientRepository : IRepository<Client>
    {
        List<Client> GetOverDueInvoice();
    }
}
