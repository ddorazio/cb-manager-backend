namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum MessageTemplateEnum
    {
        ApplicationWithoutDocuments = 0,
        IbvReminder = 1,
        References = 2,
        LoanContract = 3,
        ContractNotReceived = 4,
        IbvApplication = 27,
        LoanApplicationDenied = 14,
        InteracTransfer = 15,
        BankDebit = 16,

        Level1Collection = 17,
        Level2Collection = 18,
        Level3Collection = 19,
        CollectionAccountClosed = 20,

        LoanApplicationCancelled = 21,
        PaymentAgreement = 22,
        LoanRenewal = 23,
        InteracCollectionReminder = 24,
        ApplicationWithDocuments = 25,
        LoanAccountBalance = 28,
        LoanAccountDetail = 30
    }
}