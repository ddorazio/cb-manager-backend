namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum NonPaymentReasonEnum
    {
        LostJobTemporary = 0,
        LostJob = 1,
        WorkStoppage = 2,
        Forgot = 3,
        Other = 4
    }
}