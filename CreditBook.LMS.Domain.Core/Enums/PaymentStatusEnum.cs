namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum PaymentStatusEnum
    {
        Pending = 1,
        Nsf,
        Paid,
        ManualPayment,
        Rebate,
        StopPayment,
        PaymentChanged,
        PaymentRestarted
    }
}