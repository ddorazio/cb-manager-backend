namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum CollectionDegreeEnum
    {
        FirstNsf,
        SecondNsfConsecutive
    }
}