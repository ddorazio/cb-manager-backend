namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum LanguageEnum
    {
        English = 0,
        French = 1
    }
}