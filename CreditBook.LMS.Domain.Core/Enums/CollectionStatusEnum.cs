namespace CreditBook.LMS.Domain.Core.Enums
{
    public enum CollectionStatusEnum
    {
        RejectionsOfTheDay = 0,
        NeedFollowUp = 1,
        InteracETransfer = 2,
        WaitingForResponse = 3,
        Settled = 4
    }
}