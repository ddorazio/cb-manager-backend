namespace CreditBook.LMS.Service.Domain.Enums
{
    public enum ClientHistoryTypeEnum
    {
        ClientCreated = 0,
        NewLoanRequest = 1
    }
}