namespace CreditBook.LMS.Domain.Core
{
    public class ActiveCollectionModel
    {
        public int LoanId { get; set; }

        public int LoanNumber { get; set; }

        public string LoanStatusCode { get; set; }

        public int CollectionId { get; set; }
    }
}