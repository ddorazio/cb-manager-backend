using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class ClientHistoryModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public DateTime DateTime { get; set; }

        public string Code { get; set; }

        public string DescriptionEn { get; set; }

        public string DescriptionFr { get; set; }
    }
}