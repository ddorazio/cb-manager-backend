﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanModifyModel
    {
        public int LoanId { get; set; }
        public int NewFrequencyId { get; set; }
        public decimal NewPaymentAmount { get; set; }
        public DateTime NewPaymentFrequencyDate { get; set; }

        public int StartDay { get; set; }
        public int EndDay { get; set; }

        public bool IsLoanStop { get; set; }
        public DateTime StopPaymentDate { get; set; }
        public bool PaymentAgreement { get; set; }
        public bool Perceptech { get; set; }
        public bool Interac { get; set; }
        public int SelectedPaymentId { get; set; }
    }
}
