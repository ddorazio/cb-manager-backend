﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models.External
{
    public class CreditBookCheckNewLoanModel
    {
        public string requestId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string dateOfBirth { get; set; }
        public CreditBookCheckAddress address { get; set; }

        public string phone1 { get; set; }
        public string email { get; set; }

        public CreditBookCheckEmployment job { get; set; }
        public List<CreditBookCheckReference> references { get; set; }

        public decimal amount { get; set; }
        public string requestDate { get; set; }
        public string ipAddress { get; set; }
        public string language { get; set; }
        public string status { get; set; }
        //public string comments { get; set; }
    }

    public class CreditBookCheckAddress
    {
        public string streetAddressLine1 { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string stateProvince { get; set; }
        public string zipPostalCode { get; set; }
    }

    public class CreditBookCheckEmployment
    {
        public string companyName { get; set; }
        public string companyPhone { get; set; }
        public string supervisorName { get; set; }
        public string jobTitle { get; set; }
        public string payFrequency { get; set; }
        public string payNext { get; set; }
        public string startDate { get; set; }
    }

    public class CreditBookCheckReference
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string relationship { get; set; }
    }

    public class CreditBookCheckNewLoanResponseModel
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
