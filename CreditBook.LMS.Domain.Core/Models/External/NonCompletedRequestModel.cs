﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.External
{
    public class NonCompletedRequestModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Language { get; set; }
        public string LanguageCode { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }

        public string GoogleId { get; set; }
        public string GoogleEmail { get; set; }
        public string GooglePhoto { get; set; }
        public string GoogleFirstName { get; set; }
        public string GoogleLastName { get; set; }

        public string FacebookId { get; set; }
        public string FacebookPhoto { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookFirstName { get; set; }
        public string FacebookLastName { get; set; }
    }
}
