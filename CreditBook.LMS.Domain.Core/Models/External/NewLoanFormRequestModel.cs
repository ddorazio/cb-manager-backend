﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.External
{
    public class NewLoanFormRequestModel
    {
        public string Amount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Language { get; set; }
        public string LanguageCode { get; set; }
        //public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PrimaryEmail { get; set; }
        //public string SecondaryEmail { get; set; }

        public string CivicNumber { get; set; }
        public string StreetName { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string AptNo { get; set; }
        public DateTime ResidenceStartDate { get; set; }
        public string PostalCode { get; set; }

        public string Ref1FirstName { get; set; }
        public string Ref1LastName { get; set; }
        public string Ref1Telephone { get; set; }
        public string Ref1Email { get; set; }
        public string Ref1Relationship { get; set; }

        public string Ref2FirstName { get; set; }
        public string Ref2LastName { get; set; }
        public string Ref2Telephone { get; set; }
        public string Ref2Email { get; set; }
        public string Ref2Relationship { get; set; }

        public string Bankrupcy { get; set; }

        public bool? IsRenter { get; set; }
        public Int32 MonthlyPaymenet { get; set; }
        public Int32 MonthlyElectricalPayment { get; set; }
        public Int32 MonthlyCarPayment { get; set; }
        public Int32 MontlyAppliancePayment { get; set; }
        public Int32 GrossAnnualSalary { get; set; }

        public string PayFrequencyCode { get; set; }
        //public string PayFrequency { get; set; }
        public string IncomeTypeCode { get; set; }
        public string EmployerName { get; set; }
        public string EmployerPhoneNumber { get; set; }
        public string EmployerExtension { get; set; }
        public string SupervisorName { get; set; }
        public string Occupation { get; set; }
        public DateTime? NextPayDate { get; set; }
        public DateTime? HiringDate { get; set; }

        public DateTime? EmploymentInsuranceStartDate { get; set; }
        public DateTime? EmploymentInsuranceNextPayDate { get; set; }
        public bool DirectDeposit { get; set; }
        public DateTime? SelfEmployedStartDate { get; set; }
        public string SelfEmployedWorkTelephone { get; set; }
        public DateTime? NextDepositDate { get; set; }
        public bool LoanWithoutDocuments { get; set; }

        public string IpAddress { get; set; }
        public bool CreditBookCheckSuccessfull { get; set; }
        public string CreditBookCheckRequestId { get; set; }

        public string IBVRequestId { get; set; }
        public string IBVUrl { get; set; }
        public string IBVReportUrl { get; set; }

        public bool AutoDecline { get; set; }
        public string ReferenceNo { get; set; }

        public string GoogleId { get; set; }
        public string GoogleEmail { get; set; }
        public string GooglePhoto { get; set; }
        public string GoogleFirstName { get; set; }
        public string GoogleLastName { get; set; }

        public bool DeclaringBankrupcy { get; set; }

        public string FacebookId { get; set; }
        public string FacebookPhoto { get; set; }
        public string FacebookEmail { get; set; }
        public string FacebookFirstName { get; set; }
        public string FacebookLastName { get; set; }
    }

    public class NewLoanRequestMessageModel
    {

    }
}
