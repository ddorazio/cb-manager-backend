﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.External
{
    public class NewIBVRequestModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string IPAddress { get; set; }
        public string BrowserName { get; set; }
        public string ValidationURL { get; set; }
        public string LMSReturnURL { get; set; }
    }
}
