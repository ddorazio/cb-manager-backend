﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.External
{
    public class NewIBVRequestResponseModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public NewIBVRequestResponseDataModel data { get; set; }
    }

    public class NewIBVRequestResponseDataModel
    {
        public string url { get; set; }
        public string requestID { get; set; }
    }
}
