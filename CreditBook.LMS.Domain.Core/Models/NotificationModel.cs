﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class NotificationModel
    {
        public int? Id { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public DateTime DateCreated { get; set; }
        public string DateCreatedFormat { get; set; }
        public DateTime? DateRead { get; set; }
        public string? DateReadFormat { get; set; }
        public bool? Read { get; set; }
        public string Path { get; set; }
        public int? IdClient { get; set; }
    }

    public class ListNotificationModel
    {
        public List<NotificationModel> ListNotifs { get; set; }
        public string Type { get; set; }
    }

}
