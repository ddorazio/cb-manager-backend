﻿using CreditBook.LMS.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanPDFModel
    {
        public int? LoanId { get; set; }
        public int? IdClient { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string? DateToday { get; set; }
        public int? NumberOfPayments { get; set; }
        public int? DefaultPayments { get; set; }
        public decimal? Fees { get; set; }
        public decimal? BrokerageFees { get; set; }
        public decimal? OriginalPaymentAmount { get; set; }
        public decimal? TotalAmountRemaining { get; set; }
        public decimal? CurrentDailyBalance { get; set; }
        public decimal? Rebates { get; set; }
        public decimal? TotalInterest { get; set; }
        public int? FrequencyDuration { get; set; }
        public int? FrequencyId { get; set; }
        public string? FrequencyValue { get; set; }
        public decimal? InterestRate { get; set; }
        public bool? IsPersonalized { get; set; }
        public string LogoPath { get; set; }
        public decimal? SubFees { get; set; }
        public decimal? OriginalSubFeesPrice { get; set; }
        public bool ChargeMembershipFee { get; set; }
        public string StatusCode { get; set; }
        public decimal? BrokerageFee { get; set; }
        public List<LoanClientFrequencyModel> LoanFrequencies { get; set; }
        public List<LoanClientAmountModel> LoanAmounts { get; set; }
        public List<NewLoanClientConfigurationModel> LoanConfiguration { get; set; }
        public LoanClientInfos ClientInfos { get; set; }
        public List<LoanAccountBalancePaymentModel> ListPayment { get; set; }
        public LoanClientAddressModel Address { get; set; }

    }

    public class LoanClientInfos
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        public string LanguageClient { get; set; }


    }

    public class LoanClientFrequencyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class LoanClientAmountModel
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }

    public class NewLoanClientConfigurationModel
    {
        public int? LoanFrequencyId { get; set; }
        public string LoanFrequencyName { get; set; }
        public int? LoanAmountId { get; set; }
        public decimal LoanAmount { get; set; }
        public int NumberOfPayments { get; set; }
    }

    public class LoanAccountBalancePaymentModel
    {
        public int? LoanId { get; set; }
        public string? Status { get; set; }
        public string? StatusCode { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Decimal? Interest { get; set; }
        public Decimal? Capital { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? Balance { get; set; }
        public Decimal? MaxAmount { get; set; }
        public Decimal? Fees { get; set; }
        public Decimal? SubFeesPayment { get; set; }
        public bool? IsDeferredFee { get; set; }
    }

    public class LoanClientAddressModel
    {
        public string CivicNumber { get; set; }

        public string StreetName { get; set; }

        public string Apartment { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int? ProvinceId { get; set; }

        public string? ProvinceName { get; set; }

    }
}
