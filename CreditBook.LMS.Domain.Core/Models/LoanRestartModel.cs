﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanRestartModel
    {
        public int LoanId { get; set; }
        public int NewFrequencyId { get; set; }
        public decimal NewPaymentAmount { get; set; }

        public int StartDay { get; set; }
        public int EndDay { get; set; }

        public DateTime RestartPaymentDate { get; set; }
        public bool PaymentAgreement { get; set; }
        public bool Perceptech { get; set; }
        public bool Interac { get; set; }
    }
}
