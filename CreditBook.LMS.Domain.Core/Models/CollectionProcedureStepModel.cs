namespace CreditBook.LMS.Domain.Core.Models
{
    public class CollectionProcedureStepModel
    {
        public int? Id { get; set; }

        public int CollectionId { get; set; }

        public int ProcedureStepId { get; set; }

        public int Order { get; set; }

        public string Description { get; set; }

        public bool Checked { get; set; }
    }
}