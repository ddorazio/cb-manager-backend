using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class StoppedLoanPaymentModel
    {
        public int LoanId { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}
