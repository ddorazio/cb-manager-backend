﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models.CreditBookCheck
{
    public class ResponseSearchModel
    {
        public List<ResponseSearchResultModel> results { get; set; }
    }

    public class ResponseSearchResultModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public string phone1 { get; set; }
        public string phone2 { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string status { get; set; }
        public decimal? amount { get; set; }
        public DateTime? requestDate { get; set; }
        public string ipAddress { get; set; }
        public string language { get; set; }
        public string comments { get; set; }

        public JobModel job { get; set; }
        public List<ReferenceModel> references { get; set; }
        public AddressModel address { get; set; }
        public List<StatusHistoryModel> statusHistory { get; set; }

        public string lender { get; set; }   
    }

    public class JobModel
    {
        public string companyName { get; set; }
        public string companyPhone { get; set; }
        public string supervisorName { get; set; }
        public string jobTitle { get; set; }
        public string payFrequency { get; set; }
        public DateTime? payNext { get; set; }
        public DateTime? startDate { get; set; }
    }

    public class ReferenceModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string relationship { get; set; }
    }

    public class AddressModel
    {
        public string streetAddressLine1 { get; set; }
        public string streetAddressLine2 { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string stateProvince { get; set; }
        public string zipPostalCode { get; set; }
        public DateTime? effectiveDate { get; set; }
    }

    public class StatusHistoryModel
    {
        public DateTime? date { get; set; }
        public string reason { get; set; }
    }
}
