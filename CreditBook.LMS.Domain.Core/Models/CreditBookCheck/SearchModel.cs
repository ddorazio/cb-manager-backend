﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.CreditBookCheck
{
    public class SearchModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}
