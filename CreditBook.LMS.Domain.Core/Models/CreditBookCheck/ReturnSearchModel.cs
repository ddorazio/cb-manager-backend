﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models.CreditBookCheck
{
    public class ReturnSearchModel
    {
        public string client { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string lender { get; set; }
        public string requestDate { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
        public string comments { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public decimal? amount { get; set; }
        public string language { get; set; }

        public JobModel job { get; set; }
        public List<ReferenceModel> references { get; set; }
        public AddressModel address { get; set; }
    }
}
