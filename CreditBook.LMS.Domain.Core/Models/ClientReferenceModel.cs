using System.ComponentModel.DataAnnotations;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class ClientReferenceModel
    {
        public int? Id { get; set; }

        [Required]
        public int ClientId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public string Relation { get; set; }

        public string EmailAddress { get; set; }
    }
}