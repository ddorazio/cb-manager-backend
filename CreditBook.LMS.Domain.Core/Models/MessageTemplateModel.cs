using System.ComponentModel.DataAnnotations;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class MessageTemplateModel
    {
        [Required]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string SubjectEn { get; set; }

        public string SubjectFr { get; set; }

        public string EmailContentEn { get; set; }

        public string EmailContentFr { get; set; }

        public string SmsContentEn { get; set; }

        public string SmsContentFr { get; set; }
    }
}