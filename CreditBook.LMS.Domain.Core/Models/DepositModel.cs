namespace CreditBook.LMS.Domain.Core.Models
{
    public class DepositModel
    {
        public int Id { get; set; }
        public int LoanId { get; set; }

        public int ClientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string BankAccountNumber { get; set; }

        public decimal? LoanAmount { get; set; }

        public string StatusCode { get; set; }

        public string TypeCode { get; set; }

        public string InteracConfirmationCode { get; set; }

        public bool DepositPermitted { get; set; }
    }
}