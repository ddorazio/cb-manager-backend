using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class CollectionModel
    {
        public int Id { get; set; }

        public int LoanNumber { get; set; }

        public string LoanStatusCode { get; set; }

        public string CollectionStatusCode { get; set; }

        public string CollectionDegreeCode { get; set; }

        public string NonPaymentReasonCode { get; set; }

        public string PaymentAgreementCode { get; set; }

        public DateTime? FollowUpDate { get; set; }

        public DateTime? InteracPlannedPaymentDate { get; set; }

        public DateTime? InteracRealPaymentDate { get; set; }

        public string InteracConfirmationEmail { get; set; }

        public bool? InteracSmsReminderOnPaymentDate { get; set; }

        public IEnumerable<CollectionProcedureStepModel> Procedure { get; set; }
        
        public DateTime? LastPaymentDate { get; set; }
    }
}