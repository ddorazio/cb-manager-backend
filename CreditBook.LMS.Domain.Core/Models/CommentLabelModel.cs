namespace CreditBook.LMS.Domain.Core.Models
{
    public class CommentLabelModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Handle { get; set; }
    }
}