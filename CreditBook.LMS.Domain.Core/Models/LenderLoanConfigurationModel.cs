namespace CreditBook.LMS.Domain.Core.Models
{
    public class LenderLoanConfigurationModel
    {
        public bool LoanStoppedPaymentAllowed { get; set; }
        public bool PaymentAgreementAllowed { get; set; }
    }
}