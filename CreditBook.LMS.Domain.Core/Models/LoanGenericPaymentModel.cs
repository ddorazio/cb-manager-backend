﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanGenericPaymentModel
    {
        public int LoanId { get; set; }
        public int PaymentId { get; set; }
    }
}
