﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanRebateModel
    {
        public int LoanId { get; set; }
        public decimal RebateAmount { get; set; }
        public DateTime EffectiveDate {get;set;}
    }
}
