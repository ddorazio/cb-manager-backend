namespace CreditBook.LMS.Domain.Core.Models
{
    public class EmailConfigurationModel
    {
        public string HeaderContentEn { get; set; }

        public string HeaderContentFr { get; set; }

        public string FooterContentEn { get; set; }

        public string FooterContentFr { get; set; }

        public string LogoUrl { get; set; }
    }
}