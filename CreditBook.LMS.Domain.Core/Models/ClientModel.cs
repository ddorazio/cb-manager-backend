using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class ClientModel
    {
        public int Id { get; set; }
        public bool? PotentialMatch { get; set; }

        public List<ClientMatchModel> ClientMatchList { get; set; }

        public ClientBasicInfoModel BasicInfo { get; set; }

        public List<ClientReferenceModel> References { get; set; }

        public List<ClientIBVModel> IBVList { get; set; }

        //public decimal? InterestRate { get; set; }
        public bool? IsPersonalized { get; set; }
        public int LastPaymentDay { get; set; }
        public List<ClientLoanFrequencyModel> LoanFrequencies { get; set; }
        public List<ClientLoanAmountModel> LoanAmounts { get; set; }
        public List<ClientNewLoanConfigurationModel> NewLoanConfiguration { get; set; }

        public List<ClientLoanModel> Loans { get; set; }

        public List<ClientProvinceModel> Provinces { get; set; }
        public List<ClientGenderModel> Genders { get; set; }
        public List<ClientPayFrequencyModel> PayFrequencies { get; set; }
        public List<ClientLanguageModel> Languages { get; set; }

        public List<LoanRequestModel> NonLinkedRequests { get; set; }
        public IEnumerable<ActiveCollectionModel> ActiveCollections { get; set; }

        public List<ClientStatusModel> Status { get; set; }

        public LenderLoanConfigurationModel LoanConfiguration { get; set; }
    }

    public class ClientBasicInfoModel
    {
        public ClientPersonalInfoModel PersonalInfo { get; set; }

        public ClientAddressModel Address { get; set; }

        public ClientRevenueSourceModel RevenueSource { get; set; }

        public ClientFinancialStatusModel FinancialStatus { get; set; }

        public ClientEmploymentStatusModel EmploymentStatus { get; set; }

        public ClientBankInfoModel BankInfo { get; set; }
        public ClientSocialModel Social { get; set; }
    }

    public class ClientPersonalInfoModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string HomePhoneNumber { get; set; }

        public string CellPhoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string EmailAddress { get; set; }

        public int? GenderId { get; set; }

        public int? LanguageId { get; set; }

        public string LanguageCode { get; set; }

        public string SocialInsuranceNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public int? StatusId { get; set; }
    }

    public class ClientAddressModel
    {
        public string CivicNumber { get; set; }

        public string StreetName { get; set; }

        public string Apartment { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public int? ProvinceId { get; set; }

        public string Country { get; set; }

        public DateTime? StartOfResidencyDate { get; set; }
    }

    public class ClientRevenueSourceModel
    {
        public string IncomeTypeCode { get; set; }

        public DateTime? NextPayDate { get; set; }

        public DateTime? EmploymentInsuranceNextPayDate { get; set; }

        public DateTime? NextDepositDate { get; set; }

        public DateTime? EmploymentInsuranceStart { get; set; }

        public DateTime? EmploymentNextPay { get; set; }

        public bool? SelfEmployedDirectDeposit { get; set; }

        public string SelfEmployedPhoneNumber { get; set; }

        public int? SelfEmployedPayFrequencyId { get; set; }

        public DateTime? SelfEmployedStartDate { get; set; }
    }

    public class ClientEmploymentStatusModel
    {
        public bool SelfEmployed { get; set; }

        public string EmployerName { get; set; }

        public string SupervisorName { get; set; }

        public string WorkPhoneNumber { get; set; }

        public string WorkPhoneNumberExtension { get; set; }

        public string WorkPhoneNumberHR { get; set; }

        public string WorkPhoneNumberExtensionHR { get; set; }

        public string Occupation { get; set; }

        public DateTime? HiringDate { get; set; }

        public int? PayFrequencyId { get; set; }
    }

    public class ClientBankInfoModel
    {
        public string TransitNumber { get; set; }

        public string InstitutionNumber { get; set; }

        public string AccountNumber { get; set; }

        public bool? IsBankrupt { get; set; }

        public DateTime? BankruptEndDate { get; set; }
    }

    public class ClientFinancialStatusModel
    {
        public string HousingStatusCode { get; set; }

        public decimal GrossAnnualRevenue { get; set; }

        public decimal MonthlyHousingCost { get; set; }

        public decimal MonthlyUtilityCost { get; set; }

        public decimal MonthlyCarPayment { get; set; }

        public decimal MonthlyOtherLoanPayment { get; set; }
    }

    public class ClientIBVModel
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateFormat { get; set; }
        public string GUID { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public string URL { get; set; }
    }

    public class ClientLoanFrequencyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientLoanAmountModel
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }

    public class ClientNewLoanConfigurationModel
    {
        public int? LoanFrequencyId { get; set; }
        public string LoanFrequencyName { get; set; }

        public int? LoanAmountId { get; set; }
        public decimal LoanAmount { get; set; }

        public int NumberOfPayments { get; set; }
    }

    public class ClientLoanModel
    {
        public int Id { get; set; }
        public int? RequestNo { get; set; }
        public int? ClientId { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public int? FrequencyDuration { get; set; }
        public int FrequencyId { get; set; }
        public string Frequency { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? NumberOfPayments { get; set; }
        public int? InitialNumberOfPayments { get; set; }
        public decimal? BrokerageFee { get; set; }
        public decimal? BrokerageRate { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? MaxInterestRate { get; set; }
        public decimal? Fees { get; set; }
        public decimal? Rebates { get; set; }
        public int? LoanNumber { get; set; }
        public DateTime? CreatedDate { get; set; }
        public decimal? OriginalPaymentAmount { get; set; }

        public bool? IsLoanStopped { get; set; }
        public DateTime? StopLoanDate { get; set; }

        //public decimal? FirstPaymentAmount { get; set; }

        public decimal TotalAmountRemaining { get; set; }
        public decimal? CurrentDailyBalance { get; set; }

        public int DefaultPayments { get; set; }

        public int? BiMonthlyStartDay { get; set; }
        public int? BiMonthlyEndDay { get; set; }

        public List<ClientLoanPaymentModel> LoanPayments { get; set; }
        public bool CanDelete { get; set; }
        public bool IsPerceptech { get; set; }
        public bool PaymentProvider { get; set; }
        public bool BeforeProvider { get; set; }

        public string DigitalUrl { get; set; }
        public string ContractSignedDate { get; set; }
        public bool ContractSigned { get; set; }
        public decimal? SubFees { get; set; }
        public decimal? OriginalSubFeesPrice { get; set; }
        public bool ChargeMembershipFee { get; set; }
    }

    public class ClientLoanPaymentModel
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public int PaymentNo { get; set; }
        public int Index { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        //public bool IsManual { get; set; }

        public decimal? Interest { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Amount { get; set; }
        public decimal? NewAmount { get; set; }
        public decimal? Balance { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Fees { get; set; }
        public decimal? SubFeesPayment { get; set; }
        public bool? IsNSF { get; set; }
        public bool? IsManual { get; set; }
        public bool? IsRebate { get; set; }
        public bool? IsDeferredFee { get; set; }
        public bool? IsAmountChanged { get; set; }
        public bool? IsAmountChangedForAll { get; set; }
        public bool? IsDateChanged { get; set; }
        public bool? IsFrequencyChanged { get; set; }

        public int? FrequencyDuration { get; set; }

        public bool? IsDeleted { get; set; }
        public bool CanEdit { get; set; }
        public bool CanNSF { get; set; }

        public string Description { get; set; }

        public DateTime? PaymentDate { get; set; }
        //public bool EditLoan { get; set; }
        public bool SentToPerceptech { get; set; }

        public decimal WithdrawalFee { get; set; }
        public decimal NsfFee { get; set; }
        public decimal DeferredFee { get; set; }
    }

    public class ClientGenderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientLanguageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientProvinceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientPayFrequencyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientSocialModel
    {
        public string GoogleId { get; set; }
        public string FacebookId { get; set; }
        public string GooglePhoto { get; set; }
        public string FacebookPhoto { get; set; }
    }

    public class ClientStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClientMatchModel
    {
        public int Id { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public DateTime? Birthday { get; set; }
        public string CelPhone { get; set; }
        public string Email { get; set; }
    }
}