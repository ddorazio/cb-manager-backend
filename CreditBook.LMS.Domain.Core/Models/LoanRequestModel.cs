using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanRequestModel
    {
        public int Id { get; set; }

        public int? ClientId { get; set; }

        public string ClientFirstName { get; set; }

        public string ClientLastName { get; set; }

        public decimal? LoanAmount { get; set; }

        public decimal? AmountApproved { get; set; }
        public string? Comments { get; set; }
        public string RequestedDateFormat { get; set; }
        public DateTime? RequestedDate { get; set; }
        public bool? IBVCompleted { get; set; }
        public string IBVReport { get; set; }

        public bool? PotentialMatch { get; set; }

        public string StatusCode { get; set; }
        public string Status { get; set; }
        public bool? CheckListCompleted { get; set; }
        public List<LoanRequestChecklistModel> Checklist { get; set; }
    }

    public class LoanRequestChecklistModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool? Confirmed { get; set; }
    }
}