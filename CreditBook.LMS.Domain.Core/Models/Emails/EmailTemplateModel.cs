﻿using System;
namespace CreditBook.LMS.Domain.Core.Models.EmailTemplates
{
    public class EmailTemplateModel
    {
        public RequestWithoutDocuments WithoutDocuments { get; set; }
        public RequestWithDocuments WithDocuments { get; set; }
        public IBVMessageModel IBV { get; set; }
        public ContractModel Contract { get; set; }
        public ExistingContractModel ExistingContract { get; set; }
        public LoanRefusedModel LoanRefused { get; set; }
        public LoanDeclinedModel LoanDeclined { get; set; }
        public BankDebitModel BankDebit { get; set; }
        public CollectionModel Collection { get; set; }
        public FirstNSFModel NSF { get; set; }
        public LoanPDFMessageModel loanPDF { get; set; }
        public IBVReminderModel IBVReminder { get; set; }
    }

    public class RequestWithoutDocuments
    {
        public string IBVHtmlURL { get; set; }
        public string IBVUrl { get; set; }
        public string ClientEmail { get; set; }
        public string ClientCell { get; set; }
    }

    public class RequestWithDocuments
    {
        public string IBVHtmlURL { get; set; }
        public string ClientEmail { get; set; }
        public string ClientCell { get; set; }
    }

    public class IBVMessageModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IBVHtmlURL { get; set; }
        public string IBVUrl { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
    }

    public class ContractModel
    {
        public int LoanId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string LoanAmount { get; set; }
        public string ContractName { get; set; }
        public string ContractPath { get; set; }
        public string Language { get; set; }
        public string DigitalUrl { get; set; }
    }

    public class ExistingContractModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string Language { get; set; }
        public string DigitalUrl { get; set; }
    }

    public class LoanPDFMessageModel
    {
        public int LoanId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public int TypePDF { get; set; }
        public string LoanPDFPath { get; set; }
        public string LoanPDFName { get; set; }
        public string Language { get; set; }
    }

    public class LoanRefusedModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string LoanAmount { get; set; }
    }

    public class LoanDeclinedModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IBVHtmlURL { get; set; }
        public string IBVUrl { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
    }

    public class BankDebitModel
    {
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string LoanPaymentAmount { get; set; }
        public string LoanPaymentDate { get; set; }
    }

    public class CollectionModel
    {
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoanPaymentAmount { get; set; }
    }

    public class FirstNSFModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
    }

    public class IBVReminderModel
    {
        public string IBVHtmlURL { get; set; }
        public string IBVUrl { get; set; }
        public string EmailAddress { get; set; }
        public string CellPhone { get; set; }
    }
}
