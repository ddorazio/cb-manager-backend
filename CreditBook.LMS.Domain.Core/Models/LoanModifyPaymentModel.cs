﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanModifyPaymentModel
    {
        public int LoanId { get; set; }
        public int PaymentId { get; set; }
        public decimal? NewPaymentAmount { get; set; }
        public DateTime NewPaymentDate { get; set; }
    }
}
