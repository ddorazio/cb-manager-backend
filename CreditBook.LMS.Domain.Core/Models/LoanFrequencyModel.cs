using System.ComponentModel.DataAnnotations;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanFrequencyModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        public int? MinimumDaysBeforeDeposit { get; set; }
    }
}