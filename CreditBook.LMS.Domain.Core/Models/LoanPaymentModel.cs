﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanPaymentModel
    {
        public int LoanId { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime EffectiveDate {get;set;}
    }
}
