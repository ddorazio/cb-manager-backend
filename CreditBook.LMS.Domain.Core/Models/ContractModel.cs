﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class ContractModel
    {
        public string ContractDate { get; set; }
        public string FullName { get; set; }
        public string TotalCost { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Telephone { get; set; }
        public string CellPhone { get; set; }

        public string LoanPayment { get; set; }
        public string LoanNoOfPayments { get; set; }

        public string FileNo { get; set; }

        public string AddressLine { get; set; }

        public string InterestRate { get; set; }
        public string CapitalAmount { get; set; }

        public string TotalLoanAmount { get; set; }
        public string FirstPaymentDate { get; set; }
        public string LastPaymentDate { get; set; }

        public string AdminFees { get; set; }
        public string NsfFees { get; set; }
    }
}
