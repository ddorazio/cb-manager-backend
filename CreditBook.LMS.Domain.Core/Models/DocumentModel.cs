using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class DocumentModel
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }

        [Required]
        public int ClientId { get; set; }

        public int? ClientLoanId { get; set; }

        public int? ClientLoanNumber { get; set; }

        public string DocumentTypeCode { get; set; }

        public string Filename { get; set; }

        [Required]
        public string Title { get; set; }

        public string Note { get; set; }

        [JsonIgnore]
        public string S3Key { get; set; }

        public DateTime UploadDateTime { get; set; }

        [JsonIgnore]
        public IFormFile File { get; set; }
    }
}