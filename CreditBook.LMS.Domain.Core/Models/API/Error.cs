namespace CreditBook.LMS.Domain.Core.Models.API
{
    public class Error
    {
        public Error(string code, string message) 
        {
            Code = code;
            Message = message;
        }

        public Error(string code, string message, string details)
        {
            Code = code;
            Message = message;
            Details = details;
        }

        public string Code { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
    }
}