using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class InteracPaymentAgreementModel
    {
        public int IdLoanPayment { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PaymentDateFormatted { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? PaymentAmount { get; set; }
        public string ConfirmationDate { get; set; }
        public DateTime? DepositedDate { get; set; }
    }
}
