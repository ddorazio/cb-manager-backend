﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class PDFEmailModel
    {
        public string? PDF { get; set; }
        public int LoanId { get; set; }
        public PDFEmailClientInfosModel ClientInfos { get; set; }
        public int TypePDF { get; set; }
    }

    public class PDFEmailClientInfosModel {
        public string? Email { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }

        public string? LanguageClient { get; set; }

    } 

}
