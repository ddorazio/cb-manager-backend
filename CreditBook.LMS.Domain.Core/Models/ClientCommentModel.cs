using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    // Fields are named to match Fuse object definitions
    public class ClientCommentModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? Created { get; set; }

        public IEnumerable<CommentLabelModel> Labels { get; set; }

        public string Username { get; set; }

        public bool Archive { get; set; }

        public DateTime? Modified { get; set; }
    }
}