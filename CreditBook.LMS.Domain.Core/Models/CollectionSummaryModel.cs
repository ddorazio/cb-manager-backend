using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class CollectionSummaryModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }

        public string CollectionDegreeCode { get; set; }

        public DateTime? FollowUpDate { get; set; }

        public DateTime? InteracPaymentDate { get; set; }

        public string NonPaymentReasonCode { get; set; }

        public string ClientFirstName { get; set; }

        public string ClientLastName { get; set; }

        public string ClientEmailAddress { get; set; }

        public string ClientCellPhoneNumber { get; set; }

        public decimal Amount { get; set; }

        public string LastComment { get; set; }
        
        public DateTime? LastPaymentDate { get; set; }
    }
}