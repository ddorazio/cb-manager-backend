﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class PerceptechUploadModel
    {
        public string UploadFileName { get; set; }
        public string ErrorFileName { get; set; }
        public string UploadErrorFileName { get; set; }
        public bool UploadEmpty { get; set; }
    }

    public class PerceptechFileModel
    {
        public int ClientLoanPaymentId { get; set; }
    }

    public class PerceptechErrorModel
    {
        public string ClientName { get; set; }
        public string ClientNo { get; set; }
        public string ClientEmail { get; set; }
        public string LoanNo { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? PaymentAmount { get; set; }
        public string ErrorString { get; set; }
    }
}
