﻿using System;
namespace CreditBook.LMS.Domain.Core.Models
{
    public class NewLoanModel
    {
        public int LoanAmountId { get; set; }
        //public Decimal LoanAmount { get; set; }
        public Decimal LoanManualAmount { get; set; }
        public int ClientId { get; set; }
        public Decimal InterestRate { get; set; }
        public int FrequencyId { get; set; }
        public int NumberOfPayments { get; set; }
        public DateTime NextPayDate { get; set; }
        public int StartDay { get; set; }
        public int EndDay { get; set; }
        public int RequestNo { get; set; }
        public Decimal? Rebate { get; set; }
    }
}