using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanDelayPaymentModel
    {
        public int LoanId { get; set; }
        public int PaymentId { get; set; }
        public DateTime NewPaymentDate { get; set; }
    }
}