namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanConfigurationModel
    {
        public int? Id { get; set; }

        public string LoanFrequencyCode { get; set; }

        public decimal LoanAmount { get; set; }

        public int? TotalPayments { get; set; }
    }
}