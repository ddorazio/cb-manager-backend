﻿using System;
using System.Collections.Generic;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class IBVModel
    {
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string IPAddress { get; set; }
        public string BrowserName { get; set; }
        public string LanguageCode { get; set; }
        public string Url { get; set; }
        public string LMSReturnURL { get; set; }
        public bool? IsCompleted { get; set; }
    }

    public class IBVReturnModel
    {
        public int status { get; set; }
        public string message { get; set; }
        public IBVReturnDataModel data { get; set; }
    }

    public class IBVReturnDataModel
    {
        public string url { get; set; }
        public string requestID { get; set; }
    }
}
