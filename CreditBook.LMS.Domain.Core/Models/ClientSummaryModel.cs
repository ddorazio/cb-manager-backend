using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class ClientSummaryModel
    {
        public int Id { get; set; }

        public DateTime? CreationDate { get; set; }

        public int StatusId { get; set; }

        public string Status { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string City { get; set; }

        public string Province { get; set; }

        public string HomePhoneNumber { get; set; }

        public string CellPhoneNumber { get; set; }

        public string EmailAddress { get; set; }
    }
}