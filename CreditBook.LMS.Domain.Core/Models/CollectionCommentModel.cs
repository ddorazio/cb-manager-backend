using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class CollectionCommentModel
    {
        public int? Id { get; set; }

        public int CollectionId { get; set; }

        public DateTime Created { get; set; }

        public string Username { get; set; }

        public DateTime? Modified { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
        
        public bool Archive { get; set; }
    }
}