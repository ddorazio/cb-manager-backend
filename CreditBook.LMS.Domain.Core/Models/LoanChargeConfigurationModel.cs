using System;

namespace CreditBook.LMS.Domain.Core.Models
{
    public class LoanChargeConfigurationModel
    {
        public int? Id { get; set; }

        public string Code { get; set; }

        public decimal AmountCharged { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}