using System.Data;

namespace CreditBook.DigitalSignature.Service
{
    public interface ISqlConnectionProvider
    {
        IDbConnection GetSaaSDbConnection();
        IDbConnection GetLmsDbConnection();
    }
}