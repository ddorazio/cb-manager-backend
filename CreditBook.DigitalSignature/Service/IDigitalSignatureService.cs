using CreditBook.DigitalSignature.Models;
using System;
using System.Threading.Tasks;

namespace CreditBook.DigitalSignature.Service
{
    public interface IDigitalSignatureService
    {
        Task<ContractModel> GetContract(Guid documentId);
        Task<ContractModel> SignContract(Guid contractId, string ipAddress, string sigBase64, string browser /*, DateTime dateOfBirth*/);
        Task SaveContractToClientDocuments(Guid contractId);
        Task<ClientData> GetCustomerInfo(Guid documentId);

        Task SendContractSignedNotification(Guid documentId);
    }
}