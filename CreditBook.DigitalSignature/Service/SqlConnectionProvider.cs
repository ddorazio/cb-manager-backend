using System.Data;
using Microsoft.Data.SqlClient;

namespace CreditBook.DigitalSignature.Service
{

    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        private readonly string _saasConnectionString;
        private readonly string _lmsConnectionString;

        public SqlConnectionProvider(string connectionString, string lmsConnectionString)
        {
            _saasConnectionString = connectionString;
            _lmsConnectionString = lmsConnectionString;
        }

        public IDbConnection GetSaaSDbConnection()
        {
            return new SqlConnection(_saasConnectionString);
        }

        public IDbConnection GetLmsDbConnection()
        {
            return new SqlConnection(_lmsConnectionString);
        }
    }
}