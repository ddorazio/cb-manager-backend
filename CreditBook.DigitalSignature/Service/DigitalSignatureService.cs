using CreditBook.DigitalSignature.Models;
using CreditBook.LMS.Service.Core;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CreditBook.DigitalSignature.Service
{

    public class DigitalSignatureService : IDigitalSignatureService
    {
        private readonly string _domain;
        private readonly ISqlConnectionProvider _connectionProvider;
        private readonly IDocumentService _documentService;
        private readonly IS3StreamService _s3StreamService;
        private readonly IHttpClientFactory _httpClientFactory;
        public readonly string _adobeClientId;
        private readonly string _notificatioUrl;
        private readonly string _contractSignatureFolder;
        private readonly string _contractSignatureBaseUrl;

        public DigitalSignatureService(string domain, ISqlConnectionProvider provider, IDocumentService documentService, IS3StreamService s3StreamService, IHttpClientFactory httpClientFactory,
                                            string notificationUrl, string contractSignatureFolder, string contractSignatureBaseUrl, string adobeClientId)
        {
            _domain = domain;
            _connectionProvider = provider;
            _documentService = documentService;
            _s3StreamService = s3StreamService;
            _httpClientFactory = httpClientFactory;
            _contractSignatureFolder = contractSignatureFolder;
            _contractSignatureBaseUrl = contractSignatureBaseUrl;
            _notificatioUrl = notificationUrl;
            _adobeClientId = adobeClientId;
        }

        public async Task<ContractModel> GetContract(Guid documentId)
        {
            using var connection = _connectionProvider.GetLmsDbConnection();
            const string query =
                @"SELECT 
                    Client_Loan.ContractURL AS ContractUrl,
                    ClientLoanContractSignature.IsSigned,
                    ClientLoanContractSignature.TimeStamp,
                    Language.Code AS LanguageCode,
                    ClientLoanContractSignature.SignatureUrl
                FROM ClientLoanContractSignature
                INNER JOIN Client_Loan ON ClientLoanContractSignature.ClientLoanId = Client_Loan.ID
                INNER JOIN Client ON Client_Loan.IDClient = Client.ID 
                INNER JOIN Language ON Client.IDLanguage = Language.ID
                WHERE ClientLoanContractSignature.Guid = @documentId;";
            //var contract = await connection.QuerySingleOrDefaultAsync<ContractModel>(query, new { documentId });
            var result = connection.QuerySingleOrDefaultAsync<ContractModel>(query, new { documentId }).Result;
            result.AdobeClientId = _adobeClientId;
            if (result.TimeStamp != null)
            { result.TimeStampFormatted = result.LanguageCode == "F" ? String.Format("{0:yyyy-MM-dd HH:mm:ss}", (DateTime)result.TimeStamp) : String.Format("{0:yyyy-MM-dd HH:mm:ss}", (DateTime)result.TimeStamp); }
            else
            {
                result.IsPrevious = true;
            }
            return result;
        }

        public async Task SendContractSignedNotification(Guid documentId)
        {
            using var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_notificatioUrl);
            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(documentId), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PutAsync($"/api/Notification/NotifyContractSigned/{documentId}", httpContent);
        }

        public async Task<ContractModel> SignContract(Guid contractId /*, DateTime dateOfBirth*/, string ipAddress, string sigBase64, string browser)
        {
            using var connection = _connectionProvider.GetLmsDbConnection();
            const string query =
                @"SELECT Client.DateOfBirth, 
                    ClientLoanContractSignature.IsSigned, 
                    Client_Loan.ContractURL AS ContractUrl,
                    ClientLoanContractSignature.TimeStamp,
                    Language.Code AS LanguageCode,
                    Client.FirstName,
                    Client.LastName,
                    Client_Loan.ID AS LoanId
                FROM ClientLoanContractSignature
                INNER JOIN Client_Loan ON ClientLoanContractSignature.ClientLoanId = Client_Loan.ID
                INNER JOIN Client ON Client.ID = Client_Loan.IDClient
                INNER JOIN Language ON Client.IDLanguage = Language.ID
                WHERE ClientLoanContractSignature.Guid = @contractId;";
            var clientData = await connection.QuerySingleOrDefaultAsync<ContractModel>(query, new { contractId });
            if (clientData == null)
                throw new ArgumentException("NOT_FOUND");
            // if (clientData.IsSigned ?? false)
            //     throw new ArgumentException("ALREADY_SIGNED");
            // if (clientData.DateOfBirth != null && !clientData.DateOfBirth.Value.Date.Equals(dateOfBirth.Date))
            //     throw new ArgumentException("WRONG_BIRTH_DATE");

            if (!Directory.Exists(_contractSignatureFolder))
            {
                Directory.CreateDirectory(_contractSignatureFolder);
            }

            byte[] bytes = Convert.FromBase64String(sigBase64.Split(',')[1]);
            string signatureName = clientData.LanguageCode == "E"
                ? clientData.FirstName + "_" + clientData.LastName + "_" + clientData.LoanId.ToString().PadLeft(6, '0') + "_Signature.png"
                : clientData.FirstName + "_" + clientData.LastName + "_" + clientData.LoanId.ToString().PadLeft(6, '0') + "_Signature.png";

            using (var images = Image.FromStream(new MemoryStream(bytes)))
            {
                images.Save(_contractSignatureFolder + signatureName, ImageFormat.Png);
            }

            var dateNow = DateTime.Now;
            var signatureUrl = _contractSignatureBaseUrl + signatureName;
            await connection.ExecuteScalarAsync(
                @"UPDATE ClientLoanContractSignature SET IsSigned=1, IPAddress = @ipAddress, TimeStamp = @dateNow, SignatureUrl = @signatureUrl, BrowserName = @browser WHERE Guid = @contractId",
                new { ipAddress, dateNow, contractId, signatureUrl, browser });

            return new ContractModel
            {
                TimeStampFormatted = clientData.LanguageCode == "F"
                                    ? String.Format("{0:yyyy-MM-dd HH:mm:ss}", dateNow)
                                    : String.Format("{0:yyyy-MM-dd HH:mm:ss}", dateNow),
                SignatureUrl = signatureUrl,
                IsSigned = true
            };
        }

        public async Task SaveContractToClientDocuments(Guid documentId)
        {
            // Fetch client and contract data
            const string query =
                @"SELECT 
                    Client_Loan.IDClient AS ClientId,
                    Client_Loan.ContractURL AS ContractUrl,
                    Client_Loan.LoanNumber
                FROM ClientLoanContractSignature
                INNER JOIN Client_Loan ON ClientLoanContractSignature.ClientLoanId = Client_Loan.ID
                WHERE ClientLoanContractSignature.Guid = @documentId;";
            using var connection = _connectionProvider.GetLmsDbConnection();
            var contractData = await connection.QuerySingleOrDefaultAsync(query, new { documentId });

            // Create the document object
            var document = new LMS.Domain.Core.Models.DocumentModel();
            document.ClientId = contractData.ClientId;
            document.Filename = $"contract_{contractData.LoanNumber}.pdf";
            document.Title = $"Contract for loan #{contractData.LoanNumber} / Contrat pour prêt #{contractData.LoanNumber}";
            document.S3Key = $"{contractData.ClientId}/{document.Filename}_{DateTime.UtcNow.ToString("yyyyMMddHHmmss")}";

            // Fetch the PDF document and upload to S3
            using var client = new WebClient();
            var content = client.DownloadData(contractData.ContractUrl);
            using var stream = new MemoryStream(content);
            await _s3StreamService.UploadAsync(document.S3Key, stream);

            // If successful create the document db entry
            await _documentService.CreateDocument(document);
        }

        public async Task<ClientData> GetCustomerInfo(Guid documentId)
        {
            using var connection = _connectionProvider.GetLmsDbConnection();
            const string query =
                @"SELECT 
                    Client.ID AS ClientId,
                    Language.Code AS LanguageCode
                FROM ClientLoanContractSignature
                INNER JOIN Client_Loan ON ClientLoanContractSignature.ClientLoanId = Client_Loan.ID
                INNER JOIN Client ON Client_Loan.IDClient = Client.ID 
                INNER JOIN Language ON Client.IDLanguage = Language.ID
                WHERE ClientLoanContractSignature.Guid = @documentId;";
            return await connection.QuerySingleOrDefaultAsync<ClientData>(query, new { documentId });
        }
    }
}