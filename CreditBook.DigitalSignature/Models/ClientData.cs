namespace CreditBook.DigitalSignature.Models
{
    public class ClientData
    {
        public int ClientId { get; set; }

        public string LanguageCode { get; set; }
    }
}