using System;

namespace CreditBook.DigitalSignature.Models
{
    public class ContractModel
    {
        public string ContractUrl { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? TimeStamp { get; set; }

        public string TimeStampFormatted { get; set; }

        public bool? IsSigned { get; set; }

        public string ErrorCode { get; set; }

        public string LanguageCode { get; set; }

        public string AdobeClientId { get; set; }
        public string SignatureUrl { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int32 LoanId { get; set; }
        public bool IsPrevious {get;set;}
    }
}