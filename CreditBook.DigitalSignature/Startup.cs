using System.Net.Http;
using CreditBook.DigitalSignature.Service;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core;
using Dapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CreditBook.DigitalSignature
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // In production, the React files will be served from this directory
            // services.AddSpaStaticFiles(configuration =>
            // {
            //     configuration.RootPath = "ClientApp/build";
            // });

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });

            // Add IHttpClientFactory to DI
            services.AddHttpClient();

            // Get site config
            var domain = Configuration.GetSection("Site")["Domain"];
            var saasConnectionString = Configuration.GetConnectionString("SaaS");
            var siteConfig = GetSiteConfiguration(saasConnectionString, domain);

            // Configure EF/Dapper connections
            services.AddDbContext<CreditBook_LMS_DEVContext>(options =>
            {
                options.UseSqlServer((string)siteConfig.LmsConnectionString);
            });
            services.AddScoped<IDbContextProvider>(config => new DbContextProvider(siteConfig.LmsConnectionString));
            var connectionProvider = new SqlConnectionProvider(saasConnectionString, siteConfig.LmsConnectionString);

            // Set up AWS clients
            var s3ClientProvider = new S3ClientProvider
            (
                siteConfig.AwsAccessKeyId,
                siteConfig.AwsSecretKey
            );
            services.AddSingleton<IS3ClientProvider>(s3ClientProvider);
            services.AddSingleton<IS3StreamService>(provider => new S3StreamService
            (
                provider.GetService<IS3ClientProvider>(),
                siteConfig.S3BucketName
            ));

            // Configure services
            services.AddHttpContextAccessor();
            services.AddScoped<ISqlConnectionProvider>(_ => connectionProvider);
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IDigitalSignatureService>(provider => new DigitalSignatureService(
                domain,
                provider.GetService<ISqlConnectionProvider>(),
                provider.GetService<IDocumentService>(),
                provider.GetService<IS3StreamService>(),
                provider.GetService<IHttpClientFactory>(),
                siteConfig.NotificationEndpoint,
                siteConfig.ContractSignatureFolder,
                siteConfig.ContractSignatureBaseUrl,
                siteConfig.AdobeClientId
                ));
            services.AddScoped<IUserService, UserService>();
            //services.AddScoped<INotificationService, NotificationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Access files in wwwroot folder
            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCors(policy =>
            {
                policy.AllowAnyMethod();
                policy.AllowAnyHeader();
                policy.AllowAnyOrigin();
            });

            app.UseRouting();

            app.UseEndpoints(builder =>
            {
                builder.MapControllers();
            });
        }

        private dynamic GetSiteConfiguration(string saasConnectionString, string domain)
        {
            var connection = new SqlConnection(saasConnectionString);
            var query =
                @"SELECT 
                    SiteConfig.LmsConnectionString,
                    SiteConfig.AwsAccessKeyId,
                    SiteConfig.AwsSecretKey,
                    SiteConfig.S3BucketName,
                    SiteConfig.ContractSignatureFolder,
                    SiteConfig.ContractSignatureBaseUrl,
                    SiteConfig.AdobeClientId,
                    SiteConfig.NotificationEndpoint
                FROM CreditBook_LMS_SAS.dbo.Site
                INNER JOIN CreditBook_LMS_SAS.dbo.SiteConfig
                    ON SiteConfig.SiteId = Site.Id 
                WHERE site.[Domain] = @domain;";
            return connection.QuerySingleOrDefault(query, new { domain });
        }
    }
}
