﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using CreditBook.DigitalSignature.Models;
using CreditBook.DigitalSignature.Service;
using CreditBook.LMS.Service.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CreditBook.DigitalSignature.Controllers
{
    [ApiController]
    [Route("api/contract")]
    public class ContractController : Controller
    {
        private readonly ILogger<ContractController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IDigitalSignatureService _digitalSignatureService;
        private readonly INotificationService _notificationService;

        public ContractController(ILogger<ContractController> logger, IConfiguration configuration, IHttpClientFactory httpClientFactory, IDigitalSignatureService digitalSignatureService/*, INotificationService notificationService*/)
        {
            _logger = logger;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            _digitalSignatureService = digitalSignatureService;
            //_notificationService = notificationService;
        }

        [HttpGet("{documentId}")]
        public async Task<ActionResult<ContractModel>> GetContractUrl(Guid documentId)
        {
            var result = await _digitalSignatureService.GetContract(documentId);
            //result.AdobeClientId = _digitalSignatureService.adobeClientId;
            if (result == null) return NotFound();
            return Ok(result);
        }

        public class SignContractModel
        {
            // public Guid ContractGuid { get; set; }
            // public string ClientIp { get; set; }
            public string SigBase64 { get; set; }
            public string Browser { get; set; }
        }

        [HttpPut("{documentId}/{ipAddress}")]
        public async Task<ActionResult<ContractModel>> SignContract(Guid documentId, string ipAddress, [FromBody] SignContractModel model  /*, [FromQuery] DateTime dateOfBirth*/)
        //public async Task<ActionResult<ContractModel>> SignContract([FromBody] SignContractModel model)
        {
            ContractModel result;
            // Sign request
            try
            {
                result = await _digitalSignatureService.SignContract(documentId, ipAddress, model.SigBase64, model.Browser /*, dateOfBirth*/);
            }
            catch (ArgumentException e)
            {
                _logger.LogError($"Error signing contract {documentId} \n{e}");
                return BadRequest(new ContractModel
                {
                    ErrorCode = e.Message
                });
            }

            try
            {
                // Notify LMS users
                await _digitalSignatureService.SendContractSignedNotification(documentId);

                // Notify LMS users
                // var customer = await _digitalSignatureService.GetCustomerInfo(documentId);
                // var notif = new LMS.Domain.Core.Models.NotificationModel
                // {
                //     Type = "CONTRACT",
                //     IdClient = customer.ClientId,
                //     Path = $"/apps/customers/detail/{customer.ClientId}/documents"
                // };
                // await _notificationService.NewNotification(notif);

                // // Send push notif to SignalR
                // string URL = _configuration["SignalREndpoint"];
                // using var client = _httpClientFactory.CreateClient();
                // client.BaseAddress = new Uri(URL);
                // HttpResponseMessage response = await client.GetAsync(URL);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error sending push notification for contract {documentId} \n{e}");
            }

            try
            {
                // Save a copy of the PDF to the client's documents
                await _digitalSignatureService.SaveContractToClientDocuments(documentId);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error saving copy to client documents for contract {documentId} \n{e}");
            }

            return Ok(result);
        }
    }
}
