﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.Logic;
using HiQPdf;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class LoanService : ILoanService
    {
        private ClientLoanModel Loan { get; set; }
        private LoanCalculator Calculator { get; set; }
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IConfigurationService _configurationService;

        //private LoanerConfiguration InterestRateString { get; set; }

        public LoanService(IDbContextProvider dbContextFactory, IConfigurationService configurationService)
        {
            _dbContextProvider = dbContextFactory;
            _configurationService = configurationService;
        }

        public class DigitalSignatureModel
        {
            public string Url { get; set; }
            public string SignedDate { get; set; }
            public bool IsSigned { get; set; }
        }

        #region Common

        private ClientLoanModel LoadLaonInformation(int loandId, string language)
        {
            using var context = _dbContextProvider.GetContext();
            var loan = context.ClientLoan
                                .Where(x => x.Id == loandId)
                                .Select(x => new ClientLoanModel()
                                {
                                    Id = x.Id,
                                    ClientId = x.Idclient,
                                    LoanNumber = x.LoanNumber,
                                    Status = x.IdloanStatusNavigation.NameFr,
                                    StatusCode = x.IdloanStatusNavigation.Code,
                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,
                                    Fees = x.Fees,
                                    Rebates = x.Rebates,
                                    InterestAmount = x.InterestAmount,
                                    InterestRate = x.InterestRate,
                                    Amount = x.Amount,
                                    BrokerageFee = x.BrokerageFees,
                                    Frequency = language == "FR" ? x.IdloanFrequencyNavigation.NameFr : x.IdloanFrequencyNavigation.NameEn,
                                    FrequencyDuration = x.IdloanFrequencyNavigation.Frequency,
                                    FrequencyId = x.IdloanFrequencyNavigation.Id,
                                    CreatedDate = x.CreatedDate,
                                    OriginalPaymentAmount = x.OriginalPaymentAmount,
                                    BiMonthlyStartDay = x.BiMonthlyStartDay,
                                    BiMonthlyEndDay = x.BiMonthlyEndDay,
                                    IsLoanStopped = x.IdloanStatusNavigation.Code == "LS" ? true : false,
                                    StopLoanDate = x.StopLoanDate,
                                    IsPerceptech = x.IsPerceptech == null ? false : (bool)x.IsPerceptech,
                                    PaymentProvider = _configurationService.ValidatePaymentProvider(),
                                    InitialNumberOfPayments = x.InitialNumberOfPayments,

                                    SubFees = x.SubFees ?? null,
                                    OriginalSubFeesPrice = x.OriginalSubFeesPrice ?? null,
                                    ChargeMembershipFee = _configurationService.ValidateMembershipFee()
                                })
                                .SingleOrDefault();
            return loan;
        }

        private List<ClientLoanPaymentModel> LoadLoanPaymentInformation(int loandId, string language)
        {
            using var context = _dbContextProvider.GetContext();
            var loanPayments = context.ClientLoanPayment
                                        .Where(x => x.IdclientLoan == loandId && x.IsDeleted == false)
                                        .Select(x => new ClientLoanPaymentModel()
                                        {
                                            Id = x.Id,
                                            LoanId = loandId,
                                            Status = language == "FR" ? x.IdpaymentStatusNavigation.NameFr : x.IdpaymentStatusNavigation.NameEn,
                                            StatusCode = x.IdpaymentStatusNavigation.Code,
                                            Interest = x.Interest,
                                            Balance = x.Balance,
                                            Capital = x.Capital,
                                            Amount = x.Amount,
                                            MaxAmount = x.MaxAmount,
                                            Fees = x.Fees,
                                            PaymentDate = x.PaymentDate,
                                            IsNSF = x.IsNsf,
                                            IsManual = x.IsManual,
                                            IsRebate = x.IsRebate,
                                            IsAmountChanged = x.IsAmountChanged,
                                            IsAmountChangedForAll = false,
                                            IsDateChanged = x.IsDateChanged,
                                            IsFrequencyChanged = x.IsFrequencyChanged,
                                            IsDeferredFee = x.IsDeferredFee,
                                            FrequencyDuration = x.Frequency,
                                            IsDeleted = x.IsDeleted,
                                            SentToPerceptech = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == x.Id) != null ? true : false,
                                            NewAmount = x.Amount,

                                            SubFeesPayment = x.SubFeesPayment ?? null,

                                            NsfFee = (decimal)x.Nsffee,
                                            DeferredFee = (decimal)x.DeferredFee,
                                            WithdrawalFee = (decimal)x.TransactionFee
                                        }).OrderBy(x => x.PaymentDate).ThenBy(x => x.Id)
                                        .ToList();
            return loanPayments;
        }

        // private decimal CalculateBalance(int loanId)
        // {
        //     using var context = _dbContextProvider.GetContext();
        //     var loan = context.ClientLoan.Find(loanId);
        //     var frequency = context.LoanFrequency.Find(loan.IdloanFrequency);
        //     var payments = context.ClientLoanPayment
        //         .Where(p => p.IdclientLoan == loan.Id
        //                 && (p.IsDeferredFee == false || p.IsDeferredFee == null)
        //                 && p.IdpaymentStatusNavigation.Code != "DP"
        //                 && p.IdpaymentStatusNavigation.Code != "PR")
        //         .ToList();

        //     // Take InterestRate from ClientLoan once Loan is created
        //     var interestRate = (decimal)loan.InterestRate;
        //     // var interestRate = decimal.Parse(
        //     //     (context.LoanerConfiguration.Single(lc => lc.Key == "INTEREST_RATE")).Value);

        //     decimal result = 0;
        //     var firstPaymentDate = payments.OrderBy(p => p.PaymentDate).First().PaymentDate;
        //     decimal interestRateToDecimal = interestRate / 100;
        //     decimal convertedInterest = interestRateToDecimal / frequency.Frequency.Value;
        //     decimal dailyInterest = interestRateToDecimal / 365;

        //     if (payments.Where(x => x.PaymentDate.Value.Date <= DateTime.Today).Count() > 0)
        //     {
        //         DateTime? previousDate = null;
        //         decimal previousBalance = 0;
        //         foreach (var payment in payments.Where(x => x.IsDeleted == false))
        //         {
        //             if (payment.PaymentDate.Value.Date <= DateTime.Today)
        //             {
        //                 previousDate = payment.PaymentDate;
        //                 previousBalance = payment.Balance.Value;
        //             }
        //             else
        //             {
        //                 break;
        //             }
        //         }

        //         var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
        //         var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

        //         result = previousBalance + intervalInterest;
        //     }
        //     else if (DateTime.Today <= firstPaymentDate)
        //     {
        //         var intervalDays = (DateTime.Today - (DateTime)loan.CreatedDate.Value.Date).Days;
        //         var intervalInterest = (dailyInterest * intervalDays) * loan.Amount.GetValueOrDefault();

        //         result = loan.Amount.Value + intervalInterest;
        //     }
        //     return result;
        // }

        private ClientLoanModel UpdateLoan(ClientLoanModel model, bool deleteAll)
        {
            {
                using (var uow = new UnitOfWork(_dbContextProvider))
                {
                    uow.StartTransaction();

                    try
                    {
                        if (deleteAll)
                        {
                            var loan = uow.UClientLoanRepository
                                        .Search(x => x.Id == model.Id)
                                        .SingleOrDefault();

                            loan.EndDate = model.EndDate;
                            loan.Fees = model.Fees;
                            loan.Rebates = model.Rebates;

                            if (_configurationService.ValidateMembershipFee())
                            {
                                var subFeesList = model.LoanPayments.Where(x => x.SubFeesPayment != null).Select(x => x.SubFeesPayment).ToList();
                                loan.SubFees = (subFeesList.Count > 0) ? Convert.ToDecimal(subFeesList.Sum()) : 0;
                            }

                            loan.IdloanFrequency = model.FrequencyId;

                            var loanStatus = uow.ULoanStatusRepository
                                                .Search(x => x.Code == model.StatusCode)
                                                .SingleOrDefault();

                            loan.IdloanStatus = loanStatus.Id;
                            loan.StopLoanDate = model.StopLoanDate;
                            loan.BiMonthlyStartDay = model.BiMonthlyStartDay;
                            loan.BiMonthlyEndDay = model.BiMonthlyEndDay;
                            loan.IsPerceptech = model.IsPerceptech;

                            uow.UClientLoanRepository.Edit(loan);
                            uow.Save();

                            foreach (var payment in model.LoanPayments)
                            {
                                var paymentStatus = uow.UPaymentStatusRepository
                                                        .Search(x => x.Code == payment.StatusCode)
                                                        .SingleOrDefault();

                                // Update
                                if (payment.Id != 0)
                                {
                                    var existingPayment = uow.UClientLoanPaymentRepository
                                                            .Search(x => x.Id == payment.Id)
                                                            .SingleOrDefault();

                                    existingPayment.IdpaymentStatus = paymentStatus.Id;
                                    existingPayment.PaymentDate = payment.PaymentDate;
                                    existingPayment.Interest = payment.Interest;
                                    existingPayment.Capital = payment.Capital;
                                    existingPayment.Amount = payment.Amount;
                                    existingPayment.MaxAmount = payment.MaxAmount;
                                    existingPayment.Fees = payment.Fees;
                                    existingPayment.Balance = payment.Balance;
                                    existingPayment.IsNsf = payment.IsNSF;
                                    existingPayment.IsManual = payment.IsManual;
                                    existingPayment.IsRebate = payment.IsRebate;
                                    existingPayment.IsAmountChanged = payment.IsAmountChanged;
                                    existingPayment.IsDeferredFee = payment.IsDeferredFee;
                                    existingPayment.IsFrequencyChanged = payment.IsFrequencyChanged;
                                    existingPayment.IsDateChanged = payment.IsDateChanged;
                                    existingPayment.Frequency = payment.FrequencyDuration;
                                    existingPayment.IsDeleted = payment.IsDeleted;

                                    existingPayment.Nsffee = payment.NsfFee;
                                    existingPayment.DeferredFee = payment.DeferredFee;

                                    if (_configurationService.ValidateMembershipFee())
                                        existingPayment.SubFeesPayment = payment.SubFeesPayment;

                                    uow.UClientLoanPaymentRepository.Edit(existingPayment);
                                    uow.Save();

                                }
                                else
                                {
                                    var newPayment = new ClientLoanPayment()
                                    {
                                        IdclientLoan = loan.Id,
                                        IdpaymentStatus = paymentStatus.Id,
                                        PaymentDate = payment.PaymentDate,
                                        Interest = payment.Interest,
                                        Capital = payment.Capital,
                                        Amount = payment.Amount,
                                        MaxAmount = payment.MaxAmount,
                                        Fees = payment.Fees,
                                        Balance = payment.Balance,
                                        IsDeleted = payment.IsDeleted,
                                        IsNsf = payment.IsNSF,
                                        IsManual = payment.IsManual,
                                        IsRebate = payment.IsRebate,
                                        IsDeferredFee = payment.IsDeferredFee,
                                        IsAmountChanged = payment.IsAmountChanged,
                                        IsDateChanged = payment.IsDateChanged,
                                        IsFrequencyChanged = payment.IsFrequencyChanged,
                                        Frequency = payment.FrequencyDuration,

                                        Nsffee = _configurationService.NsfFees(),
                                        DeferredFee = _configurationService.DeferredFees(),
                                        TransactionFee = _configurationService.WithdrawalFees()
                                    };

                                    if (_configurationService.ValidateMembershipFee())
                                        newPayment.SubFeesPayment = payment.SubFeesPayment;

                                    uow.UClientLoanPaymentRepository.Add(newPayment);
                                    uow.Save();

                                    payment.Id = newPayment.Id;
                                }
                            }

                            // Remove all paymets that were deleted from payment list
                            model.LoanPayments.RemoveAll(x => x.IsDeleted == true);
                            return model;
                        }
                        else
                        {
                            // Delete all existing Loan Payments
                            foreach (var item in model.LoanPayments.Where(x => x.PaymentDate >= DateTime.Today && x.Id != 0 && x.CanEdit == true))
                            {
                                var payment = uow.UClientLoanPaymentRepository.Search(x => x.Id == item.Id).SingleOrDefault();
                                uow.UClientLoanPaymentRepository.Delete(payment);
                            }
                            uow.Save();

                            var loan = uow.UClientLoanRepository
                                        .Search(x => x.Id == model.Id)
                                        .SingleOrDefault();
                            var loanStatus = uow.ULoanStatusRepository
                                        .Search(x => x.Code == model.StatusCode)
                                        .SingleOrDefault();

                            loan.EndDate = model.EndDate;
                            loan.Fees = model.Fees;
                            loan.Rebates = model.Rebates;
                            loan.IdloanFrequency = model.FrequencyId;
                            loan.IdloanStatus = loanStatus.Id;
                            loan.StopLoanDate = model.StopLoanDate;
                            loan.IsPerceptech = model.IsPerceptech;

                            if (_configurationService.ValidateMembershipFee())
                            {
                                var subFeesList = model.LoanPayments.Where(x => x.SubFeesPayment != null).Select(x => x.SubFeesPayment).ToList();
                                loan.SubFees = (subFeesList.Count > 0) ? Convert.ToDecimal(subFeesList.Sum()) : 0;
                            }

                            uow.UClientLoanRepository.Edit(loan);
                            uow.Save();

                            // Skip Payments sent to Perceptech
                            // Skip Stopped Loan Payments
                            var _newPayments = model.LoanPayments.Where(x => ((DateTime)x.PaymentDate).Date >= DateTime.Today
                                                                            && (x.StatusCode != "STP" && x.StatusCode != "SLP"))
                                                                            .ToList();
                            foreach (var payment in _newPayments)
                            {
                                var paymentStatus = uow.UPaymentStatusRepository
                                            .Search(x => x.Code == payment.StatusCode)
                                            .SingleOrDefault();

                                var newPayment = new ClientLoanPayment()
                                {
                                    IdclientLoan = loan.Id,
                                    IdpaymentStatus = paymentStatus.Id,
                                    PaymentDate = payment.PaymentDate,
                                    Interest = payment.Interest,
                                    Capital = payment.Capital,
                                    Amount = payment.Amount,
                                    MaxAmount = payment.MaxAmount,
                                    Fees = payment.Fees,
                                    Balance = payment.Balance,
                                    IsNsf = payment.IsNSF,
                                    IsManual = payment.IsManual,
                                    IsRebate = payment.IsRebate,
                                    IsAmountChanged = payment.IsAmountChanged,
                                    IsDateChanged = payment.IsDateChanged,
                                    IsFrequencyChanged = payment.IsFrequencyChanged,
                                    Frequency = payment.FrequencyDuration,
                                    IsDeleted = payment.IsDeleted,

                                    TransactionFee = _configurationService.WithdrawalFees(),
                                    Nsffee = _configurationService.NsfFees(),
                                    DeferredFee = _configurationService.DeferredFees()
                                };

                                if (_configurationService.ValidateMembershipFee())
                                    newPayment.SubFeesPayment = payment.SubFeesPayment;

                                uow.UClientLoanPaymentRepository.Add(newPayment);
                                uow.Save();
                                payment.Id = newPayment.Id;
                            }

                            // Remove Deleted Payments
                            model.LoanPayments.RemoveAll(x => x.IsDeleted == true);
                            return model;
                        }
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
        }

        private void CalculateStatisticsLoanPDF(LoanPDFModel loan, List<LoanAccountBalancePaymentModel> newLoanPayments)
        {
            try
            {
                // Update Loan Last Payment date
                loan.EndDate = newLoanPayments.Last().PaymentDate;

                // Update Loan number of payments
                loan.NumberOfPayments = newLoanPayments
                                        .Where(x => x.StatusCode == "PEN" || x.StatusCode == "PAID" || x.StatusCode == "PC" || x.StatusCode == "STP")
                                        .Count();

                // If the Loan is stopped, take the Balance of the Stopped Payment Line
                var _stoppedLoan = newLoanPayments.Where(x => x.IsDeleted == false).ToList().LastOrDefault();
                if (_stoppedLoan.StatusCode == "SP")
                {
                    loan.TotalAmountRemaining = (decimal)_stoppedLoan.Balance;
                }
                // If the Loan is stopped and the last payment is a Payment, take the Balance of the Payment Line
                else if (_stoppedLoan.StatusCode == "SLP")
                {
                    loan.TotalAmountRemaining = (decimal)_stoppedLoan.Balance;
                }
                else
                {
                    loan.TotalAmountRemaining = newLoanPayments.Count != 0
                            ? newLoanPayments.Where(x => x.IsDeleted == false)
                                    .Where(x => x.StatusCode == "PEN" || x.StatusCode == "PC" || x.StatusCode == "PENI" || x.StatusCode == "STP")
                                    .Sum(x => (decimal)x.Amount)
                            : 0;
                }

                loan.DefaultPayments = newLoanPayments.Count != 0
                                            ? newLoanPayments
                                                    .Where(x => x.IsDeleted == false)
                                                    .Where(x => x.StatusCode == "NSF")
                                                    .Count()
                                            : 0;

                loan.Fees = newLoanPayments.Count != 0
                                ? newLoanPayments.Where(x => x.IsDeleted == false && x.StatusCode == "NSF")
                                                .Sum(x => x.Fees)
                                : 0;

                loan.Rebates = newLoanPayments.Count != 0
                                ? newLoanPayments.Where(x => x.IsDeleted == false)
                                                .Where(x => x.StatusCode == "REB")
                                                .Sum(x => x.Amount)
                                : 0;

                //
                Calculator = new LoanCalculator();
                Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                loan.CurrentDailyBalance = (decimal)Calculator.CalculateLoanPDFDailyBalanceOwing(loan);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CalculateStatistics(ClientLoanModel loan, List<ClientLoanPaymentModel> newLoanPayments, bool stopLoan)
        {
            try
            {
                // Update Loan Last Payment date
                loan.EndDate = newLoanPayments.Last().PaymentDate;

                // Update Loan number of payments
                loan.NumberOfPayments = newLoanPayments
                                        .Where(x => x.IsDeleted == false)
                                        .Where(x => x.StatusCode == "PEN" || x.StatusCode == "PC" || x.StatusCode == "PENI" || x.StatusCode == "STP")
                                        .Count();

                // If the Loan is stopped, take the Balance of the Stopped Payment Line
                var _stoppedLoan = newLoanPayments.Where(x => x.IsDeleted == false).ToList().LastOrDefault();
                if (_stoppedLoan.StatusCode == "SP")
                {
                    loan.TotalAmountRemaining = (decimal)_stoppedLoan.Balance;
                }
                // If the Loan is stopped and the last payment is a Payment, take the Balance of the Payment Line
                else if (_stoppedLoan.StatusCode == "SLP")
                {
                    loan.TotalAmountRemaining = (decimal)_stoppedLoan.Balance;
                }
                else
                {
                    loan.TotalAmountRemaining = newLoanPayments.Count != 0
                            ? newLoanPayments.Where(x => x.IsDeleted == false)
                                    .Where(x => x.StatusCode == "PEN" || x.StatusCode == "PC" || x.StatusCode == "PENI" || x.StatusCode == "STP")
                                    .Sum(x => (decimal)x.Amount)
                            : 0;
                }

                loan.DefaultPayments = newLoanPayments.Count != 0
                                            ? newLoanPayments
                                                    .Where(x => x.IsDeleted == false)
                                                    .Where(x => x.StatusCode == "NSF")
                                                    .Count()
                                            : 0;

                loan.Fees = newLoanPayments.Count != 0
                                ? newLoanPayments.Where(x => x.IsDeleted == false && x.StatusCode == "NSF")
                                                .Sum(x => x.NsfFee)
                                : 0;

                loan.Rebates = newLoanPayments.Count != 0
                                ? newLoanPayments.Where(x => x.IsDeleted == false)
                                                .Where(x => x.StatusCode == "REB")
                                                .Sum(x => x.Amount)
                                : 0;

                // Set new Loan Payments
                int index = 1;
                newLoanPayments.ForEach(x => x.PaymentNo = index++);
                loan.LoanPayments = newLoanPayments;

                //
                Calculator = new LoanCalculator();
                Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                //Calculator.InterestRate = Convert.ToDouble(InterestRateString.Value);

                loan.CurrentDailyBalance = (decimal)Calculator.CalculateLoanDailyBalanceOwing(loan, stopLoan);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateLoanSummaryInfo(ClientLoanModel loan, string language)
        {
            // Update Loan Frequencey
            using var context = _dbContextProvider.GetContext();
            var frequency = context.LoanFrequency.SingleOrDefault(x => x.Id == loan.FrequencyId);
            loan.Frequency = language == "FR" ? frequency.NameFr : frequency.NameEn;
        }

        public bool ValidateCanApplyNSF(DateTime? createdDate, string statusCode, int idclientLoanPayment, CreditBook_LMS_DEVContext context)
        {

            // If Payments are not sent to Perceptech, then NSF is always available
            if (!_configurationService.ValidatePaymentProvider())
                return true;

            // If Payments are not Mixed, then we need to validate if the Payment was sent to Perceptech or the Status of the Payment
            if (!_configurationService.ValidateMixedLoans())
            {
                if (context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == idclientLoanPayment) != null)
                    return false;

                if (statusCode == "PAID" || statusCode == "SP" || statusCode == "PR" || statusCode == "MP" || statusCode == "REB" || statusCode == "DP" || statusCode == "PENI")
                    return false;
            }

            // Loan Payments are Mixed. If the Loan was created before the Mixed Date, then NSF is always available
            if (_configurationService.ValidateMixedLoans())
            {
                var _mixedLoanDate = _configurationService.ValidateMixedLoanDate();
                if (createdDate < _mixedLoanDate)
                    return true;
            }

            // IF ALL OTHER CASES DON'T PASS
            // Validate if the Payment was sent to Perceptech or the Status of the Payment
            if (context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == idclientLoanPayment) != null)
                return false;

            if (statusCode == "PAID" || statusCode == "SP" || statusCode == "PR" || statusCode == "MP" || statusCode == "REB" || statusCode == "DP" || statusCode == "PENI")
                return false;

            return false;
        }

        public bool ValidateCanEditPayment(string statusCode, int idclientLoanPayment, DateTime paymentDate, CreditBook_LMS_DEVContext context)
        {

            var perceptechPayment = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == idclientLoanPayment);

            if (perceptechPayment != null)
                return false;

            if (/*paymentDate > DateTime.Today &&*/ (statusCode == "PAID" || statusCode == "SP" || statusCode == "PR" || statusCode == "MP" || statusCode == "REB" || statusCode == "DP" || statusCode == "PENI" || statusCode == "SLP"))
                return false;

            return true;
        }

        public bool ValidateLoanIsMixed(DateTime? createdDate)
        {
            if (!_configurationService.ValidateMixedLoans())
                return false;

            var _mixedLoanDate = _configurationService.ValidateMixedLoanDate();
            if (createdDate < _mixedLoanDate)
                return true;

            return false;
        }

        public ClientLoanModel ValidatePerceptechStatus(ClientLoanModel loan, string language)
        {
            if (loan.StatusCode == "IP" || loan.StatusCode == "PA" || loan.StatusCode == "LD")
            {
                using var context = _dbContextProvider.GetContext();
                var paymentStatusPerceptech = context.PaymentStatus
                                                .Where(x => x.Code == "STP")
                                                .SingleOrDefault();
                loan.LoanPayments.Where(x => x.SentToPerceptech == true && x.StatusCode != "NSF")
                                .ToList()
                                // .Where(x => Math.Abs((((DateTime)x.PaymentDate).Date - DateTime.Today).Days) < 5)
                                // .ToList()
                                .ForEach(x => { x.Status = language == "FR" ? paymentStatusPerceptech.NameFr : paymentStatusPerceptech.NameEn; x.StatusCode = "STP"; });
                return loan;
            }

            return loan;
        }

        public ClientLoanModel ValidatePaymentPaidStatus(ClientLoanModel loan, string language)
        {
            if (loan.StatusCode == "IP" || loan.StatusCode == "PA" || loan.StatusCode == "LD" || loan.StatusCode == "LR")
            {
                using var context = _dbContextProvider.GetContext();
                var paymentStatus = context.PaymentStatus
                                    .Where(x => x.Code == "PAID")
                                    .SingleOrDefault();

                loan.LoanPayments.Where(x => x.PaymentDate < DateTime.Today)
                                    .Where(x => (Math.Abs((((DateTime)x.PaymentDate).Date - DateTime.Today).Days) >= 5)
                                                && (x.StatusCode == "PEN" || x.StatusCode == "STP" || x.StatusCode == "PAID" || x.StatusCode == "PC"))
                                    .ToList()
                                    .ForEach(x => { x.Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn; x.StatusCode = "PAID"; x.CanEdit = false; x.CanNSF = false; });
                return loan;
            }

            return loan;
        }

        public List<ClientLoanPaymentModel> RemoveUnwantedPayments(List<ClientLoanPaymentModel> loanPayments)
        {
            // Exclude Deferred Payment fees
            var unwantedPayments = loanPayments.Where(x => x.IsDeferredFee == true || x.StatusCode == "DP" || x.StatusCode == "PR" || x.StatusCode == "SP")
                                                        .ToList();

            loanPayments.RemoveAll(x => x.StatusCode == "DP");
            loanPayments.RemoveAll(x => x.StatusCode == "PR");
            loanPayments.RemoveAll(x => x.StatusCode == "SP");
            loanPayments.RemoveAll(x => x.IsDeferredFee == true);
            return unwantedPayments;
        }

        public ClientLoanModel AddUnwantedPayments(ClientLoanModel loan, List<ClientLoanPaymentModel> unwantedPayments)
        {
            foreach (var unwantedPayment in unwantedPayments)
            {
                //removedDeferredPaymentFee.Balance = 0;
                loan.LoanPayments.Add(unwantedPayment);
            }
            loan.LoanPayments = loan.LoanPayments.OrderBy(x => x.PaymentDate).ThenBy(x => x.Id).ToList();
            int index = 1;
            loan.LoanPayments.ForEach(x => x.PaymentNo = index++);
            return loan;
        }

        public ClientLoanModel ValidateLoanIsPaidOff(ClientLoanModel loan, string language)
        {
            // Check if the last payment is a Manual Payment that clears the balance of the Loan
            var lastPayment = loan.LoanPayments.Last();

            if ((lastPayment.StatusCode == "MP" || lastPayment.StatusCode == "REB") && lastPayment.Balance == 0)
            {
                if(lastPayment.PaymentDate <= DateTime.Today)
                {
                    using var context = _dbContextProvider.GetContext();
                    loan.StatusCode = context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.Code).SingleOrDefault();
                    loan.Status = language == "FR" ?
                        context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.NameFr).SingleOrDefault() :
                        context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.NameEn).SingleOrDefault();

                    loan.CurrentDailyBalance = 0;
                    loan.TotalAmountRemaining = 0;
                    return loan;
                }
            }

            if (loan.TotalAmountRemaining <= 0 && loan.IsLoanStopped != true && loan.TotalAmountRemaining != -999)
            {
                using var context = _dbContextProvider.GetContext();
                loan.StatusCode = context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.Code).SingleOrDefault();
                loan.Status = language == "FR" ?
                    context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.NameFr).SingleOrDefault() :
                    context.LoanStatus.Where(x => x.Code == "LR").Select(x => x.NameEn).SingleOrDefault();
                return loan;
            }

            return loan;
        }

        #endregion

        public DigitalSignatureModel ContractSigned(string digitalUrl, int loanId)
        {
            // Verify if Contract was already signed
            using var context = _dbContextProvider.GetContext();
            var digitalSignatureInfo = (from cl in context.ClientLoan
                                        join clc in context.ClientLoanContractSignature on cl.Id equals clc.ClientLoanId
                                        where cl.Id == loanId
                                        select new
                                        {
                                            Id = clc.Id,
                                            Guid = clc.Guid,
                                            SignedDate = String.Format("{0:yyyy-MM-dd HH:mm:ss}", clc.TimeStamp),
                                            IsSigned = clc.IsSigned
                                        }).OrderByDescending(x => x.Id).ToList();

            if (digitalSignatureInfo.Count() != 0)
            {
                // Take last signature signed
                var lastSigned = digitalSignatureInfo.Where(x => x.IsSigned == true).LastOrDefault();
                if (lastSigned != null)
                {
                    return new DigitalSignatureModel()
                    {
                        SignedDate = lastSigned.SignedDate,
                        Url = $"{digitalUrl}{lastSigned.Guid}",
                        IsSigned = lastSigned.IsSigned
                    };
                }
            }

            return null;
        }

        public ClientModel GetClientLoans(int idClient, ClientModel model, CreditBook_LMS_DEVContext context, string language, string digitalBaseUrl)
        {
            try
            {
                var allowCustomAmounts = context.LoanerConfiguration
                                        .Where(x => x.Key == "ALLOW_CUSTOM_AMOUNTS")
                                        .SingleOrDefault();

                model.IsPersonalized = Convert.ToBoolean(allowCustomAmounts.Value);

                // Validate if Lender is setup for Automated Payments
                if (_configurationService.ValidatePaymentProvider())
                {
                    // Retreive the last payment date that was sent to Perceptech
                    var lastPaymentDayList = context.PerceptechSubmitLog.Select(x => x)
                                        .ToList();

                    if (lastPaymentDayList.Count() == 0)
                    {
                        model.LastPaymentDay = DateTime.Today.AddDays(2).Day;
                    }
                    else
                    {
                        model.LastPaymentDay = ((DateTime)lastPaymentDayList.Last().PaymentDate).Day;
                    }
                }
                else
                {
                    model.LastPaymentDay = DateTime.Today.AddDays(2).Day;
                }

                // Laod new loan configuration
                var frequencies = context.LoanFrequency.Select(x => new ClientLoanFrequencyModel()
                {
                    Id = x.Id,
                    Name = language == "FR" ? x.NameFr : x.NameEn
                }).ToList();

                model.LoanFrequencies = frequencies;

                //
                var loanAmounts = (from la in context.LoanAmount
                                   join lc in context.LoanConfiguration on la.Id equals lc.IdloanAmount
                                   select new ClientLoanAmountModel()
                                   {
                                       Id = la.Id,
                                       Amount = la.Amount
                                   }).Distinct().OrderBy(x => x.Amount).ToList();

                model.LoanAmounts = loanAmounts;

                var loanConfiguration = context.LoanConfiguration
                                                .Select(x => new ClientNewLoanConfigurationModel()
                                                {
                                                    LoanAmountId = x.IdloanAmount,
                                                    //LoanAmount = x.IdloanAmountNavigation.Amount,
                                                    LoanFrequencyId = x.IdloanFrequency,
                                                    LoanFrequencyName = language == "FR" ? x.IdloanFrequencyNavigation.NameFr : x.IdloanFrequencyNavigation.NameEn,
                                                    NumberOfPayments = (int)x.TotalPayments
                                                }).ToList();

                model.NewLoanConfiguration = loanConfiguration;

                var clientLoans = context.ClientLoan
                                        .Where(x => x.Idclient == idClient
                                        && x.IsDeleted == false)
                                        .Select(y => new ClientLoanModel()
                                        {
                                            Id = y.Id,
                                            RequestNo = y.Idrequest != null ? (int)y.Idrequest : (int?)null,
                                            LoanNumber = y.LoanNumber,
                                            Status = language == "FR" ? y.IdloanStatusNavigation.NameFr : y.IdloanStatusNavigation.NameEn,
                                            StatusCode = y.IdloanStatusNavigation.Code,
                                            FrequencyDuration = y.IdloanFrequencyNavigation.Frequency,
                                            Frequency = language == "FR" ? y.IdloanFrequencyNavigation.NameFr : y.IdloanFrequencyNavigation.NameEn,
                                            FrequencyId = y.IdloanFrequencyNavigation.Id,
                                            BrokerageFee = y.BrokerageFees,
                                            Amount = y.Amount,
                                            InitialNumberOfPayments = y.InitialNumberOfPayments,
                                            BiMonthlyStartDay = y.BiMonthlyStartDay,
                                            BiMonthlyEndDay = y.BiMonthlyEndDay,
                                            StartDate = y.StartDate,
                                            InterestAmount = y.InterestAmount,
                                            InterestRate = y.InterestRate,
                                            CreatedDate = y.CreatedDate,
                                            OriginalPaymentAmount = y.OriginalPaymentAmount,
                                            IsLoanStopped = y.IdloanStatusNavigation.Code == "LS" ? true : false,
                                            StopLoanDate = y.StopLoanDate,
                                            IsPerceptech = y.IsPerceptech == null ? false : (bool)y.IsPerceptech,
                                            PaymentProvider = _configurationService.ValidatePaymentProvider(),

                                            SubFees = y.SubFees ?? null,
                                            OriginalSubFeesPrice = y.OriginalSubFeesPrice ?? null,
                                            ChargeMembershipFee = _configurationService.ValidateMembershipFee()
                                        }).ToList();

                foreach (var loan in clientLoans)
                {
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);

                    var contract = ContractSigned(digitalBaseUrl, loan.Id);
                    if (contract != null)
                    {
                        loan.ContractSigned = contract.IsSigned;
                        loan.ContractSignedDate = contract.SignedDate;
                        loan.DigitalUrl = contract.Url;
                    }

                    var payments = context.ClientLoanPayment
                                    .Where(x => x.IdclientLoan == loan.Id && x.IsDeleted == false)
                                    //.Select((x, index) => new ClientLoanPaymentModel()
                                    .Select(x => new ClientLoanPaymentModel()
                                    {
                                        Id = x.Id,
                                        LoanId = x.IdclientLoanNavigation.Id,
                                        Status = language == "FR" ? x.IdpaymentStatusNavigation.NameFr : x.IdpaymentStatusNavigation.NameEn,
                                        StatusCode = x.IdpaymentStatusNavigation.Code,
                                        Interest = x.Interest,
                                        Balance = x.Balance,
                                        Capital = x.Capital,
                                        Amount = x.Amount,
                                        MaxAmount = x.MaxAmount,
                                        Fees = x.Fees,
                                        PaymentDate = x.PaymentDate,
                                        IsNSF = x.IsNsf,
                                        IsManual = x.IsManual,
                                        IsRebate = x.IsRebate,
                                        IsAmountChanged = x.IsAmountChanged,
                                        IsDateChanged = x.IsDateChanged,
                                        IsFrequencyChanged = x.IsFrequencyChanged,
                                        IsDeferredFee = x.IsDeferredFee,
                                        FrequencyDuration = x.Frequency,
                                        SentToPerceptech = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == x.Id) != null ? true : false,
                                        IsDeleted = x.IsDeleted,
                                        Description = x.Description,
                                        IsAmountChangedForAll = false,

                                        SubFeesPayment = x.SubFeesPayment ?? null,

                                        NsfFee = (decimal)x.Nsffee,
                                        DeferredFee = (decimal)x.DeferredFee,
                                        WithdrawalFee = (decimal)x.TransactionFee
                                    })
                                    .OrderBy(x => x.PaymentDate).ThenBy(x => x.Id)
                                    .ToList();

                    // Check to see if Loan can be deleted
                    loan.CanDelete = payments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)payments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Check to see if Payment was sent to Perceptech
                    loan.LoanPayments = payments;
                    loan.LoanPayments = ValidatePerceptechStatus(loan, language).LoanPayments;

                    // Check to see if Payment date has passed and update Status to PAID ony for Loans that are IN PROGRESS
                    loan.LoanPayments = ValidatePaymentPaidStatus(loan, language).LoanPayments;

                    // Validate CanNSF and CanEdit after Perceptech and PaidStatus
                    foreach (var payment in payments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    CalculateStatistics(loan, payments, false);

                    var _loan = ValidateLoanIsPaidOff(loan, language);

                    // Update
                    UpdateLoan(_loan, true);
                }

                model.Loans = clientLoans;

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel NewLoan(NewLoanModel model, string language)
        {
            try
            {
                int idLoanStatus;
                int idLoanPaymentStatus;
                bool isPersolnalized;
                Loan = new ClientLoanModel();
                Loan.RequestNo = model.RequestNo;

                using (var context = _dbContextProvider.GetContext())
                {
                    var frequency = context.LoanFrequency.Where(x => x.Id == model.FrequencyId).SingleOrDefault();
                    var loanerConfiguration = context.LoanerConfiguration.Select(x => x);

                    isPersolnalized = Convert.ToBoolean(loanerConfiguration
                                                            .Where(x => x.Key == "ALLOW_CUSTOM_AMOUNTS")
                                                            .SingleOrDefault()
                                                            .Value);

                    var loanAmount = context.LoanAmount
                                            .Where(x => x.Id == model.LoanAmountId)
                                            .SingleOrDefault();


                    var interestRate = loanerConfiguration
                                        .Where(x => x.Key == "INTEREST_RATE")
                                        .SingleOrDefault()
                                        .Value;

                    var brokerageRate = loanerConfiguration
                                            .Where(x => x.Key == "BROKERAGE_RATE")
                                            .SingleOrDefault()
                                            .Value;
                    var maxInterestRate = loanerConfiguration
                                            .Where(x => x.Key == "MAX_BROKERAGE_RATE")
                                            .SingleOrDefault()
                                            .Value;

                    if (isPersolnalized)
                    {
                        Calculator = new LoanCalculator((double)model.LoanManualAmount, (int)frequency.Frequency, 0, model.NumberOfPayments, Convert.ToDouble(interestRate), model.NextPayDate, model.StartDay, model.EndDay, 0, Convert.ToDouble(maxInterestRate), model.Rebate);
                        Calculator.CalculateNewLoanAnualInterest();
                    }
                    else
                    {
                        Calculator = new LoanCalculator((double)loanAmount.Amount, (int)frequency.Frequency, 0, model.NumberOfPayments, Convert.ToDouble(interestRate), model.NextPayDate, model.StartDay, model.EndDay, 0, Convert.ToDouble(maxInterestRate), model.Rebate);
                        Calculator.CalculateNewLoanAnualInterest();
                    }

                    //Recalculate Payments by daily interest
                    Calculator.CalculateNewLoanDailyInterest();

                    var loanStatus = context.LoanStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();
                    idLoanStatus = loanStatus.Id;

                    var paymentStatus = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();
                    idLoanPaymentStatus = paymentStatus.Id;

                    var loan = context.ClientLoan
                                        .Where(x => x.Idclient == model.ClientId)
                                        //.OrderByDescending(x => x.Id)
                                        .ToList();

                    var payments = Calculator.ClientPayments
                                             .OrderBy(x => x.PaymentDate);

                    //
                    Calculator.ClientPayments.ForEach(x => { x.Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn; x.IsDeleted = false; });

                    Loan.InterestRate = Convert.ToDecimal(interestRate);
                    Loan.BrokerageRate = Convert.ToDecimal(brokerageRate);
                    Loan.MaxInterestRate = Convert.ToDecimal(maxInterestRate);

                    Loan.CanDelete = true;
                    Loan.CreatedDate = (DateTime)DateTime.Now;
                    Loan.NumberOfPayments = model.NumberOfPayments;
                    Loan.InitialNumberOfPayments = model.NumberOfPayments;
                    Loan.Amount = isPersolnalized ? model.LoanManualAmount : loanAmount.Amount;
                    Loan.Status = language == "FR" ? loanStatus.NameFr : loanStatus.NameEn;
                    Loan.StatusCode = loanStatus.Code;
                    Loan.StartDate = payments.First().PaymentDate; // First payment date
                    Loan.OriginalPaymentAmount = payments.First().Amount;
                    Loan.BrokerageFee = (decimal)Calculator.BrokerageFees;
                    Loan.EndDate = payments.Last().PaymentDate; // Last payment date
                    Loan.FrequencyId = frequency.Id;
                    Loan.Frequency = language == "FR" ? frequency.NameFr : frequency.NameEn;
                    Loan.FrequencyDuration = frequency.Frequency;
                    Loan.Fees = 0;
                    Loan.Rebates = model.Rebate;
                    Loan.LoanNumber = loan.Count == 0
                                        ? 1
                                        : loan.OrderBy(x => x.Id)
                                              .Last()
                                              .LoanNumber + 1;

                    Loan.BiMonthlyStartDay = model.StartDay;
                    Loan.BiMonthlyEndDay = model.EndDay;
                    Loan.InterestAmount = (decimal)Calculator.InterestAmount;

                    Loan.TotalAmountRemaining = payments.ToList().Count != 0
                            ? payments.Where(x => x.StatusCode == "PEN" && x.PaymentDate >= DateTime.Today)
                                    .Sum(x => (decimal)x.Amount)
                            : 0;

                    Loan.DefaultPayments = payments.ToList().Count != 0
                                                ? payments.Where(x => x.StatusCode == "NSF")
                                                        .Count()
                                                : 0;
                    Loan.ChargeMembershipFee = _configurationService.ValidateMembershipFee();

                    //payments.ToList().ForEach(x => x.CanEdit = ((DateTime)x.PaymentDate).Date >= DateTime.Today.AddDays(2) && true);
                    payments.ToList().ForEach(x => x.CanEdit = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == x.Id) != null
                                                        ? false
                                                        : (x.StatusCode == "NSF" || x.StatusCode == "SP" || x.StatusCode == "PR" || x.StatusCode == "MP" || x.StatusCode == "REB")
                                                            ? false
                                                            : ((DateTime)x.PaymentDate).Date >= DateTime.Today.AddDays(2) && true);

                    payments.ToList().ForEach(x => x.CanNSF = true);

                    Loan.LoanPayments = payments.ToList();
                    Loan.CurrentDailyBalance = (decimal)Calculator.CalculateLoanDailyBalanceOwing(Loan, false);

                    // Calculate Membership Fees
                    NewLoanMembershipFees(payments.ToList());
                }

                // Save to database
                if (SaveNewLoan(Loan, model, idLoanStatus, idLoanPaymentStatus, isPersolnalized))
                {
                    int index = 1;
                    Loan.LoanPayments.ForEach(x => x.PaymentNo = index++);

                    // Add new Loan to Deposit table
                    using (var context = _dbContextProvider.GetContext())
                    {
                        var depositStatus = context.DepositStatus
                                                    .Where(x => x.Code == "WAITING")
                                                    .SingleOrDefault();

                        context.Deposit.Add(new Deposit()
                        {
                            StatusId = depositStatus.Id,
                            ClientLoanId = Loan.Id
                        });
                        context.SaveChanges();
                    }

                    return Loan;
                }

                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool SaveNewLoan(ClientLoanModel model, NewLoanModel data, int idLoanStatus, int idLoanPaymentStatus, bool isPersonalized)
        {
            {
                using (var uow = new UnitOfWork(_dbContextProvider))
                {
                    uow.StartTransaction();

                    try
                    {
                        var loan = new ClientLoan()
                        {
                            Idclient = data.ClientId,
                            IdloanStatus = idLoanStatus,
                            IdloanFrequency = data.FrequencyId,
                            Idrequest = data.RequestNo,
                            IdloanAmount = data.LoanAmountId != 0 ? data.LoanAmountId : (Int32?)null,
                            Amount = model.Amount,
                            StartDate = model.StartDate,
                            EndDate = model.EndDate,
                            BrokerageFees = model.BrokerageFee,
                            InitialNumberOfPayments = model.InitialNumberOfPayments,
                            LoanNumber = model.LoanNumber,
                            IsPersonalized = isPersonalized,
                            InterestAmount = model.InterestAmount,
                            Fees = model.Fees,
                            Rebates = model.Rebates,
                            CreatedDate = model.CreatedDate,
                            OriginalPaymentAmount = model.OriginalPaymentAmount,
                            BiMonthlyStartDay = model.BiMonthlyStartDay,
                            BiMonthlyEndDay = model.BiMonthlyEndDay,
                            InterestRate = model.InterestRate,
                            BrokerageRate = model.BrokerageRate,
                            MaxInterestRate = model.MaxInterestRate,

                            OriginalSubFeesPrice = model.OriginalSubFeesPrice,
                            SubFees = model.SubFees
                        };

                        uow.UClientLoanRepository.Add(loan);
                        uow.Save();
                        model.Id = loan.Id;

                        // Save a copy of the original Laon information for Debugging
                        var loanHistory = new ClientLoanHistory()
                        {
                            Idclient = data.ClientId,
                            IdloanFrequency = data.FrequencyId,
                            Idrequest = data.RequestNo,
                            IdloanAmount = data.LoanAmountId != 0 ? data.LoanAmountId : (Int32?)null,
                            Amount = model.Amount,
                            StartDate = model.StartDate,
                            EndDate = model.EndDate,
                            LoanNumber = model.LoanNumber,
                            IsPersonalized = isPersonalized,
                            InterestAmount = model.InterestAmount,
                            Fees = model.Fees,
                            CreatedDate = model.CreatedDate,
                            Rebates = model.Rebates,
                            OriginalPaymentAmount = model.OriginalPaymentAmount,
                            BrokerageFees = model.BrokerageFee,
                            BiMonthlyStartDay = model.BiMonthlyStartDay,
                            BiMonthlyEndDay = model.BiMonthlyEndDay
                        };
                        uow._context.ClientLoanHistory.Add(loanHistory);
                        uow._context.SaveChanges();

                        foreach (var payment in model.LoanPayments)
                        {
                            payment.IsNSF = false;
                            payment.IsRebate = false;
                            payment.IsManual = false;
                            payment.IsAmountChanged = false;
                            payment.IsFrequencyChanged = false;
                            payment.IsDateChanged = false;
                            payment.IsDeferredFee = false;

                            var newPayment = new ClientLoanPayment()
                            {
                                IdclientLoan = loan.Id,
                                IdpaymentStatus = idLoanPaymentStatus,
                                PaymentDate = payment.PaymentDate,
                                Interest = payment.Interest,
                                Capital = payment.Capital,
                                Amount = payment.Amount,
                                MaxAmount = payment.MaxAmount,
                                Fees = payment.Fees,
                                Balance = payment.Balance,
                                IsDeleted = payment.IsDeleted,
                                IsNsf = payment.IsNSF,
                                IsManual = payment.IsManual,
                                IsRebate = payment.IsRebate,
                                IsAmountChanged = payment.IsAmountChanged,
                                IsDateChanged = payment.IsDateChanged,
                                IsFrequencyChanged = payment.IsFrequencyChanged,
                                Frequency = model.FrequencyDuration,
                                SubFeesPayment = payment.SubFeesPayment,
                                Nsffee = _configurationService.NsfFees(),
                                DeferredFee = _configurationService.DeferredFees(),
                                TransactionFee = _configurationService.WithdrawalFees()
                            };

                            uow.UClientLoanPaymentRepository.Add(newPayment);
                            uow.Save();

                            payment.Id = newPayment.Id;
                        }

                        return true;
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public ClientLoanModel LoanManualNSFPayment(LoanGenericPaymentModel model, string language, bool perceptech)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Retreive current NSF fee
                    var nsfFee = _configurationService.NsfFees();

                    // Set the Payment selected status to NSF
                    var loanPayment = loanPayments.Where(x => x.Id == model.PaymentId).SingleOrDefault();
                    loanPayment.NsfFee = nsfFee;

                    var paymentStatusNSF = context.PaymentStatus
                                            .Where(x => x.Code == "NSF")
                                            .SingleOrDefault();

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                                .SingleOrDefault()
                                                .Status = language == "FR" ? paymentStatusNSF.NameFr : paymentStatusNSF.NameEn;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .StatusCode = "NSF";

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .IsNSF = true;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .CanNSF = true;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .CanEdit = false;

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, (DateTime)loanPayment.PaymentDate, nsfFee != null ? nsfFee : 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/ false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    var paymentStatusPending = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);


                    // Add Loan Payment to Collections only from LMS
                    if (!perceptech)
                    {
                        // Check to see if they are already in Collections
                        var alreadyInCollections = context.ClientLoanCollection
                                                         .Where(x => x.IdclientLoan == loan.Id && x.IdcollectionStatus == 0)
                                                         .FirstOrDefault();

                        if (alreadyInCollections != null)
                        {
                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                    .Where(x => x.Code == "MANUAL_NSF")
                                                                    .FirstOrDefault()
                                                                    .Id;
                            context.ClientLoanCollection.Update(alreadyInCollections);
                            context.SaveChanges();
                        }
                        else
                        {
                            var col1 = new ClientLoanCollection()
                            {
                                IdclientLoan = loan.Id,
                                IdcollectionStatus = context.CollectionStatus
                                            .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                            .SingleOrDefault()
                                            .Id,
                                IdcollectionDegree = context.CollectionDegree
                                            .Where(x => x.Code == "MANUAL_NSF")
                                            .SingleOrDefault()
                                            .Id,
                                CreatedDateTime = DateTime.Now
                            };

                            context.ClientLoanCollection.Add(col1);
                            context.SaveChanges();
                        }

                        // AGENT WILL SET THE CLIENTS STATUS MANUALLY
                        // Set Client Status to Collections
                        // var client = context.Client.SingleOrDefault(c => c.Id == loan.ClientId);
                        // var clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTIONS");
                        // client.IdclientStatus = clientStatus.Id;
                        // context.Client.Update(client);
                        // context.SaveChanges();
                    }

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    //
                    return UpdateLoan(loan, true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel LoanRemoveManualNSFPayment(LoanGenericPaymentModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                : true;

                    var loanPayment = loanPayments.Where(x => x.Id == model.PaymentId).SingleOrDefault();

                    // Set the status of NSF Payment to remove to Pending
                    var paymentStatus = context.PaymentStatus
                                        .Where(x => x.Code == "PEN")
                                        .SingleOrDefault();

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                                .SingleOrDefault()
                                                .Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .StatusCode = "PEN";

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .IsNSF = false;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .CanNSF = true;

                    loanPayments.Where(x => x.Id == loanPayment.Id)
                                    .SingleOrDefault()
                                    .CanEdit = true;

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, (DateTime)loanPayment.PaymentDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/  false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn; });

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    //
                    return UpdateLoan(loan, false);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel LoanAddRebate(LoanRebateModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Add new payment line for Rebate
                    var paymentStatusRebate = context.PaymentStatus
                                                  .Where(x => x.Code == "REB")
                                                  .SingleOrDefault();

                    var newRebate = new ClientLoanPaymentModel()
                    {
                        Amount = model.RebateAmount,
                        PaymentDate = model.EffectiveDate,
                        IsRebate = true,
                        MaxAmount = 0,
                        IsNSF = false,
                        IsManual = false,
                        IsAmountChanged = false,
                        IsDateChanged = false,
                        IsDeleted = false,
                        IsFrequencyChanged = false,
                        FrequencyDuration = loan.FrequencyDuration,
                        CanEdit = false,
                        CanNSF = false,
                        StatusCode = "REB",
                        Status = language == "FR" ? paymentStatusRebate.NameFr : paymentStatusRebate.NameEn
                    };

                    // Add Rebate to Loan
                    loan.Rebates = loan.Rebates + model.RebateAmount;

                    // Existing logic
                    if (loanPayments.LastOrDefault().PaymentDate == newRebate.PaymentDate)
                    {
                        newRebate.Index = 1000; //Hack to make sure rebate is before planned payment if on the same date

                        loanPayments.Add(newRebate);
                        loanPayments = loanPayments.OrderBy(p => p.PaymentDate)
                                                    .ThenByDescending(p => p.Index).ToList();
                    }
                    else
                    {
                        loanPayments.Add(newRebate);
                        loanPayments = loanPayments.OrderBy(p => p.PaymentDate).ToList();
                    }

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, model.EffectiveDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/ false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    var paymentStatusPending = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    //
                    return UpdateLoan(loan, true);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel LoanManualPayment(LoanPaymentModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Add new payment line for Rebate
                    var paymentStatusManual = context.PaymentStatus
                                            .Where(x => x.Code == "MP")
                                            .SingleOrDefault();

                    var lastPayment = loanPayments.OrderBy(x => x.PaymentDate).ThenBy(x => x.Id).LastOrDefault();

                    var manualPayment = new ClientLoanPaymentModel()
                    {
                        MaxAmount = lastPayment != null ? lastPayment.MaxAmount : loan.OriginalPaymentAmount,
                        Amount = model.PaymentAmount,
                        PaymentDate = model.EffectiveDate,
                        IsRebate = false,
                        IsNSF = false,
                        IsManual = true,
                        IsAmountChanged = false,
                        IsFrequencyChanged = false,
                        IsDateChanged = false,
                        IsDeleted = false,
                        StatusCode = "MP",
                        Status = language == "FR" ? paymentStatusManual.NameFr : paymentStatusManual.NameEn,
                        CanEdit = false,
                        CanNSF = false
                    };

                    // Existing logic
                    if (loanPayments.LastOrDefault().PaymentDate == manualPayment.PaymentDate)
                    {
                        manualPayment.Index = 1000; //Hack to make sure manual is before planned payment if on the same date

                        loanPayments.Add(manualPayment);
                        loanPayments = loanPayments.OrderBy(p => p.PaymentDate)
                                                    .ThenByDescending(p => p.Index).ToList();
                    }
                    else
                    {
                        loanPayments.Add(manualPayment);
                        loanPayments = loanPayments.OrderBy(p => p.PaymentDate).ToList();
                    }

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, model.EffectiveDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/  false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    var paymentStatusPending = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    // Validate if Loan is Paid Off
                    loan = ValidateLoanIsPaidOff(loan, language);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    //
                    return UpdateLoan(loan, true);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel LoanRemoveRebateAndManualPayment(LoanGenericPaymentModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Remove the Rebate that was selected
                    var rebatePayment = loanPayments.Where(x => x.Id == model.PaymentId).SingleOrDefault();
                    loanPayments.Remove(rebatePayment);

                    // Update database
                    var existingRebatePayment = context.ClientLoanPayment.SingleOrDefault(x => x.Id == model.PaymentId);
                    existingRebatePayment.IsDeleted = true;
                    context.ClientLoanPayment.Update(existingRebatePayment);
                    context.SaveChanges();

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, (DateTime)rebatePayment.PaymentDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/  false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    var paymentStatus = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn; });

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    //
                    return UpdateLoan(loan, false);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel LoanModifyPayment(LoanModifyPaymentModel model, bool saveToDatabse, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    var loanPayment = loanPayments.Where(x => x.Id == model.PaymentId).SingleOrDefault();

                    // Modify payment amount
                    var paymentStatusModified = context.PaymentStatus
                                                    .Where(x => x.Code == "PC")
                                                    .SingleOrDefault();

                    loanPayment.Status = language == "FR" ? paymentStatusModified.NameFr : paymentStatusModified.NameEn;
                    loanPayment.StatusCode = "PC";
                    loanPayment.IsAmountChanged = true;
                    loanPayment.IsAmountChangedForAll = false;
                    loanPayment.IsDateChanged = false;
                    //
                    Calculator = new LoanCalculator();
                    List<ClientLoanPaymentModel> newLoanPayments = new List<ClientLoanPaymentModel>();
                    if (model.NewPaymentDate.Date != loanPayment.PaymentDate)
                    {
                        //dateChanged = true;
                        loanPayment.IsDateChanged = true;
                        loanPayment.PaymentDate = model.NewPaymentDate;
                        loanPayment.NewAmount = loanPayment.Amount;
                    }

                    if (model.NewPaymentAmount != loanPayment.Amount)
                    {
                        loanPayment.NewAmount = model.NewPaymentAmount;
                    }

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, (DateTime)loanPayment.PaymentDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, false, false, /*false,*/  false, false);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments, loan.OriginalSubFeesPrice);
                    }

                    // For all new payments, update Status
                    var paymentStatusPending = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                    // Recalculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    // Validate if Payments can be edited after the change
                    loan.LoanPayments.ForEach(x => x.CanEdit = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == x.Id) != null
                                                        ? false
                                                        : (x.StatusCode == "NSF" || x.StatusCode == "SP" || x.StatusCode == "PR" || x.StatusCode == "MP" || x.StatusCode == "REB")
                                                            ? false
                                                            : ((DateTime)x.PaymentDate).Date >= DateTime.Today.AddDays(2) && true);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    if (saveToDatabse)
                    {
                        return UpdateLoan(loan, false);
                    }

                    return loan;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel ModifyLoan(LoanModifyModel model, string language)
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);
                    loan.LoanPayments = loanPayments;

                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Check if we are stopping the loan
                    if (model.IsLoanStop)
                    {
                        CalculateStatistics(loan, loanPayments, true);

                        // Update loan
                        var loanStatusStopped = context.LoanStatus
                                                .Where(x => x.Code == "LS")
                                                .SingleOrDefault();

                        // Check to see if the Loan is already in Payment Agreement
                        // Select all payments to delete
                        //if (loan.StatusCode == "PA" && !loan.IsPerceptech)
                        //{
                        loanPayments.Where(x => x.PaymentDate >= model.StopPaymentDate.Date
                                                            && x.StatusCode == "PENI")
                                                            .ToList()
                                                            .ForEach(x => x.IsDeleted = true);
                        // }
                        // else
                        // {
                        loanPayments.Where(x => x.PaymentDate >= model.StopPaymentDate.Date
                                                            && x.StatusCode == "PEN")
                                                            .ToList()
                                                            .ForEach(x => x.IsDeleted = true);
                        //}

                        loan.IsLoanStopped = true;
                        loan.StatusCode = "LS";
                        loan.Status = language == "FR" ? loanStatusStopped.NameFr : loanStatusStopped.NameEn;
                        loan.StopLoanDate = model.StopPaymentDate;

                        // Add a payment line indicating that the loan was stopped. All values are at Zero
                        var paymentStatusStopped = context.PaymentStatus
                                                    .Where(x => x.Code == "SP")
                                                    .SingleOrDefault();

                        var stopPayment = new ClientLoanPaymentModel()
                        {
                            FrequencyDuration = loan.FrequencyDuration,
                            Amount = 0,
                            PaymentDate = model.StopPaymentDate,
                            IsRebate = false,
                            MaxAmount = loan.OriginalPaymentAmount,
                            Interest = 0,
                            Capital = 0,
                            Balance = loan.CurrentDailyBalance,
                            Fees = 0,
                            IsNSF = false,
                            IsManual = false,
                            IsAmountChanged = false,
                            IsFrequencyChanged = false,
                            IsDateChanged = false,
                            IsDeleted = false,
                            CanEdit = false,
                            CanNSF = false,
                            StatusCode = "SP",
                            Status = language == "FR" ? paymentStatusStopped.NameFr : paymentStatusStopped.NameEn,
                            SubFeesPayment = 0
                        };
                        loanPayments.Add(stopPayment);

                        // Recalculate statistics
                        CalculateStatistics(loan, loanPayments.OrderBy(x => x.PaymentDate).ToList(), true);

                        return UpdateLoan(loan, true);

                        // var _loanModel = UpdateLoan(loan, true);
                        // _loanModel.LoanPayments = _loanModel.LoanPayments.OrderBy(x => x.PaymentDate).ThenBy(x => x.Id).ToList();
                        // return _loanModel;
                    }
                    else
                    {
                        //
                        Calculator = new LoanCalculator();
                        List<ClientLoanPaymentModel> newLoanPayments = new List<ClientLoanPaymentModel>();
                        bool isPaymentIdSelected = loanPayments.Exists(x => x.Id == model.SelectedPaymentId);
                        var selectedPayment = (!isPaymentIdSelected) ? loanPayments.First() : loanPayments.Where(x => x.Id == model.SelectedPaymentId).SingleOrDefault();

                        List<ClientLoanPaymentModel> paymentsChanged = new List<ClientLoanPaymentModel>();
                        List<ClientLoanPaymentModel> paymentsUnchanged = new List<ClientLoanPaymentModel>();

                        bool modifyDate = model.NewPaymentFrequencyDate != selectedPayment.PaymentDate;
                        bool modifyFrequency = model.NewFrequencyId != loan.FrequencyId;
                        bool modifyAmount = (model.NewPaymentAmount != 0 && model.NewPaymentAmount != selectedPayment.Amount);

                        var frequency = context.LoanFrequency.Where(x => x.Id == model.NewFrequencyId).SingleOrDefault();

                        if (isPaymentIdSelected)
                        {
                            paymentsChanged = loanPayments.Where(x => x.PaymentDate >= selectedPayment.PaymentDate).ToList();
                            paymentsUnchanged = loanPayments.Where(x => x.PaymentDate < selectedPayment.PaymentDate).ToList();
                        }
                        else
                        {
                            paymentsChanged = loanPayments.Where(x => x.PaymentDate >= model.NewPaymentFrequencyDate.Date).ToList();
                            paymentsUnchanged = loanPayments.Where(x => x.PaymentDate < model.NewPaymentFrequencyDate.Date).ToList();
                        }

                        paymentsChanged.ForEach(x => { x.FrequencyDuration = frequency.Frequency; x.IsFrequencyChanged = modifyFrequency; });

                        var loanPayment = paymentsChanged
                                .OrderBy(x => x.PaymentDate)
                                .First();

                        loanPayment.IsAmountChangedForAll = (!isPaymentIdSelected && selectedPayment.Amount != model.NewPaymentAmount);

                        // Modify payment
                        loanPayment.PaymentDate = (modifyDate) ? model.NewPaymentFrequencyDate : loanPayment.PaymentDate;
                        loan.FrequencyId = model.NewFrequencyId;
                        loan.FrequencyDuration = frequency.Frequency;

                        foreach (var payment in paymentsChanged)
                        {
                            payment.IsDateChanged = modifyDate;
                            payment.IsAmountChanged = modifyAmount;
                            payment.NewAmount = (modifyAmount) ? model.NewPaymentAmount : payment.Amount;
                            payment.MaxAmount = (modifyAmount) ? model.NewPaymentAmount : payment.MaxAmount;
                        }

                        newLoanPayments = paymentsUnchanged.Concat(paymentsChanged).ToList();
                        Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);

                        // Exclude Deferred Payment fees
                        var removedUnwantedPayments = RemoveUnwantedPayments(newLoanPayments);

                        if (loan.FrequencyDuration == 24)
                        {
                            // Update Loan Start and End date for BiMonthly loans
                            loan.BiMonthlyStartDay = model.StartDay == 0 ? loan.BiMonthlyStartDay : model.StartDay;
                            loan.BiMonthlyEndDay = model.EndDay == 0 ? loan.BiMonthlyEndDay : model.EndDay;
                            newLoanPayments = Calculator.CalculateDailyInterest(newLoanPayments, loan, (DateTime)loanPayment.PaymentDate, 0, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay, (modifyDate || modifyFrequency), modifyAmount, /*false,*/  false, false);
                        }
                        else
                        {
                            newLoanPayments = Calculator.CalculateDailyInterest(newLoanPayments, loan, (DateTime)loanPayment.PaymentDate, 0, model.StartDay, model.EndDay, (modifyDate || modifyFrequency), modifyAmount, /*false,*/  false, false);
                        }

                        loanPayments = newLoanPayments;

                        // For all new payments, update Status
                        var paymentStatusPending = context.PaymentStatus
                                                .Where(x => x.Code == "PEN")
                                                .SingleOrDefault();

                        newLoanPayments.Where(x => x.StatusCode == "PEN")
                                                .ToList()
                                                .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                        // Recalculate statistics
                        CalculateStatistics(loan, loanPayments, false);

                        if (_configurationService.ValidateMembershipFee())
                        {
                            loanPayments = CalculateSubFeesPayments_Update(loanPayments, loan.OriginalSubFeesPrice);
                        }

                        // Check if a Payment Agreement was asked
                        if (model.PaymentAgreement)
                        {
                            var loanAgreementStatus = context.LoanStatus.SingleOrDefault(x => x.Code == "PA");
                            loan.Status = language == "FR" ? loanAgreementStatus.NameFr : loanAgreementStatus.NameEn;
                            loan.StatusCode = loanAgreementStatus != null ? loanAgreementStatus.Code : null;
                            loan.IsPerceptech = model.Perceptech;

                            var agreementStatus = context.ClientStatus.SingleOrDefault(x => x.Code == "COLLECTIONS_AGREEMENT");
                            var client = context.Client.SingleOrDefault(x => x.Id == loan.ClientId);
                            if (client != null && agreementStatus != null)
                            {
                                client.IdclientStatus = agreementStatus.Id;
                                context.Client.Update(client);
                            }

                            loan = UpdateLoan(loan, true);
                        }

                        // Add payments to Interac Payment Agreement
                        if (model.PaymentAgreement)
                        {
                            if (model.Interac)
                            {
                                foreach (var payment in loan.LoanPayments.Where(x => x.PaymentDate >= model.NewPaymentFrequencyDate.Date))
                                {
                                    var paymentAgreement = new ClientLoanPaymentAgreement()
                                    {
                                        IdclientLoanPayment = payment.Id,
                                        IsConfirmed = false
                                    };
                                    context.ClientLoanPaymentAgreement.Add(paymentAgreement);
                                }

                                // Update Payment Status
                                var status = context.PaymentStatus.Where(x => x.Code == "PENI").SingleOrDefault();
                                loan.LoanPayments.Where(x => x.PaymentDate >= model.NewPaymentFrequencyDate.Date)
                                                 .ToList()
                                                 .ForEach(x => { x.StatusCode = "PENI"; x.Status = language == "FR" ? status.NameFr : status.NameEn; });

                                foreach (var payment in loan.LoanPayments)
                                {
                                    var existingPayment = context.ClientLoanPayment.Where(x => x.Id == payment.Id).SingleOrDefault();
                                    existingPayment.IdpaymentStatus = status.Id;
                                    context.ClientLoanPayment.Update(existingPayment);
                                    context.SaveChanges();
                                }
                            }
                        }

                        if (!model.PaymentAgreement)
                        {
                            loan = UpdateLoan(loan, true);
                        }

                        // Remove all IsDeleted from LoanPayments
                        loan.LoanPayments.RemoveAll(x => x.IsDeleted == true);
                        loan.LoanPayments = loan.LoanPayments.OrderBy(x => x.PaymentDate).ToList();

                        // 
                        int index = 1;
                        loan.LoanPayments.ForEach(x => x.PaymentNo = index++);

                        //
                        loan = ValidatePerceptechStatus(loan, language);

                        //
                        loan = ValidatePaymentPaidStatus(loan, language);

                        // Validate CanNSF after Perceptech and PaidStatus
                        foreach (var payment in loanPayments)
                        {
                            payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                        }

                        //
                        UpdateLoanSummaryInfo(loan, language);

                        // Add removed Deferred Payment fees back and orderby date
                        loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                        return loan;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ClientLoanModel RestartLoan(LoanRestartModel model, string language)
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    var restartDate = model.RestartPaymentDate;
                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    // Update loan information
                    var loanStatus = context.LoanStatus
                                                    .Where(x => x.Code == "IP")
                                                    .SingleOrDefault();

                    loan.StatusCode = "IP";
                    loan.Status = language == "FR" ? loanStatus.NameFr : loanStatus.NameEn;
                    loan.StopLoanDate = null;
                    loan.IsLoanStopped = false;

                    var frequency = context.LoanFrequency.Where(x => x.Id == model.NewFrequencyId)
                                    .SingleOrDefault();

                    loan.FrequencyDuration = frequency.Frequency;
                    loan.FrequencyId = frequency.Id;
                    loan.Frequency = language == "FR" ? frequency.NameFr : frequency.NameEn;

                    // Update Max Amount
                    loanPayments.ForEach(x => x.MaxAmount = model.NewPaymentAmount);

                    // Exclude Deferred Payment fees
                    var removedUnwantedPayments = RemoveUnwantedPayments(loanPayments);

                    // 
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterest(loanPayments, loan, restartDate, 0, model.StartDay, model.EndDay, false, false, /*false,*/  false, true);

                    if (_configurationService.ValidateMembershipFee())
                    {
                        foreach (var payment in newLoanPayments)
                        {
                            if (payment.StatusCode == "PEN")
                            {
                                payment.SubFeesPayment = 999; // To force a SubFees Update
                            }
                        }
                        newLoanPayments = CalculateSubFeesPayments_Restart(newLoanPayments, loan.OriginalSubFeesPrice, (int)loan.FrequencyDuration);
                    }

                    // For all new payments, update Status
                    var paymentStatusPending = context.PaymentStatus
                                            .Where(x => x.Code == "PEN")
                                            .SingleOrDefault();

                    newLoanPayments.Where(x => x.StatusCode == "PEN")
                                            .ToList()
                                            .ForEach(x => { x.Status = language == "FR" ? paymentStatusPending.NameFr : paymentStatusPending.NameEn; });

                    // Recalculate statistics
                    CalculateStatistics(loan, newLoanPayments, false);

                    // Add a payment line indicating that the loan was restarted. All values are at Zero
                    var paymentStatus = context.PaymentStatus
                                                .Where(x => x.Code == "PR")
                                                .SingleOrDefault();

                    var restartPayment = new ClientLoanPaymentModel()
                    {
                        Amount = 0,
                        PaymentDate = restartDate,
                        IsRebate = false,
                        MaxAmount = 0,
                        Interest = 0,
                        Capital = 0,
                        Balance = 0,
                        Fees = 0,
                        IsNSF = false,
                        IsManual = false,
                        IsAmountChanged = false,
                        IsDateChanged = false,
                        IsDeleted = false,
                        CanEdit = false,
                        CanNSF = false,
                        IsFrequencyChanged = false,
                        StatusCode = "PR",
                        Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn,
                        SubFeesPayment = 0
                    };

                    loan.LoanPayments.Add(restartPayment);

                    // Check if a Payment Agreement was asked
                    if (model.PaymentAgreement)
                    {
                        var loanAgreementStatus = context.LoanStatus.SingleOrDefault(x => x.Code == "PA");
                        loan.Status = language == "FR" ? loanAgreementStatus.NameFr : loanAgreementStatus.NameEn;
                        loan.StatusCode = loanAgreementStatus != null ? loanAgreementStatus.Code : null;
                        loan.IsPerceptech = model.Perceptech;

                        var agreementStatus = context.ClientStatus.SingleOrDefault(x => x.Code == "COLLECTIONS_AGREEMENT");
                        var client = context.Client.SingleOrDefault(x => x.Id == loan.ClientId);
                        if (client != null && agreementStatus != null)
                        {
                            client.IdclientStatus = agreementStatus.Id;
                            context.Client.Update(client);
                            context.SaveChanges();
                        }
                    }

                    var loanUpdate = UpdateLoan(loan, false);
                    loanUpdate.LoanPayments = loanUpdate.LoanPayments.OrderBy(x => x.PaymentDate).ToList();

                    // Reset payment index
                    int index = 1;
                    loanUpdate.LoanPayments.ForEach(x => x.PaymentNo = index++);
                    loanUpdate.LoanPayments = loanUpdate.LoanPayments;

                    // Check if a Payment Agreement was asked
                    if (model.PaymentAgreement)
                    {
                        var loanAgreementStatus = context.LoanStatus.SingleOrDefault(x => x.Code == "PA");
                        loan.Status = language == "FR" ? loanAgreementStatus.NameFr : loanAgreementStatus.NameEn;
                        loan.StatusCode = loanAgreementStatus != null ? loanAgreementStatus.Code : null;
                        loan.IsPerceptech = model.Perceptech;

                        var agreementStatus = context.ClientStatus.SingleOrDefault(x => x.Code == "COLLECTIONS_AGREEMENT");
                        var client = context.Client.SingleOrDefault(x => x.Id == loan.ClientId);
                        if (client != null && agreementStatus != null)
                        {
                            client.IdclientStatus = agreementStatus.Id;
                            context.Client.Update(client);
                        }

                        loanUpdate = UpdateLoan(loan, true);
                    }

                    // Add payments to Interac Payment Agreement
                    if (model.PaymentAgreement)
                    {
                        if (model.Interac)
                        {
                            foreach (var payment in loanUpdate.LoanPayments.Where(x => x.Id != 0 && x.PaymentDate >= restartDate && x.StatusCode != "PR"))
                            {
                                var paymentAgreement = new ClientLoanPaymentAgreement()
                                {
                                    IdclientLoanPayment = payment.Id,
                                    IsConfirmed = false
                                };

                                context.ClientLoanPaymentAgreement.Add(paymentAgreement);
                                context.SaveChanges();
                            }

                            // Update Payment Status
                            var status = context.PaymentStatus.Where(x => x.Code == "PENI").SingleOrDefault();
                            loanUpdate.LoanPayments.Where(x => x.Id != 0 && x.PaymentDate >= restartDate && x.StatusCode != "PR")
                                                 .ToList()
                                                 .ForEach(x => { x.StatusCode = "PENI"; x.Status = language == "FR" ? status.NameFr : status.NameEn; });

                            foreach (var payment in loanUpdate.LoanPayments.Where(x => x.Id != 0 && x.PaymentDate >= restartDate && x.StatusCode != "PR"))
                            {
                                var existingPayment = context.ClientLoanPayment.Where(x => x.Id == payment.Id).SingleOrDefault();
                                existingPayment.IdpaymentStatus = status.Id;
                                context.ClientLoanPayment.Update(existingPayment);
                                context.SaveChanges();
                            }
                        }
                    }

                    //
                    loanUpdate.LoanPayments.RemoveAll(x => x.Id == 0);

                    //
                    loan = ValidatePerceptechStatus(loanUpdate, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    // Add removed Deferred Payment fees back and orderby date
                    loan = AddUnwantedPayments(loan, removedUnwantedPayments);

                    return loan;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ClientLoanModel> DeferPayment(LoanDelayPaymentModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                using (var context = _dbContextProvider.GetContext())
                {
                    // Retreive current payment
                    var originalLoanPayment = await context.ClientLoanPayment.SingleOrDefaultAsync(x => x.Id == model.PaymentId);

                    // Retreive current Defer fee
                    var deferFee = _configurationService.DeferredFees();

                    var loan = LoadLaonInformation(model.LoanId, language);
                    loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);
                    var loanPayments = LoadLoanPaymentInformation(model.LoanId, language);

                    // Validate CanEdit
                    foreach (var payment in loanPayments)
                    {
                        payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                    }

                    // Add Deferred payment fees line
                    var paymentStatusDeferred = context.PaymentStatus
                                                  .Where(x => x.Code == "DP")
                                                  .SingleOrDefault();
                    var deferredPaymentFees = new ClientLoanPaymentModel()
                    {
                        Amount = deferFee,
                        Interest = 0,
                        Capital = 0,
                        Balance = 0,
                        PaymentDate = originalLoanPayment.PaymentDate,
                        IsRebate = false,
                        Fees = 0,
                        MaxAmount = 0,
                        IsNSF = false,
                        IsManual = false,
                        IsAmountChanged = false,
                        IsDateChanged = false,
                        IsDeleted = false,
                        IsDeferredFee = true,
                        CanEdit = false,
                        CanNSF = false,
                        StatusCode = "DP",
                        Status = language == "FR" ? paymentStatusDeferred.NameFr : paymentStatusDeferred.NameEn,
                        DeferredFee = deferFee
                    };

                    //
                    int index = 0;
                    loanPayments.ForEach(x => x.Index = index++);

                    // Check to see if Loan can be deleted
                    loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                        ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                        : true;

                    //
                    Calculator = new LoanCalculator();
                    Calculator.InterestRate = Convert.ToDouble(loan.InterestRate);
                    var newLoanPayments = Calculator.CalculateDailyInterestDeferredPayment(loanPayments, loan, originalLoanPayment, deferFee, loan.BiMonthlyStartDay, loan.BiMonthlyEndDay);

                    // Add Deferred payment to payments
                    newLoanPayments.Add(deferredPaymentFees);

                    //
                    foreach (var payment in loanPayments)
                    {
                        if (payment.PaymentDate < originalLoanPayment.PaymentDate)
                        {
                            newLoanPayments.Add(payment);
                        }
                    }

                    if (_configurationService.ValidateMembershipFee())
                    {
                        newLoanPayments = CalculateSubFeesPayments_Update(newLoanPayments.OrderBy(x => x.PaymentDate).ToList(), loan.OriginalSubFeesPrice);
                    }

                    // Calculate statistics
                    CalculateStatistics(loan, newLoanPayments.OrderBy(x => x.PaymentDate).ToList(), false);

                    // Add deferred fees to Loan
                    loan.Fees = loan.Fees + deferFee;

                    // Update status of New Payments
                    var paymentStatus = context.PaymentStatus
                                                .Where(x => x.Code == "PEN")
                                                .SingleOrDefault();

                    loan.LoanPayments.Where(x => x.Id == 0 && x.StatusCode == "PEN")
                                        .ToList()
                                        .ForEach(x => x.Status = language == "FR" ? paymentStatus.NameFr : paymentStatus.NameEn);

                    //
                    loan = ValidatePerceptechStatus(loan, language);

                    //
                    loan = ValidatePaymentPaidStatus(loan, language);

                    // Validate CanNSF after Perceptech and PaidStatus
                    foreach (var payment in loanPayments)
                    {
                        payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                    }

                    //
                    return UpdateLoan(loan, true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ClientLoanModel> StoppedLoanPayment(StoppedLoanPaymentModel model, string language)
        {
            try
            {
                // Get all payments for Loan
                var context = _dbContextProvider.GetContext();
                var loan = await context.ClientLoan
                                    .Where(x => x.Id == model.LoanId)
                                    .Select(x => new ClientLoanModel()
                                    {
                                        Id = x.Id,
                                        LoanNumber = x.LoanNumber,
                                        Status = language == "FR" ? x.IdloanStatusNavigation.NameFr : x.IdloanStatusNavigation.NameEn,
                                        StatusCode = x.IdloanStatusNavigation.Code,

                                        StartDate = x.StartDate,
                                        EndDate = x.EndDate,

                                        InterestAmount = x.InterestAmount,
                                        InterestRate = x.InterestRate,
                                        Amount = x.Amount,
                                        BrokerageFee = x.BrokerageFees,
                                        Frequency = language == "FR" ? x.IdloanFrequencyNavigation.NameFr : x.IdloanFrequencyNavigation.NameEn,
                                        FrequencyDuration = x.IdloanFrequencyNavigation.Frequency,
                                        FrequencyId = x.IdloanFrequencyNavigation.Id,
                                        CreatedDate = x.CreatedDate,
                                        OriginalPaymentAmount = x.OriginalPaymentAmount,

                                        BiMonthlyStartDay = x.BiMonthlyStartDay,
                                        BiMonthlyEndDay = x.BiMonthlyEndDay,
                                        IsLoanStopped = x.IdloanStatusNavigation.Code == "LS" && true,
                                        StopLoanDate = x.StopLoanDate,
                                        IsPerceptech = x.IsPerceptech == null ? false : true,
                                        PaymentProvider = _configurationService.ValidatePaymentProvider()
                                    })
                                    .SingleOrDefaultAsync();

                loan.BeforeProvider = ValidateLoanIsMixed(loan.CreatedDate);

                var loanPayments = await context.ClientLoanPayment
                                            .Where(x => x.IdclientLoan == model.LoanId && x.IsDeleted == false)
                                            .Select(x => new ClientLoanPaymentModel()
                                            {
                                                Id = x.Id,
                                                LoanId = model.LoanId,
                                                Status = language == "FR" ? x.IdpaymentStatusNavigation.NameFr : x.IdpaymentStatusNavigation.NameEn,
                                                StatusCode = x.IdpaymentStatusNavigation.Code,
                                                Interest = x.Interest,
                                                Balance = x.Balance,
                                                Capital = x.Capital,
                                                Amount = x.Amount,
                                                MaxAmount = x.MaxAmount,
                                                Fees = x.Fees,
                                                PaymentDate = x.PaymentDate,
                                                IsDeferredFee = x.IsDeferredFee,
                                                IsNSF = x.IsNsf,
                                                IsManual = x.IsManual,
                                                IsRebate = x.IsRebate,
                                                IsAmountChanged = x.IsAmountChanged,
                                                IsAmountChangedForAll = false,
                                                IsDateChanged = x.IsDateChanged,
                                                IsFrequencyChanged = x.IsFrequencyChanged,
                                                FrequencyDuration = x.Frequency,
                                                IsDeleted = x.IsDeleted,
                                                SentToPerceptech = context.ClientLoanPaymentPerceptech.SingleOrDefault(p => p.IdclientLoanPayment == x.Id) != null ? true : false,
                                                NewAmount = x.Amount
                                            })
                                            .OrderBy(x => x.PaymentDate).ThenBy(x => x.Id)
                                            .ToListAsync();

                // Validate CanEdit
                foreach (var payment in loanPayments)
                {
                    payment.CanEdit = ValidateCanEditPayment(payment.StatusCode, payment.Id, (DateTime)payment.PaymentDate, context);
                }

                // Check to see if Loan can be deleted
                loan.CanDelete = loanPayments.Count(x => x.StatusCode == "PAID") >= 1
                                    ? ((DateTime)loanPayments.First().PaymentDate).Date >= DateTime.Today.AddDays(2)
                                    : true;

                // Add new payment line for Stopped Loan
                var paymentStatusPayment = await context.PaymentStatus
                                              .Where(x => x.Code == "SLP")
                                              .SingleOrDefaultAsync();

                loanPayments.Add(new ClientLoanPaymentModel()
                {
                    Amount = model.PaymentAmount,
                    Interest = 0,
                    Capital = 0,
                    Balance = loanPayments.Last().Balance - model.PaymentAmount,
                    Fees = 0,
                    PaymentDate = model.PaymentDate,
                    IsRebate = false,
                    MaxAmount = 0,
                    IsNSF = false,
                    IsManual = false,
                    IsAmountChanged = false,
                    IsDateChanged = false,
                    IsDeleted = false,
                    IsFrequencyChanged = false,
                    FrequencyDuration = loan.FrequencyDuration,
                    CanEdit = false,
                    CanNSF = false,
                    StatusCode = "SLP",
                    Status = language == "FR" ? paymentStatusPayment.NameFr : paymentStatusPayment.NameEn
                });

                // Calculate statistics
                CalculateStatistics(loan, loanPayments, false);

                // Validate CanNSF after Perceptech and PaidStatus
                foreach (var payment in loanPayments)
                {
                    payment.CanNSF = ValidateCanApplyNSF(loan.CreatedDate, payment.StatusCode, payment.Id, context);
                }

                //
                var updatedLoan = UpdateLoan(loan, true);
                updatedLoan.LoanPayments = updatedLoan.LoanPayments.OrderBy(x => x.PaymentDate).ThenBy(x => x.Id).ToList();
                return updatedLoan;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<LoanRequestModel>> DeleteLoan(int loanId, string language)
        {
            try
            {
                using var context = _dbContextProvider.GetContext();
                using var transaction = context.Database.BeginTransaction();

                var clientLoan = await context.ClientLoan
                                    .Where(x => x.Id == loanId)
                                    .FirstOrDefaultAsync();

                if (clientLoan != null)
                {


                    clientLoan.IsDeleted = true;
                    context.ClientLoan.Update(clientLoan);
                    await context.SaveChangesAsync();

                    var deposit = await context.Deposit
                                            .Where(x => x.ClientLoanId == clientLoan.Id)
                                            .SingleOrDefaultAsync();
                    if (deposit != null)
                    {
                        context.Deposit.Remove(deposit);
                        await context.SaveChangesAsync();
                    }
                    await transaction.CommitAsync();

                    var requests = await (from req in context.ExternalLoanWebRequest
                                          join lrs in context.LoanRequestStatus on req.StatusId equals lrs.Id
                                          where req.Idclient == clientLoan.Idclient
                                          && !(from loan in context.ClientLoan
                                               where loan.Idrequest != null
                                               && loan.IsDeleted == false
                                               select loan.Idrequest).Contains(req.Id)
                                          && (lrs.Code == "ACCEPTED" || lrs.Code == "PREAPPROVED")
                                          select new LoanRequestModel
                                          {
                                              Id = req.Id,
                                              ClientFirstName = req.FirstName,
                                              ClientLastName = req.LastName,
                                              LoanAmount = req.Amount,
                                              RequestedDateFormat = String.Format("{0:yyyy-MM-dd}", req.RequestedDate),
                                              RequestedDate = req.RequestedDate,
                                              Status = lrs != null ? language == "FR" ? lrs.NameFr : lrs.NameEn : null
                                          }).OrderByDescending(x => x.RequestedDate).ToListAsync();

                    return requests;
                }

                return new List<LoanRequestModel>();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<LoanPDFModel> GetAccountBalance(int loanId, string language)
        {
            try
            {
                decimal? totalInterest = 0;
                using var context = _dbContextProvider.GetContext();

                var loanClient = await context.ClientLoan
                                   .Where(x => x.Id == loanId)
                                   .Select(x => new LoanPDFModel()
                                   {
                                       LoanId = x.Id,
                                       IdClient = x.Idclient,
                                       Amount = x.Amount,
                                       CreatedDate = x.CreatedDate,
                                       StartDate = x.StartDate,
                                       EndDate = x.EndDate,
                                       FrequencyId = x.IdloanFrequency ?? 0,
                                       FrequencyDuration = x.IdloanFrequencyNavigation.Frequency,
                                       BrokerageFees = x.BrokerageFees,
                                       OriginalPaymentAmount = x.OriginalPaymentAmount,
                                       DateToday = DateTime.Today.ToString("d"),
                                       LogoPath = "",
                                       InterestRate = x.InterestRate,
                                       StatusCode = x.IdloanStatusNavigation.Code,
                                       SubFees = x.SubFees,
                                       OriginalSubFeesPrice = x.OriginalSubFeesPrice ?? 0,
                                       ChargeMembershipFee = _configurationService.ValidateMembershipFee(),
                                       BrokerageFee = x.BrokerageFees
                                   }).FirstOrDefaultAsync();

                var clientInfos = await context.Client
                                    .Where(x => x.Id == loanClient.IdClient)
                                    .Select(x => new LoanClientInfos()
                                    {
                                        Firstname = x.FirstName,
                                        Lastname = x.LastName,
                                        LanguageClient = (x.Idlanguage == 1) ? "FR" : "EN"
                                    }).FirstOrDefaultAsync();

                var loansListClient = context.ClientLoanPayment
                                        .Where(x => x.IdclientLoan == loanId && x.IsDeleted == false)
                                        .Select(x => new LoanAccountBalancePaymentModel()
                                        {
                                            LoanId = x.Id,
                                            PaymentDate = x.PaymentDate,
                                            Interest = x.Interest,
                                            Capital = x.Capital,
                                            Amount = x.Amount,
                                            Balance = x.Balance,
                                            MaxAmount = x.MaxAmount,
                                            Fees = x.Nsffee,
                                            IsDeleted = x.IsDeleted,
                                            Status = (clientInfos.LanguageClient == "FR") ? x.IdpaymentStatusNavigation.NameFr : x.IdpaymentStatusNavigation.NameEn,
                                            StatusCode = x.IdpaymentStatusNavigation.Code,
                                            SubFeesPayment = x.SubFeesPayment,
                                            IsDeferredFee = x.IsDeferredFee
                                        }).OrderBy(x => x.PaymentDate).ThenBy(x => x.LoanId)
                                        .ToList();

                var clientAdress = await context.ClientAddress
                                        .Where(x => x.Idclient == loanClient.IdClient)
                                        .Select(x => new LoanClientAddressModel()
                                        {
                                            CivicNumber = x.CivicNumber,
                                            StreetName = x.StreetName,
                                            Apartment = x.Apartment,
                                            PostalCode = x.PostalCode,
                                            City = x.City,
                                            ProvinceId = x.Idprovince,
                                        }).FirstOrDefaultAsync();

                var province = await context.Province
                                       .Where(x => x.Id == clientAdress.ProvinceId).FirstOrDefaultAsync();


                foreach (var loan in loansListClient)
                {
                    totalInterest = (loan.Interest != null) ? totalInterest + loan.Interest : totalInterest;
                }

                loanClient.LogoPath = context.LoanerConfiguration
                        .Where(x => x.Key == "COMPANY_LOGO")
                        .SingleOrDefault().Value;

                var allowCustomAmounts = context.LoanerConfiguration
                                        .Where(x => x.Key == "ALLOW_CUSTOM_AMOUNTS")
                                        .SingleOrDefault();

                loanClient.InterestRate = loanClient.InterestRate;
                loanClient.IsPersonalized = Convert.ToBoolean(allowCustomAmounts.Value);

                // Laod new loan configuration
                var loanFrequency = context.LoanFrequency
                .SingleOrDefault(x => x.Id == loanClient.FrequencyId);
                loanClient.FrequencyValue = clientInfos.LanguageClient == "FR" ? loanFrequency.NameFr : loanFrequency.NameEn;

                //
                var loanAmounts = (from la in context.LoanAmount
                                   join lc in context.LoanConfiguration on la.Id equals lc.IdloanAmount
                                   select new LoanClientAmountModel()
                                   {
                                       Id = la.Id,
                                       Amount = la.Amount
                                   }).Distinct().OrderBy(x => x.Amount).ToList();

                loanClient.LoanAmounts = loanAmounts;

                var loanConfiguration = context.LoanConfiguration
                                                .Select(x => new NewLoanClientConfigurationModel()
                                                {
                                                    LoanAmountId = x.IdloanAmount,
                                                    LoanFrequencyId = x.IdloanFrequency,
                                                    LoanFrequencyName = clientInfos.LanguageClient == "FR" ? x.IdloanFrequencyNavigation.NameFr : x.IdloanFrequencyNavigation.NameEn,
                                                    NumberOfPayments = (int)x.TotalPayments
                                                }).ToList();

                loanClient.LoanConfiguration = loanConfiguration;

                LoanClientAddressModel address = new LoanClientAddressModel();
                address.CivicNumber = clientAdress.CivicNumber ?? "";
                address.StreetName = clientAdress.StreetName ?? "";
                address.Apartment = clientAdress.Apartment ?? "";
                address.PostalCode = clientAdress.PostalCode ?? "";
                address.City = clientAdress.City ?? "";
                address.ProvinceName = province.NameFr ?? null;
                loanClient.Address = address;
                loanClient.ListPayment = loansListClient;
                loanClient.ClientInfos = clientInfos;
                loanClient.TotalInterest = totalInterest;

                CalculateStatisticsLoanPDF(loanClient, loanClient.ListPayment);

                loanClient.LoanConfiguration = null;
                loanClient.LoanAmounts = null;

                return loanClient;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal CalculateTotalAmountRemaining(LoanModifyPaymentModel model, string language)
        {
            try
            {
                var loan = LoanModifyPayment(model, false, language);
                return loan.TotalAmountRemaining;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmailTemplateModel SendLoanPDF(PDFEmailModel model, string pdfPath, string serialNumber)
        {
            try
            {
                string fileName = "report_" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".pdf";

                HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                //htmlToPdfConverter.SerialNumber = serialNumber;
                htmlToPdfConverter.Document.Margins.Bottom = 1;
                htmlToPdfConverter.Document.Margins.Top = 5;
                htmlToPdfConverter.ConvertHtmlToFile(model.PDF, null, pdfPath + fileName);

                EmailTemplateModel template = new EmailTemplateModel();
                template.loanPDF = new LoanPDFMessageModel();
                template.loanPDF.EmailAddress = model.ClientInfos.Email;
                template.loanPDF.FirstName = model.ClientInfos.FirstName;
                template.loanPDF.LastName = model.ClientInfos.LastName;
                template.loanPDF.TypePDF = model.TypePDF;
                template.loanPDF.LoanPDFPath = pdfPath;
                template.loanPDF.LoanPDFName = fileName;
                template.loanPDF.Language = model.ClientInfos.LanguageClient;
                return template;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Membership Fees

        private List<ClientLoanPaymentModel> CalculateSubFeesPayments_New(List<ClientLoanPaymentModel> payments, decimal? originalMemberFee)
        {
            using var context = _dbContextProvider.GetContext();

            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            var paymentIsFirst = true;

            var memberFee = (originalMemberFee == null) ?
                 Convert.ToDecimal(Convert.ToDouble(context.LoanerConfiguration
                             .Where(x => x.Key == "MEMBERSHIP_FEE")
                             .SingleOrDefault().Value))
                 : originalMemberFee;

            foreach (var payment in payments)
            {
                if (payment.StatusCode == "PEN" || payment.StatusCode == "PC" || payment.StatusCode == "STP" || payment.StatusCode == "NSF" || payment.StatusCode == "PAID")
                {
                    endDate = payment.PaymentDate ?? new DateTime();

                    // if (payment.StatusCode == "DP")
                    // {
                    //     payment.SubFeesPayment = 0;
                    // }
                    if (payment.StatusCode == "NSF")
                    {
                        payment.SubFeesPayment = 0;
                        startDate = payment.PaymentDate ?? new DateTime();
                        //paymentIsFirst = false;
                    }
                    else if (originalMemberFee == null || !(payment.SubFeesPayment == null && payment.PaymentDate < DateTime.Now))
                    {
                        payment.SubFeesPayment = (paymentIsFirst) ? memberFee : CalculateSubFees(startDate, endDate, memberFee);
                        startDate = payment.PaymentDate ?? new DateTime();
                        //paymentIsFirst = false;
                    }
                }
                else
                {
                    payment.SubFeesPayment = 0;
                }

                paymentIsFirst = false;
            }

            return payments;

        }

        private List<ClientLoanPaymentModel> CalculateSubFeesPayments_Update(List<ClientLoanPaymentModel> payments, decimal? originalMemberFee)
        {
            using var context = _dbContextProvider.GetContext();

            DateTime startDate = (DateTime)payments.First().PaymentDate;
            DateTime endDate = new DateTime();
            var paymentIsFirst = true;

            foreach (var payment in payments)
            {
                if (payment.StatusCode == "PEN" || payment.StatusCode == "PC" || payment.StatusCode == "STP" || payment.StatusCode == "NSF" || payment.StatusCode == "PAID")
                {
                    endDate = payment.PaymentDate ?? new DateTime();

                    if (payment.StatusCode == "NSF")
                    {
                        payment.SubFeesPayment = 0;
                        startDate = payment.PaymentDate ?? new DateTime();
                    }

                    if (payment.SubFeesPayment != null && payment.SubFeesPayment != 0)
                    {
                        payment.SubFeesPayment = (paymentIsFirst) ? originalMemberFee : CalculateSubFees(startDate, endDate, originalMemberFee);
                        startDate = payment.PaymentDate ?? new DateTime();
                    }
                    // New payments that were created but not yet saved
                    else if (payment.Id == 0)
                    {
                        payment.SubFeesPayment = (paymentIsFirst) ? originalMemberFee : CalculateSubFees(startDate, endDate, originalMemberFee);
                        startDate = payment.PaymentDate ?? new DateTime();
                    }
                    else
                    {
                        payment.SubFeesPayment = 0;
                    }
                }
                else
                {
                    payment.SubFeesPayment = 0;
                }

                paymentIsFirst = false;
            }

            return payments;
        }

        private List<ClientLoanPaymentModel> CalculateSubFeesPayments_Restart(List<ClientLoanPaymentModel> payments, decimal? originalMemberFee, int loanFrequency)
        {
            using var context = _dbContextProvider.GetContext();
            DateTime startDate = (DateTime)payments.Where(x => x.Id == 0).OrderBy(x => x.PaymentDate).First().PaymentDate;
            DateTime endDate = new DateTime();
            var paymentIsFirst = true;

            foreach (var payment in payments.Where(x => x.Id == 0).OrderBy(x => x.PaymentDate).ToList())
            {
                if (payment.StatusCode == "PEN")
                {
                    endDate = payment.PaymentDate ?? new DateTime();
                    if (paymentIsFirst)
                    {
                        switch (loanFrequency)
                        {
                            case 52:
                                payment.SubFeesPayment = originalMemberFee;
                                break;
                            case 26:
                                payment.SubFeesPayment = originalMemberFee * 2;
                                break;
                            default:
                                payment.SubFeesPayment = (paymentIsFirst) ? originalMemberFee : CalculateSubFees(startDate, endDate, originalMemberFee);
                                break;
                        }

                        startDate = payment.PaymentDate ?? new DateTime();
                        paymentIsFirst = false;
                    }
                    else
                    {
                        payment.SubFeesPayment = (paymentIsFirst) ? originalMemberFee : CalculateSubFees(startDate, endDate, originalMemberFee);
                        startDate = payment.PaymentDate ?? new DateTime();
                        paymentIsFirst = false;
                    }
                }
                else
                {
                    payment.SubFeesPayment = 0;
                }

                paymentIsFirst = false;
            }

            return payments;
        }

        private decimal CalculateSubFees(DateTime start, DateTime end, decimal? memberFee)
        {
            using var context = _dbContextProvider.GetContext();
            double feeByDay = (Convert.ToDouble(memberFee) / 7);
            return Convert.ToDecimal((end.Date - start.Date).Days * feeByDay);
        }

        private void NewLoanMembershipFees(List<ClientLoanPaymentModel> payments)
        {
            if (_configurationService.ValidateMembershipFee())
            {
                using var context = _dbContextProvider.GetContext();
                Loan.OriginalSubFeesPrice = Convert.ToDecimal(Convert.ToDouble(
                                            context.LoanerConfiguration
                                            .Where(x => x.Key == "MEMBERSHIP_FEE")
                           .SingleOrDefault().Value));
                Loan.LoanPayments = CalculateSubFeesPayments_New(payments, null);
                Loan.SubFees = Convert.ToDecimal(payments.Where(x => x.StatusCode != "NSF").Select(x => x.SubFeesPayment).ToList().Sum());
            }
        }

        #endregion

    }
}
