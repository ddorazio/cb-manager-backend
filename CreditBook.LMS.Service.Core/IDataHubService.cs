using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface IDataHubService
    {
        Task UpdateLoanRequestStatus(LoanRequestModel model);
    }
}