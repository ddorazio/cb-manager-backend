﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using GemBox.Document;
using Xceed.Words.NET;
using Microsoft.EntityFrameworkCore;
using CreditBook.LMS.Data.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public class ContractService : IContractService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IMessageService _messageService;

        public ContractService(IDbContextProvider dbContextProvider, IMessageService messageService)
        {
            _dbContextProvider = dbContextProvider;
            _messageService = messageService;
        }

        public async Task<bool> VerifyContractSigned(string contractPath, string digitalUrl, int loanId, string emailAddress)
        {
            // Verify if Contract was already signed
            using var context = _dbContextProvider.GetContext();
            var digitalSignatureInfo = await (from cl in context.ClientLoan
                                              join clc in context.ClientLoanContractSignature on cl.Id equals clc.ClientLoanId
                                              where cl.Id == loanId
                                              select new
                                              {
                                                  Id = clc.Id,
                                                  Guid = clc.Guid,
                                                  IsSigned = clc.IsSigned
                                              }).OrderByDescending(x => x.Id).ToListAsync();

            if (digitalSignatureInfo.Count() != 0)
            {
                // Take last contract signed
                var lastSigned = digitalSignatureInfo.Where(x => x.IsSigned == true).LastOrDefault();
                if (lastSigned != null)
                {
                    var loan = await context.ClientLoan
                            .Where(x => x.Id == loanId)
                            .SingleOrDefaultAsync();

                    var client = await context.Client
                                            .Where(x => x.Id == loan.Idclient)
                                            .SingleOrDefaultAsync();

                    var language = await context.Language
                                            .Where(x => x.Id == client.Idlanguage)
                                            .SingleOrDefaultAsync();

                    var clientEmail = await context.ClientEmailAddress
                                                .Where(x => x.Idclient == loan.Idclient && x.IsPrimary == true)
                                                .SingleOrDefaultAsync();

                    await _messageService.SendMessage_ExistingContract(new EmailTemplateModel()
                    {
                        ExistingContract = new ExistingContractModel()
                        {
                            FirstName = client.FirstName,
                            LastName = client.LastName,
                            EmailAddress = emailAddress,
                            CellPhone = client.CellPhoneNumber.Replace(" ", ""),
                            DigitalUrl = $"{digitalUrl}{lastSigned.Guid}",
                            Language = language.Code
                        }
                    });

                    return false;
                }

                // Take last contract that was sent but was not signed
                var lastDigitalSignature = digitalSignatureInfo.LastOrDefault();
                if (lastDigitalSignature != null)
                {
                    var loan = await context.ClientLoan
                            .Where(x => x.Id == loanId)
                            .SingleOrDefaultAsync();

                    var client = await context.Client
                                            .Where(x => x.Id == loan.Idclient)
                                            .SingleOrDefaultAsync();

                    var language = await context.Language
                                            .Where(x => x.Id == client.Idlanguage)
                                            .SingleOrDefaultAsync();

                    var clientEmail = await context.ClientEmailAddress
                                                .Where(x => x.Idclient == loan.Idclient && x.IsPrimary == true)
                                                .SingleOrDefaultAsync();

                    await _messageService.SendMessage_Contract(new EmailTemplateModel()
                    {
                        Contract = new ContractModel()
                        {
                            FirstName = client.FirstName,
                            LastName = client.LastName,
                            EmailAddress = emailAddress,
                            CellPhone = client.CellPhoneNumber.Replace(" ", ""),
                            LoanAmount = String.Format("{0:0.##}", loan.Amount),
                            ContractPath = contractPath,
                            DigitalUrl = $"{digitalUrl}{lastDigitalSignature.Guid}",
                            Language = language.Code
                        }
                    });

                    return false;
                }
            }

            return true;
        }

        public async Task<ContractModel> GenerateLoanContract(string contractPath, string baseUrl, string digitalUrl, string contractFilePath, int loanId)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                {
                    var loan = await context.ClientLoan
                                        .Where(x => x.Id == loanId)
                                        .SingleOrDefaultAsync();

                    var loanPayments = await context.ClientLoanPayment
                                                .Where(x => x.IdclientLoan == loanId)
                                                .OrderBy(x => x.PaymentDate)
                                                .ToListAsync();

                    var client = await context.Client
                                            .Where(x => x.Id == loan.Idclient)
                                            .SingleOrDefaultAsync();

                    var language = await context.Language
                                            .Where(x => x.Id == client.Idlanguage)
                                            .SingleOrDefaultAsync();

                    var clientEmail = await context.ClientEmailAddress
                                                .Where(x => x.Idclient == loan.Idclient && x.IsPrimary == true)
                                                .SingleOrDefaultAsync();

                    var clientAddress = await context.ClientAddress
                                                    .Where(x => x.Idclient == client.Id && x.IsPrimary == true)
                                                    .SingleOrDefaultAsync();

                    var province = await context.Province
                                            .Where(x => x.Id == clientAddress.Idprovince)
                                            .SingleOrDefaultAsync();

                    // Take InterestRate from ClientLoan once Loan is created
                    var interestRate = loan.InterestRate;
                    // var interestRate = await context.LoanerConfiguration
                    //                             .Where(x => x.Key == "INTEREST_RATE")
                    //                             .SingleOrDefaultAsync();

                    var nsfFee = await context.LoanChargeConfiguration
                                        .Where(x => x.IdloanChargeTypeNavigation.Code == "nsf" && x.EffectiveDate <= DateTime.Now)
                                        .OrderByDescending(x => x.EffectiveDate)
                                        .FirstOrDefaultAsync();

                    var delayedFee = await context.LoanChargeConfiguration
                                        .Where(x => x.IdloanChargeTypeNavigation.Code == "delayed" && x.EffectiveDate <= DateTime.Now)
                                        .OrderByDescending(x => x.EffectiveDate)
                                        .FirstOrDefaultAsync();

                    var autoWithdrawal = await context.LoanChargeConfiguration
                                        .Where(x => x.IdloanChargeTypeNavigation.Code == "auto_withdrawal_amount" && x.EffectiveDate <= DateTime.Now)
                                        .OrderByDescending(x => x.EffectiveDate)
                                        .FirstOrDefaultAsync();

                    var initialNumberOfPayments = (loan.InitialNumberOfPayments != null) ? loan.InitialNumberOfPayments.ToString() : "";

                    var contractFile = language.Code == "E" ? System.IO.Path.Combine(contractFilePath, "Contracts/EN Version Contrat(MOD).docx")
                                        : System.IO.Path.Combine(contractFilePath, "Contracts/FR Version Contrat(MOD).docx");

                    var outputDir = new DirectoryInfo(contractFile);
                    var fileNameTemplate = contractFile;

                    var doc = DocX.Load(fileNameTemplate);

                    // Replace text
                    doc.ReplaceText("%CDATE%", String.Format("{0:yyyy-MM-dd}", DateTime.Today));
                    doc.ReplaceText("%FULLNAME%", client.FirstName + " " + client.LastName);
                    doc.ReplaceText("%TOTALFEES%", loan.BrokerageFees.ToString());
                    doc.ReplaceText("%LName%", client.LastName);
                    doc.ReplaceText("%FName%", client.FirstName);
                    doc.ReplaceText("%ADDRESS%", string.IsNullOrEmpty(clientAddress.Apartment)
                        ? clientAddress.CivicNumber + "" + clientAddress.StreetName : clientAddress.CivicNumber + "" + clientAddress.StreetName + ", " + clientAddress.Apartment);
                    doc.ReplaceText("%CITY%", clientAddress.City);
                    doc.ReplaceText("%PROVINCE%", language.Code == "E" ? province.NameEn : province.NameFr);
                    doc.ReplaceText("%POSTALCODE%", clientAddress.PostalCode);

                    doc.ReplaceText("%PHONE%", !string.IsNullOrEmpty(client.HomePhoneNumber) && client.HomePhoneNumber.Length == 10
                                                ? String.Format("{0:(###) ###-####}", Convert.ToInt64(client.HomePhoneNumber.Replace(" ", "")))
                                                : "");
                    doc.ReplaceText("%CELL%", String.Format("{0:(###) ###-####}", Convert.ToInt64(client.CellPhoneNumber.Replace(" ", ""))));
                    doc.ReplaceText("%LOANPAYMENT%", String.Format("{0:0.##}", loan.OriginalPaymentAmount));
                    doc.ReplaceText("%NOPAYMENTS%", initialNumberOfPayments);
                    doc.ReplaceText("%LOANNO%", loan.Id.ToString().PadLeft(6, '0'));
                    doc.ReplaceText("%ADDRESSLINE%", string.IsNullOrEmpty(clientAddress.Apartment)
                        ? clientAddress.CivicNumber + "" + clientAddress.StreetName : clientAddress.CivicNumber + "" + clientAddress.StreetName + ", " + clientAddress.Apartment);
                    //doc.ReplaceText("%CITY%", clientAddress.City);
                    doc.ReplaceText("%FULLNAMELINE%", client.FirstName + " " + client.LastName);
                    doc.ReplaceText("%INTRATE%", interestRate.Value.ToString());
                    doc.ReplaceText("%CAPITAL%", String.Format("{0:0.##}", loan.Amount + loan.BrokerageFees));
                    doc.ReplaceText("%LOANTOTAL%", String.Format("{0:0.##}", loan.Amount + loan.BrokerageFees + loan.InterestAmount));
                    doc.ReplaceText("%FPAYDATE%", String.Format("{0:yyyy-MM-dd}", loanPayments.First().PaymentDate));
                    doc.ReplaceText("%LPAYDATE%", String.Format("{0:yyyy-MM-dd}", loanPayments.Last().PaymentDate));
                    doc.ReplaceText("%NSF%", String.Format("{0:0.##}", nsfFee.AmountCharged));
                    doc.ReplaceText("%AUTOWITHDRAWAL%", String.Format("{0:0.##}", autoWithdrawal.AmountCharged));
                    doc.ReplaceText("%DELAYED%", String.Format("{0:0.##}", delayedFee.AmountCharged));

                    if (!Directory.Exists(contractPath))
                    {
                        Directory.CreateDirectory(contractPath);
                    }

                    string contractFileNameWord = language.Code == "E"
                        ? client.FirstName + "_" + client.LastName + "_" + loan.Id.ToString().PadLeft(6, '0') + "_Contract.docx"
                        : client.FirstName + "_" + client.LastName + "_" + loan.Id.ToString().PadLeft(6, '0') + "_Contrat.docx";
                    doc.SaveAs(contractPath + contractFileNameWord);

                    // Generate PDF
                    string contractFileNamePDF = language.Code == "E"
                        ? client.FirstName + "_" + client.LastName + "_" + loan.Id.ToString().PadLeft(6, '0') + "_Contract.pdf"
                        : client.FirstName + "_" + client.LastName + "_" + loan.Id.ToString().PadLeft(6, '0') + "_Contrat.pdf";

                    ComponentInfo.SetLicense("DN-2020Aug13-zwwuH+oAnBff7J1a1zAaEIVoKfelv2jqQsrelE21G5jPXAZZtPyQztt93J4IyAxZZJeNWpMWg/sqKfV35Zk8yZss/5Q==A");
                    DocumentModel document = DocumentModel.Load(contractPath + contractFileNameWord);
                    document.Save(contractPath + contractFileNamePDF);

                    // Save contract URL for Digital Signature
                    loan.ContractUrl = baseUrl + contractFileNamePDF;
                    context.ClientLoan.Update(loan);
                    await context.SaveChangesAsync();

                    // Create digital Signature request
                    var digitalSignature = new ClientLoanContractSignature()
                    {
                        ClientLoanId = loan.Id,
                        Guid = Guid.NewGuid(),
                        IsSigned = false,
                        RequestedDate = DateTime.Now,
                        Email = clientEmail.EmailAddress
                    };
                    context.ClientLoanContractSignature.Add(digitalSignature);
                    await context.SaveChangesAsync();

                    return new ContractModel()
                    {
                        FirstName = client.FirstName,
                        LastName = client.LastName,
                        EmailAddress = clientEmail.EmailAddress,
                        CellPhone = client.CellPhoneNumber.Replace(" ", ""),
                        LoanAmount = String.Format("{0:0.##}", loan.Amount),
                        ContractName = contractFileNamePDF,
                        ContractPath = contractPath,
                        DigitalUrl = $"{digitalUrl}{digitalSignature.Guid}",
                        Language = language.Code
                    };
                }
            }
        }
    }
}