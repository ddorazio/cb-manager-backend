using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Newtonsoft.Json;

namespace CreditBook.LMS.Service.Core
{
    public class APIHubClient_24HR : IAPIHubClient_24HR
    {
        private readonly HttpClient _httpClient;

        private readonly IDbContextProvider _dbContextProvider;

        public APIHubClient_24HR(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
            using var context = _dbContextProvider.GetContext();
            var webhookConfiguration = context.WebhookConfiguration.ToList();
            var webHookEndpoint = webhookConfiguration.Where(x => x.Key == "24HR_WEBHOOK_ENDPOINT").SingleOrDefault();

            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(webHookEndpoint.Value);
            //_httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("API-KEY", apiKey);
        }

        public async Task PostAsync(string path, object payload)
        {
            var body = ObjectToStringContent(payload);
            var result1 = await _httpClient.PostAsync(path, body);
            if (!result1.IsSuccessStatusCode)
            {
                var responseBody = await result1.Content.ReadAsStringAsync();
                throw new Exception($"Non-success response from primary APIHubClient_24HR.\nRequest body: {body}\nResponse body: {responseBody}");
            }

            // Send to a secondary endpoint
            var _secondaryHttpClient = new HttpClient();
            var secondaryWebHookEndpoint = "https://hooks.zapier.com/hooks/catch/447529/b3an7io/";
            _secondaryHttpClient.BaseAddress = new Uri(secondaryWebHookEndpoint);
            var result2 = await _secondaryHttpClient.PostAsync(path, body);
            if (!result2.IsSuccessStatusCode)
            {
                var responseBody = await result2.Content.ReadAsStringAsync();
                throw new Exception($"Non-success response from secondary APIHubClient_24HR.\nRequest body: {body}\nResponse body: {responseBody}");
            }
        }

        private StringContent ObjectToStringContent(object payload)
            => new StringContent
            (
                JsonConvert.SerializeObject(payload),
                Encoding.UTF8,
                "application/json"
            );
    }
}