using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class DocumentService : IDocumentService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public DocumentService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<IEnumerable<DocumentModel>> GetDocuments(int clientId)
        {
            var result = new List<DocumentModel>();
            using var context = _dbContextProvider.GetContext();
            var query = from doc in context.ClientDocument
                        where doc.Idclient == clientId && !doc.IsDeleted
                        select doc;
            var docs = await query.ToListAsync();
            foreach (var doc in docs)
            {
                var model = new DocumentModel
                {
                    Id = doc.Id,
                    Guid = doc.Guid.Value,
                    ClientId = doc.Idclient,
                    ClientLoanId = doc.IdclientLoan,
                    DocumentTypeCode = doc.IddocumentTypeNavigation?.Code,
                    Note = doc.Note,
                    Filename = doc.Filename,
                    Title = doc.Title,
                    S3Key = doc.S3key,
                    UploadDateTime = doc.UploadDateTime
                };
                // Navigations don't seem to resolve properly for this table so we get this data manually
                model.DocumentTypeCode = (await context.DocumentType.FindAsync(doc.IddocumentType))?.Code;
                model.ClientLoanNumber = (await context.ClientLoan.FindAsync(doc.IdclientLoan))?.LoanNumber;
                result.Add(model);
            }
            return result;
        }

        public async Task<DocumentModel> GetDocument(Guid guid)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ClientDocument.SingleOrDefaultAsync(doc => doc.Guid == guid);
            if (entity.IsDeleted) return null;
            var model = new DocumentModel
            {
                Id = entity.Id,
                Guid = entity.Guid.Value,
                ClientId = entity.Idclient,
                ClientLoanId = entity.IdclientLoan,
                Note = entity.Note,
                Filename = entity.Filename,
                Title = entity.Title,
                S3Key = entity.S3key,
                UploadDateTime = entity.UploadDateTime
            };
            // Navigations don't seem to resolve properly for this table so we get this data manually
            model.DocumentTypeCode = (await context.DocumentType.FindAsync(entity.IddocumentType))?.Code;
            model.ClientLoanNumber = (await context.ClientLoan.FindAsync(entity.IdclientLoan))?.LoanNumber;
            return model;
        }

        public async Task<DocumentModel> CreateDocument(DocumentModel model)
        {
            using var context = _dbContextProvider.GetContext();

            var entity = new ClientDocument
            {
                Idclient = model.ClientId,
                IdclientLoan = model.ClientLoanId,
                Note = model.Note,
                Filename = model.Filename,
                Title = model.Title,
                S3key = model.S3Key
            };
            entity.IddocumentType = (await context.DocumentType.SingleOrDefaultAsync(dt => dt.Code == model.DocumentTypeCode))?.Id;

            await context.ClientDocument.AddAsync(entity);
            await context.SaveChangesAsync();

            model.Id = entity.Id;
            model.Guid = (Guid)entity.Guid;
            model.UploadDateTime = entity.UploadDateTime;
            if (entity.IdclientLoan != null)
            {
                model.ClientLoanNumber = (await context.ClientLoan.SingleOrDefaultAsync(cl => cl.Id == entity.IdclientLoan)).LoanNumber;
            }
            return model;
        }

        public async Task<DocumentModel> UpdateDocument(DocumentModel model)
        {
            using var context = _dbContextProvider.GetContext();

            var entity = await context.ClientDocument.FindAsync(model.Id);
            if (entity == null)
                entity = await context.ClientDocument.SingleOrDefaultAsync(doc => doc.Guid == model.Guid);
            if (entity == null)
                throw new ArgumentException("Document does not exist.");

            entity.IdclientLoan = model.ClientLoanId;
            entity.Title = model.Title;
            entity.Note = model.Note;

            context.ClientDocument.Update(entity);
            await context.SaveChangesAsync();

            model.ClientId = entity.Idclient;
            model.UploadDateTime = entity.UploadDateTime;
            model.ClientLoanNumber = (await context.ClientLoan.SingleOrDefaultAsync(cl => cl.Id == entity.IdclientLoan))?.LoanNumber;

            return model;
        }

        public async Task DeleteDocument(Guid guid, string userName)
        {
            using var context = _dbContextProvider.GetContext();

            var entity = await context.ClientDocument.SingleOrDefaultAsync(doc => doc.Guid == guid);
            entity.IsDeleted = true;
            entity.DeleteUsername = userName;
            entity.DeleteDateTime = DateTime.UtcNow;

            await context.SaveChangesAsync();
        }
    }
}