using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface IDepositService
    {
        Task<IEnumerable<DepositModel>> GetDeposits();
        Task<DepositModel> UpdateDeposit(DepositModel model);
    }
}