using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class LoanRequestService : ILoanRequestService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IMessageService _messageService;
        private readonly IDataHubService _dataHubService;

        public LoanRequestService(IDbContextProvider dbContextProvider, IMessageService messageService, IDataHubService dataHubService)
        {
            _dbContextProvider = dbContextProvider;
            _messageService = messageService;
            _dataHubService = dataHubService;
        }

        public async Task<LoanRequestModel> GetLoanRequest(int requestId)
        {
            using var context = _dbContextProvider.GetContext();
            var result = await context.ExternalLoanWebRequest.FindAsync(requestId);
            return new LoanRequestModel
            {
                Id = result.Id,
                ClientId = result.Idclient,
                ClientFirstName = result.FirstName,
                ClientLastName = result.LastName,
                LoanAmount = result.Amount,
                RequestedDateFormat = String.Format("{0:yyyy-MM-dd HH:mm}", result.RequestedDate),
                StatusCode = result.Status?.Code
            };
        }

        public async Task<IEnumerable<LoanRequestModel>> GetLoanRequestList(int? clientId = null, string statusCode = null)
        {
            using var context = _dbContextProvider.GetContext();
            var query = from req in context.ExternalLoanWebRequest
                        join c in context.Client on req.Idclient equals c.Id
                        select new LoanRequestModel
                        {
                            Id = req.Id,
                            ClientId = req.Idclient,
                            ClientFirstName = c.FirstName,
                            ClientLastName = c.LastName,
                            LoanAmount = req.Amount,
                            PotentialMatch = (bool)req.ClientMatched,
                            RequestedDateFormat = String.Format("{0:yyyy-MM-dd HH:mm}", req.RequestedDate),
                            RequestedDate = req.RequestedDate,
                            StatusCode = req.Status != null ? req.Status.Code : "NEW",
                            CheckListCompleted = req.ChecklistCompleted,
                            IBVCompleted = (from ibv in context.ClientIbv
                                            join ibvs in context.ClientIbvStatus on ibv.IdclientIbvstatus equals ibvs.Id
                                            join elwr in context.ExternalLoanWebRequest on ibv.Guid equals elwr.IbvrequestId
                                            where elwr.IbvrequestId == req.IbvrequestId && ibvs.Code == "COM"
                                            select true
                                           ).SingleOrDefault(),
                            IBVReport = (from ibv in context.ClientIbv
                                         join elwr in context.ExternalLoanWebRequest on ibv.Guid equals elwr.IbvrequestId
                                         where elwr.IbvrequestId == req.IbvrequestId
                                         select ibv.Url
                                           ).SingleOrDefault()
                        };

            if (clientId != null)
            {
                query = query.Where(req => req.ClientId == clientId.Value);
            }
            if (!string.IsNullOrEmpty(statusCode))
            {
                query = query.Where(req => req.StatusCode == statusCode);
            }
            return await query.OrderByDescending(x => x.RequestedDate).ToListAsync();
        }

        public async Task<IEnumerable<LoanRequestModel>> GetLoanRequestClientList(int clientId, LanguageEnum language)
        {
            using var context = _dbContextProvider.GetContext();
            var query = await (from req in context.ExternalLoanWebRequest
                               where req.Idclient == clientId
                               select new LoanRequestModel
                               {
                                   Id = req.Id,
                                   ClientId = req.Idclient,
                                   ClientFirstName = req.FirstName,
                                   ClientLastName = req.LastName,
                                   LoanAmount = req.Amount,
                                   AmountApproved = req.AmountApproved,
                                   PotentialMatch = req.ClientMatched,
                                   RequestedDateFormat = String.Format("{0:yyyy-MM-dd HH:mm}", req.RequestedDate),
                                   RequestedDate = req.RequestedDate,
                                   StatusCode = req.Status != null ? req.Status.Code : null,
                                   CheckListCompleted = req.ChecklistCompleted,
                                   Checklist = (
                                                (from ewlrc in context.ExternalLoanWebRequestChecklist
                                                 join lrc in context.LoanRequestChecklist on ewlrc.IdloanRequestChecklist equals lrc.Id
                                                 where ewlrc.IdexternalLoanWebRequest == req.Id
                                                 select new LoanRequestChecklistModel()
                                                 {
                                                     Id = ewlrc.Id,
                                                     Name = language == LanguageEnum.English ? lrc.NameEn : lrc.NameFr,
                                                     Code = lrc.Code,
                                                     Confirmed = ewlrc.Confirmed
                                                 }).ToList()
                                   )
                               }).OrderByDescending(x => x.RequestedDate).ToListAsync();

            return query;
        }

        public async Task<LoanRequestModel> UpdateRequestChecklist(LoanRequestModel model)
        {
            using var context = _dbContextProvider.GetContext();
            var checklist = await context.ExternalLoanWebRequestChecklist
                                .Where(x => x.IdexternalLoanWebRequest == model.Id)
                                .ToListAsync();

            foreach (var item in checklist)
            {
                item.Confirmed = model.Checklist.Where(x => x.Id == item.Id).SingleOrDefault().Confirmed;
            }
            await context.SaveChangesAsync();

            // If all checkboxes are checked
            if (model.Checklist.Where(x => x.Confirmed == true).Count() == model.Checklist.Count())
            {
                var request = await context.ExternalLoanWebRequest
                                    .Where(x => x.Id == model.Id)
                                    .SingleAsync();
                request.ChecklistCompleted = true;
                model.CheckListCompleted = true;


                // Set Loan Request status to Approved
                var requestStatus = await context.LoanRequestStatus
                                        .Where(x => x.Code == "ACCEPTED")
                                        .SingleOrDefaultAsync();

                request.StatusId = requestStatus.Id;
                model.StatusCode = requestStatus.Code;
                await context.SaveChangesAsync();
            }

            return model;
        }

        public async Task<LoanRequestModel> UpdateLoanRequest(LoanRequestModel model)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ExternalLoanWebRequest
                               .Include(x => x.IdlanguageNavigation)
                               .Where(x => x.Id == model.Id)
                               .SingleOrDefaultAsync();

            if (model == null) throw new KeyNotFoundException("No loan request found with this Id.");

            var statusId = (await context.LoanRequestStatus
                .SingleOrDefaultAsync(status => status.Code == model.StatusCode))?.Id;
            if (statusId == null)
            {
                throw new ArgumentException("Loan request status code is either missing or invalid.");
            }

            entity.StatusId = statusId;
            if (model.AmountApproved != null)
            {
                entity.AmountApproved = model.AmountApproved;
            }

            context.ExternalLoanWebRequest.Update(entity);
            await context.SaveChangesAsync();

            // If loan is declined, send Email and SMS
            if (model.StatusCode.Contains("DENIED"))
            {
                var language = entity.IdlanguageNavigation.Code == "E"
                        ? LanguageEnum.English
                        : LanguageEnum.French;

                await _messageService.SendMessage_LoanRefused(language, new LMS.Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                {
                    LoanRefused = new LMS.Domain.Core.Models.EmailTemplates.LoanRefusedModel()
                    {
                        CellPhone = entity.CellPhoneNumber,
                        EmailAddress = entity.PrimaryEmail,
                        FirstName = entity.FirstName,
                        LastName = entity.LastName,
                        LoanAmount = entity.Amount.ToString()
                    }
                });
            }

            // If client declines loan, send Email and SMS
            if (model.StatusCode == "CANCELLED_BY_CLIENT")
            {
                var language = entity.IdlanguageNavigation.Code == "E"
                        ? LanguageEnum.English
                        : LanguageEnum.French;

                await _messageService.SendMessage_LoanDeclined(language, new LMS.Domain.Core.Models.EmailTemplates.EmailTemplateModel()
                {
                    LoanDeclined = new LMS.Domain.Core.Models.EmailTemplates.LoanDeclinedModel()
                    {
                        CellPhone = entity.CellPhoneNumber,
                        EmailAddress = entity.PrimaryEmail,
                        FirstName = entity.FirstName,
                        LastName = entity.LastName
                    }
                }); ;
            }

            // Update CreditbookCheck only if initial CreditBook Request was succcessfull
            if ((bool)entity.CreditBookCheckSuccessfull)
            {
                // Can not update for certain SLOAN status until CreditBook Check allows them
                // Renouvellement possible
                if (model.StatusCode != "POSSIBLE_RENEWAL")
                {
                    model.RequestedDate = entity.RequestedDate;
                    await _dataHubService.UpdateLoanRequestStatus(model);
                }
            }

            return model;
        }
    }
}