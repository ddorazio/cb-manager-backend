using System;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;

namespace CreditBook.LMS.Service.Core
{
    public class DataHubService : IDataHubService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IDataHubClient _datahubClient;
        private readonly IConfigurationService _configurationService;
        private readonly IWebhookService_24HR _webhookService24HR;

        public DataHubService(IDbContextProvider dbContextProvider, IDataHubClient datahubClient, IConfigurationService configurationService, IWebhookService_24HR webhookService24HR)
        {
            _dbContextProvider = dbContextProvider;
            _datahubClient = datahubClient;
            _configurationService = configurationService;
            _webhookService24HR = webhookService24HR;
        }

        public async Task UpdateLoanRequestStatus(LoanRequestModel model)
        {
            // Get the datahub request ID
            using var context = _dbContextProvider.GetContext();
            var request = await context.ExternalLoanWebRequest.FindAsync(model.Id);
            // Perform the request proper
            var endpoint = $"loanrequests/{request.CreditBookCheckRequestId}/status/";
            var reason = GetReason(model.StatusCode);

            // Verify Lender
            bool usePreviousApiKey = false;
            if (_configurationService.ValidateLenderIs_24HR())
            {
                if (model.RequestedDate <= new DateTime(2021, 06, 01, 11, 28, 00))
                    usePreviousApiKey = true;
            }

            if (reason != "")
            {
                if (model.StatusCode != "DENIED_LOAN_IN_PROGRESS")
                {
                    var requestBody = new
                    {
                        status = GetStatus(model.StatusCode),
                        date = DateTime.UtcNow,
                        reason = GetReason(model.StatusCode),
                        comments = (model.Comments != null) ? model.Comments : "",
                    };

                    if (usePreviousApiKey)
                    {
                        await _datahubClient.PostAsync_Custom(endpoint, requestBody);
                    }
                    else
                    {
                        await _datahubClient.PostAsync(endpoint, requestBody);
                    }
                }
                else
                {
                    var requestBody = new
                    {
                        status = GetStatus(model.StatusCode),
                        date = DateTime.UtcNow,
                        comments = (model.Comments != null) ? model.Comments : "",
                    };

                    if (usePreviousApiKey)
                    {
                        await _datahubClient.PostAsync_Custom(endpoint, requestBody);
                    }
                    else
                    {
                        await _datahubClient.PostAsync(endpoint, requestBody);
                    }
                }
            }
            else
            {
                var requestBody = new
                {
                    status = GetStatus(model.StatusCode),
                    date = DateTime.UtcNow,
                    comments = (model.Comments != null) ? model.Comments : "",
                };

                if (usePreviousApiKey)
                {
                    await _datahubClient.PostAsync_Custom(endpoint, requestBody);
                }
                else
                {
                    await _datahubClient.PostAsync(endpoint, requestBody);
                }
            }

            try
            {
                await _webhookService24HR.RequestStatusChange_24HR(model);
            }
            catch { }
        }

        // [new, refused, in-collection, fraudster, bankruptcy, consumer-proposal, warning, accepted, in-payment-agreement, request-canceled]
        private string GetStatus(string status) => status switch
        {
            "NEW" => "new",
            "ACCEPTED" => "accepted",
            "PREAPPROVED" => "pre-approbation",
            "WAITING_MISSING_CONTRACT" => "missing-signed-contract",
            "WAITING_MISSING_IBV" => "missing-IBV",
            "WAITING_MISSING_INFO" => "missing-information",
            "CANCELLED_BY_CLIENT" => "request-canceled",
            "DUPLICATE" => "duplicates",
            "IN_COLLECTIONS" => "in-collection",
            "DENIED_EXCESS_LOANS" => "refused",
            "DENIED_ALREADY_REJECTED" => "refused",
            "DENIED_EXCESS_REQUESTS" => "refused",
            "DENIED_NO_JOB" => "refused",
            "DENIED_EXCESS_NSF" => "refused",
            "DENIED_COLLECTION" => "refused",
            "DENIED_OTHER" => "refused",
            "DENIED_NO_CAPACITY" => "refused",
            "DENIED_LOAN_IN_PROGRESS" => "refused",
            "DENIED_STOPPED_PAYMENTS" => "refused",

            "IN_PAYMENT_AGREEMENT" => "in-payment-agreement",
            "BANKRUPTCY" => "bankruptcy",
            "LOAN_PAY_IN_FULL" => "loan-pay-in-full",
            "FRAUDSTER" => "fraudster",
            "WARNING" => "warning",
            _ => throw new Exception("Loan request status field missing or not valid for CreditBookCheck.")
        };

        private string GetReason(string status) => status switch
        {
            "DENIED_EXCESS_LOANS" => "too-many-loans",
            "DENIED_ALREADY_REJECTED" => "already-refused-recently",
            "DENIED_EXCESS_REQUESTS" => "too-many-requests",
            "DENIED_NO_JOB" => "no-job",
            "DENIED_EXCESS_NSF" => "too-many-nsf",
            "DENIED_COLLECTION" => "in-collection-with-others-companies",
            "DENIED_OTHER" => "other-see-comments",
            "DENIED_NO_CAPACITY" => "no-capacity",
            "DENIED_STOPPED_PAYMENTS" => "stopped-payments",
            _ => ""
        };
    }
}