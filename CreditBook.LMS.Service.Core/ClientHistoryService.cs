using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class ClientHistoryService : IClientHistoryService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IUserService _userService;

        public ClientHistoryService(IDbContextProvider dbContextProvider, IUserService userService)
        {
            _dbContextProvider = dbContextProvider;
            _userService = userService;
        }

        public async Task<ClientHistoryModel> AddClientHistoryEntry(int clientId, ClientHistoryTypeEnum clientHistoryType)
        {
            // For external Web requests, there is not User logged in
            string username = null;
            try
            {
                username = _userService.GetUserName();
            }
            catch (Exception e) { }
                
            var entity = new ClientHistory
            {
                // DateTime field is populated by SQL on insert
                Idclient = clientId,
                IdclientHistoryType = (int)clientHistoryType,
                Username = username
            };

            using var context = _dbContextProvider.GetContext();
            await context.ClientHistory.AddAsync(entity);
            await context.SaveChangesAsync();
            return new ClientHistoryModel
            {
                Id = entity.Id,
                ClientId = entity.Idclient,
                DateTime = entity.DateTime,
            };
        }

        public async Task<IEnumerable<ClientHistoryModel>> GetClientHistory(int clientId)
        {
            using var context = _dbContextProvider.GetContext();
            var history = await context.ClientHistory
                .Where(ch => ch.Idclient == clientId)
                .ToListAsync();

            var result = new List<ClientHistoryModel>();
            foreach (var hist in history)
            {
                var histType = await context.ClientHistoryType.FindAsync(hist.IdclientHistoryType);
                result.Add(new ClientHistoryModel
                {
                    Id = hist.Id,
                    ClientId = hist.Idclient,
                    DateTime = hist.DateTime,
                    Code = histType.Code,
                    DescriptionEn = histType.DescriptionEn,
                    DescriptionFr = histType.DescriptionFr
                });
            }
            return result;
        }
    }
}