﻿using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core
{
    public class NotificationService : INotificationService
    {

        private readonly IDbContextProvider _dbContextProvider;
        private readonly IUserService _userService;
        private IHubContext<NotificationsHub> _hubContext { get; set; }


        public NotificationService(IDbContextProvider dbContextProvider, IUserService userService, IHubContext<NotificationsHub> hubcontext)
        {
            _dbContextProvider = dbContextProvider;
            _userService = userService;
            _hubContext = hubcontext;

        }

        public async Task NewNotification(NotificationModel model)
        {
            using var context = _dbContextProvider.GetContext();
            var users = context.AspNetUsers.ToList();

            try
            {
                var notif = new Notification()
                {
                    IdClient = model.IdClient,
                    Type = model.Type,
                    DateCreated = DateTime.Now,
                    Path = model.Path
                };

                await context.Notification.AddAsync(notif);
                await context.SaveChangesAsync();

                var newNotif = new NotificationModel
                {
                    Type = notif.Type,
                    DateCreatedFormat = DateTime.Now.ToString(),
                    Path = notif.Path,
                    Read = false,
                };

                await context.SaveChangesAsync();
                await this._hubContext.Clients.All.SendAsync("NewNotification", newNotif.Type);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<NotificationModel>> GetNotifications(string type, string language)
        {
            try
            {
                using var context = _dbContextProvider.GetContext();
                var listNotifs = await (from notif in context.Notification
                                        join notifMessage in context.NotificationMessage
                                        on notif.Type equals notifMessage.Type
                                        join client in context.Client
                                        on notif.IdClient equals client.Id
                                        where notif.Type == type
                                        orderby notif.Read == true ascending, notif.DateCreated descending
                                        select new NotificationModel
                                        {
                                            Id = notif.Id,
                                            Type = notif.Type,
                                            Message = GetMessageByLanguage(language, notifMessage, client),
                                            DateCreated = notif.DateCreated ?? DateTime.Now,
                                            DateCreatedFormat = notif.DateCreated.ToString() ?? "",
                                            DateRead = notif.DateRead,
                                            DateReadFormat = notif.DateRead.ToString() ?? "",
                                            Path = notif.Path,
                                            Read = notif.Read ?? false
                                        }).ToListAsync();
                return listNotifs;
            }
            catch (Exception ex)
            {
                if (ex == null)
                {
                    return new List<NotificationModel>();
                }
                else
                {
                    throw ex;
                }
            }
        }

        public async Task SetNotificationRead(int id)
        {
            using var context = _dbContextProvider.GetContext();
            var notification = await (from notif in context.Notification
                                      where notif.Id == id
                                      select new Notification
                                      {
                                          Id = notif.Id,
                                          Type = notif.Type,
                                          IdClient = notif.IdClient,
                                          DateCreated = notif.DateCreated ?? DateTime.Now,
                                          DateRead = DateTime.Now,
                                          Path = notif.Path,
                                          Read = true,
                                      }).FirstOrDefaultAsync();


            context.Notification.Update(notification);
            context.SaveChanges();
            await this._hubContext.Clients.All.SendAsync("NewNotification", notification.Type);
        }

        public async Task<NotificationModel> CreateNotificationIBVModel(string requestId)
        {
            using var context = _dbContextProvider.GetContext();
            var ibv = await context.ClientIbv.Where(x => x.Guid == requestId)
                                            .SingleOrDefaultAsync();

            NotificationModel model = new NotificationModel
            {
                Type = "IBV",
                IdClient = ibv.Idclient,
                Path = "/apps/customers/detail/" + ibv.Idclient + "/ibv"
            };


            return model;
        }

        public async Task<NotificationModel> CreateNotificationRejectModel(LoanGenericPaymentModel modelLoan)
        {
            using var context = _dbContextProvider.GetContext();
            var client = await context.ClientLoan.Where(x => x.Id == modelLoan.LoanId)
                                        .Select(x => new
                                        {
                                            IdClient = x.Idclient,
                                        })
                                        .SingleOrDefaultAsync();

            NotificationModel model = new NotificationModel
            {
                Type = "REJECT",
                IdClient = client.IdClient,
                Path = "/apps/customers/detail/" + client.IdClient + "/collections"
            };

            return model;
        }

        public async Task<NotificationModel> CreateNotificationContractSignedModel(Guid documentId)
        {
            using var context = _dbContextProvider.GetContext();
            var client = await (from clcs in context.ClientLoanContractSignature
                                join cl in context.ClientLoan on clcs.ClientLoanId equals cl.Id
                                join c in context.Client on cl.Idclient equals c.Id
                                where clcs.Guid == documentId
                                select new
                                {
                                    ClientId = c.Id
                                }).SingleOrDefaultAsync();

            NotificationModel model = new NotificationModel
            {
                Type = "CONTRACT",
                IdClient = client.ClientId,
                Path = $"/apps/customers/detail/{client.ClientId}/documents"
            };

            return model;
        }

        private static string GetMessageByLanguage(string language, NotificationMessage message, Client client)
        {

            string msg = "";

            try
            {
                if (language == "EN")
                {
                    msg = message.MessageEn.Replace("NAME", client.FirstName + "'s " + client.LastName + "'s") ?? "";
                }
                else if (language == "FR")
                {
                    msg = message.MessageFr.Replace("NAME", client.FirstName + " " + client.LastName) ?? "";
                }

                return msg;
            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}
