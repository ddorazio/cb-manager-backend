using System;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using CreditBook.LMS.External.Core;

namespace CreditBook.LMS.Service.Core
{
    public interface IMessageService
    {
        Task SendMessage_ApplicationWithoutDocuments(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_ApplicationWithDocuments(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_IbvApplication(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_Contract(EmailTemplateModel model);
        Task SendMessage_ExistingContract(EmailTemplateModel model);
        Task SendMessage_Loan(EmailTemplateModel email);
        Task SendMessage_LoanRefused(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_LoanDeclined(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_BankDebit(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_CollectionLevel1(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_CollectionLevel2(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_CollectionLevel3(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_CollectionAccountClosed(LanguageEnum language, EmailTemplateModel model);
        Task SendMessage_FirstNSF(LanguageEnum language, EmailTemplateModel model);
        Task SendInteracPaymentReminder(LanguageEnum language, string emailTo, DateTime dueDate, decimal amountDue);
        Task SendMessage_IbvReminder(LanguageEnum language, EmailTemplateModel model);

    }
}