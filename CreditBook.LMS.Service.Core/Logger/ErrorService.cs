using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CreditBook.LMS.External.Core;

namespace CreditBook.LMS.Service.Core.Logger
{
    public class ErrorService : IErrorService
    {
        private readonly string _path;
        public ErrorService(string path)
        {
            _path = path;
        }

        public void Logger(Exception ex, string model)
        {
            // Create directory if it does not exist
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }

            // Save Error to TXT File
            using (StreamWriter writer = new StreamWriter(_path + "Error.txt", true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() + "<br/>");
                writer.WriteLine("Model :" + model);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }

        public void LoanRequestLogger(string model)
        {
            // Create directory if it does not exist
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }

            // Save Error to TXT File
            using (StreamWriter writer = new StreamWriter(_path + "Error.txt", true))
            {
                //writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() + "<br/>");
                writer.WriteLine("Loan Request" + DateTime.Now + " :" + model);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
    }
}