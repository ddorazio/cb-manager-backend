using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface IDocumentService
    {
        Task<DocumentModel> CreateDocument(DocumentModel model);
        Task DeleteDocument(Guid documentId, string userName);
        Task<DocumentModel> GetDocument(Guid documentId);
        Task<IEnumerable<DocumentModel>> GetDocuments(int clientId);
        Task<DocumentModel> UpdateDocument(DocumentModel model);
    }
}