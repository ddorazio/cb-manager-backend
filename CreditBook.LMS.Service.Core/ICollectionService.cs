using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface ICollectionService
    {
        Task<IEnumerable<CollectionSummaryModel>> GetCollectionList(CollectionStatusEnum? status = null);
        Task<CollectionModel> GetCollection(int collectionId);
        Task<CollectionModel> UpdateCollection(CollectionModel data);
        Task<CollectionCommentModel> SaveComment(CollectionCommentModel data);
        Task DeleteComment(int collectionCommentId);
        Task<CollectionProcedureStepModel> ToggleProcedureStep(CollectionProcedureStepModel step);
        Task<IEnumerable<CollectionCommentModel>> GetComments(int collectionId);
        Task SendCollectionReminders(IEnumerable<int> collectionIds);
    }
}