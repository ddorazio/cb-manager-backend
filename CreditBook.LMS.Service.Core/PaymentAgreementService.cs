
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class PaymentAgreementService : IPaymentAgreement
    {
        private readonly IDbContextProvider _dbContextProvider;
        public PaymentAgreementService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<List<InteracPaymentAgreementModel>> InteracPaymentAgreement()
        {
            using var context = _dbContextProvider.GetContext();
            var paymentAgreements = await (from agr in context.ClientLoanPaymentAgreement
                                           join payment in context.ClientLoanPayment on agr.IdclientLoanPayment equals payment.Id
                                           join loan in context.ClientLoan on payment.IdclientLoan equals loan.Id
                                           join client in context.Client on loan.Idclient equals client.Id
                                           select new InteracPaymentAgreementModel()
                                           {
                                               IdLoanPayment = payment.Id,
                                               FirstName = client.FirstName,
                                               LastName = client.LastName,
                                               ConfirmationDate = String.Format("{0:yyyy-MM-dd}", agr.ConfirmedDate),
                                               PaymentAmount = payment.Amount,
                                               PaymentDate = payment.PaymentDate,
                                               PaymentDateFormatted = String.Format("{0:yyyy-MM-dd}", payment.PaymentDate)
                                           })
                                           .OrderBy(x => x.PaymentDate)
                                           .ToListAsync();

            return paymentAgreements;
        }

        public async Task<InteracPaymentAgreementModel> UpdateAgreement(InteracPaymentAgreementModel model)
        {
            using var context = _dbContextProvider.GetContext();
            var paymentAgreement = await context.ClientLoanPaymentAgreement
                                                .Where(x => x.IdclientLoanPayment == model.IdLoanPayment)
                                                .SingleOrDefaultAsync();
            paymentAgreement.ConfirmedDate = model.DepositedDate;
            context.ClientLoanPaymentAgreement.Update(paymentAgreement);
            await context.SaveChangesAsync();

            var status = await context.PaymentStatus
                                        .Where(x => x.Code == "PAID")
                                        .SingleOrDefaultAsync();
            var loanPayment = await context.ClientLoanPayment
                                            .Where(x => x.Id == model.IdLoanPayment)
                                            .SingleOrDefaultAsync();
            loanPayment.Description = "Virement interac/Interac payment";
            loanPayment.IdpaymentStatus = status.Id;
            context.ClientLoanPayment.Update(loanPayment);
            await context.SaveChangesAsync();

            model.ConfirmationDate = String.Format("{0:yyyy-MM-dd}", model.DepositedDate);
            return model;
        }
    }
}