using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class MessageService : IMessageService
    {
        private readonly bool _adminMode;
        private readonly bool _sendSms;
        private readonly bool _sendEmail;
        private readonly string _phoneNumber;
        private readonly string _emailFrom;
        private readonly IDbContextProvider _dbContextProvider;
        private readonly ITwilioClient _twilioClient;
        private readonly ISendgridClient _sendgridClient;

        public MessageService(IDbContextProvider dbContextProvider, ITwilioClient twilioClient, ISendgridClient sendgridClient)
        {
            _dbContextProvider = dbContextProvider;
            _twilioClient = twilioClient;
            _sendgridClient = sendgridClient;

            using var context = _dbContextProvider.GetContext();
            var configuration = context.LoanerConfiguration.ToList();

            _adminMode = Convert.ToBoolean(configuration.Where(x => x.Key == "ADMIN_MODE").SingleOrDefault().Value);
            _sendSms = Convert.ToBoolean(configuration.Where(x => x.Key == "SEND_SMS").SingleOrDefault().Value);
            _sendEmail = Convert.ToBoolean(configuration.Where(x => x.Key == "SEND_EMAIL").SingleOrDefault().Value);
            _emailFrom = Convert.ToString(configuration.Where(x => x.Key == "ADMIN_EMAIL").SingleOrDefault().Value);
        }

        #region Email & SMS Messages

        public async Task SendMessage_ApplicationWithoutDocuments(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$ibv_link", model.WithoutDocuments.IBVUrl);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.ApplicationWithoutDocuments);

            if (_sendEmail)
            {
                using var context = _dbContextProvider.GetContext();
                var documentAddress = (await context.LoanerConfiguration.SingleAsync(conf => conf.Key == "ADMIN_EMAIL")).Value;

                Func<string, string> replaceTokens = (string body) => body
                    .Replace("$email_document", documentAddress)
                    .Replace("$ibv_link", model.WithoutDocuments.IBVHtmlURL);

                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.WithoutDocuments.ClientEmail),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.WithoutDocuments.ClientCell, smsBody);
            }
        }

        public async Task SendMessage_ApplicationWithDocuments(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            using var context = _dbContextProvider.GetContext();
            var documentAddress = (await context.LoanerConfiguration.SingleAsync(conf => conf.Key == "ADMIN_EMAIL")).Value;

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$email_document", documentAddress);
            }

            Func<string, string> replaceTokens = (string body) => body
                .Replace("$email_document", documentAddress)
                .Replace("$ibv_link", model.WithDocuments.IBVHtmlURL);

            var template = await GetMessageTemplate(MessageTemplateEnum.ApplicationWithDocuments);

            if (_sendEmail)
            {
                // Prepare email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.WithDocuments.ClientEmail),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.WithDocuments.ClientCell, smsBody);
            }
        }

        public async Task SendMessage_IbvApplication(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$first_name", model.IBV.FirstName)
                    .Replace("$last_name", model.IBV.LastName)
                    .Replace("$ibv_link", model.IBV.IBVUrl);
            }

            string replaceTokens(string body)
            {
                return body
                    .Replace("$first_name", model.IBV.FirstName)
                    .Replace("$last_name", model.IBV.LastName)
                    .Replace("$ibv_link", model.IBV.IBVHtmlURL);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.IbvApplication);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.IBV.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.IBV.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_Contract(EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            var language = model.Contract.Language == "E" ? LanguageEnum.English : LanguageEnum.French;
            var digitalUrl = model.Contract.Language == "E"
                            ? "<a href='" + model.Contract.DigitalUrl + "'>contract</a>"
                            : "<a href='" + model.Contract.DigitalUrl + "'>contrat</a>";

            string replaceTokens(string body)
            {
                return body
                    .Replace("$first_name", model.Contract.FirstName)
                    .Replace("$last_name", model.Contract.LastName)
                    .Replace("$loaner_amount", model.Contract.LoanAmount)
                    .Replace("$digital", digitalUrl);
            }

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$first_name", model.Contract.FirstName)
                    .Replace("$last_name", model.Contract.LastName)
                    .Replace("$loaner_amount", model.Contract.LoanAmount)
                    .Replace("$digital", model.Contract.DigitalUrl);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.LoanContract);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.Contract.EmailAddress),
                    emailSubject,
                    emailBody
                //model.Contract.ContractPath,
                //model.Contract.ContractName
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.Contract.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_ExistingContract(EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            var language = model.ExistingContract.Language == "E" ? LanguageEnum.English : LanguageEnum.French;
            var digitalUrl = model.ExistingContract.Language == "E"
                            ? "<a href='" + model.ExistingContract.DigitalUrl + "'>contract</a>"
                            : "<a href='" + model.ExistingContract.DigitalUrl + "'>contrat</a>";

            var emailTemplateEn = $@"<p>Good morning Mr/Mrs {model.ExistingContract.FirstName} {model.ExistingContract.LastName},</p>
                            <p></p>
                            <p>Here is a copy of you loan contract {digitalUrl}</p>
                            <p>Thank you for trusting us.</p>";

            var emailTemplateFr = $@"<p>Bonjour Monsieur/Madame {model.ExistingContract.FirstName} {model.ExistingContract.LastName},</p>
                            <p></p>
                            <p>Voici une copie de votre contrat {digitalUrl}</p>
                            <p>Merci de nous faire confiance.</p>";

            var smsTemplateEn = $@"Good morning Mr/Mrs {model.ExistingContract.FirstName} {model.ExistingContract.LastName} 
                                Here is a copy of you loan contract {model.ExistingContract.DigitalUrl}";

            var smsTemplateFr = $@"Bonjour Monsieur/Madame {model.ExistingContract.FirstName} {model.ExistingContract.LastName} 
                                Voici une copie de votre contrat {model.ExistingContract.DigitalUrl}";

            // Prepare Email body and send
            var emailSubject = language == LanguageEnum.French ? "Votre contrat de prêt" : "Your loan contract";
            var emailBody = language == LanguageEnum.French ? emailTemplateFr : emailTemplateEn;
            emailBody = await BuildEmailTemplate(emailBody);
            await _sendgridClient.SendEmail
            (
                new MailAddress(_emailFrom),
                new MailAddress(model.ExistingContract.EmailAddress),
                emailSubject,
                emailBody
            );

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = language == LanguageEnum.French ? smsTemplateFr : smsTemplateEn;
                await _twilioClient.SendSms(model.ExistingContract.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_Loan(EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            var language = model.loanPDF.Language == "EN" ? LanguageEnum.English : LanguageEnum.French;

            //string replaceTokens(string body)
            //{
            //    return body
            //        .Replace("$first_name", model.loanPDF.FirstName)
            //        .Replace("$last_name", model.loanPDF.LastName)
            //}
            var msgTemplateType = (model.loanPDF.TypePDF == 0) ? MessageTemplateEnum.LoanAccountBalance : MessageTemplateEnum.LoanAccountDetail;
            var template = await GetMessageTemplate(msgTemplateType);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                //emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.loanPDF.EmailAddress),
                    emailSubject,
                    emailBody,
                    model.loanPDF.LoanPDFPath,
                    model.loanPDF.LoanPDFName
                );
            }

            // Prepare SMS body and send
            //var smsBody = template.GetSmsContent(language);
            //smsBody = replaceTokens(smsBody);
            //await _twilioClient.SendSms(model.Contract.CellPhone, smsBody);
        }

        public async Task SendMessage_LoanRefused(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            string replaceTokens(string body)
            {
                return body
                    .Replace("$first_name", model.LoanRefused.FirstName)
                    .Replace("$last_name", model.LoanRefused.LastName)
                    .Replace("$loaner_amount", model.LoanRefused.LoanAmount);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.LoanApplicationDenied);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.LoanRefused.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokens(smsBody);
                await _twilioClient.SendSms(model.LoanRefused.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_LoanDeclined(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            using var context = _dbContextProvider.GetContext();
            var lender = (await context.LoanerConfiguration.SingleAsync(conf => conf.Key == "COMPANY_NAME")).Value;

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$first_name", model.LoanDeclined.FirstName)
                    .Replace("$last_name", model.LoanDeclined.LastName)
                    .Replace("$loaner_name", lender)
                    .Replace("$ibv_link", model.LoanDeclined.IBVUrl);
            }

            string replaceTokens(string body)
            {
                return body
                    .Replace("$first_name", model.LoanDeclined.FirstName)
                    .Replace("$last_name", model.LoanDeclined.LastName)
                    .Replace("$loaner_name", lender)
                    .Replace("$ibv_link", model.LoanDeclined.IBVHtmlURL);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.LoanApplicationCancelled);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.LoanDeclined.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.LoanDeclined.CellPhone, smsBody);
            }
        }

        public async Task SendInteracPaymentReminder(LanguageEnum language, string emailTo, DateTime dueDate, decimal amountDue)
        {
            if (_adminMode)
                return;

            var template = await GetMessageTemplate(MessageTemplateEnum.InteracCollectionReminder);

            if (_sendEmail)
            {
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(emailTo),
                    emailSubject,
                    emailBody
                );
            }
        }

        public async Task SendMessage_IbvReminder(LanguageEnum language, EmailTemplateModel model)
        {
            if (_adminMode)
                return;

            using var context = _dbContextProvider.GetContext();
            var lender = (await context.LoanerConfiguration.SingleAsync(conf => conf.Key == "COMPANY_NAME")).Value;
            var adminEmail = (await context.LoanerConfiguration.SingleAsync(conf => conf.Key == "ADMIN_EMAIL")).Value;

            string replaceTokensSms(string body)
            {
                return body
                    .Replace("$loaner_name", lender)
                    .Replace("$ibv_link", model.IBVReminder.IBVUrl);
            }

            string replaceTokens(string body)
            {
                return body
                    .Replace("$loaner_name", lender)
                    .Replace("$ibv_link", model.IBVReminder.IBVHtmlURL)
                    .Replace("$email_contact", adminEmail);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.IbvReminder);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.IBVReminder.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokensSms(smsBody);
                await _twilioClient.SendSms(model.IBVReminder.CellPhone, smsBody);
            }
        }


        public async Task SendMessage_CollectionLevel1(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$amount_due", model.Collection.LoanPaymentAmount)
                    .Replace("$first_name", model.Collection.FirstName)
                    .Replace("$last_name", model.Collection.LastName);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.Level1Collection);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.Collection.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokens(smsBody);
                await _twilioClient.SendSms(model.Collection.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_CollectionLevel2(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$amount_due", model.Collection.LoanPaymentAmount)
                    .Replace("$first_name", model.Collection.FirstName)
                    .Replace("$last_name", model.Collection.LastName);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.Level2Collection);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.Collection.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokens(smsBody);
                await _twilioClient.SendSms(model.Collection.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_CollectionLevel3(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$amount_due", model.Collection.LoanPaymentAmount)
                    .Replace("$first_name", model.Collection.FirstName)
                    .Replace("$last_name", model.Collection.LastName);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.Level3Collection);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.Collection.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokens(smsBody);
                await _twilioClient.SendSms(model.Collection.CellPhone, smsBody);
            }
        }

        public async Task SendMessage_CollectionAccountClosed(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$amount_due", model.Collection.LoanPaymentAmount)
                    .Replace("$first_name", model.Collection.FirstName)
                    .Replace("$last_name", model.Collection.LastName);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.CollectionAccountClosed);

            if (_sendEmail)
            {
                // Prepare Email body and send
                var emailSubject = template.GetEmailSubject(language);
                var emailBody = template.GetEmailContent(language);
                emailBody = replaceTokens(emailBody);
                emailBody = await BuildEmailTemplate(emailBody);
                await _sendgridClient.SendEmail
                (
                    new MailAddress(_emailFrom),
                    new MailAddress(model.Collection.EmailAddress),
                    emailSubject,
                    emailBody
                );
            }

            // Prepare SMS body and send
            if (_sendSms)
            {
                var smsBody = template.GetSmsContent(language);
                smsBody = replaceTokens(smsBody);
                await _twilioClient.SendSms(model.Collection.CellPhone, smsBody);
            }
        }

        #region Not Used
        public async Task SendMessage_BankDebit(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$due_date", model.BankDebit.LoanPaymentDate)
                    .Replace("$amount_due", model.BankDebit.LoanPaymentAmount);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.BankDebit);

            // Prepare Email body and send
            var emailSubject = template.GetEmailSubject(language);
            var emailBody = template.GetEmailContent(language);
            emailBody = replaceTokens(emailBody);
            emailBody = await BuildEmailTemplate(emailBody);
            await _sendgridClient.SendEmail
            (
                new MailAddress(_emailFrom),
                new MailAddress(model.BankDebit.EmailAddress),
                emailSubject,
                emailBody
            );

            // Prepare SMS body and send
            var smsBody = template.GetSmsContent(language);
            smsBody = replaceTokens(smsBody);
            await _twilioClient.SendSms(model.BankDebit.CellPhone, smsBody);
        }

        public async Task SendMessage_FirstNSF(LanguageEnum language, EmailTemplateModel model)
        {
            string replaceTokens(string body)
            {
                return body
                    .Replace("$first_name", model.NSF.FirstName)
                    .Replace("$last_name", model.NSF.LastName);
            }

            var template = await GetMessageTemplate(MessageTemplateEnum.Level1Collection);

            // Prepare Email body and send
            var emailSubject = template.GetEmailSubject(language);
            var emailBody = template.GetEmailContent(language);
            emailBody = await BuildEmailTemplate(emailBody);
            await _sendgridClient.SendEmail
            (
                new MailAddress(_emailFrom),
                new MailAddress(model.NSF.EmailAddress),
                emailSubject,
                emailBody
            );

            // Prepare SMS body and send
            var smsBody = template.GetSmsContent(language);
            smsBody = replaceTokens(smsBody);
            await _twilioClient.SendSms(model.NSF.CellPhone, smsBody);
        }

        #endregion

        #endregion

        private async Task<MessageTemplate> GetMessageTemplate(MessageTemplateEnum templateId)
        {
            using var context = _dbContextProvider.GetContext();
            return await context.MessageTemplate.FindAsync((int)templateId);
        }

        private async Task<string> BuildEmailTemplate(string body)
        {
            const string logoToken = "$logoUrl";
            using var context = _dbContextProvider.GetContext();
            var template = await context.EmailConfiguration.FirstOrDefaultAsync();
            if (template == null) return body;
            var header = (template.HeaderContentFr ?? "")
                .Replace(logoToken, $"<img src={template.LogoUrl}></img>");
            var footer = (template.FooterContentFr ?? "")
                .Replace(logoToken, $"<img src={template.LogoUrl}></img>");

            //return header + body + footer;
            return body + footer;
        }
    }

    public static class MessageTemplateExtensions
    {
        // Extend the EF-generated class with accessors
        public static string GetEmailSubject(this MessageTemplate template, LanguageEnum language)
            => language switch
            {
                LanguageEnum.English => template.SubjectEn,
                LanguageEnum.French => template.SubjectFr,
                _ => ""
            };

        public static string GetEmailContent(this MessageTemplate template, LanguageEnum language)
            => language switch
            {
                LanguageEnum.English => template.EmailContentEn,
                LanguageEnum.French => template.EmailContentFr,
                _ => ""
            };

        public static string GetSmsContent(this MessageTemplate template, LanguageEnum language)
            => language switch
            {
                LanguageEnum.English => template.SmsContentEn,
                LanguageEnum.French => template.SmsContentFr,
                _ => ""
            };
    }
}