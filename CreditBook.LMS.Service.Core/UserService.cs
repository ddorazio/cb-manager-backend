﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace CreditBook.LMS.Service.Core
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor accessor;

        public UserService(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }

        public ClaimsPrincipal GetUser()
        {
            return accessor?.HttpContext?.User as ClaimsPrincipal;
        }

        public string GetUserName()
        {
            ClaimsPrincipal user = GetUser();
            return user.Identities.FirstOrDefault().Claims.FirstOrDefault().Value; ;
        }
    }
}
