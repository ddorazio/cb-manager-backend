﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CreditBook.LMS.Common.Utilities;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Core.Exceptions;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace CreditBook.LMS.Service.Core.Logic
{
    // enum CollectionDegree
    // {
    //     FIRST,
    //     SECOND,
    //     THIRD
    // }

    public class PerceptechService : IPerceptechService
    {
        private readonly IConfigurationService _configurationService;
        private readonly IDbContextProvider _dbContextProvider;
        private List<PerceptechErrorModel> _errorLog;
        private PerceptechUploadErrors _uploadErrorLog;
        private List<PerceptechFileModel> _fileLog;
        private PerceptechUploadModel _uploadLog;
        private StringBuilder _file;
        private string _language;
        private CollectionDegree _degree;

        private bool _dataValid = true;
        private int _paymentCount = 0;
        private decimal _paymentSum = 0;

        private int _errorCount = 0;

        public PerceptechService(IDbContextProvider dbContextFactory, IConfigurationService configurationService)
        {
            _dbContextProvider = dbContextFactory;
            _configurationService = configurationService;
            _errorLog = new List<PerceptechErrorModel>();
            _uploadErrorLog = new PerceptechUploadErrors();
            _fileLog = new List<PerceptechFileModel>();
            _uploadLog = new PerceptechUploadModel();
            _file = new StringBuilder();
        }

        #region Perceptech Daily Report
        private string GenerateFileHeader(DateTime fileDate, string clientNumber, string company, string currencyCode, string fileVersion, string fileNumber, string processingCenter)
        {
            try
            {
                //if (clientNumber.Length != 10)
                //{
                //    throw new ArgumentException("invalid client number.");
                //}

                //if (company == String.Empty || company.Length > 30)
                //{
                //    throw new ArgumentException("invalid company name.");
                //}

                //if (currencyCode.Length != 3)
                //{
                //    throw new ArgumentException("invalid currency code.");
                //}

                //if (fileVersion.Length != 2)
                //{
                //    throw new ArgumentException("invalid file version.");
                //}

                //if (fileNumber.Length != 4)
                //{
                //    throw new ArgumentException("invalid file number.");
                //}

                //if (processingCenter.Length != 5)
                //{
                //    throw new ArgumentException("invalid processing center.");
                //}

                StringBuilder line = new StringBuilder();
                var emptySpace = " ";
                var emptyString = String.Empty;

                line.Append("A"); //1
                line.Append(emptySpace); //2

                line.Append(fileDate.ToString("ddMMyy")); //3-8
                line.Append(emptySpace); //9

                line.Append(clientNumber.ToUpper()); //10-19
                line.Append(emptySpace); //20

                line.Append(fileVersion.ToUpper()); //21-22
                line.Append(emptySpace); //23

                line.Append(fileNumber.ToUpper()); //24-27
                line.Append(emptySpace); //28

                line.Append(processingCenter.ToUpper()); //29-33
                line.Append(emptySpace); //34

                line.Append(company.PadRight(30).ToUpper()); //35-64
                line.Append(emptySpace); //65

                line.Append(currencyCode.ToUpper()); //66-68
                line.Append(emptySpace); //69

                line.Append(emptyString.PadRight(15)); //70-84
                line.Append(emptyString.PadRight(36)); //85-120

                if (line.Length == 120)
                {
                    return line.ToString();
                }
                else
                {
                    throw new ArgumentException("Invalid line A creation.");
                }
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GenerateFileData(int paymentId, string clientNumber, string loanNumber, string clientName, string transit, string institution, string account, string currencyCode, string destinationCountryCode, DateTime transactionDate, decimal amount, decimal withdrawalFee)
        {
            try
            {
                var line = new StringBuilder();
                var emptySpace = " ";
                var emptyString = String.Empty;
                var isValid = true;

                if (clientNumber.Length + loanNumber.ToString().Length > 19)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[numéro de client invalide]"); } else { line.Append("[invalid client number]"); };
                    isValid = false;
                }

                if (clientName == String.Empty || clientName.Length > 30)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[nom de client invalide]"); } else { line.Append("[invalid client name]"); }
                    isValid = false;
                }

                // IF BANKING INFORMATION IS NOT VALID, SEND IT TO PERCEPTECH JUST THE SAME
                // ONLY REMOVE IS THE ACCOUNT INFORMATION IS EMPTY
                if (transit == String.Empty /*|| transit.Length != 5*/)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[transit invalide]"); } else { line.Append("[invalid transit]"); }
                    isValid = false;
                }

                if (institution == String.Empty /*|| institution.Length != 3*/)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[institution invalide]"); } else { line.Append("[invalid institution]"); }
                    isValid = false;
                }

                if (account == String.Empty /*|| account.Length > 12*/)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[numéro de compte invalide]"); } else { line.Append("[invalid account number]"); }
                    isValid = false;
                }

                if (currencyCode.Length != 3)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[code de devise non valide]"); } else { line.Append("[invalid currency code]"); }
                    isValid = false;
                }

                if (destinationCountryCode.Length != 2)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[code de pays de destination invalide]"); } else { line.Append("[invalid destination country code]"); }
                    isValid = false;
                }

                if (amount == 0)
                {
                    if (_language.ToUpper() == "FR") { line.Append("[montant invalide]"); } else { line.Append("[invalid amount]"); }
                    isValid = false;
                }

                _dataValid = isValid;

                if (isValid)
                {
                    _fileLog.Add(new PerceptechFileModel() { ClientLoanPaymentId = paymentId });

                    line.AppendLine(emptyString);

                    //countData++;

                    line.Append("D"); //1
                    line.Append(emptySpace); //2

                    var clientLoan = (clientNumber + "-" + loanNumber).PadRight(20).ToUpper();
                    //line.Append(clientNumber.PadRight(20).ToUpper()); //3-21
                    line.Append(clientLoan); //3-21
                    line.Append(clientName.PadRight(30).ToUpper()); //23-52
                    line.Append(emptySpace); //53

                    // TAKE ONLY 5 DIGITS FOR TRANSIT NUMBER
                    line.Append(transit.Length > 5 ? transit.Substring(0, 5).ToUpper() : transit.ToUpper()); //54-58
                    line.Append(emptySpace); //59

                    // TAKE ONLY 3 DIGITS FOR INSTITUTION NUMBER
                    line.Append(institution.Length > 3 ? institution.Substring(0, 3).ToUpper() : institution.ToUpper()); //60-62
                    line.Append(emptySpace); //63

                    // TAKE ONLY 12 DIGITS WHEN ACCOUNT NUMBER
                    line.Append(account.Length > 12 ? account.Substring(0, 12).PadRight(12).ToUpper() : account.PadRight(12).ToUpper()); //64-75
                    line.Append(emptySpace); //76
                    line.Append(emptySpace); //77
                    line.Append(emptySpace); //78

                    line.Append(currencyCode.ToUpper()); //79-81
                    line.Append(emptySpace); //82
                    line.Append(emptySpace); //83

                    line.Append(destinationCountryCode.ToUpper()); //84-85
                    line.Append(emptySpace); //86
                    line.Append(emptySpace); //87

                    line.Append(transactionDate.ToString("ddMMyy")); //88-93
                    line.Append(emptySpace); //94

                    var t = amount + withdrawalFee;
                    var amountString = new string((Math.Round(amount + withdrawalFee, 2)).ToString("G", CultureInfo.InvariantCulture).Where(c => char.IsDigit(c)).ToArray());
                    line.Append(amountString.PadLeft(10, '0')); //95-104
                    line.Append(emptySpace); //105

                    line.Append(emptyString.PadRight(15)); //106-120

                    _paymentCount++;
                    _paymentSum = _paymentSum + Math.Round(amount + withdrawalFee, 2);

                    //if (isValid == false)
                    //{
                    //    line.AppendLine(emptyString);
                    //}

                    //line.AppendLine(emptyString);
                    return line.ToString();
                }

                _dataValid = false;
                return line.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GenerateFileFooter(int transactionCountTypeC, decimal transactionTotalTypeC, decimal transactionCountTypeD, decimal transactionTotalTypeD)
        {
            try
            {
                var line = new StringBuilder();
                var emptySpace = " ";
                var emptyString = String.Empty;

                // If the Data file is empty
                //if(countData == 0)
                //{
                line.AppendLine(emptyString);
                //}

                line.Append("Z"); //1
                line.Append(emptySpace); //2

                line.Append(transactionCountTypeC.ToString().PadLeft(8, '0')); //3-10
                line.Append(emptySpace); //11

                var amountStringTypeC = new string(transactionTotalTypeC.ToString("G", CultureInfo.InvariantCulture).Where(c => char.IsDigit(c)).ToArray());
                line.Append(amountStringTypeC.PadLeft(14, '0')); //12-25
                line.Append(emptySpace); //26

                line.Append(transactionCountTypeD.ToString().PadLeft(8, '0')); //27-34
                line.Append(emptySpace); //35

                var amountStringTypeD = new string(transactionTotalTypeD.ToString("G", CultureInfo.InvariantCulture).Where(c => char.IsDigit(c)).ToArray());
                line.Append(amountStringTypeD.PadLeft(14, '0')); //36-49
                line.Append(emptySpace); //50

                line.Append(emptyString.PadLeft(8, '0')); //51-58 - Type U
                line.Append(emptySpace); //59

                line.Append(emptyString.PadLeft(14, '0')); //60-73
                line.Append(emptySpace); //74

                line.Append(emptyString.PadLeft(8, '0')); //75-82
                line.Append(emptySpace); //83

                line.Append(emptyString.PadLeft(14, '0')); //84-97
                line.Append(emptyString.PadLeft(23, '0')); //98-120

                return line.ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void SavePerceptechFile(string reportFolder)
        {
            try
            {
                // Create daily folder
                var folder = reportFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/";

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                _uploadLog.UploadFileName = "Perceptech_" + (DateTime.Now).ToString("yyyy-MM-dd");
                string newFile = null;

                if (!File.Exists(folder + _uploadLog.UploadFileName + ".fdp"))
                {
                    newFile = _uploadLog.UploadFileName + ".fdp";
                    _uploadLog.UploadFileName = newFile;
                }
                else
                {
                    // Rename file
                    _uploadLog.UploadFileName = _uploadLog.UploadFileName + "_"
                                + DateTime.Now.Hour
                                + DateTime.Now.Minute
                                + DateTime.Now.Second
                                + DateTime.Now.Millisecond
                                + ".fdp";

                    newFile = _uploadLog.UploadFileName;
                }

                using (FileStream fs = new FileStream(Path.Combine(folder, newFile), FileMode.CreateNew))
                {
                    //using (StreamWriter outputFile = new StreamWriter(Path.Combine(folder, newFile)))
                    //{
                    //    outputFile.WriteLine(_file.ToString());
                    //}
                    using (StreamWriter outputFile = new StreamWriter(fs, Encoding.GetEncoding("ISO-8859-1")))
                    {
                        outputFile.WriteLine(_file.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GenerateErrorFile(string reportFolder)
        {
            try
            {
                // Create daily folder
                var folder = reportFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/";

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                _uploadLog.ErrorFileName = _language.ToUpper() == "FR" ? "RapportLignesSupprimées_" + (DateTime.Now).ToString("yyyy-MM-dd")
                                                : "RemovedItemsReport_" + (DateTime.Now).ToString("yyyy-MM-dd");
                var sheetName = _language.ToUpper() == "FR" ? "Rapport Erreur" : "Error Report";

                ExcelWorksheet worksheet;
                FileInfo newFile = null;

                if (!File.Exists(folder + _uploadLog.ErrorFileName + ".xlsx"))
                {
                    newFile = new FileInfo(folder + _uploadLog.ErrorFileName + ".xlsx");
                    _uploadLog.ErrorFileName = _uploadLog.ErrorFileName + ".xlsx";
                }
                else
                {
                    // Rename file
                    _uploadLog.ErrorFileName = _uploadLog.ErrorFileName + "_"
                                + DateTime.Now.Hour
                                + DateTime.Now.Minute
                                + DateTime.Now.Second
                                + DateTime.Now.Millisecond
                                + ".xlsx";

                    newFile = new FileInfo(folder + _uploadLog.ErrorFileName);
                }

                //Set non commercial
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var pck = new ExcelPackage(newFile))
                {
                    //Create the worksheet
                    worksheet = pck.Workbook.Worksheets.Add(sheetName);

                    worksheet.Cells[1, 1].Style.Font.Bold = true;
                    worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 1].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 1].Style.Font.Size = 22;
                    worksheet.Cells[1, 1].Value = _language.ToUpper() == "FR" ? "Nom Client" : "Client Name";

                    worksheet.Cells[1, 2].Style.Font.Bold = true;
                    worksheet.Cells[1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 2].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 2].Style.Font.Size = 22;
                    worksheet.Cells[1, 2].Value = _language.ToUpper() == "FR" ? "No Client" : "Client No";

                    worksheet.Cells[1, 3].Style.Font.Bold = true;
                    worksheet.Cells[1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 3].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 3].Style.Font.Size = 22;
                    worksheet.Cells[1, 3].Value = _language.ToUpper() == "FR" ? "Courriel" : "Email";

                    worksheet.Cells[1, 4].Style.Font.Bold = true;
                    worksheet.Cells[1, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 4].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 4].Style.Font.Size = 22;
                    worksheet.Cells[1, 4].Value = _language.ToUpper() == "FR" ? "No Prêt" : "Loan No";

                    worksheet.Cells[1, 5].Style.Font.Bold = true;
                    worksheet.Cells[1, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 5].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 5].Style.Font.Size = 22;
                    worksheet.Cells[1, 5].Value = _language.ToUpper() == "FR" ? "Montant" : "Amount";

                    worksheet.Cells[1, 6].Style.Font.Bold = true;
                    worksheet.Cells[1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 6].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 6].Style.Font.Size = 22;
                    worksheet.Cells[1, 6].Value = _language.ToUpper() == "FR" ? "Date du paiement" : "Payment Date";

                    worksheet.Cells[1, 7].Style.Font.Bold = true;
                    worksheet.Cells[1, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 7].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 7].Style.Font.Size = 22;
                    worksheet.Cells[1, 7].Value = _language.ToUpper() == "FR" ? "Erreurs" : "Errors";

                    int row = 2;

                    foreach (var error in _errorLog)
                    {
                        worksheet.Cells[row, 1].Style.Font.Bold = false;
                        //worksheet.Cells[row, 1].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 1].Style.Font.Size = 22;
                        worksheet.Cells[row, 1].Value = error.ClientName;

                        worksheet.Cells[row, 2].Style.Font.Bold = false;
                        //worksheet.Cells[row, 2].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 2].Style.Font.Size = 22;
                        worksheet.Cells[row, 2].Value = error.ClientNo;

                        worksheet.Cells[row, 3].Style.Font.Bold = false;
                        //worksheet.Cells[row, 3].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 3].Style.Font.Size = 22;
                        worksheet.Cells[row, 3].Value = error.ClientEmail;

                        worksheet.Cells[row, 4].Style.Font.Bold = false;
                        //worksheet.Cells[row, 4].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 4].Style.Font.Size = 22;
                        worksheet.Cells[row, 4].Value = error.LoanNo;

                        worksheet.Cells[row, 5].Style.Font.Bold = false;
                        //worksheet.Cells[row, 5].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 5].Style.Font.Size = 22;
                        worksheet.Cells[row, 5].Value = ((decimal)error.PaymentAmount).ToString("0.00") + "$";

                        worksheet.Cells[row, 6].Style.Font.Bold = false;
                        //worksheet.Cells[row, 6].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 6].Style.Font.Size = 22;
                        worksheet.Cells[row, 6].Value = String.Format("{0:dd-MM-yyyy}", error.PaymentDate);

                        worksheet.Cells[row, 7].Style.Font.Bold = false;
                        //worksheet.Cells[row, 7].Style.Font.Color.SetColor(Color.Black);
                        worksheet.Cells[row, 7].Style.Font.Size = 22;
                        worksheet.Cells[row, 7].Value = error.ErrorString;

                        row++;
                    }

                    // CRASHES ON MAC. TRY TO FIX LATER
                    worksheet.Cells.AutoFitColumns();

                    pck.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GenerateUploadErrorFile(string uploadErrorFolder)
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    var language = context.LoanerConfiguration
                                            .Where(x => x.Key == "DEFAULT_LANGUAGE")
                                            .SingleOrDefault();

                    _language = language.Value;
                }

                // Create daily folder
                var folder = uploadErrorFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/";

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                _uploadLog.UploadErrorFileName = _language.ToUpper() == "FR" ? "RapportErreurUpload_" + (DateTime.Now).ToString("yyyy-MM-dd")
                                                : "UploadErrorReport_" + (DateTime.Now).ToString("yyyy-MM-dd");
                var sheetName = _language.ToUpper() == "FR" ? "Rapport Erreur" : "Error Report";

                ExcelWorksheet worksheet;
                FileInfo newFile = null;

                if (!File.Exists(folder + _uploadLog.UploadErrorFileName + ".xlsx"))
                {
                    newFile = new FileInfo(folder + _uploadLog.UploadErrorFileName + ".xlsx");
                    _uploadLog.UploadErrorFileName = _uploadLog.UploadErrorFileName + ".xlsx";
                }
                else
                {
                    // Rename file
                    _uploadLog.UploadErrorFileName = _uploadLog.UploadErrorFileName + "_"
                                + DateTime.Now.Hour
                                + DateTime.Now.Minute
                                + DateTime.Now.Second
                                + DateTime.Now.Millisecond
                                + ".xlsx";

                    newFile = new FileInfo(folder + _uploadLog.UploadErrorFileName);
                }

                //Set non commercial
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                using (var pck = new ExcelPackage(newFile))
                {
                    //Create the worksheet
                    worksheet = pck.Workbook.Worksheets.Add(sheetName);

                    worksheet.Cells[1, 1].Style.Font.Bold = true;
                    worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 1].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 1].Style.Font.Size = 22;
                    worksheet.Cells[1, 1].Value = _language.ToUpper() == "FR" ? "Filename" : "Nom du fichier";

                    worksheet.Cells[1, 2].Style.Font.Bold = true;
                    worksheet.Cells[1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 2].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 2].Style.Font.Size = 22;
                    worksheet.Cells[1, 2].Value = _language.ToUpper() == "FR" ? "Log Date" : "Date";

                    worksheet.Cells[1, 3].Style.Font.Bold = true;
                    worksheet.Cells[1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 3].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 3].Style.Font.Size = 22;
                    worksheet.Cells[1, 3].Value = _language.ToUpper() == "FR" ? "Error Code" : "Code d'erreur";

                    worksheet.Cells[1, 4].Style.Font.Bold = true;
                    worksheet.Cells[1, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //worksheet.Cells[1, 4].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[1, 4].Style.Font.Size = 22;
                    worksheet.Cells[1, 4].Value = _language.ToUpper() == "FR" ? "Error Message" : "Message d'erreur";

                    worksheet.Cells[2, 1].Style.Font.Bold = false;
                    //worksheet.Cells[row, 1].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[2, 1].Style.Font.Size = 22;
                    worksheet.Cells[2, 1].Value = _uploadErrorLog.FileName;

                    worksheet.Cells[2, 2].Style.Font.Bold = false;
                    //worksheet.Cells[row, 2].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[2, 2].Style.Font.Size = 22;
                    worksheet.Cells[2, 2].Value = String.Format("{0:dd-MM-yyyy}", _uploadErrorLog.LogDate);

                    worksheet.Cells[2, 3].Style.Font.Bold = false;
                    //worksheet.Cells[row, 3].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[2, 3].Style.Font.Size = 22;
                    worksheet.Cells[2, 3].Value = _uploadErrorLog.ErrorCode;

                    worksheet.Cells[2, 4].Style.Font.Bold = false;
                    //worksheet.Cells[row, 4].Style.Font.Color.SetColor(Color.Black);
                    worksheet.Cells[2, 4].Style.Font.Size = 22;
                    worksheet.Cells[2, 4].Value = _uploadErrorLog.ErrorMessage;


                    // CRASHES ON MAC. TRY TO FIX LATER
                    //worksheet.Cells.AutoFitColumns();

                    pck.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LogFileData()
        {
            using var context = _dbContextProvider.GetContext();
            using (var uow = context.Database.BeginTransaction())
            {
                var date = DateTime.Now;

                try
                {
                    foreach (var payment in _fileLog)
                    {
                        var log = new ClientLoanPaymentPerceptech()
                        {
                            IdclientLoanPayment = payment.ClientLoanPaymentId,
                            SubmittedDate = date
                        };

                        context.ClientLoanPaymentPerceptech.Add(log);
                        context.SaveChanges();
                    }

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw ex;
                }
            }
        }

        public async Task ValidateLoanPayments(string apiKey, string dailyReportUploadFolder, string dailyReportRemovedLinesFolder, string clientNumber, string company, string fileVersion, string processingCenter)
        {
            try
            {
                //
                var currentDate = DateTime.Today;
                using (var context = _dbContextProvider.GetContext())
                {
                    var language = context.LoanerConfiguration
                                            .Where(x => x.Key == "DEFAULT_LANGUAGE")
                                            .SingleOrDefault();

                    _language = language.Value;
                }

                using (var context = _dbContextProvider.GetContext())
                {
                    // Get 
                    var withdrawal = context.LoanChargeConfiguration
                                        .Where(x => x.IdloanChargeTypeNavigation.Code == "auto_withdrawal_amount" && x.EffectiveDate <= DateTime.Now)
                                        .OrderByDescending(x => x.EffectiveDate)
                                        .FirstOrDefault();

                    // 
                    var noOfDays = ValidateNoOfDays(DateTime.Today);

                    // Only choose loan payments that are scheduled 48 hours from now and loans that have been started
                    // Only take Loans that are not Stopped
                    var loanPayments = (from cl in context.ClientLoan
                                        join ls in context.LoanStatus on cl.IdloanStatus equals ls.Id
                                        join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
                                        join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                        where clp.PaymentDate == DateTime.Today.AddDays(noOfDays)
                                        && ls.Code == "IP" // Ony send In Progress Loans
                                        && (ps.Code == "PC" || ps.Code == "PEN" || ps.Code == "DP") // Send Pending, Changed Loan and Deferred Loan Payment fee
                                        && clp.IsDeleted == false // Take payments that have not been Deleted
                                        && cl.IsDeleted == false // Take loans that have not been Deleted
                                        && !(from clpp in context.ClientLoanPaymentPerceptech
                                             select clpp.IdclientLoanPayment).Contains(clp.Id)
                                        select new
                                        {
                                            Id = clp.Id,
                                            LoanNo = cl.LoanNumber,
                                            Amount = clp.Amount,
                                            PaymentDate = clp.PaymentDate,
                                            ClientId = cl.Idclient
                                        }).ToList();

                    foreach (var payment in loanPayments)
                    {
                        var client = (from c in context.Client
                                      join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                      //join cbd in context.ClientBankDetails on c.Id equals cbd.Idclient
                                      join cbd in context.ClientBankDetails on c.Id equals cbd.Idclient into bi_lj
                                      from cbd_lj in bi_lj.DefaultIfEmpty()
                                      where c.Id == payment.ClientId && cea.IsPrimary == true
                                      select new
                                      {
                                          Number = c.Id,
                                          Email = cea.EmailAddress, //c.ClientEmailAddress.Where(x => x.IsPrimary == true).SingleOrDefault().EmailAddress,
                                          Name = c.FirstName + " " + c.LastName,
                                          Transit = cbd_lj.TransitNumber,
                                          Institution = cbd_lj.InstitutionNumber,
                                          Account = cbd_lj.AccountNumber
                                      }
                                     ).SingleOrDefault();

                        // Create File Data
                        var data = GenerateFileData(payment.Id,
                                                    client.Number.ToString(),
                                                    payment.LoanNo.ToString(),
                                                    client.Name,
                                                    client.Transit != null ? client.Transit.ToString() : "",
                                                    client.Institution != null ? client.Institution.ToString() : "",
                                                    client.Account != null ? client.Account.ToString() : "",
                                                    "CAD",
                                                    "CA",
                                                    (DateTime)payment.PaymentDate,
                                                    (decimal)payment.Amount,
                                                    withdrawal != null ? withdrawal.AmountCharged : 0);

                        if (!_dataValid)
                        {
                            _errorCount++;

                            _errorLog.Add(new PerceptechErrorModel()
                            {
                                ClientName = client.Name,
                                ClientNo = client.Number.ToString().PadLeft(6, '0'),
                                ClientEmail = client.Email,
                                LoanNo = payment.LoanNo.ToString().PadLeft(6, '0'),
                                PaymentDate = payment.PaymentDate,
                                PaymentAmount = payment.Amount,
                                ErrorString = data
                            });

                            _dataValid = true;
                        }
                    }
                }

                // Reset
                _paymentCount = 0;
                _paymentSum = 0;
                _fileLog = new List<PerceptechFileModel>();

                // Generate Perceptech Error File to send to Preteur
                if (_errorCount != 0)
                {
                    GenerateErrorFile(dailyReportRemovedLinesFolder);
                }

                // Send Error Report by Email
                if (_errorCount != 0)
                {
                    var emails = new List<string>();

                    using var context = _dbContextProvider.GetContext();

                    var adminEmail = context.LoanerConfiguration
                                            .Where(x => x.Key == "ADMIN_EMAIL")
                                            .SingleOrDefault();

                    var companyName = context.LoanerConfiguration
                                            .Where(x => x.Key == "COMPANY_NAME")
                                            .SingleOrDefault();

                    emails.Add(adminEmail.Value);
                    emails.Add("d.dorazio@creditbook.ca");

                    // Check to see if the Error report is empty
                    await EmailManager.SendPerceptechErrorReport(dailyReportRemovedLinesFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/", _uploadLog.ErrorFileName, apiKey, emails, companyName.Value, _language);
                }
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ValidateNoOfDays(DateTime date) =>
            date.DayOfWeek switch
            {
                DayOfWeek.Monday => 4,
                DayOfWeek.Tuesday => 6,
                DayOfWeek.Wednesday => 6,
                DayOfWeek.Thursday => 6,
                DayOfWeek.Friday => 6,
                _ => throw new ArgumentException(message: "Invalid day of the week")
            };


        private class LoanPaymentsModel
        {
            public int Id { get; set; }
            public int? LoanNo { get; set; }
            public decimal? Amount { get; set; }
            public DateTime? PaymentDate { get; set; }
            public DateTime? LoanCreatedDate { get; set; }
            public int? ClientId { get; set; }
            public decimal? OriginalLoanMembershipFee { get; set; }
            public decimal? MembershipFee { get; set; }
        }

        public PerceptechUploadModel ProgramLoanPayments(string apiKey, string dailyReportUploadFolder, string dailyReportRemovedLinesFolder, string clientNumber, string company, string fileVersion, string processingCenter)
        {
            try
            {
                // If Payment Provider is False do nothing
                if (!_configurationService.ValidatePaymentProvider())
                    return null;

                using var context = _dbContextProvider.GetContext();

                var language = context.LoanerConfiguration
                                        .Where(x => x.Key == "DEFAULT_LANGUAGE")
                                        .SingleOrDefault();

                _language = language.Value;

                // Create File Header
                var currentDate = DateTime.Today;
                var header = GenerateFileHeader(currentDate, clientNumber, company, "CAD", fileVersion, "0001", processingCenter);
                _file.Append(header);

                var noOfDays = ProgramPaymentNoOfDays(DateTime.Today);

                // Only choose loan payments that are scheduled 48 hours from now and loans that have been started
                // Take Loans that are not Stopped
                List<LoanPaymentsModel> _loanPayments = null;

                // Check to see if Lender is using MemberShipFee
                if (_configurationService.ValidateMembershipFee())
                {
                    _loanPayments = (from cl in context.ClientLoan
                                     join c in context.Client on cl.Idclient equals c.Id
                                     join ls in context.LoanStatus on cl.IdloanStatus equals ls.Id
                                     join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
                                     join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                     where clp.PaymentDate == DateTime.Today.AddDays(noOfDays)
                                     && (ls.Code == "IP" || ls.Code == "PA" || ls.Code == "LD") // In progress, Payment Agreement, Loan Default
                                     && (ps.Code == "PEN" || ps.Code == "PC" || ps.Code == "DP") // Send Pending, Payment Changed and Deferred Payment fee
                                     && clp.IsDeleted == false // Take payments that have not been Deleted
                                     && cl.IsDeleted == false // Take loans that have not been Deleted
                                     && c.IsDeleted == false
                                     && !(from clpp in context.ClientLoanPaymentPerceptech
                                          select clpp.IdclientLoanPayment).Contains(clp.Id)
                                     select new LoanPaymentsModel()
                                     {
                                         Id = clp.Id,
                                         LoanNo = cl.LoanNumber,
                                         Amount = clp.Amount,
                                         PaymentDate = clp.PaymentDate,
                                         LoanCreatedDate = cl.CreatedDate,
                                         ClientId = cl.Idclient,
                                         OriginalLoanMembershipFee = cl.OriginalSubFeesPrice,
                                         MembershipFee = clp.SubFeesPayment
                                     }).ToList();
                }
                else
                {
                    if (_configurationService.ValidateMixedLoans())
                    {
                        var mixedLoanDate = _configurationService.ValidateMixedLoanDate();
                        _loanPayments = (from cl in context.ClientLoan
                                         join c in context.Client on cl.Idclient equals c.Id
                                         join ls in context.LoanStatus on cl.IdloanStatus equals ls.Id
                                         join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
                                         join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                         where clp.PaymentDate == DateTime.Today.AddDays(noOfDays)
                                         && cl.CreatedDate >= mixedLoanDate // Take Loans that were created as of the MixedLoanDate
                                         && (ls.Code == "IP" || ls.Code == "PA" || ls.Code == "LD") // In progress, Payment Agreement, Loan Default
                                         && (ps.Code == "PEN" || ps.Code == "PC" || ps.Code == "DP") // Send Pending, Payment Changed and Deferred Payment fee
                                         && clp.IsDeleted == false // Take payments that have not been Deleted
                                         && cl.IsDeleted == false // Take loans that have not been Deleted
                                         && c.IsDeleted == false
                                         && !(from clpp in context.ClientLoanPaymentPerceptech
                                              select clpp.IdclientLoanPayment).Contains(clp.Id)
                                         select new LoanPaymentsModel()
                                         {
                                             Id = clp.Id,
                                             LoanNo = cl.LoanNumber,
                                             Amount = clp.Amount,
                                             PaymentDate = clp.PaymentDate,
                                             ClientId = cl.Idclient
                                         }).ToList();
                    }
                    else
                    {
                        _loanPayments = (from cl in context.ClientLoan
                                         join c in context.Client on cl.Idclient equals c.Id
                                         join ls in context.LoanStatus on cl.IdloanStatus equals ls.Id
                                         join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
                                         join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                         where clp.PaymentDate == DateTime.Today.AddDays(noOfDays)
                                         && (ls.Code == "IP" || ls.Code == "PA" || ls.Code == "LD") // In progress, Payment Agreement, Loan Default
                                         && (ps.Code == "PEN" || ps.Code == "PC" || ps.Code == "DP") // Send Pending, Payment Changed and Deferred Payment fee
                                         && clp.IsDeleted == false // Take payments that have not been Deleted
                                         && cl.IsDeleted == false // Take loans that have not been Deleted
                                         && c.IsDeleted == false
                                         && !(from clpp in context.ClientLoanPaymentPerceptech
                                              select clpp.IdclientLoanPayment).Contains(clp.Id)
                                         select new LoanPaymentsModel()
                                         {
                                             Id = clp.Id,
                                             LoanNo = cl.LoanNumber,
                                             Amount = clp.Amount,
                                             PaymentDate = clp.PaymentDate,
                                             ClientId = cl.Idclient
                                         }).ToList();
                    }
                }

                var withdrawal = context.LoanChargeConfiguration
                                    .Where(x => x.IdloanChargeTypeNavigation.Code == "auto_withdrawal_amount" && x.EffectiveDate <= DateTime.Now)
                                    .OrderByDescending(x => x.EffectiveDate)
                                    .FirstOrDefault();

                foreach (var payment in _loanPayments)
                {
                    var client = (from c in context.Client
                                  join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                  //join cbd in context.ClientBankDetails on c.Id equals cbd.Idclient
                                  join cbd in context.ClientBankDetails on c.Id equals cbd.Idclient into bi_lj
                                  from cbd_lj in bi_lj.DefaultIfEmpty()
                                  where c.Id == payment.ClientId && cea.IsPrimary == true
                                  select new
                                  {
                                      Number = c.Id,
                                      Email = cea.EmailAddress,
                                      Name = c.FirstName + " " + c.LastName,
                                      Transit = cbd_lj.TransitNumber,
                                      Institution = cbd_lj.InstitutionNumber,
                                      Account = cbd_lj.AccountNumber
                                  }
                                ).SingleOrDefault();


                    // Check to see if Lender is using MemberShipFee
                    string data = null;
                    if (_configurationService.ValidateMembershipFee())
                    {
                        data = GenerateFileData(payment.Id,
                                                client.Number.ToString(),
                                                payment.LoanNo.ToString(),
                                                client.Name,
                                                client.Transit != null ? client.Transit.ToString() : "",
                                                client.Institution != null ? client.Institution.ToString() : "",
                                                client.Account != null ? client.Account.ToString() : "",
                                                "CAD",
                                                "CA",
                                                (DateTime)payment.PaymentDate,
                                                (decimal)payment.Amount,
                                                withdrawal != null
                                                            ? payment.LoanCreatedDate <= new DateTime(2020, 12, 7) ? withdrawal.AmountCharged
                                                            : payment.OriginalLoanMembershipFee == null ? 40 : (decimal)payment.MembershipFee
                                                            : 0);
                    }
                    else
                    {
                        // Create File Data
                        data = GenerateFileData(payment.Id,
                                                client.Number.ToString(),
                                                payment.LoanNo.ToString(),
                                                client.Name,
                                                client.Transit != null ? client.Transit.ToString() : "",
                                                client.Institution != null ? client.Institution.ToString() : "",
                                                client.Account != null ? client.Account.ToString() : "",
                                                "CAD",
                                                "CA",
                                                (DateTime)payment.PaymentDate,
                                                (decimal)payment.Amount,
                                                withdrawal != null ? withdrawal.AmountCharged : 0);
                    }

                    if (_dataValid)
                    {
                        _file.Append(data);
                    }
                }

                // Create File Footer
                var footer = GenerateFileFooter(0, 0, _paymentCount, _paymentSum);
                _file.Append(footer);

                // Generate Perceptech file only of Payments exist
                if (_loanPayments.Count != 0)
                {
                    SavePerceptechFile(dailyReportUploadFolder);
                }

                // Check if the Upload file is empty
                if (_paymentCount == 0)
                {
                    _uploadLog.UploadEmpty = true;
                }

                return _uploadLog;

            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ProgramPaymentNoOfDays(DateTime date) =>
                date.DayOfWeek switch
                {
                    DayOfWeek.Monday => 3,
                    DayOfWeek.Tuesday => 3,
                    DayOfWeek.Wednesday => 5,
                    DayOfWeek.Thursday => 5,
                    DayOfWeek.Friday => 5,
                    _ => throw new ArgumentException(message: "Invalid day of the week")
                };

        public async Task LogUploadErrors(string result, string fileName, string apiKey, string dailyReportUploadErrorFolder)
        {
            try
            {
                var errorCode = "";
                var errorMessage = "";
                var errorFound = false;

                if (result.Contains("-21") || result.Contains("-41"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Format de fichier invalide"
                                                     : "File format is invalid";
                }

                else if (result.Contains("-22"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Date de création invalide"
                                                     : "Creation date is invalid";
                }

                else if (result.Contains("-23"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "La date de création ne peut être dans le futur"
                                                     : "The creation date can not be in the future";
                }

                else if (result.Contains("-24"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "La date de création ne peut être plus ancienne que 7 jours"
                                                     : "The creation date can not be older than 7 days";
                }

                else if (result.Contains("-25"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Le numéro Perceptech est invalide (ou vous n’avez pas les droits d’accéder à cette compagnie)"
                                                     : "The Perceptech number is invalid (or you do not have the right to access this company)";
                }

                else if (result.Contains("-26"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "La version du fichier est inconnue"
                                                     : "The file version is unknown";
                }

                else if (result.Contains("-42"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Les totaux de la ligne Z ne concordent pas"
                                                     : "The totals on line Z do not match";
                }

                else if (result.Contains("-43"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Incapable d’importer le client #"
                                                     : "Unable to import customer #";
                }

                else if (result.Contains("-44"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Connexion perdue"
                                                     : "Lost connection";
                }

                else if (result.Contains("-81"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Incapable de sauvegarder le fichier"
                                                     : "Unable to save the file";
                }

                else if (result.Contains("-90"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Incapable de lire le fichier"
                                                     : "Unable to read the file";
                }

                else if (result.Contains("-100"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Fichier déjà traité"
                                                     : "File already processed";
                }

                else if (result.Contains("-101") || result.Contains("-102"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Communiquer avec Perceptech inc"
                                                     : "Please contact Perceptech inc";
                }

                else if (result.Contains("-103"))
                {
                    errorFound = true;
                    errorCode = result.Replace('"', ' ').Trim();
                    errorMessage = _language == "FR" ? "Problème avec votre fichier"
                                                     : "There is a problem with your file";
                }

                // File was uploaded successfully
                else if (result == "1\n")
                {
                    var noOfDays = ProgramPaymentNoOfDays(DateTime.Today);
                    using var context = _dbContextProvider.GetContext();
                    context.PerceptechSubmitLog.Add(new PerceptechSubmitLog()
                    {
                        DateSubmitted = DateTime.Now,
                        PaymentDate = DateTime.Today.AddDays(noOfDays),
                        ReturnCode = 1
                    });
                    await context.SaveChangesAsync();
                }
                // Unexpected error
                else
                {
                    errorFound = true;
                    errorCode = "999";
                    errorMessage = result;
                }

                if (errorFound)
                {
                    // Generate Perceptech Upload Error File to send to Preteur
                    _uploadErrorLog.LogDate = DateTime.Today;
                    _uploadErrorLog.FileName = _uploadLog.UploadErrorFileName;
                    _uploadErrorLog.ErrorCode = errorCode;
                    _uploadErrorLog.ErrorMessage = errorMessage;

                    GenerateUploadErrorFile(dailyReportUploadErrorFolder);

                    // Send email to inform Admin
                    var emails = new List<string>();

                    using var context = _dbContextProvider.GetContext();

                    //var adminEmail = context.LoanerConfiguration
                    //                        .Where(x => x.Key == "ADMIN_EMAIL")
                    //                        .SingleOrDefault();

                    var companyName = context.LoanerConfiguration
                                            .Where(x => x.Key == "COMPANY_NAME")
                                            .SingleOrDefault();

                    //emails.Add(adminEmail.Value);
                    emails.Add("d.dorazio@creditbook.ca");

                    // Check to see if the Error report is empty
                    await EmailManager.SendUploadError(apiKey, emails, _language, dailyReportUploadErrorFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/", _uploadLog.UploadErrorFileName, companyName.Value);
                }
                else
                {
                    // Save data to Database
                    LogFileData();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task LogSubmittedDate()
        {
            var noOfDays = ProgramPaymentNoOfDays(DateTime.Today);
            using var context = _dbContextProvider.GetContext();
            context.PerceptechSubmitLog.Add(new PerceptechSubmitLog()
            {
                DateSubmitted = DateTime.Now,
                PaymentDate = DateTime.Today.AddDays(noOfDays),
                ReturnCode = 1
            });
            await context.SaveChangesAsync();
        }

        public async Task LogUploadFailedUpload(string apiKey, string dailyReportUploadErrorFolder)
        {
            var noOfDays = ProgramPaymentNoOfDays(DateTime.Today);
            using var context = _dbContextProvider.GetContext();
            context.PerceptechSubmitLog.Add(new PerceptechSubmitLog()
            {
                DateSubmitted = DateTime.Now,
                PaymentDate = DateTime.Today.AddDays(noOfDays),
                ReturnCode = 500
            });
            await context.SaveChangesAsync();

            var language = context.LoanerConfiguration
                        .Where(x => x.Key == "DEFAULT_LANGUAGE")
                        .SingleOrDefault();

            // Generate Perceptech Upload Error File to send to Preteur
            var errorCode = "500";
            var errorMessage = language.Value == "FR" ? "Une erreur inattendue s'est produite lors de la soumission du fichier."
                                             : "Unable to save the file";

            _uploadErrorLog.LogDate = DateTime.Today;
            _uploadErrorLog.FileName = _uploadLog.UploadErrorFileName;
            _uploadErrorLog.ErrorCode = errorCode;
            _uploadErrorLog.ErrorMessage = errorMessage;

            GenerateUploadErrorFile(dailyReportUploadErrorFolder);

            // Send email to inform Admin
            var emails = new List<string>();

            //using var context = _dbContextProvider.GetContext();
            var companyName = context.LoanerConfiguration
                                    .Where(x => x.Key == "COMPANY_NAME")
                                    .SingleOrDefault();

            emails.Add("d.dorazio@creditbook.ca");

            // Check to see if the Error report is empty
            await EmailManager.SendUploadError(apiKey, emails, _language, dailyReportUploadErrorFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/", _uploadLog.UploadErrorFileName, companyName.Value);
        }

        #endregion

        #region Perceptech Daily Error Report

        //private string SavePerceptechErrorReport(string errorFolder, string data)
        //{
        //    try
        //    {
        //        // Create daily folder
        //        var folder = errorFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/";

        //        if (!Directory.Exists(folder))
        //        {
        //            Directory.CreateDirectory(folder);
        //        }

        //        _uploadLog.UploadFileName = "DailyRejectionErrorReport_" + (DateTime.Now).ToString("yyyy-MM-dd");
        //        string newFile = null;

        //        if (!File.Exists(folder + _uploadLog.UploadFileName + ".csv"))
        //        {
        //            newFile = _uploadLog.UploadFileName + ".csv";
        //            _uploadLog.UploadFileName = newFile;
        //        }
        //        else
        //        {
        //            // Rename file
        //            _uploadLog.UploadFileName = _uploadLog.UploadFileName + "_"
        //                        + DateTime.Now.Hour
        //                        + DateTime.Now.Minute
        //                        + DateTime.Now.Second
        //                        + DateTime.Now.Millisecond
        //                        + ".csv";

        //            newFile = _uploadLog.UploadFileName;
        //        }

        //        using (StreamWriter outputFile = new StreamWriter(Path.Combine(folder, newFile)))
        //        {
        //            outputFile.WriteLine(data);
        //        }

        //        return newFile;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void UpdateLoanPayments(string dailyRejectionReportFolder, string data, string language)
        //{
        //    try
        //    {
        //        // Save CSV file
        //        var fileName = SavePerceptechErrorReport(dailyRejectionReportFolder, data);
        //        //var fileName = "DailyRejectionErrorReport_2020-11-23_164532747.csv";

        //        //using (var reader = new StreamReader(dailyRejectionReportFolder + "2020-07-15" + "/" + fileName))
        //        using (var reader = new StreamReader(dailyRejectionReportFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/" + fileName))
        //        {
        //            while (!reader.EndOfStream)
        //            {
        //                var line = reader.ReadLine();

        //                if (!string.IsNullOrEmpty(line))
        //                {
        //                    var columns = line.Split(',');

        //                    // Column 2 contains the Client Number and LoanNo
        //                    var clientId = columns[1].Split('-')[0].Replace('"', ' ').Trim();
        //                    //var splitPaymentId = columns[1].Split('-');
        //                    var splitLoanId = columns[1].Split('-');

        //                    // Column 12 contains the Payment Date
        //                    var paymentDate = columns[11].Replace('"', ' ').Trim();

        //                    if (splitLoanId.Length != 1)
        //                    {
        //                        var loanNo = splitLoanId[1].Replace('"', ' ').Trim();
        //                        //var paymentId = splitPaymentId[1].Replace('"', ' ').Trim();

        //                        // Column 17 has the Error Code
        //                        var errorCode = columns[17].Replace('"', ' ').Trim();

        //                        // Column 20 contains the Error Description
        //                        //var errorDescription = columns[20].Replace('"', ' ').Trim();

        //                        using (var context = _dbContextProvider.GetContext())
        //                        {
        //                            // Check to see if the Payment ID can be converted to INT
        //                            //int paymentIdConverted;
        //                            //bool success = Int32.TryParse(paymentId, out paymentIdConverted);

        //                            // Check to see if the Payment date can be converted to DateTime
        //                            DateTime paymentDateConverted;
        //                            bool successPaymentDate = DateTime.TryParse(paymentDate, out paymentDateConverted);

        //                            if (successPaymentDate)
        //                            {
        //                                // Validate that the Loan was not deleted.
        //                                // This is to handle the error when payments were sent for Loans that were deleted
        //                                // Retreive PaymentID with ClientID, LoanNo and PaymentDate
        //                                var item = (from c in context.Client
        //                                            join cl in context.ClientLoan on c.Id equals cl.Idclient
        //                                            join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
        //                                            where c.Id == Convert.ToInt32(clientId)
        //                                            && cl.LoanNumber == Convert.ToInt32(loanNo)
        //                                            && clp.PaymentDate == paymentDateConverted
        //                                            select new
        //                                            {
        //                                                PaymentId = clp.Id,
        //                                                LoanDeleted = cl.IsDeleted
        //                                            }).SingleOrDefault();

        //                                var clientPayment = context.ClientLoanPayment.Where(x => x.Id == item.PaymentId).SingleOrDefault();
        //                                //var clientLoan = context.ClientLoan.Where(x => x.Id == clientPayment.IdclientLoan).SingleOrDefault();

        //                                if (item.LoanDeleted == true)
        //                                {
        //                                    continue;
        //                                }

        //                                //if (clientPayment != null)
        //                                //{
        //                                // Stop the loan
        //                                if (errorCode == "910")
        //                                {
        //                                    StopLoan(item.PaymentId, language);
        //                                }
        //                                // Update the current Loan Payment
        //                                else
        //                                {
        //                                    // Check to see if the Error Code can be converted to INT
        //                                    int errorCodeConverted;
        //                                    bool successErrorCode = Int32.TryParse(errorCode, out errorCodeConverted);

        //                                    if (successErrorCode)
        //                                    {
        //                                        //Set the Description of the Payment that is NSF
        //                                        var perceptechError = context.PerceptechErrorCode
        //                                                                       .Where(x => x.Code == errorCodeConverted)
        //                                                                       .SingleOrDefault();

        //                                        clientPayment.IdperceptechErrorCode = perceptechError.Id;
        //                                        clientPayment.Description = perceptechError.MessageFr + "/" + perceptechError.MessageEn;
        //                                    }

        //                                    context.ClientLoanPayment.Update(clientPayment);
        //                                    context.SaveChanges();

        //                                    // Call the NSF method from LoanService that already takes care of the process
        //                                    // This can be moved to a shared Business Logic module
        //                                    var loanService = new LoanService(_dbContextProvider);
        //                                    loanService.LoanManualNSFPayment(new LoanGenericPaymentModel() { LoanId = (int)clientPayment.IdclientLoan, PaymentId = clientPayment.Id }, language, true);
        //                                }

        //                                // Insert into ClientLoanCollection
        //                                var result = CreateClientLoanCollection(Convert.ToInt32(clientPayment.Id), errorCode);

        //                                // Set Client Status to Collections only if error processing payment has occurred
        //                                if (result)
        //                                {
        //                                    var client = context.Client.SingleOrDefault(c => c.Id == Convert.ToInt32(clientId));
        //                                    var clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTIONS");
        //                                    client.IdclientStatus = clientStatus.Id;
        //                                    context.Client.Update(client);
        //                                    context.SaveChanges();
        //                                }
        //                                //}
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void StopLoan(int idLoanPayment, string language)
        //{
        //    try
        //    {
        //        using (var context = _dbContextProvider.GetContext())
        //        {
        //            // var perceptechError = context.PerceptechErrorCode
        //            //                        .Where(x => x.Code == 910)
        //            //                        .SingleOrDefault();

        //            // var stopPaymentDate = DateTime.Today;

        //            // var loanId = context.ClientLoanPayment
        //            //                     .Where(x => x.Id == idLoanPayment)
        //            //                     .SingleOrDefault()
        //            //                     .Id;

        //            // var loanStatusStopped = context.LoanStatus
        //            //     .Where(x => x.Code == "LS")
        //            //     .SingleOrDefault();

        //            // var loan = context.ClientLoan
        //            //                     .Where(x => x.Id == loanId)
        //            //                     .Select(x => new ClientLoanModel()
        //            //                     {
        //            //                         Id = x.Id,
        //            //                         Status = language == "FR" ? loanStatusStopped.NameFr : loanStatusStopped.NameEn,
        //            //                         IsLoanStopped = true,
        //            //                         StopLoanDate = stopPaymentDate
        //            //                     })
        //            //                     .SingleOrDefault();

        //            // var loanPayments = context.ClientLoanPayment
        //            //                             .Where(x => x.IdclientLoan == loanId && x.IsDeleted == false)
        //            //                             .Select(x => new ClientLoanPaymentModel()
        //            //                             {
        //            //                                 Id = x.Id
        //            //                             })
        //            //                             .OrderBy(x => x.PaymentDate)
        //            //                             .ToList();

        //            // // Set all payments to delete
        //            // var paymentsToDelete =  loanPayments.Where(x => x.PaymentDate > stopPaymentDate)
        //            //                             .ToList();

        //            // // Update all loan payments to Deleted
        //            // foreach(var payment in paymentsToDelete)
        //            // {
        //            //     var existingPayment = context.ClientLoanPayment
        //            //                             .Where(x => x.Id == payment.Id)
        //            //                             .SingleOrDefault();

        //            //     existingPayment.IsDeleted = true;
        //            //     context.SaveChanges();

        //            // }

        //            // // Add a payment line indicating that the loan was stopped. All values are at Zero
        //            // var paymentStatusStopped = context.PaymentStatus
        //            //                             .Where(x => x.Code == "SP")
        //            //                             .SingleOrDefault();

        //            // var stopPaymentLine = new ClientLoanPayment()
        //            // {
        //            //     IdclientLoan = loan.Id,
        //            //     Description = perceptechError.MessageFr + "/" + perceptechError.MessageEn,
        //            //     IdpaymentStatus = paymentStatusStopped.Id,
        //            //     PaymentDate = stopPaymentDate,
        //            //     Interest = 0,
        //            //     Capital = 0,
        //            //     Amount = 0,
        //            //     MaxAmount = 0,
        //            //     Fees = 0,
        //            //     Balance = 0,
        //            //     IsDeleted = false,

        //            //     IsNsf = false,
        //            //     IsManual = false,
        //            //     IsRebate = false,
        //            //     IsAmountChanged = false,
        //            //     IsDateChanged = false,
        //            //     IsFrequencyChanged = false,
        //            //     Frequency = 0
        //            // };

        //            // // Save payment line indicating that the loan to Database
        //            // context.ClientLoanPayment.Add(stopPaymentLine);
        //            // context.SaveChanges();

        //            //

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private bool CreateClientLoanCollection(int idLoanPayment, string errorCode)
        //{
        //    using (var context = _dbContextProvider.GetContext())
        //    {
        //        bool isCollections = false;
        //        var loanId = context.ClientLoanPayment
        //            .Where(x => x.Id == idLoanPayment)
        //            .SingleOrDefault()
        //            .IdclientLoan;

        //        // Check to see if they are already in Collections
        //        var alreadyInCollections = context.ClientLoanCollection
        //                                          .Where(x => x.IdclientLoan == idLoanPayment && x.IdcollectionStatus == 0) // Rejections of the day
        //                                          .SingleOrDefault();

        //        switch (errorCode)
        //        {
        //            // PERTES
        //            case "910":

        //                var col1 = new ClientLoanCollection()
        //                {
        //                    IdclientLoan = (int)loanId,
        //                    IdcollectionStatus = context.CollectionStatus
        //                                .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                .SingleOrDefault()
        //                                .Id,
        //                    IdcollectionDegree = context.CollectionDegree
        //                                .Where(x => x.Code == "")
        //                                .SingleOrDefault()
        //                                .Id,
        //                    CreatedDateTime = DateTime.Now
        //                };

        //                context.ClientLoanCollection.Add(col1);
        //                context.SaveChanges();
        //                isCollections = true;

        //                break;

        //            // COLLECTION 3
        //            case "905":

        //                if (alreadyInCollections != null)
        //                {
        //                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                            .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
        //                                                            .SingleOrDefault()
        //                                                            .Id;
        //                    context.ClientLoanCollection.Update(alreadyInCollections);
        //                    context.SaveChanges();
        //                }
        //                else
        //                {
        //                    var col2 = new ClientLoanCollection()
        //                    {
        //                        IdclientLoan = (int)loanId,
        //                        IdcollectionStatus = context.CollectionStatus
        //                                    .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                    .SingleOrDefault()
        //                                    .Id,
        //                        IdcollectionDegree = context.CollectionDegree
        //                                    .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
        //                                    .SingleOrDefault()
        //                                    .Id,
        //                        CreatedDateTime = DateTime.Now
        //                    };

        //                    context.ClientLoanCollection.Add(col2);
        //                    context.SaveChanges();
        //                }

        //                isCollections = true;

        //                break;

        //            // COLLECTION 1,2 or 3
        //            default:

        //                // Verify what degree of Collection
        //                var loanPayments = (from clp in context.ClientLoanPayment
        //                                    join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
        //                                    where (clp.IdclientLoan == loanId && clp.IsDeleted == false)
        //                                    select (new ClientLoanPaymentModel()
        //                                    {
        //                                        Id = clp.Id,
        //                                        StatusCode = ps.Code,
        //                                        PaymentDate = clp.PaymentDate
        //                                    }))
        //                                        .OrderBy(x => x.PaymentDate)
        //                                        .ToList();

        //                var loanPaymentIndex = loanPayments.FindIndex(x => x.Id == idLoanPayment);
        //                ClientLoanPaymentModel previousLoanPayment = null;
        //                ClientLoanPaymentModel previousSecondLoanPayment = null;

        //                if (loanPaymentIndex != 0)
        //                {
        //                    if (loanPaymentIndex - 1 >= 0)
        //                    {
        //                        previousLoanPayment = loanPayments[loanPaymentIndex - 1];
        //                    }

        //                    if (loanPaymentIndex - 2 >= 0)
        //                    {
        //                        previousSecondLoanPayment = loanPayments[loanPaymentIndex - 2];
        //                    }
        //                }

        //                if (previousLoanPayment != null && previousSecondLoanPayment != null)
        //                {
        //                    // Collection 3
        //                    if (previousLoanPayment.StatusCode == "NSF" && previousSecondLoanPayment.StatusCode == "NSF")
        //                    {
        //                        if (alreadyInCollections != null)
        //                        {
        //                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                    .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
        //                                                                    .SingleOrDefault()
        //                                                                    .Id;
        //                            context.ClientLoanCollection.Update(alreadyInCollections);
        //                            context.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                            {
        //                                IdclientLoan = (int)loanId,
        //                                IdcollectionStatus = context.CollectionStatus
        //                                               .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                IdcollectionDegree = context.CollectionDegree
        //                                               .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                CreatedDateTime = DateTime.Now
        //                            });
        //                            context.SaveChanges();
        //                        }

        //                        isCollections = true;
        //                    }

        //                    // Collection 2
        //                    if (previousLoanPayment.StatusCode == "NSF")
        //                    {
        //                        if (alreadyInCollections != null)
        //                        {
        //                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                    .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
        //                                                                    .SingleOrDefault()
        //                                                                    .Id;
        //                            context.ClientLoanCollection.Update(alreadyInCollections);
        //                            context.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                            {
        //                                IdclientLoan = (int)loanId,
        //                                IdcollectionStatus = context.CollectionStatus
        //                                               .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                IdcollectionDegree = context.CollectionDegree
        //                                               .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                CreatedDateTime = DateTime.Now
        //                            });
        //                            context.SaveChanges();
        //                        }

        //                        isCollections = true;
        //                    }

        //                    // Collection 1
        //                    else
        //                    {
        //                        if (alreadyInCollections != null)
        //                        {
        //                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                    .Where(x => x.Code == "FISRT_NSF")
        //                                                                    .SingleOrDefault()
        //                                                                    .Id;
        //                            context.ClientLoanCollection.Update(alreadyInCollections);
        //                            context.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                            {
        //                                IdclientLoan = (int)loanId,
        //                                IdcollectionStatus = context.CollectionStatus
        //                                                    .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                                    .SingleOrDefault()
        //                                                    .Id,
        //                                IdcollectionDegree = context.CollectionDegree
        //                                                    .Where(x => x.Code == "FIRST_NSF")
        //                                                    .SingleOrDefault()
        //                                                    .Id,
        //                                CreatedDateTime = DateTime.Now
        //                            });
        //                            context.SaveChanges();
        //                        }

        //                        isCollections = true;
        //                    }
        //                }

        //                else if (previousLoanPayment != null && previousSecondLoanPayment == null)
        //                {
        //                    // Collection 2
        //                    if (previousLoanPayment.StatusCode == "NSF")
        //                    {
        //                        if (alreadyInCollections != null)
        //                        {
        //                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                    .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
        //                                                                    .SingleOrDefault()
        //                                                                    .Id;
        //                            context.ClientLoanCollection.Update(alreadyInCollections);
        //                            context.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                            {
        //                                IdclientLoan = (int)loanId,
        //                                IdcollectionStatus = context.CollectionStatus
        //                                               .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                IdcollectionDegree = context.CollectionDegree
        //                                               .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                CreatedDateTime = DateTime.Now
        //                            });
        //                            context.SaveChanges();
        //                        }

        //                        isCollections = true;
        //                    }
        //                    // Collection 1
        //                    else
        //                    {
        //                        if (alreadyInCollections != null)
        //                        {
        //                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                    .Where(x => x.Code == "FIRST_NSF")
        //                                                                    .SingleOrDefault()
        //                                                                    .Id;
        //                            context.ClientLoanCollection.Update(alreadyInCollections);
        //                            context.SaveChanges();
        //                        }
        //                        else
        //                        {
        //                            context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                            {
        //                                IdclientLoan = (int)loanId,
        //                                IdcollectionStatus = context.CollectionStatus
        //                                               .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                IdcollectionDegree = context.CollectionDegree
        //                                               .Where(x => x.Code == "FIRST_NSF")
        //                                               .SingleOrDefault()
        //                                               .Id,
        //                                CreatedDateTime = DateTime.Now
        //                            });
        //                            context.SaveChanges();
        //                        }

        //                        isCollections = true;
        //                    }
        //                }

        //                // Collection 1
        //                else if (previousLoanPayment == null && previousSecondLoanPayment == null)
        //                {
        //                    if (alreadyInCollections != null)
        //                    {
        //                        alreadyInCollections.IdcollectionDegree = context.CollectionDegree
        //                                                                .Where(x => x.Code == "FIRST_NSF")
        //                                                                .SingleOrDefault()
        //                                                                .Id;
        //                        context.ClientLoanCollection.Update(alreadyInCollections);
        //                        context.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        context.ClientLoanCollection.Add(new ClientLoanCollection()
        //                        {
        //                            IdclientLoan = (int)loanId,
        //                            IdcollectionStatus = context.CollectionStatus
        //                                           .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
        //                                           .SingleOrDefault()
        //                                           .Id,
        //                            IdcollectionDegree = context.CollectionDegree
        //                                           .Where(x => x.Code == "FIRST_NSF")
        //                                           .SingleOrDefault()
        //                                           .Id,
        //                            CreatedDateTime = DateTime.Now
        //                        });
        //                        context.SaveChanges();
        //                    }

        //                    isCollections = true;
        //                }

        //                break;
        //        }

        //        return isCollections;
        //    }
        //}

        //public string GetLanguage()
        //{
        //    try
        //    {
        //        using (var context = _dbContextProvider.GetContext())
        //        {
        //            var language = context.LoanerConfiguration
        //                                    .Where(x => x.Key == "DEFAULT_LANGUAGE")
        //                                    .SingleOrDefault();

        //            return language.Value;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion
    }
}
