﻿using System;
using System.Collections.Generic;
using System.Linq;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using Microsoft.VisualBasic;

namespace CreditBook.LMS.Service.Core.Logic
{
    public class LoanCalculator
    {

        #region Properties

        public double Principal { get; set; }
        public double InterestRate { get; set; }
        public double BrokerageFees { get; set; }
        public double Fees { get; set; }
        public decimal? Rebate { get; set; }
        public double BrokerageRate { get; set; }
        public double InterestAmount { get; set; }
        public int Frequency { get; set; }
        public int FrequencyDelay { get; set; }
        public int NumberPayments { get; set; }
        public DateTime StartDate { get; set; }
        public double TotalPayment { get; set; }
        public double MonthlyPayment { get; set; }
        public int editPaymentIndex { get; set; }
        public double editPaymentNewAmount { get; set; }
        public int StartDay { get; set; }
        public int EndDay { get; set; }

        public List<PaymentStaticItem> TempPayments { get; set; }
        public List<ClientLoanPaymentModel> ClientPayments { get; set; }

        #endregion

        public LoanCalculator() { }

        public LoanCalculator(double principal, int frequency, double fees, int numberPayments, double interestRate, DateTime startDate, int startDay, int endDay, int delay, double brokerageRate, decimal? rebate)
        {
            Principal = principal;
            Frequency = frequency;
            FrequencyDelay = delay;
            BrokerageRate = brokerageRate;
            Fees = fees;
            NumberPayments = numberPayments;
            InterestRate = interestRate;
            StartDate = startDate;
            StartDay = startDay;
            EndDay = endDay;
            Rebate = rebate;
        }

        public void CalculateNewLoanAnualInterest()
        {
            var payments = new LoanPayments();
            var paymentDates = payments.GetPaymentDates(Frequency, StartDate, NumberPayments, StartDay, EndDay, FrequencyDelay, false);

            double interestRateToDecimal = InterestRate / 100;//InterestRate / 100;
            double convertedInterest = interestRateToDecimal / Frequency; // Monthly

            var indexPayment = 1;

            while (indexPayment <= NumberPayments)
            {
                InterestAmount += Financial.IPmt(convertedInterest, indexPayment, NumberPayments, -Principal, 0);
                indexPayment++;
            }

            BrokerageFees = BrokerageRate == 0 ? 0 : (Principal * (1 + BrokerageRate / 100)) - Principal - InterestAmount;
            BrokerageFees = BrokerageFees - (Double)Rebate;
            Principal = Principal + BrokerageFees;

            double monthlyPayment = Math.Round(Financial.Pmt(convertedInterest, NumberPayments, -Principal, 0), 2);
            MonthlyPayment = monthlyPayment;
            double totalPayments = Math.Round(monthlyPayment * NumberPayments, 2);
            double newBalance = Math.Round(Principal + Fees, 2);

            //Create payments
            indexPayment = 1;
            InterestAmount = 0;

            TempPayments = new List<PaymentStaticItem>();
            //TempPayments = new List<ClientLoanPaymentModel>();

            // FEES NOT EQUAL TO ZERO WILL BE HANDLED LATER
            if (Fees > 0)
            {
                while (newBalance > 0)
                {
                    var payment = Math.Round((newBalance < monthlyPayment) ? newBalance : monthlyPayment, 2);
                    var interestPaid = Math.Round(newBalance * convertedInterest, 2);
                    var deductedBalance = payment - interestPaid;
                    newBalance = (newBalance < monthlyPayment) ? 0 : newBalance - deductedBalance;
                    InterestAmount += interestPaid;

                    if (newBalance < 2.50)
                    {
                        newBalance = 0;
                    }

                    //TempPayments.Add(new ClientLoanPaymentModel()
                    //{
                    //});

                    TempPayments.Add(new PaymentStaticItem()
                    {
                        Index = indexPayment,
                        PaymentDate = paymentDates[indexPayment - 1],
                        Payment1 = monthlyPayment,
                        Interest = interestPaid,
                        Principal = deductedBalance,
                        Balance = newBalance,
                        ContratMaxAmount = monthlyPayment,
                        Status = "PEN"
                    });

                    TotalPayment += monthlyPayment;

                    indexPayment++;
                }
            }
            else
            {
                while (indexPayment <= NumberPayments)
                {
                    var interestPaid = Math.Round(newBalance * convertedInterest, 2);
                    var deductedBalance = Math.Round(monthlyPayment - interestPaid, 2);
                    newBalance = newBalance - deductedBalance;
                    InterestAmount += interestPaid;

                    //// Check if the Payment falls on a weekend
                    //DateTime paymentDate = paymentDates[indexPayment - 1];

                    //if (paymentDates[indexPayment - 1].DayOfWeek == DayOfWeek.Saturday)
                    //{
                    //    paymentDate = paymentDates[indexPayment - 1].AddDays(2);
                    //}

                    //if (paymentDates[indexPayment - 1].DayOfWeek == DayOfWeek.Sunday)
                    //{
                    //    paymentDate = paymentDates[indexPayment - 1].AddDays(1);
                    //}

                    //TempPayments.Add(new ClientLoanPaymentModel()
                    //{
                    //    StatusCode = "PEN",
                    //    Index = indexPayment,
                    //    Interest =  Convert.ToDecimal(interestPaid),
                    //    Capital = Convert.ToDecimal(deductedBalance),
                    //    Amount = Convert.ToDecimal(monthlyPayment),
                    //    Balance = Convert.ToDecimal(newBalance),
                    //    PaymentDate = paymentDate//paymentDates[indexPayment - 1]
                    //});

                    TempPayments.Add(new PaymentStaticItem()
                    {
                        Index = indexPayment,
                        PaymentDate = paymentDates[indexPayment - 1],
                        Payment1 = monthlyPayment,
                        Interest = interestPaid,
                        Principal = deductedBalance,
                        Balance = newBalance,
                        //ContratMaxAmount = monthlyPayment,
                        Status = "PEN"
                    });

                    TotalPayment += monthlyPayment;

                    indexPayment++;
                }
            }
        }

        public void CalculateNewLoanDailyInterest()
        {
            var loanGrid = TempPayments;

            //var updatedGrid = new List<PaymentStaticItem>();

            //Load dates
            var payments = new LoanPayments();
            //var paymentDates = payments.GetPaymentDates(Frequency, StartDate, NumberPayments, StartDay, EndDay);

            double interestRateToDecimal = InterestRate / 100;
            double convertedInterest = interestRateToDecimal / Frequency;
            double dailyInterest = interestRateToDecimal / 365;

            double remainingBalance = Principal;// + BrokerageFees;

            //Reset all payment indexes
            loanGrid.ForEach(p => p.Index = 0);

            var indexPayment = 0;

            while (remainingBalance > 0)
            {
                var current = (indexPayment < loanGrid.Count) ? loanGrid[indexPayment] : null;
                var previous = (indexPayment > 0) ? loanGrid[indexPayment - 1] : null;
                var intervalDays = 0;
                var payment = 0.0;

                //Working on existing payments
                if (current != null)
                {
                    // Checnk for weekend days in order to properly calculate interest
                    current.PaymentDate = ValidateWeekend((DateTime)current.PaymentDate);

                    intervalDays = (previous != null) ? (current.PaymentDate - previous.PaymentDate).Days : 14;
                    var intervalInterest = Math.Round((dailyInterest * intervalDays) * ((previous != null) ? previous.Balance : remainingBalance), 2);

                    if (indexPayment == 0)
                    {
                        //Maintain first interval interest amount
                        intervalInterest = current.Interest;
                    }

                    current.Index = indexPayment + 1;

                    //Handle default payments
                    if (current.IsManual)
                    {
                        current.Fees = 0;
                        current.Interest = intervalInterest;
                        //current.Status = "<span class=\"text-default\">Paiement manuel</span>"; // Replace by LMS Status
                    }
                    else if (current.IsNSF)
                    {
                        current.Payment1 = 0;
                        current.Fees = 50;
                        current.Interest = 0;
                        //current.Status = "<span class=\"text-danger\">Sans provisions</span>"; // Replace by LMS Status
                    }
                    else if (current.IsRebate)
                    {
                        current.Fees = 0;
                        current.Interest = intervalInterest;
                        //current.Status = "<span class=\"text-default\">Rabais</span>"; // Replace by LMS Status
                    }
                    else if (current.IsAmountChanged)
                    {
                        MonthlyPayment = current.NewAmount;
                        payment = (remainingBalance + intervalInterest < MonthlyPayment) ? remainingBalance + intervalInterest : MonthlyPayment;
                        current.Payment1 = Math.Round(payment, 2);
                        current.Fees = 0;
                        current.Interest = intervalInterest;
                        //current.Status = "<span class=\"text-warning\">Nouveau Montant</span>"; // Replace by LMS Status
                    }
                    else
                    {
                        payment = (remainingBalance + intervalInterest < MonthlyPayment) ? remainingBalance + intervalInterest : MonthlyPayment;
                        current.Payment1 = Math.Round(payment, 2);
                        current.Fees = 0;
                        current.Interest = /*current.Interest;*/intervalInterest;
                        current.Status = "PEN";

                    }

                    var currentPrincipal = Math.Round(current.Payment1 - intervalInterest - current.Fees, 2);
                    remainingBalance = Math.Round(remainingBalance - currentPrincipal, 2);

                    if (remainingBalance < 2.50)
                    {
                        remainingBalance = 0;
                    }
                    current.ContratMaxAmount = MonthlyPayment;
                    current.Principal = currentPrincipal;
                    current.Balance = remainingBalance;

                }
                else //Need to create new payments
                {
                    var newPayment = new PaymentStaticItem();
                    var previousRegular = (previous != null) ? loanGrid.Where(p => p.IsManual == false || p.IsNSF == false || p.IsRebate == false).LastOrDefault() : null;

                    newPayment.Index = indexPayment + 1;
                    newPayment.PaymentDate = payments.GetNextPaymentDate(Frequency, previousRegular.PaymentDate, StartDay, EndDay, false, false);

                    // Checnk for weekend days in order to properly calculate interest
                    newPayment.PaymentDate = ValidateWeekend((DateTime)newPayment.PaymentDate);

                    intervalDays = (newPayment.PaymentDate - previous.PaymentDate).Days;

                    //var test = Financial.IPmt(convertedInterest, newPayment.Index, loanGrid.Count + 1, -Principal, 0);
                    //var test2 = Financial.IPmt(convertedInterest, newPayment.Index, loanGrid.Count + 1, -previousRegular.Balance, 0);

                    var intervalInterest = Math.Round((dailyInterest * intervalDays) * previous.Balance, 2);
                    payment = (remainingBalance < MonthlyPayment) ? remainingBalance + intervalInterest : MonthlyPayment;


                    var currentPrincipal = payment - intervalInterest;

                    newPayment.Payment1 = Math.Round(payment, 2);
                    newPayment.Interest = intervalInterest;
                    newPayment.Principal = currentPrincipal;
                    newPayment.ContratMaxAmount = MonthlyPayment;

                    remainingBalance = remainingBalance - currentPrincipal;

                    if (remainingBalance < 2.50)
                    {
                        remainingBalance = 0;
                    }

                    newPayment.Balance = remainingBalance;

                    newPayment.Status = "PEN";
                    loanGrid.Add(newPayment);

                }

                indexPayment++;
            }

            //Remove extra payments
            loanGrid.RemoveAll(p => p.Index == 0);

            //Interest total adjustments
            InterestAmount = loanGrid.Sum(x => x.Interest);

            //return loanGrid;

            // Convert Temp Stats object using Double amounts
            ClientPayments = new List<ClientLoanPaymentModel>();

            foreach (var loan in loanGrid)
            {
                // Check if the Payment falls on a weekend
                //DateTime paymentDate = loan.PaymentDate;

                //if (loan.PaymentDate.DayOfWeek == DayOfWeek.Saturday)
                //{
                //    paymentDate = loan.PaymentDate.AddDays(2);
                //}

                //if (loan.PaymentDate.DayOfWeek == DayOfWeek.Sunday)
                //{
                //    paymentDate = loan.PaymentDate.AddDays(1);
                //}

                ClientPayments.Add(new ClientLoanPaymentModel()
                {
                    StatusCode = loan.Status,
                    Interest = Convert.ToDecimal(loan.Interest),
                    Capital = Convert.ToDecimal(loan.Principal),
                    Amount = Convert.ToDecimal(loan.Payment1),
                    MaxAmount = Convert.ToDecimal(loan.ContratMaxAmount),
                    Balance = Convert.ToDecimal(loan.Balance),
                    Fees = 0,
                    PaymentDate = loan.PaymentDate//paymentDate
                }); ;
            }
        }

        public List<ClientLoanPaymentModel> CalculateDailyInterest(List<ClientLoanPaymentModel> loanPayments, ClientLoanModel loan, DateTime paymentDate, decimal fees, int? startDay, int? endDay, bool modifyFrequency, bool modifyAmount, /*bool deferPaymentDate,*/ bool deferPayment, bool restartLoan)
        {
            int indexChangeFrequency = 0;
            //int indexDeferPayment = 0;

            //Load dates
            var loanPaymentCalculator = new LoanPayments();
            var loanInterestRate = Convert.ToDouble(InterestRate);
            var maxDateArray = 400;

            var frequency = loan.FrequencyDuration;
            double interestRateToDecimal = loanInterestRate / 100;
            double convertedInterest = interestRateToDecimal / (int)frequency;
            double dailyInterest = interestRateToDecimal / 365;
            double maxContractAmount = Convert.ToDouble(loan.Amount);
            var isChangingForAll = false;

            List<DateTime> paymentDates;
            paymentDates = loanPaymentCalculator.GetPaymentDates((int)frequency, paymentDate, maxDateArray, startDay, endDay, 0, false);

            double remainingBalance = 0;
            remainingBalance = (double)loan.Amount.GetValueOrDefault() + (double)loan.BrokerageFee;

            //Reset all payment indexes
            loanPayments.ForEach(p => p.Index = 0);

            var indexPayment = 0;

            while (remainingBalance > 0)
            {
                var current = (indexPayment < loanPayments.Count) ? loanPayments[indexPayment] : null;
                var previous = (indexPayment > 0) ? loanPayments[indexPayment - 1] : null;
                var intervalDays = 0;
                var payment = 0.0;

                //Working on existing payments
                if (current != null)
                {
                    maxContractAmount = (isChangingForAll) ? maxContractAmount : Convert.ToDouble(current.MaxAmount);

                    if (current.PaymentDate < paymentDate.Date /*&& !deferPaymentDate*/)
                    {
                        //Skip previous payments
                        remainingBalance = Convert.ToDouble(current.Balance);
                        current.Index = indexPayment + 1;
                        indexPayment++;
                        continue;
                    }
                    else
                    {
                        // Update the date for all existing Payments when changing Frequency
                        if (modifyFrequency)
                        {
                            current.PaymentDate = paymentDates[indexChangeFrequency];
                            current.FrequencyDuration = frequency;
                            indexChangeFrequency++;
                        }

                        // Update the date for all future payments when Defer Payment
                        // if (deferPaymentDate)
                        // {
                        //     current.PaymentDate = paymentDates[indexDeferPayment];
                        //     indexDeferPayment++;
                        // }
                    }

                    // Checnk for weekend days in order to properly calculate interest
                    current.PaymentDate = ValidateWeekend((DateTime)current.PaymentDate);

                    //
                    intervalDays = (previous != null) ? ((DateTime)current.PaymentDate - (DateTime)previous.PaymentDate).Days : 14;

                    var intervalInterest = Math.Round((dailyInterest * intervalDays) * ((previous != null) ? Convert.ToDouble(previous.Balance) : remainingBalance), 2);
                    current.Index = indexPayment + 1;

                    if (indexPayment == 0)
                    {
                        //Determine first interval interest amount
                        double days = 365 / Convert.ToDouble(frequency);
                        double iInterest = (365 / Convert.ToDouble(frequency)) * dailyInterest;

                        double interest = interest = iInterest * Convert.ToDouble(loan.Amount.GetValueOrDefault());
                        intervalInterest = interest;
                    }

                    //Handle default payments
                    if ((bool)current.IsManual)
                    {
                        current.Fees = 0;
                        current.Interest = Convert.ToDecimal(intervalInterest);
                    }
                    else if ((bool)current.IsNSF)
                    {
                        current.Amount = 0;
                        current.Fees = fees;
                        current.Interest = 0;
                    }
                    else if ((bool)current.IsRebate)
                    {
                        current.Fees = 0;
                        current.Interest = Convert.ToDecimal(intervalInterest);
                    }
                    // else if (current.StatusCode == "DP")
                    // {
                    //     // DO NOTHING FOR NOW
                    // }
                    // else if (current.StatusCode == "SP")
                    // {
                    //     // DO NOTHING FOR NOW
                    // }
                    // Used for modifying a Payment
                    else if ((bool)current.IsAmountChanged)
                    {
                        if ((bool)current.IsAmountChangedForAll)
                        {
                            maxContractAmount = (double)current.NewAmount;
                            isChangingForAll = true;
                            payment = (remainingBalance + intervalInterest < maxContractAmount) ? remainingBalance + intervalInterest : maxContractAmount;
                        }
                        else
                        {
                            payment = (remainingBalance + intervalInterest < (double)current.NewAmount) ? remainingBalance + intervalInterest : (double)current.NewAmount;
                        }

                        current.Amount = Convert.ToDecimal(Math.Round(payment, 2));
                        current.MaxAmount = Convert.ToDecimal(maxContractAmount);
                        current.Fees = 0;
                        current.Interest = (decimal)intervalInterest;

                        //Reset amount changed label
                        if (loan.Amount == current.Amount)
                        {
                            current.IsAmountChanged = false;
                        }
                    }
                    else
                    {
                        // Do nothing for Deferred Payment, Stop Payment and Stopped Loan Payment
                        if (!current.IsDeferredFee == null && current.StatusCode != "SP" && current.StatusCode != "SLP")
                        {
                            payment = (remainingBalance + intervalInterest < maxContractAmount) ? remainingBalance + intervalInterest : maxContractAmount;
                            current.Amount = Convert.ToDecimal(Math.Round(payment, 2));

                            current.MaxAmount = Convert.ToDecimal(maxContractAmount);
                            current.Fees = 0;
                            current.Interest = Convert.ToDecimal(intervalInterest);
                        }
                    }

                    // FOR ALL
                    Decimal? currentPrincipal = 0;
                    currentPrincipal = Convert.ToDecimal(current.Amount) - Convert.ToDecimal(intervalInterest) - current.Fees;

                    if (current.StatusCode != "SP" || current.StatusCode != "SLP")
                    {
                        remainingBalance = remainingBalance - Convert.ToDouble(currentPrincipal);
                        current.Capital = currentPrincipal;
                    }

                    if (remainingBalance < 2.50)
                    {
                        remainingBalance = 0;
                    }

                    if (current.StatusCode == "SP")
                    {
                        current.Balance = 0;
                    }
                    else
                    {
                        if (current.StatusCode != "SLP")
                        {
                            current.Balance = Convert.ToDecimal(Math.Round(remainingBalance, 2));
                        }
                    }
                }
                else //Need to create new payments
                {
                    // If the loan is stopped, do not create new payments
                    if ((bool)loan.IsLoanStopped)
                    {
                        remainingBalance = 0;
                    }

                    if (!(bool)loan.IsLoanStopped)
                    {
                        var newPayment = new ClientLoanPaymentModel();
                        newPayment.IsAmountChanged = false;
                        newPayment.IsManual = false;
                        newPayment.IsNSF = false;
                        newPayment.IsRebate = false;
                        newPayment.IsFrequencyChanged = modifyFrequency;
                        newPayment.IsDateChanged = false;
                        newPayment.IsDeleted = false;
                        newPayment.CanEdit = true;
                        newPayment.CanNSF = true;

                        newPayment.FrequencyDuration = frequency;
                        newPayment.Index = indexPayment + 1;

                        if (newPayment.Index >= maxDateArray + 1)
                        {
                            //Loan is in infinite loop, stop calculation
                            loanPayments.Last().StatusCode = "SP";
                            break;
                        }

                        var previousRegular = (previous != null) ? loanPayments.Where(p => p.IsManual == false || p.IsNSF == false || p.IsRebate == false).LastOrDefault() : null;

                        // Check for weekend days in order to properly calculate interest
                        newPayment.PaymentDate = previousRegular != null
                                                                  ? loanPaymentCalculator.GetNextPaymentDate((int)frequency, (DateTime)previousRegular.PaymentDate, (int)startDay, (int)endDay, false, restartLoan ? false : true)
                                                                  : loanPaymentCalculator.GetNextPaymentDate((int)frequency, paymentDate, (int)startDay, (int)endDay, false, restartLoan ? false : true);
                        newPayment.PaymentDate = ValidateWeekend((DateTime)newPayment.PaymentDate);

                        //
                        if (newPayment.PaymentDate < paymentDate)
                        {
                            newPayment.PaymentDate = paymentDate;
                        }

                        intervalDays = previous != null ? ((DateTime)newPayment.PaymentDate - (DateTime)previous.PaymentDate).Days
                                                        : ((DateTime)newPayment.PaymentDate - paymentDate.Date).Days;

                        var intervalInterest = previous != null ? Math.Round((Convert.ToDecimal(dailyInterest) * intervalDays) * Convert.ToDecimal(previous.Balance), 2)
                                                                : Math.Round((Convert.ToDecimal(dailyInterest) * intervalDays) * Convert.ToDecimal(remainingBalance), 2);

                        payment = (remainingBalance + Convert.ToDouble(intervalInterest) < maxContractAmount)
                                                    ? remainingBalance + Convert.ToDouble(intervalInterest) : maxContractAmount;

                        var currentPrincipal = Convert.ToDecimal(payment) - intervalInterest;

                        newPayment.Amount = Convert.ToDecimal(Math.Round(payment, 2));
                        newPayment.Interest = intervalInterest;
                        newPayment.Capital = currentPrincipal;
                        newPayment.MaxAmount = Convert.ToDecimal(maxContractAmount);
                        newPayment.Fees = 0;

                        remainingBalance = remainingBalance - Convert.ToDouble(currentPrincipal);

                        if (remainingBalance < 2.50)
                        {
                            remainingBalance = 0;
                        }

                        newPayment.Balance = Convert.ToDecimal(remainingBalance);

                        newPayment.StatusCode = "PEN";
                        loanPayments.Add(newPayment);
                    }
                }

                indexPayment++;
            }

            //Remove extra payments and set IsDeleted = True
            loanPayments.Where(X => X.Index == 0)
                        .ToList()
                        .ForEach(x => x.IsDeleted = true);

            return loanPayments;
        }

        public List<ClientLoanPaymentModel> CalculateDailyInterestDeferredPayment(List<ClientLoanPaymentModel> loanPayments, ClientLoanModel loan, ClientLoanPayment loanPayment, decimal fees, int? startDay, int? endDay)
        {
            //Load dates
            var loanPaymentCalculator = new LoanPayments();
            var loanInterestRate = Convert.ToDouble(InterestRate);
            var maxDateArray = 200;

            var frequency = loan.FrequencyDuration;
            double interestRateToDecimal = loanInterestRate / 100;
            double convertedInterest = interestRateToDecimal / (int)frequency;
            double dailyInterest = interestRateToDecimal / 365;
            double maxContractAmount = Convert.ToDouble(loan.Amount);

            var selectedPayments = loanPayments.Where(x => x.PaymentDate >= loanPayment.PaymentDate).ToList();
            var paymentDates = loanPaymentCalculator.GetPaymentDates((int)frequency,
                                                selectedPayments.Count() == 1
                                                ? (DateTime)loanPayment.PaymentDate
                                                : (DateTime)selectedPayments[0].PaymentDate, maxDateArray, startDay, endDay, 0, true);

            double remainingBalance = 0;
            remainingBalance = (double)selectedPayments.Where(x => x.StatusCode == "PEN" || x.StatusCode == "PAID" || x.StatusCode == "PC").Sum(x => x.Capital);

            // Clone original payments
            List<ClientLoanPaymentModel> originalPayments = new List<ClientLoanPaymentModel>();
            foreach (var item in loanPayments)
            {
                originalPayments.Add(new ClientLoanPaymentModel()
                {
                    Id = item.Id,
                    Index = item.Index,
                    LoanId = item.LoanId,
                    Status = item.Status,
                    StatusCode = item.StatusCode,
                    Interest = item.Interest,
                    Balance = item.Balance,
                    Capital = item.Capital,
                    Amount = item.Amount,
                    MaxAmount = item.MaxAmount,
                    Fees = item.Fees,
                    PaymentDate = item.PaymentDate,

                    IsNSF = item.IsNSF,
                    IsManual = item.IsManual,
                    IsRebate = item.IsRebate,
                    IsAmountChanged = item.IsAmountChanged,
                    IsAmountChangedForAll = false,
                    IsDateChanged = item.IsDateChanged,
                    IsFrequencyChanged = item.IsFrequencyChanged,
                    IsDeferredFee = item.IsDeferredFee,

                    FrequencyDuration = item.FrequencyDuration,
                    CanEdit = item.CanEdit,
                    CanNSF = item.CanNSF,
                    IsDeleted = item.IsDeleted,

                    NewAmount = item.NewAmount
                });
            }

            //Reset all payment indexes
            selectedPayments.ForEach(p => p.Index = 0);

            var indexPayment = 0;

            while (remainingBalance > 0)
            {
                var current = (indexPayment < selectedPayments.Count) ? selectedPayments[indexPayment] : null;
                var previous = indexPayment != 0 ? selectedPayments[indexPayment - 1] : null;
                var intervalDays = 0;
                var payment = 0.0;

                //Working on existing payments
                if (current != null)
                {
                    maxContractAmount = Convert.ToDouble(current.MaxAmount);
                    current.PaymentDate = paymentDates[indexPayment];
                    current.PaymentDate = ValidateWeekend((DateTime)current.PaymentDate);

                    // Check if it is the first payment
                    var currentOriginal = originalPayments.SingleOrDefault(x => x.Id == current.Id);
                    if (currentOriginal.Index == 0)
                    {
                        intervalDays = ((DateTime)paymentDates[0] - (DateTime)loanPayment.PaymentDate).Days;
                    }
                    else
                    {
                        intervalDays = previous != null ? ((DateTime)current.PaymentDate - (DateTime)previous.PaymentDate).Days
                                                          : ((DateTime)originalPayments[currentOriginal.Index].PaymentDate - (DateTime)originalPayments[currentOriginal.Index - 1].PaymentDate).Days;
                    }

                    var intervalInterest = Math.Round((dailyInterest * intervalDays) * ((previous != null) ? Convert.ToDouble(previous.Balance) : remainingBalance), 2);
                    current.Index = indexPayment + 1;

                    if ((bool)current.IsManual)
                    {
                        current.Fees = 0;
                        current.Interest = Convert.ToDecimal(intervalInterest);
                    }
                    else
                    {
                        // Do nothing for Deferred Payment, Stop Payment and Stopped Loan Payment
                        if (!current.IsDeferredFee == null && current.StatusCode != "SP" && current.StatusCode != "SLP")
                        {
                            payment = (remainingBalance + intervalInterest < maxContractAmount) ? remainingBalance + intervalInterest : maxContractAmount;
                            current.Amount = Convert.ToDecimal(Math.Round(payment, 2));
                            current.MaxAmount = Convert.ToDecimal(maxContractAmount);
                            current.Interest = Convert.ToDecimal(intervalInterest);
                        }
                    }

                    // var currentPrincipal = Convert.ToDecimal(current.Amount) - Convert.ToDecimal(intervalInterest);
                    // remainingBalance = remainingBalance - Convert.ToDouble(currentPrincipal);

                    // if (remainingBalance < 2.50)
                    // {
                    //     remainingBalance = 0;
                    // }

                    // current.Capital = currentPrincipal;
                    // current.Balance = Convert.ToDecimal(Math.Round(remainingBalance, 2));

                    // FOR ALL
                    Decimal? currentPrincipal = 0;
                    currentPrincipal = Convert.ToDecimal(current.Amount) - Convert.ToDecimal(intervalInterest);

                    if (current.StatusCode != "SP" || current.StatusCode != "SLP")
                    {
                        remainingBalance = remainingBalance - Convert.ToDouble(currentPrincipal);
                        current.Capital = currentPrincipal;
                    }

                    if (remainingBalance < 2.50)
                    {
                        remainingBalance = 0;
                    }

                    if (current.StatusCode == "SP")
                    {
                        current.Balance = 0;
                    }
                    else
                    {
                        if (current.StatusCode != "SLP")
                        {
                            current.Balance = Convert.ToDecimal(Math.Round(remainingBalance, 2));
                        }
                    }
                }
                else //Need to create new payments
                {
                    var newPayment = new ClientLoanPaymentModel();
                    newPayment.IsAmountChanged = false;
                    newPayment.IsManual = false;
                    newPayment.IsNSF = false;
                    newPayment.IsRebate = false;
                    newPayment.IsDateChanged = false;
                    newPayment.IsDeleted = false;
                    newPayment.CanEdit = true;
                    newPayment.CanNSF = true;

                    newPayment.FrequencyDuration = frequency;
                    newPayment.Index = indexPayment + 1;

                    if (newPayment.Index >= maxDateArray + 1)
                    {
                        selectedPayments.Last().StatusCode = "SP";
                        break;
                    }

                    // Check if previous balance is null
                    var previousRegular = (previous != null) ? selectedPayments.Where(p => p.IsManual == false || p.IsNSF == false || p.IsRebate == false).LastOrDefault() : null;

                    // Checnk for weekend days in order to properly calculate interest
                    newPayment.PaymentDate = previousRegular != null ? loanPaymentCalculator.GetNextPaymentDate((int)previousRegular.FrequencyDuration, (DateTime)previousRegular.PaymentDate, (int)startDay, (int)endDay, true, false)
                                                              : loanPaymentCalculator.GetNextPaymentDate((int)frequency, (DateTime)loanPayment.PaymentDate, (int)startDay, (int)endDay, true, false);

                    intervalDays = previous != null ? ((DateTime)newPayment.PaymentDate - (DateTime)previous.PaymentDate).Days
                                                    : ((DateTime)newPayment.PaymentDate - (DateTime)loanPayment.PaymentDate).Days;

                    var intervalInterest = previous != null ? Math.Round((Convert.ToDecimal(dailyInterest) * intervalDays) * Convert.ToDecimal(previous.Balance), 2)
                                                            : Math.Round((Convert.ToDecimal(dailyInterest) * intervalDays) * Convert.ToDecimal(remainingBalance), 2);

                    payment = (remainingBalance + Convert.ToDouble(intervalInterest) < maxContractAmount)
                                                ? remainingBalance + Convert.ToDouble(intervalInterest) : maxContractAmount;

                    var currentPrincipal = Convert.ToDecimal(payment) - intervalInterest;

                    //newPayment.Payment1 = Math.Round(payment, 2);
                    newPayment.Amount = Convert.ToDecimal(Math.Round(payment, 2));
                    newPayment.Interest = intervalInterest;
                    newPayment.Capital = currentPrincipal;
                    newPayment.MaxAmount = Convert.ToDecimal(maxContractAmount);
                    newPayment.Fees = 0;

                    remainingBalance = remainingBalance - Convert.ToDouble(currentPrincipal);

                    if (remainingBalance < 2.50)
                    {
                        remainingBalance = 0;
                    }

                    newPayment.Balance = Convert.ToDecimal(remainingBalance);
                    newPayment.StatusCode = "PEN";
                    selectedPayments.Add(newPayment);
                }

                indexPayment++;
            }

            //Remove extra payments and set IsDeleted = True
            selectedPayments.Where(X => X.Index == 0)
                        .ToList()
                        .ForEach(x => x.IsDeleted = true);

            return selectedPayments;
        }

        public DateTime ValidateWeekend(DateTime paymentDate)
        {
            if (((DateTime)paymentDate).DayOfWeek == DayOfWeek.Saturday)
            {
                paymentDate = ((DateTime)paymentDate).AddDays(2);
            }

            if (((DateTime)paymentDate).DayOfWeek == DayOfWeek.Sunday)
            {
                paymentDate = ((DateTime)paymentDate).AddDays(1);
            }

            return paymentDate;
        }

        public double CalculateLoanDailyBalanceOwing(ClientLoanModel loan, bool stopLoan)
        {
            // If the Loan is stopped, take the Balance of the Stopped Payment Line
            var _stoppedLoan = loan.LoanPayments.Where(x => x.IsDeleted == false).LastOrDefault();
            if (_stoppedLoan.StatusCode == "SP")
                return (double)_stoppedLoan.Balance;

            // If the Loan is stopped and the last payment is a Payment, take the Balance of the Payment Line
            if (_stoppedLoan.StatusCode == "SLP")
                return (double)_stoppedLoan.Balance;

            // If all payments are smaller than current date
            var _futurePayments = loan.LoanPayments.Where(x => x.IsDeleted == false
                                                               && (x.StatusCode != "PR" && x.StatusCode != "DP" && x.StatusCode != "SP")
                                                               && x.PaymentDate > DateTime.Today
                                                               ).Count();
            if (_futurePayments == 0)
                return 0;

            // Payment Agreements
            if (loan.StatusCode == "PA" && !stopLoan)
                return -999;

            var result = 0.0d;
            var _payments = loan.LoanPayments.Where(p => (p.IsDeferredFee == false || p.IsDeferredFee == null)
                        && p.StatusCode != "DP"
                        && p.StatusCode != "PR")
                .ToList();
            var firstPaymentDate = _payments.First().PaymentDate;

            double interestRateToDecimal = InterestRate / 100;
            double convertedInterest = interestRateToDecimal / (double)loan.FrequencyDuration;
            double dailyInterest = interestRateToDecimal / 365;

            if (_payments.Where(x => x.PaymentDate.Value.Date <= DateTime.Today).Count() > 0)
            {
                DateTime? previousDate = null;
                Double previousBalance = 0;

                // Check to see if the Loan is a Payment Agreement
                // if (loan.StatusCode == "PA")
                // {
                //     // If all previous payments are not paid
                //     var unpaid = _payments.Where(x => x.IsDeleted == false
                //                                     && x.PaymentDate.Value.Date <= DateTime.Today
                //                                     && x.StatusCode == "PENI").ToList();
                //     if (unpaid.Count() > 0)
                //         return 0;

                //     foreach (var payment in _payments.Where(x => x.IsDeleted == false && (x.StatusCode == "PAID" || x.StatusCode == "STP" || x.StatusCode == "MP" || x.StatusCode == "REB")))
                //     {
                //         if (payment.PaymentDate.Value.Date <= DateTime.Today)
                //         {
                //             // Exclude Payments that have a Zero balance 
                //             if (payment.Balance != 0)
                //             {
                //                 previousDate = payment.PaymentDate;
                //                 previousBalance = (double)payment.Balance;
                //             }
                //         }
                //         else
                //         {
                //             break;
                //         }
                //     }

                //     var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
                //     var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

                //     result = previousBalance + intervalInterest;
                // }
                // else
                // {
                foreach (var payment in _payments/*.Where(x => x.IsDeleted == false && (x.StatusCode != "PR" && x.StatusCode != "DP"))*/)
                {
                    if (payment.PaymentDate.Value.Date <= DateTime.Today)
                    {
                        // Exclude Payments that have a Zero balance 
                        if (payment.Balance != 0)
                        {
                            previousDate = payment.PaymentDate;
                            previousBalance = (double)payment.Balance;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
                var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

                result = previousBalance + intervalInterest;
                //}
            }
            else if (DateTime.Today <= firstPaymentDate)
            {
                var intervalDays = (DateTime.Today - (DateTime)loan.CreatedDate.Value.Date).Days;
                var intervalInterest = (dailyInterest * intervalDays) * ((double)loan.Amount.GetValueOrDefault() + (double)loan.BrokerageFee);

                result = Math.Round((double)loan.Amount + (double)loan.BrokerageFee + intervalInterest, 2);
            }
            return result;
        }

        public double CalculateLoanPDFDailyBalanceOwing(LoanPDFModel loan)
        {
            // If the Loan is stopped, take the Balance of the Stopped Payment Line
            var _stoppedLoan = loan.ListPayment.Where(x => x.IsDeleted == false).LastOrDefault();
            if (_stoppedLoan.StatusCode == "SP")
                return (double)_stoppedLoan.Balance;

            // If the Loan is stopped and the last payment is a Payment, take the Balance of the Payment Line
            if (_stoppedLoan.StatusCode == "SLP")
                return (double)_stoppedLoan.Balance;

            // If all payments are smaller than current date
            var _futurePayments = loan.ListPayment.Where(x => x.IsDeleted == false
                                                               && (x.StatusCode != "PR" && x.StatusCode != "DP" && x.StatusCode != "SP")
                                                               && x.PaymentDate > DateTime.Today
                                                               ).Count();
            if (_futurePayments == 0)
                return 0;

            // Payment Agreements
            if (loan.StatusCode == "PA")
                return -999;

            var result = 0.0d;
            var _payments = loan.ListPayment.Where(p => (p.IsDeferredFee == false || p.IsDeferredFee == null)
                        && p.StatusCode != "DP"
                        && p.StatusCode != "PR")
                .ToList();
            var firstPaymentDate = _payments.First().PaymentDate;

            double interestRateToDecimal = InterestRate / 100;
            double convertedInterest = interestRateToDecimal / (double)loan.FrequencyDuration;
            double dailyInterest = interestRateToDecimal / 365;

            if (_payments.Where(x => x.PaymentDate.Value.Date <= DateTime.Today).Count() > 0)
            {
                DateTime? previousDate = null;
                Double previousBalance = 0;

                // Check to see if the Loan is a Payment Agreement
                // if (loan.StatusCode == "PA")
                // {
                //     // If all previous payments are not paid
                //     var unpaid = _payments.Where(x => x.IsDeleted == false
                //                                     && x.PaymentDate.Value.Date <= DateTime.Today
                //                                     && x.StatusCode == "PENI").ToList();
                //     if (unpaid.Count() > 0)
                //         return 0;

                //     foreach (var payment in _payments.Where(x => x.IsDeleted == false && (x.StatusCode == "PAID" || x.StatusCode == "STP" || x.StatusCode == "MP" || x.StatusCode == "REB")))
                //     {
                //         if (payment.PaymentDate.Value.Date <= DateTime.Today)
                //         {
                //             // Exclude Payments that have a Zero balance 
                //             if (payment.Balance != 0)
                //             {
                //                 previousDate = payment.PaymentDate;
                //                 previousBalance = (double)payment.Balance;
                //             }
                //         }
                //         else
                //         {
                //             break;
                //         }
                //     }

                //     var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
                //     var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

                //     result = previousBalance + intervalInterest;
                // }
                // else
                // {
                foreach (var payment in _payments.Where(x => x.IsDeleted == false && (x.StatusCode != "PR" && x.StatusCode != "DP")))
                {
                    if (payment.PaymentDate.Value.Date <= DateTime.Today)
                    {
                        // Exclude Payments that have a Zero balance 
                        if (payment.Balance != 0)
                        {
                            previousDate = payment.PaymentDate;
                            previousBalance = (double)payment.Balance;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
                var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

                result = previousBalance + intervalInterest;
                //}
            }
            else if (DateTime.Today <= firstPaymentDate)
            {
                var intervalDays = (DateTime.Today - (DateTime)loan.CreatedDate.Value.Date).Days;
                var intervalInterest = (dailyInterest * intervalDays) * ((double)loan.Amount.GetValueOrDefault() + (double)loan.BrokerageFee);

                result = Math.Round((double)loan.Amount + (double)loan.BrokerageFee + intervalInterest, 2);
            }
            return result;
        }
    }
}
