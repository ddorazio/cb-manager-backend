﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CreditBook.LMS.Common.Utilities;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Newtonsoft.Json;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;
using Microsoft.EntityFrameworkCore;
using CreditBook.LMS.External.Core;

namespace CreditBook.LMS.Service.Core.Logic
{
    enum CollectionDegree
    {
        FIRST,
        SECOND,
        THIRD,
        ACCOUNT_CLOSED
    }

    public class PerceptechDailyReturnService : IPerceptechDailyReturnService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IConfigurationService _configurationService;
        private readonly IMessageService _messageService;
        private PerceptechUploadModel _uploadLog;
        private readonly IHttpClientFactory _httpClientFactory;
        private CollectionDegree _degree;

        public PerceptechDailyReturnService(IDbContextProvider dbContextFactory, IHttpClientFactory httpClientFactory, IMessageService messageService, IConfigurationService configurationService)
        {
            _dbContextProvider = dbContextFactory;
            _configurationService = configurationService;
            _httpClientFactory = httpClientFactory;
            _messageService = messageService;
            _uploadLog = new PerceptechUploadModel();
        }

        public async Task<bool> ValidateReportReceived(DateTime rejectionDate)
        {
            using var context = _dbContextProvider.GetContext();
            var rejectionLog = await context.PerceptechRejectionLog.SingleOrDefaultAsync(x => x.RejectionDate == rejectionDate && x.Received == true);

            if (rejectionLog != null)
                return true;

            return false;
        }

        // REMOVED
        // public async Task<bool> ValidateLoanIsReimbursed(int idLoan)
        // {
        //     decimal totalAmountRemaining = 0;
        //     using var context = _dbContextProvider.GetContext();

        //     var loan = await context.ClientLoan.Where(x => x.Id == idLoan)
        //                             .SingleOrDefaultAsync();

        //     var loanPayments = await context.ClientLoanPayment.Where(x => x.IdclientLoan == loan.Id && x.IsDeleted == false)
        //                                                         .Include(x=> x.IdpaymentStatusNavigation)
        //                                                         .OrderBy(x=> x.PaymentDate).ThenBy(x=> x.Id).ToListAsync();

        //     // If the Loan is stopped, take the Balance of the Stopped Payment Line
        //     var lastPayment = loanPayments.LastOrDefault();
        //     if (lastPayment.IdpaymentStatusNavigation.Code == "SP")
        //     {
        //         totalAmountRemaining = (decimal)loanPayments.LastOrDefault().Balance;
        //     }
        //     // If the Loan is stopped and the last payment is a Payment, take the Balance of the Payment Line
        //     else if (lastPayment.IdpaymentStatusNavigation.Code == "SLP")
        //     {
        //         totalAmountRemaining = (decimal)loanPayments.LastOrDefault().Balance;
        //     }
        //     else
        //     {
        //         totalAmountRemaining = loanPayments.Count != 0
        //                 ? loanPayments.Where(x => x.IdpaymentStatusNavigation.Code == "PEN" || x.IdpaymentStatusNavigation.Code == "PC" || x.IdpaymentStatusNavigation.Code == "PENI")
        //                         .Sum(x => (decimal)x.Amount)
        //                 : 0;
        //     }

        //     if (totalAmountRemaining == 0)
        //         return true;

        //     if ((lastPayment.IdpaymentStatusNavigation.Code == "MP" || lastPayment.IdpaymentStatusNavigation.Code == "REB") && lastPayment.Balance == 0)
        //         return true;

        //     return false;
        // }


        public async Task UpdatePerecptechRejectionLog(DateTime rejectionDate, bool received, bool empty)
        {
            using var context = _dbContextProvider.GetContext();
            context.PerceptechRejectionLog.Add(new PerceptechRejectionLog()
            {
                RejectionDate = rejectionDate,
                DateSubmitted = DateTime.Now,
                Received = received,
                Empty = empty
            });
            await context.SaveChangesAsync();
        }

        public async Task UpdateLoanPayments(string dailyRejectionReportFolder, string data, string language, string lmsEndpoint)
        {
            try
            {
                // Save CSV file
                var fileName = SavePerceptechErrorReport(dailyRejectionReportFolder, data);
                //var fileName = "DailyRejectionErrorReport_2021-09-15.csv";

                //using (var reader = new StreamReader(dailyRejectionReportFolder + "2021-09-15" + "/" + fileName))
                using (var reader = new StreamReader(dailyRejectionReportFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/" + fileName))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();

                        if (!string.IsNullOrEmpty(line))
                        {
                            var columns = line.Split(',');

                            // Column 2 contains the Client Number and LoanNo
                            var clientId = columns[1].Split('-')[0].Replace('"', ' ').Trim();

                            // Check to see if the Client ID can be converted to INT
                            int clientIdConverted;
                            bool successClientId = Int32.TryParse(clientId, out clientIdConverted);

                            if (successClientId)
                            {
                                // Column 12 contains the Payment Date
                                var paymentDate = columns[12].Replace('"', ' ').Trim();

                                // Check to see if the Payment date can be converted to DateTime
                                DateTime paymentDateConverted;
                                bool successPaymentDate = DateTime.TryParse(paymentDate, out paymentDateConverted);

                                if (successPaymentDate)
                                {
                                    // Old Logic
                                    if (paymentDateConverted < new DateTime(2020, 12, 14))
                                    {
                                        var splitPaymentId = columns[1].Split('-');

                                        if (splitPaymentId.Length != 1)
                                        {
                                            var paymentId = splitPaymentId[1].Replace('"', ' ').Trim();

                                            // Column 17 has the Error Code
                                            var errorCode = columns[17].Replace('"', ' ').Trim();

                                            using (var context = _dbContextProvider.GetContext())
                                            {
                                                // Check to see if the Payment ID can be converted to INT
                                                int paymentIdConverted;
                                                bool success = Int32.TryParse(paymentId, out paymentIdConverted);

                                                if (success)
                                                {
                                                    // Validate that the Loan was not deleted.
                                                    // This is to handle the error when payments were sent for Loans that were deleted
                                                    var clientPayment = context.ClientLoanPayment.Where(x => x.Id == paymentIdConverted).SingleOrDefault();

                                                    if (clientPayment != null)
                                                    {
                                                        var clientLoan = context.ClientLoan.Where(x => x.Id == clientPayment.IdclientLoan).SingleOrDefault();
                                                        if (clientLoan.IsDeleted == true)
                                                        {
                                                            continue;
                                                        }

                                                        //if (clientPayment != null)
                                                        //{
                                                        // Stop the loan
                                                        if (errorCode == "910")
                                                        {
                                                            StopLoan(Convert.ToInt32(paymentId), language);
                                                        }
                                                        // Update the current Loan Payment
                                                        else
                                                        {
                                                            // Check to see if the Error Code can be converted to INT
                                                            int errorCodeConverted;
                                                            bool successErrorCode = Int32.TryParse(errorCode, out errorCodeConverted);

                                                            if (successErrorCode)
                                                            {
                                                                //Set the Description of the Payment that is NSF
                                                                var perceptechError = context.PerceptechErrorCode
                                                                                               .Where(x => x.Code == errorCodeConverted)
                                                                                               .SingleOrDefault();

                                                                clientPayment.IdperceptechErrorCode = perceptechError.Id;
                                                                clientPayment.Description = perceptechError.MessageFr + "/" + perceptechError.MessageEn;
                                                            }

                                                            context.ClientLoanPayment.Update(clientPayment);
                                                            context.SaveChanges();

                                                            // Call the NSF method from LoanService that already takes care of the process
                                                            // This can be moved to a shared Business Logic module
                                                            var loanService = new LoanService(_dbContextProvider, _configurationService);
                                                            loanService.LoanManualNSFPayment(new LoanGenericPaymentModel() { LoanId = (int)clientPayment.IdclientLoan, PaymentId = Convert.ToInt32(paymentId) }, language, true);
                                                        }

                                                        // Insert into ClientLoanCollection
                                                        var result = CreateClientLoanCollection(Convert.ToInt32(paymentId), errorCode);

                                                        // Set Client Status to Collections only if error processing payment has occurred
                                                        if (result)
                                                        {
                                                            await SendRejectionNotification(lmsEndpoint, (int)clientPayment.IdclientLoan);

                                                            ClientStatus clientStatus = null;
                                                            switch (_degree)
                                                            {
                                                                case CollectionDegree.FIRST:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION1");
                                                                    break;
                                                                case CollectionDegree.SECOND:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION2");
                                                                    break;
                                                                case CollectionDegree.THIRD:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION3");
                                                                    break;
                                                                case CollectionDegree.ACCOUNT_CLOSED:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "ACCOUNT_CLOSED");
                                                                    break;
                                                            }

                                                            var client = context.Client.SingleOrDefault(c => c.Id == Convert.ToInt32(clientId));
                                                            client.IdclientStatus = clientStatus.Id;
                                                            context.Client.Update(client);
                                                            context.SaveChanges();

                                                            // Send Email/SMS message
                                                            var clientInfo = (from c in context.Client
                                                                              join e in context.ClientEmailAddress on c.Id equals e.Idclient
                                                                              where e.IsPrimary == true && c.Id == clientLoan.Idclient
                                                                              select new
                                                                              {
                                                                                  FirstName = c.FirstName,
                                                                                  LastName = c.LastName,
                                                                                  Email = e.EmailAddress,
                                                                                  CelPhone = c.CellPhoneNumber
                                                                              }).SingleOrDefault();

                                                            // Retreive client language
                                                            var clientLanguage = context.Client
                                                                                .Where(x => x.Id == Convert.ToInt32(clientId))
                                                                                .Include(x => x.IdlanguageNavigation)
                                                                                .SingleOrDefault();

                                                            switch (_degree)
                                                            {
                                                                case CollectionDegree.FIRST:
                                                                    await _messageService.SendMessage_CollectionLevel1(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.SECOND:
                                                                    await _messageService.SendMessage_CollectionLevel2(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.THIRD:
                                                                    await _messageService.SendMessage_CollectionLevel3(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.ACCOUNT_CLOSED:
                                                                    await _messageService.SendMessage_CollectionAccountClosed(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else // New Logic
                                    {
                                        var splitLoanId = columns[1].Split('-');

                                        if (splitLoanId.Length != 1)
                                        {
                                            var loanNo = splitLoanId[1].Replace('"', ' ').Trim();

                                            // Check to see if the Loan No can be converted to INT
                                            int loanNoConverted;
                                            bool successLoanNo = Int32.TryParse(loanNo, out loanNoConverted);

                                            if (successLoanNo)
                                            {
                                                // Column 17 has the Error Code
                                                var errorCode = columns[17].Replace('"', ' ').Trim();

                                                using (var context = _dbContextProvider.GetContext())
                                                {
                                                    // Validate that the Loan was not deleted.
                                                    // This is to handle the error when payments were sent for Loans that were deleted
                                                    // Retreive PaymentID with ClientID, LoanNo and PaymentDate
                                                    var item = (from c in context.Client
                                                                join cl in context.ClientLoan on c.Id equals cl.Idclient
                                                                join ls in context.LoanStatus on cl.IdloanStatus equals ls.Id
                                                                join clp in context.ClientLoanPayment on cl.Id equals clp.IdclientLoan
                                                                join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                                                where c.Id == Convert.ToInt32(clientId)
                                                                && cl.LoanNumber == Convert.ToInt32(loanNo)
                                                                && clp.PaymentDate == paymentDateConverted
                                                                && ls.Code != "LS" // Exclude Stopped Loans
                                                                && (ps.Code == "PEN" || ps.Code == "PAID" || ps.Code == "STP" || ps.Code == "PC" || ps.Code == "DP")
                                                                && clp.IsDeleted == false
                                                                && cl.IsDeleted == false
                                                                select new
                                                                {
                                                                    IdLoan = cl.Id,
                                                                    PaymentId = clp.Id,
                                                                    LoanDeleted = cl.IsDeleted
                                                                }).SingleOrDefault();

                                                    if (item != null)
                                                    {
                                                        // REMOVED
                                                        // if (await ValidateLoanIsReimbursed(item.IdLoan))
                                                        //     continue;

                                                        var clientPayment = context.ClientLoanPayment.Where(x => x.Id == item.PaymentId).SingleOrDefault();

                                                        if (item.LoanDeleted == true)
                                                        {
                                                            continue;
                                                        }

                                                        // NO LONGER NEEDED
                                                        // Stop the loan
                                                        //if (errorCode == "910")
                                                        //{
                                                        //    StopLoan(item.PaymentId, language);
                                                        //}
                                                        //// Update the current Loan Payment
                                                        //else
                                                        //{
                                                        // Check to see if the Error Code can be converted to INT
                                                        int errorCodeConverted;
                                                        bool successErrorCode = Int32.TryParse(errorCode, out errorCodeConverted);

                                                        if (successErrorCode)
                                                        {
                                                            //Set the Description of the Payment that is NSF
                                                            var perceptechError = context.PerceptechErrorCode
                                                                                           .Where(x => x.Code == errorCodeConverted)
                                                                                           .SingleOrDefault();

                                                            clientPayment.IdperceptechErrorCode = perceptechError.Id;
                                                            clientPayment.Description = perceptechError.MessageFr + "/" + perceptechError.MessageEn;
                                                        }

                                                        context.ClientLoanPayment.Update(clientPayment);
                                                        context.SaveChanges();

                                                        // Call the NSF method from LoanService that already takes care of the process
                                                        // This can be moved to a shared Business Logic module
                                                        var loanService = new LoanService(_dbContextProvider, _configurationService);
                                                        loanService.LoanManualNSFPayment(new LoanGenericPaymentModel() { LoanId = (int)clientPayment.IdclientLoan, PaymentId = clientPayment.Id }, language, true);
                                                        //}

                                                        // Insert into ClientLoanCollection
                                                        var result = CreateClientLoanCollection(Convert.ToInt32(clientPayment.Id), errorCode);

                                                        // Set Client Status to Collections only if error processing payment has occurred
                                                        if (result)
                                                        {
                                                            await SendRejectionNotification(lmsEndpoint, (int)clientPayment.IdclientLoan);

                                                            ClientStatus clientStatus = null;
                                                            switch (_degree)
                                                            {
                                                                case CollectionDegree.FIRST:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION1");
                                                                    break;
                                                                case CollectionDegree.SECOND:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION2");
                                                                    break;
                                                                case CollectionDegree.THIRD:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "COLLECTION3");
                                                                    break;
                                                                case CollectionDegree.ACCOUNT_CLOSED:
                                                                    clientStatus = context.ClientStatus.SingleOrDefault(cs => cs.Code == "ACCOUNT_CLOSED");
                                                                    break;
                                                            }

                                                            var client = context.Client.SingleOrDefault(c => c.Id == Convert.ToInt32(clientId));
                                                            client.IdclientStatus = clientStatus.Id;
                                                            context.Client.Update(client);
                                                            context.SaveChanges();

                                                            var loanStatus = context.LoanStatus.SingleOrDefault(x => x.Code == "LD");
                                                            var loan = context.ClientLoan
                                                                .Where(x => x.Id == clientPayment.IdclientLoan)
                                                                .Include(x => x.IdloanStatusNavigation)
                                                                .SingleOrDefault();

                                                            if (loan.IdloanStatusNavigation.Code == "LR")
                                                            {
                                                                loan.IdloanStatus = loanStatus.Id;
                                                                context.ClientLoan.Update(loan);
                                                                context.SaveChanges();
                                                            }

                                                            // Send Email/SMS message
                                                            var clientInfo = (from c in context.Client
                                                                              join e in context.ClientEmailAddress on c.Id equals e.Idclient
                                                                              where e.IsPrimary == true && c.Id == Convert.ToInt32(clientId)
                                                                              select new
                                                                              {
                                                                                  FirstName = c.FirstName,
                                                                                  LastName = c.LastName,
                                                                                  Email = e.EmailAddress,
                                                                                  CelPhone = c.CellPhoneNumber
                                                                              }).SingleOrDefault();

                                                            // Retreive client language
                                                            var clientLanguage = context.Client
                                                                                .Where(x => x.Id == Convert.ToInt32(clientId))
                                                                                .Include(x => x.IdlanguageNavigation)
                                                                                .SingleOrDefault();
                                                            switch (_degree)
                                                            {
                                                                case CollectionDegree.FIRST:
                                                                    await _messageService.SendMessage_CollectionLevel1(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.SECOND:
                                                                    await _messageService.SendMessage_CollectionLevel2(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.THIRD:
                                                                    await _messageService.SendMessage_CollectionLevel3(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                                case CollectionDegree.ACCOUNT_CLOSED:
                                                                    await _messageService.SendMessage_CollectionAccountClosed(
                                                                        clientLanguage.IdlanguageNavigation.Code == "F" ? LMS.Domain.Core.Enums.LanguageEnum.French : LMS.Domain.Core.Enums.LanguageEnum.English,
                                                                        new EmailTemplateModel()
                                                                        {
                                                                            Collection = new LMS.Domain.Core.Models.EmailTemplates.CollectionModel
                                                                            {
                                                                                FirstName = clientInfo.FirstName,
                                                                                LastName = clientInfo.LastName,
                                                                                CellPhone = clientInfo.CelPhone,
                                                                                EmailAddress = clientInfo.Email,
                                                                                LoanPaymentAmount = $"{clientPayment.Amount}$"
                                                                            }
                                                                        }
                                                                    );
                                                                    break;
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SavePerceptechErrorReport(string errorFolder, string data)
        {
            try
            {
                // Create daily folder
                var folder = errorFolder + (DateTime.Now).ToString("yyyy-MM-dd") + "/";

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                _uploadLog.UploadFileName = "DailyRejectionErrorReport_" + (DateTime.Now).ToString("yyyy-MM-dd");
                string newFile = null;

                if (!File.Exists(folder + _uploadLog.UploadFileName + ".csv"))
                {
                    newFile = _uploadLog.UploadFileName + ".csv";
                    _uploadLog.UploadFileName = newFile;
                }
                else
                {
                    // Rename file
                    _uploadLog.UploadFileName = _uploadLog.UploadFileName + "_"
                                + DateTime.Now.Hour
                                + DateTime.Now.Minute
                                + DateTime.Now.Second
                                + DateTime.Now.Millisecond
                                + ".csv";

                    newFile = _uploadLog.UploadFileName;
                }

                using (StreamWriter outputFile = new StreamWriter(Path.Combine(folder, newFile)))
                {
                    outputFile.WriteLine(data);
                }

                return newFile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void StopLoan(int idLoanPayment, string language)
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    // var perceptechError = context.PerceptechErrorCode
                    //                        .Where(x => x.Code == 910)
                    //                        .SingleOrDefault();

                    // var stopPaymentDate = DateTime.Today;

                    // var loanId = context.ClientLoanPayment
                    //                     .Where(x => x.Id == idLoanPayment)
                    //                     .SingleOrDefault()
                    //                     .Id;

                    // var loanStatusStopped = context.LoanStatus
                    //     .Where(x => x.Code == "LS")
                    //     .SingleOrDefault();

                    // var loan = context.ClientLoan
                    //                     .Where(x => x.Id == loanId)
                    //                     .Select(x => new ClientLoanModel()
                    //                     {
                    //                         Id = x.Id,
                    //                         Status = language == "FR" ? loanStatusStopped.NameFr : loanStatusStopped.NameEn,
                    //                         IsLoanStopped = true,
                    //                         StopLoanDate = stopPaymentDate
                    //                     })
                    //                     .SingleOrDefault();

                    // var loanPayments = context.ClientLoanPayment
                    //                             .Where(x => x.IdclientLoan == loanId && x.IsDeleted == false)
                    //                             .Select(x => new ClientLoanPaymentModel()
                    //                             {
                    //                                 Id = x.Id
                    //                             })
                    //                             .OrderBy(x => x.PaymentDate)
                    //                             .ToList();

                    // // Set all payments to delete
                    // var paymentsToDelete =  loanPayments.Where(x => x.PaymentDate > stopPaymentDate)
                    //                             .ToList();

                    // // Update all loan payments to Deleted
                    // foreach(var payment in paymentsToDelete)
                    // {
                    //     var existingPayment = context.ClientLoanPayment
                    //                             .Where(x => x.Id == payment.Id)
                    //                             .SingleOrDefault();

                    //     existingPayment.IsDeleted = true;
                    //     context.SaveChanges();

                    // }

                    // // Add a payment line indicating that the loan was stopped. All values are at Zero
                    // var paymentStatusStopped = context.PaymentStatus
                    //                             .Where(x => x.Code == "SP")
                    //                             .SingleOrDefault();

                    // var stopPaymentLine = new ClientLoanPayment()
                    // {
                    //     IdclientLoan = loan.Id,
                    //     Description = perceptechError.MessageFr + "/" + perceptechError.MessageEn,
                    //     IdpaymentStatus = paymentStatusStopped.Id,
                    //     PaymentDate = stopPaymentDate,
                    //     Interest = 0,
                    //     Capital = 0,
                    //     Amount = 0,
                    //     MaxAmount = 0,
                    //     Fees = 0,
                    //     Balance = 0,
                    //     IsDeleted = false,

                    //     IsNsf = false,
                    //     IsManual = false,
                    //     IsRebate = false,
                    //     IsAmountChanged = false,
                    //     IsDateChanged = false,
                    //     IsFrequencyChanged = false,
                    //     Frequency = 0
                    // };

                    // // Save payment line indicating that the loan to Database
                    // context.ClientLoanPayment.Add(stopPaymentLine);
                    // context.SaveChanges();

                    //

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CreateClientLoanCollection(int idLoanPayment, string errorCode)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                bool isCollections = false;
                var loanId = context.ClientLoanPayment
                    .Where(x => x.Id == idLoanPayment)
                    .SingleOrDefault()
                    .IdclientLoan;

                // Check to see if they are already in Collections
                var alreadyInCollections = context.ClientLoanCollection
                                                  .Where(x => x.IdclientLoan == loanId && x.IdcollectionStatus == 0) // Rejections of the day
                                                  .FirstOrDefault();

                switch (errorCode)
                {
                    // PERTES
                    case "910":

                        if (alreadyInCollections != null)
                        {
                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                    .Where(x => x.Code == "ACCOUNT_CLOSED")
                                                                    .SingleOrDefault()
                                                                    .Id;
                            context.ClientLoanCollection.Update(alreadyInCollections);
                            context.SaveChanges();
                        }
                        else
                        {
                            var col1 = new ClientLoanCollection()
                            {
                                IdclientLoan = (int)loanId,
                                IdcollectionStatus = context.CollectionStatus
                                            .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                            .SingleOrDefault()
                                            .Id,
                                IdcollectionDegree = context.CollectionDegree
                                            .Where(x => x.Code == "ACCOUNT_CLOSED")
                                            .SingleOrDefault()
                                            .Id,
                                CreatedDateTime = DateTime.Now
                            };

                            context.ClientLoanCollection.Add(col1);
                            context.SaveChanges();
                            isCollections = true;
                        }

                        break;

                    // ACCOUNT CLOSED
                    case "905":

                        if (alreadyInCollections != null)
                        {
                            alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                    .Where(x => x.Code == "ACCOUNT_CLOSED")
                                                                    .SingleOrDefault()
                                                                    .Id;
                            context.ClientLoanCollection.Update(alreadyInCollections);
                            context.SaveChanges();
                        }
                        else
                        {
                            var col2 = new ClientLoanCollection()
                            {
                                IdclientLoan = (int)loanId,
                                IdcollectionStatus = context.CollectionStatus
                                            .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                            .SingleOrDefault()
                                            .Id,
                                IdcollectionDegree = context.CollectionDegree
                                            .Where(x => x.Code == "ACCOUNT_CLOSED")
                                            .SingleOrDefault()
                                            .Id,
                                CreatedDateTime = DateTime.Now
                            };

                            context.ClientLoanCollection.Add(col2);
                            context.SaveChanges();
                        }

                        isCollections = true;
                        _degree = CollectionDegree.ACCOUNT_CLOSED;

                        break;

                    // COLLECTION 1,2 or 3
                    default:

                        // Verify what degree of Collection
                        var loanPayments = (from clp in context.ClientLoanPayment
                                            join ps in context.PaymentStatus on clp.IdpaymentStatus equals ps.Id
                                            where (clp.IdclientLoan == loanId && clp.IsDeleted == false)
                                            select (new ClientLoanPaymentModel()
                                            {
                                                Id = clp.Id,
                                                StatusCode = ps.Code,
                                                PaymentDate = clp.PaymentDate
                                            }))
                                                .OrderBy(x => x.PaymentDate)
                                                .ToList();

                        var loanPaymentIndex = loanPayments.FindIndex(x => x.Id == idLoanPayment);
                        ClientLoanPaymentModel previousLoanPayment = null;
                        ClientLoanPaymentModel previousSecondLoanPayment = null;

                        if (loanPaymentIndex != 0)
                        {
                            if (loanPaymentIndex - 1 >= 0)
                            {
                                previousLoanPayment = loanPayments[loanPaymentIndex - 1];
                            }

                            if (loanPaymentIndex - 2 >= 0)
                            {
                                previousSecondLoanPayment = loanPayments[loanPaymentIndex - 2];
                            }
                        }

                        if (previousLoanPayment != null && previousSecondLoanPayment != null)
                        {
                            // Collection 3
                            if (previousLoanPayment.StatusCode == "NSF" && previousSecondLoanPayment.StatusCode == "NSF")
                            {
                                if (alreadyInCollections != null)
                                {
                                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                            .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
                                                                            .SingleOrDefault()
                                                                            .Id;
                                    context.ClientLoanCollection.Update(alreadyInCollections);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    context.ClientLoanCollection.Add(new ClientLoanCollection()
                                    {
                                        IdclientLoan = (int)loanId,
                                        IdcollectionStatus = context.CollectionStatus
                                                       .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                       .SingleOrDefault()
                                                       .Id,
                                        IdcollectionDegree = context.CollectionDegree
                                                       .Where(x => x.Code == "THIRD_NSF_CONSECUTIVE")
                                                       .SingleOrDefault()
                                                       .Id,
                                        CreatedDateTime = DateTime.Now
                                    });
                                    context.SaveChanges();
                                }

                                isCollections = true;

                                // Set global Collection Degree
                                _degree = CollectionDegree.THIRD;

                            }

                            // Collection 2
                            else if (previousLoanPayment.StatusCode == "NSF" && previousSecondLoanPayment.StatusCode != "NSF")
                            {
                                if (alreadyInCollections != null)
                                {
                                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                            .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
                                                                            .SingleOrDefault()
                                                                            .Id;
                                    context.ClientLoanCollection.Update(alreadyInCollections);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    context.ClientLoanCollection.Add(new ClientLoanCollection()
                                    {
                                        IdclientLoan = (int)loanId,
                                        IdcollectionStatus = context.CollectionStatus
                                                       .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                       .SingleOrDefault()
                                                       .Id,
                                        IdcollectionDegree = context.CollectionDegree
                                                       .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
                                                       .SingleOrDefault()
                                                       .Id,
                                        CreatedDateTime = DateTime.Now
                                    });
                                    context.SaveChanges();
                                }

                                isCollections = true;

                                // Set global Collection Degree
                                _degree = CollectionDegree.SECOND;
                            }

                            // Collection 1
                            else
                            {
                                if (alreadyInCollections != null)
                                {
                                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                            .Where(x => x.Code == "FIRST_NSF")
                                                                            .SingleOrDefault()
                                                                            .Id;
                                    context.ClientLoanCollection.Update(alreadyInCollections);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    context.ClientLoanCollection.Add(new ClientLoanCollection()
                                    {
                                        IdclientLoan = (int)loanId,
                                        IdcollectionStatus = context.CollectionStatus
                                                            .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                            .SingleOrDefault()
                                                            .Id,
                                        IdcollectionDegree = context.CollectionDegree
                                                            .Where(x => x.Code == "FIRST_NSF")
                                                            .SingleOrDefault()
                                                            .Id,
                                        CreatedDateTime = DateTime.Now
                                    });
                                    context.SaveChanges();
                                }

                                isCollections = true;

                                // Set global Collection Degree
                                _degree = CollectionDegree.FIRST;
                            }
                        }

                        else if (previousLoanPayment != null && previousSecondLoanPayment == null)
                        {
                            // Collection 2
                            if (previousLoanPayment.StatusCode == "NSF")
                            {
                                if (alreadyInCollections != null)
                                {
                                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                            .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
                                                                            .SingleOrDefault()
                                                                            .Id;
                                    context.ClientLoanCollection.Update(alreadyInCollections);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    context.ClientLoanCollection.Add(new ClientLoanCollection()
                                    {
                                        IdclientLoan = (int)loanId,
                                        IdcollectionStatus = context.CollectionStatus
                                                       .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                       .SingleOrDefault()
                                                       .Id,
                                        IdcollectionDegree = context.CollectionDegree
                                                       .Where(x => x.Code == "SECOND_NSF_CONSECUTIVE")
                                                       .SingleOrDefault()
                                                       .Id,
                                        CreatedDateTime = DateTime.Now
                                    });
                                    context.SaveChanges();
                                }

                                isCollections = true;

                                // Set global Collection Degree
                                _degree = CollectionDegree.SECOND;
                            }
                            // Collection 1
                            else
                            {
                                if (alreadyInCollections != null)
                                {
                                    alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                            .Where(x => x.Code == "FIRST_NSF")
                                                                            .SingleOrDefault()
                                                                            .Id;
                                    context.ClientLoanCollection.Update(alreadyInCollections);
                                    context.SaveChanges();
                                }
                                else
                                {
                                    context.ClientLoanCollection.Add(new ClientLoanCollection()
                                    {
                                        IdclientLoan = (int)loanId,
                                        IdcollectionStatus = context.CollectionStatus
                                                       .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                       .SingleOrDefault()
                                                       .Id,
                                        IdcollectionDegree = context.CollectionDegree
                                                       .Where(x => x.Code == "FIRST_NSF")
                                                       .SingleOrDefault()
                                                       .Id,
                                        CreatedDateTime = DateTime.Now
                                    });
                                    context.SaveChanges();
                                }

                                isCollections = true;

                                // Set global Collection Degree
                                _degree = CollectionDegree.FIRST;
                            }
                        }

                        // Collection 1
                        else if (previousLoanPayment == null && previousSecondLoanPayment == null)
                        {
                            if (alreadyInCollections != null)
                            {
                                alreadyInCollections.IdcollectionDegree = context.CollectionDegree
                                                                        .Where(x => x.Code == "FIRST_NSF")
                                                                        .SingleOrDefault()
                                                                        .Id;
                                context.ClientLoanCollection.Update(alreadyInCollections);
                                context.SaveChanges();
                            }
                            else
                            {
                                context.ClientLoanCollection.Add(new ClientLoanCollection()
                                {
                                    IdclientLoan = (int)loanId,
                                    IdcollectionStatus = context.CollectionStatus
                                                   .Where(x => x.Code == "REJECTIONS_OF_THE_DAY")
                                                   .SingleOrDefault()
                                                   .Id,
                                    IdcollectionDegree = context.CollectionDegree
                                                   .Where(x => x.Code == "FIRST_NSF")
                                                   .SingleOrDefault()
                                                   .Id,
                                    CreatedDateTime = DateTime.Now
                                });
                                context.SaveChanges();
                            }

                            isCollections = true;

                            // Set global Collection Degree
                            _degree = CollectionDegree.FIRST;
                        }

                        break;
                }

                return isCollections;
            }
        }

        public string GetLanguage()
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    var language = context.LoanerConfiguration
                                            .Where(x => x.Key == "DEFAULT_LANGUAGE")
                                            .SingleOrDefault();

                    return language.Value;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendRejectionNotification(string URL, int loanId)
        {
            using var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(URL);
            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(loanId), Encoding.UTF8, "application/json");
            //var request = new HttpRequestMessage(HttpMethod.Put, $"/NotifyDailyRejections/{loanId}");
            HttpResponseMessage response = await client.PutAsync($"NotifyDailyRejections/{loanId}", httpContent);
        }
        public async Task SendDailyRejectionReporErrorEmail(string apiKey)
        {
            using var context = _dbContextProvider.GetContext();
            // Send email to inform Admin
            var emails = new List<string>();

            //using var context = _dbContextProvider.GetContext();
            var companyName = context.LoanerConfiguration
                                    .Where(x => x.Key == "COMPANY_NAME")
                                    .SingleOrDefault();

            emails.Add("d.dorazio@creditbook.ca");

            // Check to see if the Error report is empty
            await EmailManager.SendDailyRejecttionError(apiKey, emails, companyName.Value);
        }
    }
}
