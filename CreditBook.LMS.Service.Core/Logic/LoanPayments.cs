﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CreditBook.LMS.Service.Core.Logic
{

    public class PaymentStaticItem
    {
        public int Index { get; set; }
        public DateTime PaymentDate { get; set; }
        public double Payment1 { get; set; }
        public double Fees { get; set; }
        public double Interest { get; set; }
        public double Principal { get; set; }
        public double Balance { get; set; }
        public string Status { get; set; }
        public bool IsNSF { get; set; }
        public bool IsManual { get; set; }
        public bool IsRebate { get; set; }
        public bool IsAmountChanged { get; set; }
        public bool IsAmountChangedForAll { get; set; }
        public double NewAmount { get; set; }
        public double ContratMaxAmount { get; set; }
        public bool IsValidated { get; set; }
        public DateTime? ValidatedDate { get; set; }
    }

    public class LoanPayments
    {
        public List<DateTime> GetPaymentDates(int frequency, DateTime startDate, int paymentCount, int? startDay, int? endDay, int frequencyDelay, bool deferredPayment)
        {
            var result = new List<DateTime>();

            if (frequency == 52)
            {
                result = GetPaymentDatesWeekly(startDate, paymentCount, frequencyDelay, deferredPayment);
            }
            else if (frequency == 26)
            {
                result = GetPaymentDatesBiWeekly(startDate, paymentCount, frequencyDelay, deferredPayment);
            }
            else if (frequency == 24)
            {
                result = GetPaymentDatesBiMonthly(startDate, paymentCount, (int)startDay, (int)endDay, frequencyDelay, deferredPayment, false);
            }
            else if (frequency == 12)
            {
                result = GetPaymentDatesMonthly(startDate, paymentCount, frequencyDelay, deferredPayment);
            }

            return result;
        }

        public DateTime GetNextPaymentDate(int frequency, DateTime startDate, int startDay, int endDay, bool deferredPayment, bool modifyLoan)
        {
            var result = new DateTime();

            if (frequency == 52)
            {
                result = GetPaymentDatesWeekly(startDate, 1, 0, deferredPayment).FirstOrDefault();
            }
            else if (frequency == 26)
            {
                result = GetPaymentDatesBiWeekly(startDate, 1, 0, deferredPayment).FirstOrDefault();
            }
            else if (frequency == 24)
            {
                result = GetPaymentDatesBiMonthly(startDate, 1, startDay, endDay, 0, deferredPayment, modifyLoan).FirstOrDefault();
            }
            else if (frequency == 12)
            {
                result = GetPaymentDatesMonthly(startDate, 1, 0, deferredPayment).FirstOrDefault();
            }

            return result;
        }

        public List<DateTime> GetPaymentDatesWeekly(DateTime startDate, int paymentCount, int frequencyDelay, bool deferredPayment)
        {
            var result = new List<DateTime>();
            var lastDate = startDate;

            if (deferredPayment)
            {
                lastDate = lastDate.AddDays(7);
                result.Add(lastDate);
                result.Remove(result.First());
            }

            // Validate if the Frequency delay is big enough
            // if ((startDate - DateTime.Today).TotalDays < frequencyDelay)
            // {
            //     lastDate = lastDate.AddDays(7);
            // }

            if (paymentCount == 1)
            {
                lastDate = lastDate.AddDays(7);
                result.Add(lastDate);
            }
            else
            {
                while (paymentCount != 0)
                {
                    result.Add(lastDate);
                    lastDate = lastDate.AddDays(7);
                    paymentCount += -1;
                }
            }

            return result;
        }

        public List<DateTime> GetPaymentDatesBiWeekly(DateTime startDate, int paymentCount, int frequencyDelay, bool deferredPayment)
        {
            var result = new List<DateTime>();
            var lastDate = startDate.Date;

            if (deferredPayment)
            {
                lastDate = lastDate.AddDays(14);
                result.Add(lastDate.Date);
                result.Remove(result.First());
            }

            // Validate if the Frequency delay is big enough
            // if ((startDate - DateTime.Today).TotalDays < frequencyDelay)
            // {
            //     lastDate = lastDate.AddDays(14);
            // }

            if (paymentCount == 1)
            {
                lastDate = lastDate.Date.AddDays(14);
                result.Add(lastDate.Date);
            }
            else
            {
                while (paymentCount != 0)
                {
                    result.Add(lastDate.Date);
                    lastDate = lastDate.Date.AddDays(14);
                    paymentCount += -1;
                }
            }

            return result;
        }

        public List<DateTime> GetPaymentDatesBiMonthly(DateTime startDate, int paymentCount, int startDay1, int endDay1, int frequencyDelay, bool deferredPayment, bool modifyLoan)
        {
            var result = new List<DateTime>();
            var _startDay = startDay1;
            var _endDay = endDay1;
            var _startDate = startDate;

            // Validate StartDay is larger than EndDay
            if (startDay1 > endDay1)
            {
                _endDay = startDay1;
                _startDay = endDay1;
            }

            var _initialPaymentCOunt = paymentCount;
            while (paymentCount >= 0)
            {
                int currentDay = result.Count() == 0 ? _startDay : result.Last().Day;
                var addMonth = false;

                if (result.Count() == 0)
                {
                    if (deferredPayment)
                    {
                        if (_startDate.Day < _startDay)
                        {
                            currentDay = _startDay;
                        }
                        else if (_startDate.Day >= _startDay && _startDate.Day < _endDay)
                        {
                            currentDay = _endDay;
                        }
                        else
                        {
                            currentDay = _startDay;
                            addMonth = true;
                        }
                    }
                    else
                    {
                        if (_startDate.Day <= _startDay)
                        {
                            currentDay = _startDay;
                        }
                        else if (_startDate.Day > _startDay && _startDate.Day <= _endDay)
                        {
                            currentDay = _endDay;
                        }
                        else
                        {
                            currentDay = _startDay;
                            addMonth = true;
                        }
                    }
                }
                else
                {
                    if (currentDay <= _startDay)
                    {
                        currentDay = _endDay;
                    }
                    else
                    {
                        currentDay = _startDay;
                        addMonth = true;
                    }
                }

                DateTime nextDate = new DateTime();

                if (result.Count() != 0)
                {
                    nextDate = result.Last();
                }
                else
                {
                    switch (startDate.Month)
                    {
                        case 1:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                        case 2:
                            if (currentDay > 28) { nextDate = new DateTime(startDate.Year, startDate.Month, 28); } else { nextDate = new DateTime(startDate.Year, startDate.Month, currentDay); };
                            break;
                        case 3:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                        case 4:
                            if (currentDay > 30) { nextDate = new DateTime(startDate.Year, startDate.Month, 30); } else { nextDate = new DateTime(startDate.Year, startDate.Month, currentDay); };
                            break;
                        case 5:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            //result.Add(new DateTime(startDate.Year, startDate.Month, currentDay));
                            break;
                        case 6:
                            if (currentDay > 30) { nextDate = new DateTime(startDate.Year, startDate.Month, 30); } else { nextDate = new DateTime(startDate.Year, startDate.Month, currentDay); };
                            break;
                        case 7:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                        case 8:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                        case 9:
                            if (currentDay > 30) { nextDate = new DateTime(startDate.Year, startDate.Month, 30); } else { nextDate = new DateTime(startDate.Year, startDate.Month, currentDay); };
                            break;
                        case 10:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                        case 11:
                            if (currentDay > 30) { nextDate = new DateTime(startDate.Year, startDate.Month, 30); } else { nextDate = new DateTime(startDate.Year, startDate.Month, currentDay); };
                            break;
                        case 12:
                            nextDate = new DateTime(startDate.Year, startDate.Month, currentDay);
                            break;
                    }
                }

                if (addMonth)
                {
                    nextDate = result.Count() == 0 ? startDate.AddMonths(1) : result.Last().AddMonths(1);
                    //nextDate = result.Last().AddMonths(1);
                    addMonth = false;
                }

                switch (nextDate.Month)
                {
                    case 1:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 2:
                        if (currentDay > 28) { result.Add(new DateTime(nextDate.Year, nextDate.Month, 28)); } else { result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay)); };
                        break;
                    case 3:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 4:
                        if (currentDay > 30) { result.Add(new DateTime(nextDate.Year, nextDate.Month, 30)); } else { result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay)); };
                        break;
                    case 5:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 6:
                        if (currentDay > 30) { result.Add(new DateTime(nextDate.Year, nextDate.Month, 30)); } else { result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay)); };
                        break;
                    case 7:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 8:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 9:
                        if (currentDay > 30) { result.Add(new DateTime(nextDate.Year, nextDate.Month, 30)); } else { result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay)); };
                        break;
                    case 10:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                    case 11:
                        if (currentDay > 30) { result.Add(new DateTime(nextDate.Year, nextDate.Month, 30)); } else { result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay)); };
                        break;
                    case 12:
                        result.Add(new DateTime(nextDate.Year, nextDate.Month, currentDay));
                        break;
                }

                paymentCount += -1;
            }

            // if (deferredPayment)
            // {
            //     result.Remove(result.First());
            //     result.Remove(result.Last());
            // }

            if (modifyLoan)
            {
                result.Remove(result.First());
            }

            return result;
        }

        public List<DateTime> GetPaymentDatesMonthly(DateTime startDate, int paymentCount, int frequencyDelay, bool deferredPayment)
        {
            var result = new List<DateTime>();
            var payDateOriginal = startDate;
            var lastDate = startDate;

            if (deferredPayment)
            {
                lastDate = GetNextMonthlyDay(lastDate, payDateOriginal, 1);
                payDateOriginal = lastDate;

                if (IsWeekEnd(lastDate))
                {
                    lastDate = GetNextBusinessDay(lastDate);
                }
                result.Add(lastDate);
                result.Remove(result.First());
            }

            // Validate if the Frequency delay is big enough
            // if ((startDate - DateTime.Today).TotalDays < frequencyDelay)
            // {
            //     lastDate = GetNextMonthlyDay(lastDate, payDateOriginal, 1);

            //     if (IsWeekEnd(lastDate))
            //     {
            //         lastDate = GetNextBusinessDay(lastDate);
            //     }
            //     result.Add(lastDate);
            // }

            var index = 1;

            if (paymentCount == 1)
            {
                lastDate = GetNextMonthlyDay(lastDate, payDateOriginal, index);

                if (IsWeekEnd(lastDate))
                {
                    lastDate = GetNextBusinessDay(lastDate);
                }
                result.Add(lastDate);
            }
            else
            {
                while (paymentCount != 0)
                {
                    result.Add(lastDate);

                    lastDate = GetNextMonthlyDay(lastDate, payDateOriginal, index);

                    if (IsWeekEnd(lastDate))
                    {
                        lastDate = GetNextBusinessDay(lastDate);
                    }

                    //result.Add(lastDate);
                    paymentCount += -1;
                    index += 1;
                }
            }

            return result;
        }

        public static DateTime GetNextMonthlyDay(DateTime lastDate, DateTime originalDate, int index)
        {
            var nextDateMonth = originalDate.AddMonths(index).Month;
            var nextDateYear = originalDate.AddMonths(index).Year;

            if (originalDate.Day > DateTime.DaysInMonth(lastDate.Year, nextDateMonth)) //Check if date is possible
            {
                lastDate = new DateTime(nextDateYear, nextDateMonth, DateTime.DaysInMonth(nextDateYear, nextDateMonth)); //Keep original pay day
            }
            else
            {
                lastDate = new DateTime(nextDateYear, nextDateMonth, originalDate.Day); //Keep original pay day
            }
            return lastDate;
        }

        public static readonly HashSet<DateTime> Holidays = new HashSet<DateTime>();
        public static bool IsHoliday(DateTime date)
        {
            return Holidays.Contains(date);
        }

        public static bool IsWeekEnd(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday
                || date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static DateTime GetNextBusinessDay(DateTime date)
        {
            do
            {
                date = date.AddDays(1);
            } while (IsHoliday(date) || IsWeekEnd(date));
            return date;
        }
        public static DateTime GetPreviousBusinessDay(DateTime date)
        {
            do
            {
                date = date.AddDays(-1);
            } while (IsHoliday(date) || IsWeekEnd(date));
            return date;
        }

        // MISSING REFERENCE
        //public static List<PaymentStaticItem> ConvertDbPaymentsToStaticItems(List<Payment> payments)
        //{
        //    var staticPayments = new List<PaymentStaticItem>();
        //    DateTime? nullDate = null;
        //    foreach (var pmt in payments)
        //    {
        //        var staticPmt = new PaymentStaticItem()
        //        {
        //            Index = pmt.Index.GetValueOrDefault(),
        //            PaymentDate = pmt.PaymentDate.GetValueOrDefault(),
        //            Payment1 = Convert.ToDouble(pmt.Payment1.GetValueOrDefault()),
        //            Fees = Convert.ToDouble(pmt.Fees.GetValueOrDefault()),
        //            Interest = Convert.ToDouble(pmt.Interest.GetValueOrDefault()),
        //            Principal = Convert.ToDouble(pmt.Principal.GetValueOrDefault()),
        //            Balance = Convert.ToDouble(pmt.Balance.GetValueOrDefault()),
        //            Status = pmt.Status,
        //            IsNSF = pmt.IsNSF.GetValueOrDefault(),
        //            IsManual = pmt.IsManual.GetValueOrDefault(),
        //            IsRebate = pmt.IsRebate.GetValueOrDefault(),
        //            IsAmountChanged = pmt.IsAmountChanged.GetValueOrDefault(),
        //            NewAmount = Convert.ToDouble(pmt.NewAmount.GetValueOrDefault()),
        //            IsValidated = pmt.IsValidated.GetValueOrDefault(),
        //            ValidatedDate = (pmt.ValidatedDate != DateTime.MinValue) ? pmt.ValidatedDate.GetValueOrDefault() : nullDate,
        //            ContratMaxAmount = Convert.ToDouble(pmt.ContractMaxAmount.GetValueOrDefault())
        //        };
        //        staticPayments.Add(staticPmt);
        //    }

        //    return staticPayments;
        //}
    }

    //public class PaymentListItem
    //{
    //    public string NoClient { get; set; }
    //    public int PaymentId { get; set; }
    //    public string Company { get; set; }
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public DateTime? PaymentDate { get; set; }
    //    public double Amount { get; set; }
    //    public double Balance { get; set; }
    //    public int LoanNo { get; set; }

    //}
}
