using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface IClientCommentService
    {
        Task<IEnumerable<ClientCommentModel>> GetClientComments(int clientId);
        Task<ClientCommentModel> CreateClientComment(ClientCommentModel data);
        Task<ClientCommentModel> UpdateClientComment(ClientCommentModel data);
        Task DeleteClientComment(int clientCommentId, string deletedBy);
        Task<IEnumerable<CommentLabelModel>> GetCommentLabels();
        Task<CommentLabelModel> AddLabelToClientComment(int clientCommentId, int commentLabelId);
        Task RemoveLabelFromClientComment(int clientCommentId, int commentLabelId);
    }
}