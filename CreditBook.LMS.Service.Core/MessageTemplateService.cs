using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class MessageTemplateService : IMessageTemplateService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public MessageTemplateService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<EmailConfigurationModel> GetEmailConfiguration()
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.EmailConfiguration.FirstOrDefaultAsync();
            return new EmailConfigurationModel
            {
                HeaderContentEn = entity?.HeaderContentEn,
                HeaderContentFr = entity?.HeaderContentFr,
                FooterContentEn = entity?.FooterContentEn,
                FooterContentFr = entity?.FooterContentFr,
                LogoUrl = entity?.LogoUrl
            };
        }

        public async Task<MessageTemplateModel> GetMessageTemplate(int messageTemplateId)
        {
            using var context = _dbContextProvider.GetContext();
            var result = await context.MessageTemplate.FindAsync(messageTemplateId);
            return new MessageTemplateModel
            {
                Id = result.Id,
                Title = result.Title,
                Description = result.Description,
                SubjectEn = result.SubjectEn,
                SubjectFr = result.SubjectFr,
                EmailContentEn = result.EmailContentEn,
                EmailContentFr = result.EmailContentFr,
                SmsContentEn = result.SmsContentEn,
                SmsContentFr = result.SmsContentFr
            };
        }

        public async Task<IEnumerable<MessageTemplateModel>> GetMessageTemplateList()
        {
            using var context = _dbContextProvider.GetContext();
            return await context.MessageTemplate.Select(mt => new MessageTemplateModel
            {
                Id = mt.Id,
                Title = mt.Title,
                Description = mt.Description
            }).ToListAsync();
        }

        public async Task SaveEmailConfiguration(EmailConfigurationModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.EmailConfiguration.FirstOrDefaultAsync();
            // If there isn't a saved configuration create a new one
            if (entity == null)
            {
                entity = new EmailConfiguration();
            }
            entity.HeaderContentEn = data.HeaderContentEn;
            entity.HeaderContentFr = data.HeaderContentFr;
            entity.FooterContentEn = data.FooterContentEn;
            entity.FooterContentFr = data.FooterContentFr;
            entity.LogoUrl = data.LogoUrl;
            context.Update(entity);
            await context.SaveChangesAsync();
            return;
        }

        public async Task<MessageTemplateModel> UpdateMessageTemplate(MessageTemplateModel model)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.MessageTemplate.FindAsync(model.Id);

            entity.Title = model.Title;
            entity.Description = model.Description;
            entity.SubjectEn = model.SubjectEn;
            entity.SubjectFr = model.SubjectFr;
            entity.EmailContentEn = model.EmailContentEn;
            entity.EmailContentFr = model.EmailContentFr;
            entity.SmsContentEn = model.SmsContentEn;
            entity.SmsContentFr = model.SmsContentFr;
            
            context.MessageTemplate.Update(entity);
            await context.SaveChangesAsync();
            return model;
        }
    }
}