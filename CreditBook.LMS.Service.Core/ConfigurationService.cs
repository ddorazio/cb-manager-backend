using System;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public ConfigurationService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        #region Loan Fees

        public decimal DeferredFees()
        {
            using var context = _dbContextProvider.GetContext();
            var deferFee = context.LoanChargeConfiguration
                                .Where(x => x.IdloanChargeTypeNavigation.Code == "delayed" && x.EffectiveDate <= DateTime.Now)
                                .OrderByDescending(x => x.EffectiveDate)
                                .FirstOrDefault();

            if (deferFee == null)
                throw new PerceptechHandledException("Lender is not configured for Deferred Fees");

            return deferFee.AmountCharged;
        }

        public decimal NsfFees()
        {
            using var context = _dbContextProvider.GetContext();
            var nsfFee = context.LoanChargeConfiguration
                                .Where(x => x.IdloanChargeTypeNavigation.Code == "nsf" && x.EffectiveDate <= DateTime.Now)
                                .OrderByDescending(x => x.EffectiveDate)
                                .FirstOrDefault();

            if (nsfFee == null)
                throw new PerceptechHandledException("Lender is not configured for NSF Fees");

            return nsfFee.AmountCharged;
        }

        public decimal WithdrawalFees()
        {
            using var context = _dbContextProvider.GetContext();
            var withdrawalFee = context.LoanChargeConfiguration
                                .Where(x => x.IdloanChargeTypeNavigation.Code == "auto_withdrawal_amount" && x.EffectiveDate <= DateTime.Now)
                                .OrderByDescending(x => x.EffectiveDate)
                                .FirstOrDefault();

            if (withdrawalFee == null)
                throw new PerceptechHandledException("Lender is not configured for Withdrawal Fees");

            return withdrawalFee.AmountCharged;
        }

        #endregion

        #region  Loan Feature Configuration

        public async Task<LenderLoanConfigurationModel> LoadLenderLoanConfiguration()
        {
            return new LenderLoanConfigurationModel()
            {
                LoanStoppedPaymentAllowed = await LoanStoppedPaymentAllowed(),
                PaymentAgreementAllowed = await PaymentAgreementAllowed()
            };
        }

        private async Task<bool> LoanStoppedPaymentAllowed()
        {
            bool _loanStoppedPayment;
            using var context = _dbContextProvider.GetContext();
            var loanStoppedPayment = await context.FeatureConfiguration
                                            .Where(x => x.Key == "STOPPED_LOAN_PAYMENT")
                                            .SingleOrDefaultAsync();

            if (!Boolean.TryParse(loanStoppedPayment.Value, out _loanStoppedPayment))
                throw new PerceptechHandledException("Lender Stopped Payment Configuration setting is not valid");

            return _loanStoppedPayment;
        }


        private async Task<bool> PaymentAgreementAllowed()
        {
            bool _paymentAgreement;
            using var context = _dbContextProvider.GetContext();
            var paymentAgreement = await context.FeatureConfiguration
                                            .Where(x => x.Key == "PAYMENT_AGREEMENT")
                                            .SingleOrDefaultAsync();

            if (!Boolean.TryParse(paymentAgreement.Value, out _paymentAgreement))
                throw new PerceptechHandledException("Payment Agreement Configuration setting is not valid");

            return _paymentAgreement;
        }

        #endregion

        #region Lender Configuration
        public bool ValidateMixedLoans()
        {
            bool _mixedLoans;

            // Validate if Lender has Automatic Payments and Manual Payments
            using var context = _dbContextProvider.GetContext();
            var loanerConfiguration = context.LoanerConfiguration.ToList();
            var mixedLoans = loanerConfiguration.Where(x => x.Key == "MIXED_LOANS").SingleOrDefault();

            if (mixedLoans == null)
                throw new PerceptechHandledException("Lender is not configured for Mixed Loans");

            if (!Boolean.TryParse(mixedLoans.Value, out _mixedLoans))
                throw new PerceptechHandledException("Lender Mixed Loans setting is not valid");

            return _mixedLoans;
        }

        public bool ValidatePaymentProvider()
        {
            bool _paymentProvider;

            // Validate if Lender is setup for Automatic Payments
            using var context = _dbContextProvider.GetContext();
            var loanerConfiguration = context.LoanerConfiguration.ToList();
            var paymentProvider = loanerConfiguration.Where(x => x.Key == "PAYMENT_PROVIDER").SingleOrDefault();

            if (paymentProvider == null)
                throw new PerceptechHandledException("Lender is not configured for Payment Provider");

            if (!Boolean.TryParse(paymentProvider.Value, out _paymentProvider))
                throw new PerceptechHandledException("Lender Payment Provider setting is not valid");

            return _paymentProvider;
        }

        public DateTime ValidateMixedLoanDate()
        {
            DateTime _mixedLoanDate;

            // Validate if Lender is setup for Automatic Payments
            using var context = _dbContextProvider.GetContext();
            var loanerConfiguration = context.LoanerConfiguration.ToList();
            var mixedLoanDate = loanerConfiguration.Where(x => x.Key == "MIXED_LOAN_DATE").SingleOrDefault();

            if (mixedLoanDate == null)
                throw new PerceptechHandledException("Lender does not have a Mixed Loan Date");

            if (!DateTime.TryParse(mixedLoanDate.Value, out _mixedLoanDate))
                throw new PerceptechHandledException("Lender Mixed Loan Date setting is not valid");

            return _mixedLoanDate;
        }

        public bool ValidateMembershipFee()
        {
            bool _memberShipFee;

            // Validate if Lender has Automatic Payments and Manual Payments
            using var context = _dbContextProvider.GetContext();
            var loanerConfiguration = context.LoanerConfiguration.ToList();
            var memberShipFee = loanerConfiguration.Where(x => x.Key == "CHARGE_MEMBERSHIP_FEE").SingleOrDefault();

            if (memberShipFee == null)
                throw new PerceptechHandledException("Lender is not configured for Membership Fees");

            if (!Boolean.TryParse(memberShipFee.Value, out _memberShipFee))
                throw new PerceptechHandledException("Lender Membership Fees setting is not valid");

            return _memberShipFee;
        }

        #endregion

        #region 24HR Configuration

        public bool ValidateWebhooksStatus_24HR()
        {
            bool _webhookActive;

            using var context = _dbContextProvider.GetContext();
            var webhookConfiguration = context.WebhookConfiguration.ToList();
            var webhookActive = webhookConfiguration.Where(x => x.Key == "24HR_WEBHOOKS").SingleOrDefault();

            if (webhookActive == null)
                throw new PerceptechHandledException("Lender is not configured for Webhooks");

            if (!Boolean.TryParse(webhookActive.Value, out _webhookActive))
                throw new PerceptechHandledException("Lender Webhook Status setting is not valid");

            return _webhookActive;
        }

        public bool ValidateLenderIs_24HR()
        {
            using var context = _dbContextProvider.GetContext();
            var loanerConfiguration = context.LoanerConfiguration.ToList();
            var lenderName = loanerConfiguration.Where(x => x.Key == "COMPANY_NAME").SingleOrDefault();

            if (lenderName.Value.ToUpper() == "24HR CASH")
                return true;

            return false;
        }

        #endregion
    }
}