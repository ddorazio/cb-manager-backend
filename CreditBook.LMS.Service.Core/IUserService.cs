﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace CreditBook.LMS.Service.Core
{
    public interface IUserService
    {
        ClaimsPrincipal GetUser();
        string GetUserName();
    }
}
