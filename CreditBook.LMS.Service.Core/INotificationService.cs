﻿using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core
{
    public interface INotificationService
    {
        Task NewNotification(NotificationModel model);
        Task<List<NotificationModel>> GetNotifications(string type, string language);
        Task SetNotificationRead(int id);
        Task<NotificationModel> CreateNotificationIBVModel(string requestId);
        Task<NotificationModel> CreateNotificationRejectModel(LoanGenericPaymentModel model);
        Task<NotificationModel> CreateNotificationContractSignedModel(Guid documentId);
    }
}
