using System;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class WebhookService_24HR : IWebhookService_24HR
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IAPIHubClient_24HR _apiHubClient;
        private readonly IConfigurationService _configurationService;

        public WebhookService_24HR(IDbContextProvider dbContextProvider, IAPIHubClient_24HR apiHubClient, IConfigurationService configurationService)
        {
            _dbContextProvider = dbContextProvider;
            _apiHubClient = apiHubClient;
            _configurationService = configurationService;
        }

        public async Task NewClientAdded_24HR(ExternalLoanWebRequest model)
        {
            if (_configurationService.ValidateWebhooksStatus_24HR())
            {
                using var context = _dbContextProvider.GetContext();
                var webhookConfiguration = context.WebhookConfiguration.ToList();
                //var webHookEndpoint = webhookConfiguration.Where(x => x.Key == "24HR_WEBHOOK_ENDPOINT").SingleOrDefault();

                var incomeType = await context.IncomeType
                                        .Where(x => x.Id == model.IdincomeType)
                                        .SingleOrDefaultAsync();

                var payFrequency = await context.PayFrequency
                                    .Where(x => x.Id == model.IdpayFrequency)
                                    .SingleOrDefaultAsync();

                var language = await context.Language
                                .Where(x => x.Id == model.Idlanguage)
                                .SingleOrDefaultAsync();

                var requestStatus = await context.LoanRequestStatus
                                            .Where(x => x.Id == model.StatusId)
                                            .SingleOrDefaultAsync();

                var body = new
                {
                    ID = model.Idclient,
                    FistName = model.FirstName,
                    LastName = model.LastName,
                    Phone = "",
                    Cellphone = model.CellPhoneNumber,
                    Email = model.PrimaryEmail,
                    Language = language.NameEn,
                    DateOfBirth = model.Birthday,
                    Address = model.CivicNumber + " " + model.StreetName,
                    City = model.City,
                    Province = model.Province,
                    PostalCode = model.PostalCode,
                    SourceOfIncome = incomeType.Code,
                    DateOfNextPay = model.NextPayDate,
                    NameOfEmployer = model.EmployerName,
                    SupervisorName = model.SupervisorName,
                    JobPhone = model.EmployerPhoneNumber,
                    JobExt = model.EmployerExtension,
                    HrPhone = "",
                    HrExt = "",
                    PositionTitle = model.Occupation,
                    HiringDate = model.HiringDate,
                    PayFrequency = payFrequency.NameEn,
                    Reference1 = new
                    {
                        Firstname = model.Ref1FirstName,
                        Lastname = model.Ref1LastName,
                        Phone = model.Ref1Telephone,
                        RelationshipLink = model.Ref1Relationship,
                        Email = model.Ref1Email
                    },
                    Reference2 = new
                    {
                        Firstname = model.Ref2FirstName,
                        Lastname = model.Ref2LastName,
                        Phone = model.Ref2Telephone,
                        RelationshipLink = model.Ref2Relationship,
                        Email = model.Ref2Email
                    },
                    IBV = new
                    {
                        No = model.IbvrequestId,
                        Status = "Request sent",
                        ReportLink = string.IsNullOrEmpty(model.IbvrequestId) ? "" : "https://production.creditbookibv.ca/#!/Report/" + model.IbvrequestId
                    },
                    RequestedLoanAmount = model.Amount,
                    ApprovedLoanAmount = "",
                    RequestStatus = requestStatus.NameEn
                };

                await _apiHubClient.PostAsync(null, body);
            }
        }

        public async Task IBVStatusChange_24HR(ClientIbv model)
        {
            if (_configurationService.ValidateWebhooksStatus_24HR())
            {
                using var context = _dbContextProvider.GetContext();
                var webhookConfiguration = context.WebhookConfiguration.ToList();
                //var webHookEndpoint = webhookConfiguration.Where(x => x.Key == "24HR_WEBHOOK_ENDPOINT").SingleOrDefault();

                var client = await context.Client
                                    .Where(x => x.Id == model.Idclient)
                                    .SingleOrDefaultAsync();

                var clientEmail = await context.ClientEmailAddress
                                    .Where(x => x.Idclient == model.Idclient && x.IsPrimary == true)
                                    .SingleOrDefaultAsync();

                var ibvStatus = await context.ClientIbvStatus
                                                .Where(x => x.Id == model.IdclientIbvstatus)
                                                .SingleOrDefaultAsync();

                var body = new
                {
                    ID = model.Id,
                    FirstName = client.FirstName,
                    LastName = client.LastName,
                    Phone = client.HomePhoneNumber,
                    Cellphone = client.CellPhoneNumber,
                    Email = clientEmail.EmailAddress,
                    IBVStatus = ibvStatus.NameEn
                };

                await _apiHubClient.PostAsync(null, body);
            }
        }

        public async Task RequestStatusChange_24HR(LoanRequestModel model)
        {
            if (_configurationService.ValidateWebhooksStatus_24HR())
            {
                using var context = _dbContextProvider.GetContext();
                var webhookConfiguration = context.WebhookConfiguration.ToList();
                //var webHookEndpoint = webhookConfiguration.Where(x => x.Key == "24HR_WEBHOOK_ENDPOINT").SingleOrDefault();

                var request = await context.ExternalLoanWebRequest
                                   .Include(x => x.IdlanguageNavigation)
                                   .Where(x => x.Id == model.Id)
                                   .SingleOrDefaultAsync();

                var client = await context.Client
                                    .Where(x => x.Id == request.Idclient)
                                    .SingleOrDefaultAsync();

                var clientEmail = await context.ClientEmailAddress
                                    .Where(x => x.Idclient == request.Idclient && x.IsPrimary == true)
                                    .SingleOrDefaultAsync();

                var requestStatus = await context.LoanRequestStatus
                                            .Where(x => x.Code == model.StatusCode)
                                            .SingleOrDefaultAsync();

                var body = new
                {
                    ID = request.Id,
                    FirstName = client.FirstName,
                    LastName = client.LastName,
                    Phone = client.HomePhoneNumber,
                    Cellphone = client.CellPhoneNumber,
                    Email = clientEmail.EmailAddress,
                    RequestStatus = requestStatus.NameEn
                };

                await _apiHubClient.PostAsync(null, body);
            }
        }
    }
}