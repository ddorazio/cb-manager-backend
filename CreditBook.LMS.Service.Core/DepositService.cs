using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class DepositService : IDepositService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public DepositService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<IEnumerable<DepositModel>> GetDeposits()
        {
            using var context = _dbContextProvider.GetContext();
            var lastPaymentDate = context.PerceptechSubmitLog.Select(x => x).ToList().LastOrDefault();
            var query = await (from dep in context.Deposit
                               where dep.IsDeleted != true
                               join status in context.DepositStatus on dep.StatusId equals status.Id
                               join loan in context.ClientLoan on dep.ClientLoanId equals loan.Id
                               join client in context.Client on loan.Idclient equals client.Id
                               //join bankInfo in context.ClientBankDetails on client.Id equals bankInfo.Idclient
                               join bankInfo in context.ClientBankDetails on client.Id equals bankInfo.Idclient into bi_lj
                               from bankInfo_lj in bi_lj.DefaultIfEmpty()
                               select new DepositModel
                               {
                                   Id = dep.Id,
                                   LoanId = loan.Id,
                                   ClientId = client.Id,
                                   FirstName = client.FirstName,
                                   LastName = client.LastName,
                                   BankAccountNumber = bankInfo_lj.AccountNumber,
                                   LoanAmount = loan.Amount,
                                   StatusCode = status.Code,
                                   TypeCode = dep.Type != null ? dep.Type.Code : null
                               }).OrderByDescending(x => x.Id)
                         .ToListAsync();

            foreach (var deposit in query)
            {
                deposit.DepositPermitted = ValidateDepositAllowed(deposit.LoanId, lastPaymentDate == null ? null : lastPaymentDate.PaymentDate);
            }

            return query;
        }

        public bool ValidateDepositAllowed(int loanId, DateTime? lastPaymentDate)
        {
            using var context = _dbContextProvider.GetContext();
            //var loanPayments = context.ClientLoanPayment.Where(x => x.IdclientLoan == loanId).ToList();

            if (lastPaymentDate == null)
                return true;

            var submittedPayments = context.ClientLoanPayment.Where(x => x.IdclientLoan == loanId && x.PaymentDate <= lastPaymentDate).ToList();
            if (submittedPayments.Count > 0)
                return false;

            return true;
        }

        public async Task<DepositModel> UpdateDeposit(DepositModel model)
        {
            using var context = _dbContextProvider.GetContext();
            using var contextTransaction = context.Database.BeginTransaction();

            var entity = await context.Deposit.FindAsync(model.Id);
            var status = await context.DepositStatus.SingleOrDefaultAsync(ds => ds.Code == model.StatusCode);
            if (status == null) throw new ArgumentException("Deposit status code is missing or invalid.");
            var type = await context.DepositType.SingleOrDefaultAsync(dt => dt.Code == model.TypeCode);
            if (type == null) throw new ArgumentException("Deposit type code is missing or invalid.");

            entity.StatusId = status.Id;
            entity.TypeId = type.Id;
            entity.InteracConfirmationCode = model.InteracConfirmationCode;
            context.Deposit.Update(entity);
            await context.SaveChangesAsync();

            // When Deposit is complete, automatically start Loan so that payments can be processed
            var inProgressStatus = await context.LoanStatus
                                    .SingleOrDefaultAsync(ls => ls.Code == "IP");
            var loan = await context.ClientLoan
                                    .SingleOrDefaultAsync(cl => cl.Id == entity.ClientLoanId);
            loan.IdloanStatus = inProgressStatus.Id;
            context.Update(loan);
            await context.SaveChangesAsync();

            // Set Client Status to In Progress only if not already in collections
            var client = await context.Client.SingleOrDefaultAsync(c => c.Id == model.ClientId);
            var currentClientStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Id == client.IdclientStatus);

            if (currentClientStatus == null)
            {
                var newStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Code == "IN_PROGRESS");
                client.IdclientStatus = newStatus.Id;
                context.Update(client);
                await context.SaveChangesAsync();
            }
            else if (currentClientStatus.Code != "COLLECTIONS")
            {
                var newStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Code == "IN_PROGRESS");
                client.IdclientStatus = newStatus.Id;
                context.Update(client);
                await context.SaveChangesAsync();
            }

            await contextTransaction.CommitAsync();
            return model;
        }
    }
}