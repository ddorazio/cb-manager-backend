using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Service.Domain.Enums;

namespace CreditBook.LMS.Service.Core
{
    public interface IClientHistoryService
    {
        Task<IEnumerable<ClientHistoryModel>> GetClientHistory(int clientId);
        Task<ClientHistoryModel> AddClientHistoryEntry(int clientId, ClientHistoryTypeEnum clientHistoryType);
    }
}