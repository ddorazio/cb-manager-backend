﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core;
using CreditBook.LMS.Domain.Core.Models.CreditBookCheck;
using CreditBook.LMS.External.Core;

namespace CreditBook.LMS.Service.Core
{
    public class ClientService : IClientService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IConfigurationService _configurationService;
        private readonly IWebhookService_24HR _webhookService24HR;

        public ClientService(IDbContextProvider dbContextFactory, IConfigurationService configurationService, IWebhookService_24HR webhookService24HR)
        {
            _dbContextProvider = dbContextFactory;
            _configurationService = configurationService;
            _webhookService24HR = webhookService24HR;
        }

        public async Task<ClientModel> GetClientInfo(int clientId)
        {
            using var context = _dbContextProvider.GetContext();
            return await (from client in context.Client
                          where client.Id == clientId
                          select new ClientModel
                          {
                              BasicInfo = new ClientBasicInfoModel
                              {
                                  PersonalInfo = new ClientPersonalInfoModel
                                  {
                                      LanguageCode = context.Language.Where(x => x.Id == client.Idlanguage).SingleOrDefault().Code
                                  }
                              }
                          }).SingleOrDefaultAsync();
        }

        public ClientModel NewClientData(string language)
        {
            try
            {
                using (var context = _dbContextProvider.GetContext())
                {
                    var model = new ClientModel();

                    // Client reference tables
                    var provinces = context.Province
                                            .Select(x => new ClientProvinceModel()
                                            {
                                                Id = x.Id,
                                                Name = language == "FR" ? x.NameFr : x.NameEn
                                            })
                                            .ToList();

                    model.Provinces = provinces;

                    var languages = context.Language
                        .Select(x => new ClientLanguageModel
                        {
                            Id = x.Id,
                            Name = language == "FR" ? x.NameFr : x.NameEn
                        })
                        .ToList();

                    model.Languages = languages;

                    var genders = context.Gender
                        .Select(x => new ClientGenderModel()
                        {
                            Id = x.Id,
                            Name = language == "FR" ? x.NameFr : x.NameEn
                        })
                        .ToList();

                    model.Genders = genders;

                    var payFrequencies = context.PayFrequency
                        .Select(x => new ClientPayFrequencyModel()
                        {
                            Id = x.Id,
                            Name = language == "FR" ? x.NameFr : x.NameEn
                        })
                        .ToList();

                    model.PayFrequencies = payFrequencies;


                    return model;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ClientModel> GetClient(int clientId, string language, string digitalBaseUrl)
        {
            using var context = _dbContextProvider.GetContext();
            var clientModel = await (from client in context.Client
                                     join address in context.ClientAddress
                                      on new { c1 = client.Id, c2 = true } equals new { c1 = (int)address.Idclient, c2 = (bool)address.IsPrimary }
                                     join email in context.ClientEmailAddress
                                      on new { c1 = client.Id, c2 = true } equals new { c1 = (int)email.Idclient, c2 = (bool)email.IsPrimary }
                                     join employment in context.ClientEmploymentStatus on client.Id equals employment.Idclient
                                     join bankInfo in context.ClientBankDetails on client.Id equals bankInfo.Idclient into bi_lj
                                     from bankInfo_lj in bi_lj.DefaultIfEmpty()
                                     join social in context.ClientSocial on client.Id equals social.Idclient into s_lj
                                     from social_lj in s_lj.DefaultIfEmpty()
                                     where client.Id == clientId
                                     select new ClientModel
                                     {
                                         Id = client.Id,
                                         PotentialMatch = client.PotentialMatch,
                                         BasicInfo = new ClientBasicInfoModel
                                         {
                                             PersonalInfo = new ClientPersonalInfoModel
                                             {
                                                 FirstName = client.FirstName,
                                                 LastName = client.LastName,
                                                 HomePhoneNumber = client.HomePhoneNumber,
                                                 CellPhoneNumber = client.CellPhoneNumber,
                                                 FaxNumber = client.FaxNumber,
                                                 EmailAddress = email.EmailAddress,
                                                 GenderId = client.Idgender,
                                                 StatusId = client.IdclientStatus,
                                                 LanguageId = client.Idlanguage,
                                                 LanguageCode = context.Language.Where(x => x.Id == client.Idlanguage).SingleOrDefault().Code,
                                                 SocialInsuranceNumber = client.SocialInsuranceNumber,
                                                 DateOfBirth = client.DateOfBirth
                                             },
                                             Address = new ClientAddressModel
                                             {
                                                 CivicNumber = address.CivicNumber,
                                                 StreetName = address.StreetName,
                                                 Apartment = address.Apartment,
                                                 PostalCode = address.PostalCode,
                                                 City = address.City,
                                                 ProvinceId = address.Idprovince,
                                                 Country = address.Country,
                                                 StartOfResidencyDate = address.StartOfResidencyDate
                                             },
                                             EmploymentStatus = new ClientEmploymentStatusModel
                                             {
                                                 EmployerName = employment.EmployerName,
                                                 SupervisorName = employment.SupervisorName,
                                                 WorkPhoneNumber = employment.PhoneNumber,
                                                 WorkPhoneNumberExtension = employment.Extension,
                                                 WorkPhoneNumberHR = employment.PhoneNumberHr,
                                                 WorkPhoneNumberExtensionHR = employment.ExtensionHr,
                                                 Occupation = employment.Occupation,
                                                 HiringDate = employment.HiringDate,
                                                 PayFrequencyId = employment.IdpayFrequency
                                             },
                                             BankInfo = new ClientBankInfoModel
                                             {
                                                 TransitNumber = bankInfo_lj.TransitNumber,
                                                 InstitutionNumber = bankInfo_lj.InstitutionNumber,
                                                 AccountNumber = bankInfo_lj.AccountNumber,
                                                 IsBankrupt = bankInfo_lj.IsBankrupt,
                                                 BankruptEndDate = bankInfo_lj.BankruptEndDate
                                             },
                                             Social = new ClientSocialModel
                                             {

                                                 FacebookId = social_lj.FacebookId,
                                                 GoogleId = social_lj.GoogleId,
                                                 GooglePhoto = social_lj.GooglePhoto,
                                                 FacebookPhoto = social_lj.FacebookPhoto
                                             }
                                         }
                                     }).SingleOrDefaultAsync();

            // Find 
            List<ClientMatchModel> clientsFound = null;

            // Check for perfect match
            clientsFound = (from c in context.Client
                            join ca in context.ClientAddress on c.Id equals ca.Idclient
                            join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                            where c.FirstName.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.FirstName.Trim().Replace(" ", "").ToUpper()
                            && c.LastName.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.LastName.Trim().Replace(" ", "").ToUpper()
                            && c.CellPhoneNumber == clientModel.BasicInfo.PersonalInfo.CellPhoneNumber
                            && cea.EmailAddress.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.EmailAddress.Trim().Replace(" ", "").ToUpper()
                            && cea.IsPrimary == true
                            && c.Id != clientModel.Id
                            && c.ProfileMerged == false
                            && c.IsDeleted == false
                            select new ClientMatchModel()
                            {
                                Id = c.Id,
                                FName = c.FirstName,
                                LName = c.LastName,
                                Birthday = c.DateOfBirth,
                                CelPhone = c.CellPhoneNumber,
                                Email = cea.EmailAddress
                            })
                            .ToList();

            // 1: Check for Phone Number match
            if (clientsFound.Count == 0)
            {
                clientsFound = (from c in context.Client
                                join ca in context.ClientAddress on c.Id equals ca.Idclient
                                join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                where c.CellPhoneNumber == clientModel.BasicInfo.PersonalInfo.CellPhoneNumber
                                && c.Id != clientModel.Id
                                && c.ProfileMerged == false
                                && c.IsDeleted == false
                                select new ClientMatchModel()
                                {
                                    Id = c.Id,
                                    FName = c.FirstName,
                                    LName = c.LastName,
                                    Birthday = c.DateOfBirth,
                                    CelPhone = c.CellPhoneNumber,
                                    Email = cea.EmailAddress
                                })
                              .ToList();
            }

            // 2: Check for email match
            if (clientsFound.Count == 0)
            {
                clientsFound = (from c in context.Client
                                join ca in context.ClientAddress on c.Id equals ca.Idclient
                                join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                where cea.EmailAddress.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.EmailAddress.Trim().Replace(" ", "").ToUpper()
                                && cea.IsPrimary == true
                                && c.Id != clientModel.Id
                                && c.ProfileMerged == false
                                && c.IsDeleted == false
                                select new ClientMatchModel()
                                {
                                    Id = c.Id,
                                    FName = c.FirstName,
                                    LName = c.LastName,
                                    Birthday = c.DateOfBirth,
                                    CelPhone = c.CellPhoneNumber,
                                    Email = cea.EmailAddress
                                })
                              .ToList();
            }

            // 3: Check for First Name, Last Name and Date Of Birth match
            if (clientsFound.Count == 0)
            {
                clientsFound = (from c in context.Client
                                join ca in context.ClientAddress on c.Id equals ca.Idclient
                                join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                where c.FirstName.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.FirstName.Trim().Replace(" ", "").ToUpper()
                                && c.LastName.Trim().Replace(" ", "").ToUpper() == clientModel.BasicInfo.PersonalInfo.LastName.Trim().Replace(" ", "").ToUpper()
                                && c.DateOfBirth == clientModel.BasicInfo.PersonalInfo.DateOfBirth
                                && c.Id != clientModel.Id
                                && c.ProfileMerged == false
                                && c.IsDeleted == false
                                select new ClientMatchModel()
                                {
                                    Id = c.Id,
                                    FName = c.FirstName,
                                    LName = c.LastName,
                                    Birthday = c.DateOfBirth,
                                    CelPhone = c.CellPhoneNumber,
                                    Email = cea.EmailAddress
                                })
                              .ToList();
            }
            clientModel.ClientMatchList = clientsFound;

            // Revenue table
            var revenue = await context.ClientRevenueSource.SingleOrDefaultAsync(crs => crs.ClientId == clientId);
            clientModel.BasicInfo.RevenueSource = new ClientRevenueSourceModel
            {
                IncomeTypeCode = (await context.IncomeType.FindAsync(revenue.IncomeTypeId)).Code,
                NextPayDate = revenue?.NextPayDate,
                NextDepositDate = revenue?.NextDepositDate,
                EmploymentInsuranceNextPayDate = revenue?.EmploymentInsuranceNextPayDate,
                EmploymentInsuranceStart = revenue?.EmploymentInsuranceStart,
                EmploymentNextPay = revenue?.EmploymentNextPay,
                SelfEmployedDirectDeposit = revenue?.SelfEmployedDirectDeposit,
                SelfEmployedPhoneNumber = revenue?.SelfEmployedPhoneNumber,
                SelfEmployedPayFrequencyId = revenue?.SelfEmployedPayFrequencyId,
                SelfEmployedStartDate = revenue?.SelfEmployedStartDate
            };

            // Financial status table
            var financialStatus = await context.ClientFinancialStatus.SingleOrDefaultAsync(cfs => cfs.ClientId == clientId);
            if (financialStatus != null)
            {
                clientModel.BasicInfo.FinancialStatus = new ClientFinancialStatusModel
                {
                    HousingStatusCode = financialStatus.HousingStatusId != null
                        ? (await context.HousingStatus.FindAsync(financialStatus.HousingStatusId)).Code // For some reason the HousingStatus relation is null when it shouldn't be
                        : null,
                    GrossAnnualRevenue = financialStatus.GrossAnnualRevenue,
                    MonthlyHousingCost = financialStatus.MonthlyHousingCost,
                    MonthlyUtilityCost = financialStatus.MonthlyUtilityCost,
                    MonthlyCarPayment = financialStatus.MonthlyCarPayment,
                    MonthlyOtherLoanPayment = financialStatus.MonthlyOtherLoanPayment
                };
            }

            // Client reference tables
            var provinces = await context.Province
                                    .Select(x => new ClientProvinceModel()
                                    {
                                        Id = x.Id,
                                        Name = language == "FR" ? x.NameFr : x.NameEn
                                    })
                                    .ToListAsync();

            clientModel.Provinces = provinces;

            var languages = await context.Language
                .Select(x => new ClientLanguageModel
                {
                    Id = x.Id,
                    Name = language == "FR" ? x.NameFr : x.NameEn
                })
                .ToListAsync();

            clientModel.Languages = languages;

            var genders = await context.Gender
                .Select(x => new ClientGenderModel()
                {
                    Id = x.Id,
                    Name = language == "FR" ? x.NameFr : x.NameEn
                })
                .ToListAsync();

            clientModel.Genders = genders;

            var payFrequencies = await context.PayFrequency
                .Select(x => new ClientPayFrequencyModel()
                {
                    Id = x.Id,
                    Name = language == "FR" ? x.NameFr : x.NameEn
                })
                .ToListAsync();

            clientModel.PayFrequencies = payFrequencies;

            var clientStatus = await context.ClientStatus
             .Select(x => new ClientStatusModel()
             {
                 Id = x.Id,
                 Name = language == "FR" ? x.NameFr : x.NameEn
             })
             .OrderBy(x => x.Name)
             .ToListAsync();

            clientModel.Status = clientStatus;

            // Client IBV
            clientModel.IBVList = await context.ClientIbv
                .Where(x => x.Idclient == clientModel.Id)
                .Select(x => new ClientIBVModel()
                {
                    Id = x.Id,
                    GUID = x.Guid,
                    CreatedDateFormat = String.Format("{0:yyyy-MM-dd HH:mm}", x.CreatedDate),
                    Status = language == "FR" ? x.IdclientIbvstatusNavigation.NameFr : x.IdclientIbvstatusNavigation.NameEn,
                    StatusCode = x.IdclientIbvstatusNavigation.Code,
                    URL = x.Url
                }).ToListAsync();

            // Get ClientLoan Information
            var loanService = new LoanService(_dbContextProvider, _configurationService);
            clientModel = loanService.GetClientLoans(clientId, clientModel, context, language, digitalBaseUrl);

            // Check if the client has active collections
            var collections = from loan in context.ClientLoan
                              join status in context.LoanStatus on loan.IdloanStatus equals status.Id
                              join collection in context.ClientLoanCollection on loan.Id equals collection.IdclientLoan
                              where loan.Idclient == clientId && collection.IdcollectionStatus != (int)CollectionStatusEnum.Settled
                              select new ActiveCollectionModel
                              {
                                  LoanId = loan.Id,
                                  LoanNumber = loan.LoanNumber.Value,
                                  LoanStatusCode = status.Code,
                                  CollectionId = collection.Id
                              };
            clientModel.ActiveCollections = await collections.ToListAsync();

            // Get Client Requests
            clientModel.NonLinkedRequests = await (from req in context.ExternalLoanWebRequest
                                                   join lrs in context.LoanRequestStatus on req.StatusId equals lrs.Id
                                                   //join l in context.ClientLoan on req.Id equals l.Idrequest into l_lj
                                                   //from loan_lj in l_lj.DefaultIfEmpty()
                                                   where req.Idclient == clientId
                                                   && !(from loan in context.ClientLoan
                                                        where loan.Idrequest != null
                                                        && loan.IsDeleted == false
                                                        select loan.Idrequest).Contains(req.Id)
                                                   && (lrs.Code == "ACCEPTED" || lrs.Code == "PREAPPROVED")
                                                   select new LoanRequestModel
                                                   {
                                                       Id = req.Id,
                                                       ClientFirstName = req.FirstName,
                                                       ClientLastName = req.LastName,
                                                       LoanAmount = req.Amount,
                                                       AmountApproved = req.AmountApproved,
                                                       RequestedDateFormat = String.Format("{0:yyyy-MM-dd}", req.RequestedDate),
                                                       RequestedDate = req.RequestedDate,
                                                       Status = lrs != null ? language == "FR" ? lrs.NameFr : lrs.NameEn : null
                                                   }).OrderByDescending(x => x.RequestedDate).ToListAsync();

            // Get Loan Configuration
            clientModel.LoanConfiguration = await _configurationService.LoadLenderLoanConfiguration();

            return clientModel;
        }

        public IEnumerable<ClientSummaryModel> GetClientSummaryList(string language)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                return (from client in context.Client
                        join status in context.ClientStatus on client.IdclientStatus equals status.Id
                        where client.IsDeleted == false
                        select new ClientSummaryModel
                        {
                            Id = client.Id,
                            Status = language == "FR" ? status.NameFr : status.NameEn,
                            CreationDate = client.CreationDate,
                            FirstName = client.FirstName,
                            LastName = client.LastName,
                            City = client.ClientAddress.Single(c => (bool)c.IsPrimary).City,
                            Province = client.ClientAddress.Single(c => (bool)c.IsPrimary).IdprovinceNavigation.NameFr,
                            HomePhoneNumber = String.Format("{0:(###) ###-####}", Convert.ToInt64(client.HomePhoneNumber.Replace(" ", ""))),
                            CellPhoneNumber = String.Format("{0:(###) ###-####}", Convert.ToInt64(client.CellPhoneNumber.Replace(" ", ""))),
                            EmailAddress = client.ClientEmailAddress.Single(c => (bool)c.IsPrimary).EmailAddress
                        }).ToList();
            }
        }

        public ClientModel SaveNewClient(ClientModel model)
        {
            using (var unit = new UnitOfWork(_dbContextProvider))
            {
                try
                {
                    var context = _dbContextProvider.GetContext();

                    var personalInfo = model.BasicInfo.PersonalInfo;
                    var newClient = new Client()
                    {
                        IsActive = true,
                        FirstName = personalInfo.FirstName,
                        LastName = personalInfo.LastName,
                        HomePhoneNumber = personalInfo.HomePhoneNumber,
                        CellPhoneNumber = personalInfo.CellPhoneNumber,
                        FaxNumber = personalInfo.FaxNumber,
                        DateOfBirth = personalInfo.DateOfBirth,
                        Idgender = personalInfo.GenderId,
                        Idlanguage = personalInfo.LanguageId,
                        SocialInsuranceNumber = personalInfo.SocialInsuranceNumber
                    };
                    newClient.ClientEmailAddress.Add(new ClientEmailAddress
                    {
                        EmailAddress = personalInfo.EmailAddress,
                        IsPrimary = true
                    });

                    var address = model.BasicInfo.Address;
                    newClient.ClientAddress.Add(new ClientAddress
                    {
                        CivicNumber = address.CivicNumber,
                        StreetName = address.StreetName,
                        Apartment = address.Apartment,
                        PostalCode = address.PostalCode,
                        City = address.City,
                        Idprovince = address.ProvinceId,
                        Country = address.Country,
                        IsPrimary = true,
                        StartOfResidencyDate = address.StartOfResidencyDate
                    });

                    var revenueSource = model.BasicInfo.RevenueSource;
                    var incomeTypeId = context.IncomeType.SingleOrDefault(ic => ic.Code == revenueSource.IncomeTypeCode).Id;
                    newClient.ClientRevenueSource = new ClientRevenueSource
                    {
                        IncomeTypeId = incomeTypeId,
                        EmploymentInsuranceStart = revenueSource.EmploymentInsuranceStart,
                        NextPayDate = revenueSource.NextPayDate,
                        NextDepositDate = revenueSource.NextDepositDate,
                        EmploymentInsuranceNextPayDate = revenueSource.EmploymentInsuranceNextPayDate,
                        EmploymentNextPay = revenueSource.EmploymentNextPay,
                        SelfEmployedDirectDeposit = revenueSource.SelfEmployedDirectDeposit,
                        SelfEmployedPhoneNumber = revenueSource.SelfEmployedPhoneNumber,
                        SelfEmployedPayFrequencyId = revenueSource.SelfEmployedPayFrequencyId,
                        SelfEmployedStartDate = revenueSource.SelfEmployedStartDate
                    };

                    var employmentStatus = model.BasicInfo.EmploymentStatus;
                    newClient.ClientEmploymentStatus.Add(new ClientEmploymentStatus
                    {
                        EmployerName = employmentStatus.EmployerName,
                        SupervisorName = employmentStatus.SupervisorName,
                        PhoneNumber = employmentStatus.WorkPhoneNumber,
                        Extension = employmentStatus.WorkPhoneNumberExtension,
                        Occupation = employmentStatus.Occupation,
                        HiringDate = employmentStatus.HiringDate,
                        IdpayFrequency = employmentStatus.PayFrequencyId,
                        PhoneNumberHr = employmentStatus.WorkPhoneNumberHR,
                        ExtensionHr = employmentStatus.WorkPhoneNumberExtensionHR
                    });

                    var bankInfo = model.BasicInfo.BankInfo;
                    newClient.ClientBankDetails.Add(new ClientBankDetails
                    {
                        TransitNumber = bankInfo.TransitNumber,
                        InstitutionNumber = bankInfo.InstitutionNumber,
                        AccountNumber = bankInfo.AccountNumber,
                        IsBankrupt = bankInfo.IsBankrupt.Value,
                        BankruptEndDate = bankInfo.BankruptEndDate
                    });

                    var financialStatus = model.BasicInfo.FinancialStatus;
                    newClient.ClientFinancialStatus = new ClientFinancialStatus
                    {
                        HousingStatusId = !string.IsNullOrEmpty(financialStatus.HousingStatusCode)
                            ? context.HousingStatus.Single(hs => hs.Code == financialStatus.HousingStatusCode).Id
                            : null as int?,
                        GrossAnnualRevenue = financialStatus.GrossAnnualRevenue,
                        MonthlyHousingCost = financialStatus.MonthlyHousingCost,
                        MonthlyUtilityCost = financialStatus.MonthlyUtilityCost,
                        MonthlyCarPayment = financialStatus.MonthlyCarPayment,
                        MonthlyOtherLoanPayment = financialStatus.MonthlyOtherLoanPayment
                    };

                    if (model.References != null && model.References.Any())
                    {
                        foreach (var cref in model.References)
                        {
                            newClient.ClientReference.Add(new ClientReference
                            {
                                FirstName = cref.FirstName,
                                LastName = cref.LastName,
                                PhoneNumber = cref.PhoneNumber,
                                EmailAddress = cref.EmailAddress,
                                Relation = cref.Relation
                            });
                        }
                    }

                    var repo = unit.StartFor<Client>();
                    repo.Add(newClient);
                    unit.Finish();
                    model.Id = newClient.Id;
                    return model;
                }
                catch (Exception e)
                {
                    unit.Rollback();
                    throw e;
                }
            }
        }

        #region IBV

        public ClientIBVModel SaveNewIBV(int clientId, string requestId, string url, string language)
        {
            using (var uow = new UnitOfWork(_dbContextProvider))
            {
                try
                {
                    var createdDate = DateTime.Now;

                    var newIBV = new ClientIbv()
                    {
                        Idclient = clientId,
                        IdclientIbvstatus = 1, // Request Sent
                        CreatedDate = createdDate,
                        Guid = requestId,
                        Url = url.Replace("IBV", "Report")
                    };

                    var repo = uow.StartFor<ClientIbv>();
                    repo.Add(newIBV);
                    uow.Finish();

                    using (var context = _dbContextProvider.GetContext())
                    {
                        var ibvStatus = context.ClientIbvStatus
                                                .Where(x => x.Code == "RS")
                                                .SingleOrDefault();

                        return new ClientIBVModel()
                        {
                            ClientId = clientId,
                            GUID = requestId,
                            CreatedDate = createdDate,
                            CreatedDateFormat = String.Format("{0:yyyy-MM-dd HH:mm}", createdDate),
                            StatusCode = "RS",
                            Status = language == "FR" ? ibvStatus.NameFr : ibvStatus.NameEn,
                            URL = url
                        };
                    }
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw ex;
                }
            }
        }

        #endregion

        public async Task<ClientModel> UpdateClient(ClientModel data)
        {
            using var context = _dbContextProvider.GetContext();
            using var trx = context.Database.BeginTransaction();
            try
            {
                var client = await context.Client.SingleOrDefaultAsync(c => c.Id == data.Id);
                client.FirstName = data.BasicInfo.PersonalInfo.FirstName;
                client.LastName = data.BasicInfo.PersonalInfo.LastName;
                client.HomePhoneNumber = data.BasicInfo.PersonalInfo.HomePhoneNumber;
                client.CellPhoneNumber = data.BasicInfo.PersonalInfo.CellPhoneNumber;
                client.FaxNumber = data.BasicInfo.PersonalInfo.FaxNumber;
                client.Idgender = data.BasicInfo.PersonalInfo.GenderId;
                client.IdclientStatus = data.BasicInfo.PersonalInfo.StatusId;
                client.Idlanguage = data.BasicInfo.PersonalInfo.LanguageId;
                client.SocialInsuranceNumber = data.BasicInfo.PersonalInfo.SocialInsuranceNumber;
                client.DateOfBirth = data.BasicInfo.PersonalInfo.DateOfBirth;
                context.Client.Update(client);

                var email = await context.ClientEmailAddress.SingleOrDefaultAsync(cea => cea.Idclient == data.Id && cea.IsPrimary.Value);
                email.EmailAddress = data.BasicInfo.PersonalInfo.EmailAddress;
                context.ClientEmailAddress.Update(email);

                var address = await context.ClientAddress.SingleOrDefaultAsync(ca => ca.Idclient == data.Id && ca.IsPrimary.Value);
                address.CivicNumber = data.BasicInfo.Address.CivicNumber;
                address.StreetName = data.BasicInfo.Address.StreetName;
                address.Apartment = data.BasicInfo.Address.Apartment;
                address.PostalCode = data.BasicInfo.Address.PostalCode;
                address.City = data.BasicInfo.Address.City;
                address.Idprovince = data.BasicInfo.Address.ProvinceId;
                address.Country = data.BasicInfo.Address.Country;
                address.StartOfResidencyDate = data.BasicInfo.Address.StartOfResidencyDate;
                context.ClientAddress.Update(address);

                var employment = await context.ClientEmploymentStatus.SingleOrDefaultAsync(ces => ces.Idclient == data.Id);
                employment.EmployerName = data.BasicInfo.EmploymentStatus.EmployerName;
                employment.SupervisorName = data.BasicInfo.EmploymentStatus.SupervisorName;
                employment.PhoneNumber = data.BasicInfo.EmploymentStatus.WorkPhoneNumber;
                employment.Extension = data.BasicInfo.EmploymentStatus.WorkPhoneNumberExtension;
                employment.Occupation = data.BasicInfo.EmploymentStatus.Occupation;
                employment.HiringDate = data.BasicInfo.EmploymentStatus.HiringDate;
                employment.IdpayFrequency = data.BasicInfo.EmploymentStatus.PayFrequencyId;
                employment.PhoneNumberHr = (data.BasicInfo.EmploymentStatus.WorkPhoneNumberHR != null &&
                                            data.BasicInfo.EmploymentStatus.WorkPhoneNumberHR.Length > 0) ? data.BasicInfo.EmploymentStatus.WorkPhoneNumberHR : null;
                employment.ExtensionHr = data.BasicInfo.EmploymentStatus.WorkPhoneNumberExtensionHR;
                context.ClientEmploymentStatus.Update(employment);

                var bank = await context.ClientBankDetails.SingleOrDefaultAsync(cbd => cbd.Idclient == data.Id);
                // If Bank details are empty, then save new line else update
                if (bank == null)
                {
                    bank = new ClientBankDetails();
                    client.ClientBankDetails.Add(new ClientBankDetails()
                    {
                        AccountNumber = data.BasicInfo.BankInfo.AccountNumber,
                        TransitNumber = data.BasicInfo.BankInfo.TransitNumber,
                        InstitutionNumber = data.BasicInfo.BankInfo.InstitutionNumber
                    });
                }
                else
                {
                    bank.TransitNumber = data.BasicInfo.BankInfo.TransitNumber;
                    bank.InstitutionNumber = data.BasicInfo.BankInfo.InstitutionNumber;
                    bank.AccountNumber = data.BasicInfo.BankInfo.AccountNumber;
                    bank.IsBankrupt = data.BasicInfo.BankInfo.IsBankrupt ?? false;
                    bank.BankruptEndDate = data.BasicInfo.BankInfo.BankruptEndDate;
                    context.ClientBankDetails.Update(bank);
                }

                var revenue = await context.ClientRevenueSource.SingleOrDefaultAsync(rev => rev.ClientId == data.Id);
                var incomeTypeId = context.IncomeType.SingleOrDefault(ic => ic.Code == data.BasicInfo.RevenueSource.IncomeTypeCode).Id;
                revenue.IncomeTypeId = incomeTypeId;
                revenue.EmploymentInsuranceStart = data.BasicInfo.RevenueSource.EmploymentInsuranceStart;
                revenue.NextPayDate = data.BasicInfo.RevenueSource.NextPayDate;
                revenue.EmploymentInsuranceNextPayDate = data.BasicInfo.RevenueSource.EmploymentInsuranceNextPayDate;
                revenue.NextDepositDate = data.BasicInfo.RevenueSource.NextDepositDate;
                revenue.EmploymentNextPay = data.BasicInfo.RevenueSource.EmploymentNextPay;
                revenue.SelfEmployedDirectDeposit = data.BasicInfo.RevenueSource.SelfEmployedDirectDeposit;
                revenue.SelfEmployedPhoneNumber = data.BasicInfo.RevenueSource.SelfEmployedPhoneNumber;
                revenue.SelfEmployedPayFrequencyId = data.BasicInfo.RevenueSource.SelfEmployedPayFrequencyId;
                revenue.SelfEmployedStartDate = data.BasicInfo.RevenueSource.SelfEmployedStartDate;

                var financialStatus = await context.ClientFinancialStatus.SingleOrDefaultAsync(fs => fs.ClientId == data.Id);
                if (financialStatus == null)
                {
                    financialStatus = new ClientFinancialStatus();
                    client.ClientFinancialStatus = financialStatus;
                }

                if (data.BasicInfo.FinancialStatus != null)
                {
                    if (data.BasicInfo.FinancialStatus.HousingStatusCode != null && data.BasicInfo.FinancialStatus.HousingStatusCode != "")
                    {
                        financialStatus.HousingStatusId = (await context.HousingStatus.SingleAsync(hs => hs.Code == data.BasicInfo.FinancialStatus.HousingStatusCode)).Id;
                    }
                    else
                    {
                        financialStatus.HousingStatusId = null;
                    }

                    financialStatus.GrossAnnualRevenue = data.BasicInfo.FinancialStatus.GrossAnnualRevenue;
                    financialStatus.MonthlyHousingCost = data.BasicInfo.FinancialStatus.MonthlyHousingCost;
                    financialStatus.MonthlyUtilityCost = data.BasicInfo.FinancialStatus.MonthlyUtilityCost;
                    financialStatus.MonthlyCarPayment = data.BasicInfo.FinancialStatus.MonthlyCarPayment;
                    context.ClientFinancialStatus.Update(financialStatus);
                }

                context.SaveChanges();
                trx.Commit();
                return data;
            }
            catch
            {
                trx.Rollback();
                throw;
            }
        }

        public bool DeleteClient(int id)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                try
                {
                    var client = context.Client.Where(x => x.Id == id).SingleOrDefault();
                    client.IsDeleted = true;

                    context.SaveChanges();

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public async Task<IEnumerable<ClientReferenceModel>> GetClientReferences(int clientId)
        {
            var context = _dbContextProvider.GetContext();
            return await context.ClientReference.Where(cref => cref.Idclient == clientId)
                .Select(cref => new ClientReferenceModel
                {
                    Id = cref.Id,
                    ClientId = cref.Idclient,
                    FirstName = cref.FirstName,
                    LastName = cref.LastName,
                    PhoneNumber = cref.PhoneNumber,
                    EmailAddress = cref.EmailAddress,
                    Relation = cref.Relation
                }).ToListAsync();
        }

        public async Task<ClientReferenceModel> UpdateClientReference(ClientReferenceModel reference)
        {
            using var context = _dbContextProvider.GetContext();
            var cref = await context.ClientReference.SingleAsync(cr => cr.Id == reference.Id);
            cref.FirstName = reference.FirstName;
            cref.LastName = reference.LastName;
            cref.PhoneNumber = reference.PhoneNumber;
            cref.EmailAddress = reference.EmailAddress;
            cref.Relation = reference.Relation;
            context.ClientReference.Update(cref);
            await context.SaveChangesAsync();
            return reference;
        }

        public async Task<ClientReferenceModel> CreateClientReference(ClientReferenceModel reference)
        {
            using var context = _dbContextProvider.GetContext();
            var newref = new ClientReference
            {
                Idclient = reference.ClientId,
                FirstName = reference.FirstName,
                LastName = reference.LastName,
                PhoneNumber = reference.PhoneNumber,
                EmailAddress = reference.EmailAddress,
                Relation = reference.Relation
            };
            await context.ClientReference.AddAsync(newref);
            await context.SaveChangesAsync();
            reference.Id = newref.Id;
            return reference;
        }

        public async Task DeleteClientReference(int refId)
        {
            using var context = _dbContextProvider.GetContext();
            var cref = await context.ClientReference.SingleOrDefaultAsync(cr => cr.Id == refId);
            context.ClientReference.Remove(cref);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ClientLoanModel>> GetClientLoans(int clientId)
        {
            using var context = _dbContextProvider.GetContext();
            var result = await context.ClientLoan
                .Where(cl => cl.Idclient == clientId)
                .Select(cl => new ClientLoanModel
                {
                    Id = cl.Id,
                    Status = cl.IdloanStatusNavigation.NameEn,
                    StatusCode = cl.IdloanStatusNavigation.Code,
                    FrequencyDuration = cl.IdloanFrequencyNavigation.Frequency,
                    Frequency = cl.IdloanFrequencyNavigation.Code,
                    Amount = cl.Amount,
                    StartDate = cl.StartDate,
                    EndDate = cl.EndDate,
                    InterestAmount = cl.InterestAmount,
                    Fees = cl.Fees,
                    LoanNumber = cl.LoanNumber,
                    CreatedDate = cl.CreatedDate,
                    // FirstPaymentAmount = ,
                    // CurrentDailyBalance = 
                }).ToListAsync();
            return result;
        }

        public async Task<bool> UpdateIBVStatusStart(string requestId)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                try
                {
                    var ibv = await context.ClientIbv
                                            .Where(x => x.Guid == requestId)
                                            .SingleOrDefaultAsync();

                    if (ibv != null)
                    {
                        var ibvStatus = await context.ClientIbvStatus
                                                        .Where(x => x.Code == "IP")
                                                        .SingleOrDefaultAsync();

                        ibv.IdclientIbvstatus = ibvStatus.Id;
                        await context.SaveChangesAsync();

                        try
                        {
                            await _webhookService24HR.IBVStatusChange_24HR(ibv);
                        }
                        catch { }

                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public async Task<bool> UpdateIBVStatusCompleted(string requestId)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                try
                {
                    var ibv = await context.ClientIbv
                                            .Where(x => x.Guid == requestId)
                                            .SingleOrDefaultAsync();

                    if (ibv != null)
                    {
                        var ibvStatus = await context.ClientIbvStatus
                                                        .Where(x => x.Code == "COM")
                                                        .SingleOrDefaultAsync();

                        ibv.IdclientIbvstatus = ibvStatus.Id;
                        await context.SaveChangesAsync();

                        try
                        {
                            await _webhookService24HR.IBVStatusChange_24HR(ibv);
                        }
                        catch { }

                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public async Task<IBVModel> GetIBV(int ibvId)
        {
            using var context = _dbContextProvider.GetContext();
            var query = from ibv in context.ClientIbv
                        join client in context.Client on ibv.Idclient equals client.Id
                        join language in context.Language on client.Idlanguage equals language.Id
                        join email in context.ClientEmailAddress on client.Id equals email.Idclient
                        where ibv.Id == ibvId && email.IsPrimary == true
                        select new IBVModel
                        {
                            ClientId = client.Id,
                            FirstName = client.FirstName,
                            LastName = client.LastName,
                            Email = email.EmailAddress,
                            PhoneNumber = client.CellPhoneNumber,
                            LanguageCode = language.Code,
                            Url = ibv.Url,
                            IsCompleted = ibv.IdclientIbvstatus == 3
                        };
            return await query.SingleOrDefaultAsync();
        }

        public async Task<List<IBVModel>> GetAllIBV()
        {
            using var context = _dbContextProvider.GetContext();
            var query = (from ibv in context.ClientIbv
                         join client in context.Client on ibv.Idclient equals client.Id
                         join request in context.ExternalLoanWebRequest on client.Id equals request.Idclient
                         join request2 in context.ExternalLoanWebRequest on ibv.Guid equals request2.IbvrequestId
                         join language in context.Language on client.Idlanguage equals language.Id
                         join email in context.ClientEmailAddress on client.Id equals email.Idclient
                         where email.IsPrimary == true
                         && request.Status.Code == "WAITING_MISSING_IBV"
                         select new IBVModel
                         {
                             ClientId = client.Id,
                             FirstName = client.FirstName,
                             LastName = client.LastName,
                             Email = email.EmailAddress,
                             PhoneNumber = client.CellPhoneNumber,
                             LanguageCode = language.Code,
                             Url = ibv.Url
                             //IsCompleted = ibv.IdclientIbvstatus == 3
                         }).Distinct();
            return await query.ToListAsync();
        }

        public async Task<List<ReturnSearchModel>> FormatCreditBookSearchResults(ResponseSearchModel response, string language)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                var data = new List<ReturnSearchModel>();
                var creditBookCheckStatus = await context.CreditBookCheckStatus
                                                    .Select(x => x)
                                                    .ToListAsync();

                var payFrequency = await context.PayFrequency
                                    .Select(x => x)
                                    .ToListAsync();

                foreach (var item in response.results)
                {
                    var status = string.IsNullOrEmpty(item.status)
                                 ? null
                                 : creditBookCheckStatus.Where(x => x.Code == item.status).SingleOrDefault() != null
                                 ? language == "EN" ? creditBookCheckStatus.Where(x => x.Code == item.status).SingleOrDefault().NameEn
                                 : creditBookCheckStatus.Where(x => x.Code == item.status).SingleOrDefault().NameFr
                                 : null;

                    var returnSearchModel = new ReturnSearchModel()
                    {
                        client = item.firstName + " " + item.lastName,
                        phone = item.phone1,
                        status = status,
                        lender = item.lender,
                        email = item.email,
                        amount = item.amount != null ? (decimal)item.amount : (decimal?)null,
                        language = language,
                        comments = item.comments,
                        dateOfBirth = item.dateOfBirth,
                        requestDate = item.requestDate != null ? String.Format("{0:yyyy-MM-dd HH:mm}", item.requestDate) : null
                    };

                    // Check to see if the Job object is returned
                    if (item.job != null)
                    {
                        var frequency = string.IsNullOrEmpty(item.job.payFrequency)
                                    ? null
                                    : language == "EN" ? payFrequency.Where(x => x.CreditBookCheckCode == item.job.payFrequency).SingleOrDefault().NameEn
                                    : payFrequency.Where(x => x.CreditBookCheckCode == item.job.payFrequency).SingleOrDefault().NameFr;

                        returnSearchModel.job = new JobModel()
                        {
                            companyName = item.job.companyName,
                            companyPhone = item.job.companyPhone,
                            supervisorName = item.job.supervisorName,
                            jobTitle = item.job.jobTitle,
                            payFrequency = frequency,
                            payNext = item.job.payNext,
                            startDate = item.job.startDate
                        };
                    }

                    returnSearchModel.address = new AddressModel()
                    {
                        streetAddressLine1 = item.address.streetAddressLine1,
                        city = item.address.city,
                        country = item.address.country,
                        stateProvince = item.address.stateProvince,
                        zipPostalCode = item.address.zipPostalCode,
                        effectiveDate = item.address.effectiveDate
                    };

                    returnSearchModel.references = new List<ReferenceModel>();

                    //// Check to see if References are empty
                    if (item.references != null)
                    {
                        foreach (var reference in item.references)
                        {
                            returnSearchModel.references.Add(new ReferenceModel()
                            {
                                firstName = reference.firstName,
                                lastName = reference.lastName,
                                phone = reference.phone,
                                email = reference.email,
                                relationship = reference.relationship
                            });
                        }
                    }

                    // Retreive most recent reason
                    if (item.statusHistory != null)
                    {
                        var lastStatus = item.statusHistory
                                                .OrderByDescending(x => x.date)
                                                .FirstOrDefault();

                        returnSearchModel.reason = language == "EN" ? GetReasonEN(lastStatus.reason) : GetReasonFR(lastStatus.reason);
                    }

                    data.Add(returnSearchModel);
                }

                return data;
            }
        }

        private string GetReasonEN(string reason) => reason switch
        {
            "too-many-loans" => "Too many loans",
            "already-refused-recently" => "Recently refused",
            "too-many-requests" => "Too many requests",
            "no-job" => "No job",
            "too-many-nsf" => "Too many NSF",
            "in-collection-with-others-companies" => "In collections",
            "other-see-comments" => "See comments",
            "no-capacity" => "No capacity",
            "stopped-payments" => "Stopped payments",
            _ => ""
        };

        private string GetReasonFR(string reason) => reason switch
        {
            "too-many-loans" => "Trop de prêts",
            "already-refused-recently" => "Déja refusé récemment",
            "too-many-requests" => "Trop de demandes",
            "no-job" => "Pas d'emploi",
            "too-many-nsf" => "Trop de NSF",
            "in-collection-with-others-companies" => "En collection",
            "other-see-comments" => "Voir commentaires",
            "no-capacity" => "Pas de capacité",
            "stopped-payments" => "Arrets de paiement",
            _ => ""
        };

        public async Task<int> MergeClientProfile(int oldClientId, int newClientId)
        {
            try
            {
                using var context = _dbContextProvider.GetContext();
                using var contextTransaction = context.Database.BeginTransaction();

                // Take base info from New Profile only
                var oldClient = await context.Client.SingleOrDefaultAsync(c => c.Id == oldClientId);
                oldClient.IsDeleted = true;
                oldClient.ProfileMerged = true;
                context.Client.Update(oldClient);
                await context.SaveChangesAsync();

                // Merge References to New Profile
                var oldClientReferences = await context.ClientReference.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientReferences = await context.ClientReference.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var reference in oldClientReferences)
                {
                    reference.Id = 0;
                    reference.Idclient = newClientId;
                    await context.ClientReference.AddAsync(reference);
                    await context.SaveChangesAsync();
                }

                // Merge IBV to New Profile
                var oldClientIbv = await context.ClientIbv.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientIbv = await context.ClientIbv.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var ibv in oldClientIbv)
                {
                    ibv.Id = 0;
                    ibv.Idclient = newClientId;
                    await context.ClientIbv.AddAsync(ibv);
                    await context.SaveChangesAsync();
                }

                // Merge Global Requests to New Profile
                var oldClientRequests = await context.ExternalLoanWebRequest.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientRequests = await context.ExternalLoanWebRequest.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var request in oldClientRequests)
                {
                    request.ClientMatched = false;
                    request.Idclient = newClientId;
                    context.ExternalLoanWebRequest.Update(request);
                    await context.SaveChangesAsync();
                }

                // Merge Documents to New Profile
                // Because of Unique GUID, update the IDClient instead of inseritng a new Document for the merged profile
                var oldClientDocuments = await context.ClientDocument.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientDocuments = await context.ClientDocument.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var document in oldClientDocuments)
                {
                    //document.Id = 0;
                    document.Idclient = newClientId;
                    context.ClientDocument.Update(document);
                    await context.SaveChangesAsync();
                }

                // Merge Loans to New Profile
                var oldClientLoans = await context.ClientLoan.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientLoans = await context.ClientLoan.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var loan in oldClientLoans)
                {
                    var oldLoanId = loan.Id;
                    loan.Id = 0;
                    loan.Idclient = newClientId;
                    await context.ClientLoan.AddAsync(loan);
                    await context.SaveChangesAsync();

                    // Merge Loan Payments to New Profile
                    var oldLoanPayments = await context.ClientLoanPayment.Where(lp => lp.IdclientLoan == oldLoanId).ToListAsync();
                    foreach (var loanPayment in oldLoanPayments)
                    {
                        loanPayment.Id = 0;
                        loanPayment.IdclientLoan = loan.Id;
                        await context.ClientLoanPayment.AddAsync(loanPayment);
                        await context.SaveChangesAsync();
                    }

                    // Transfer Deposit to New Loan
                    var deposit = await context.Deposit.Where(cr => cr.ClientLoanId == oldLoanId).SingleOrDefaultAsync();
                    deposit.ClientLoanId = loan.Id;
                    context.Deposit.Update(deposit);
                    await context.SaveChangesAsync();
                }

                // Merge Comments to New Profile
                var oldClientComments = await context.ClientComment.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientComments = await context.ClientComment.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var comment in oldClientComments)
                {
                    comment.Id = 0;
                    comment.Idclient = newClientId;
                    await context.ClientComment.AddAsync(comment);
                    await context.SaveChangesAsync();
                }

                // Merge History to New Profile
                var oldClientHistory = await context.ClientHistory.Where(cr => cr.Idclient == oldClientId).ToListAsync();
                //var newClientHistory = await context.ClientHistory.Where(cr => cr.Idclient == newClientId).ToListAsync();
                foreach (var history in oldClientHistory)
                {
                    history.Id = 0;
                    history.Idclient = newClientId;
                    await context.ClientHistory.AddAsync(history);
                    await context.SaveChangesAsync();
                }

                // Create a record to Keep track of Merged Profiles
                foreach (var request in oldClientRequests)
                {
                    context.ClientMerge.Add(new ClientMerge()
                    {
                        IdclientOld = oldClientId,
                        IdclientNew = newClientId,
                        IdexternalLoanWebRequest = request.Id,
                        TimeStamp = DateTime.Now
                    });
                    await context.SaveChangesAsync();
                }

                // foreach (var loan in oldClientLoans)
                // {
                //     if (loan.Deposit.Count > 0)
                //     {
                //         var deposit = await context.Deposit.Where(cr => cr.ClientLoanId == loan.Id).SingleOrDefaultAsync();
                //         deposit.IsDeleted = true;
                //         context.Deposit.Update(deposit);
                //         await context.SaveChangesAsync();
                //     }
                // }

                await contextTransaction.CommitAsync();

                return newClientId;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
