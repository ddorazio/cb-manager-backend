using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class ClientCommentService : IClientCommentService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public ClientCommentService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<CommentLabelModel> AddLabelToClientComment(int clientCommentId, int commentLabelId)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = new ClientCommentLabel
            {
                IdclientComment = clientCommentId,
                IdcommentLabel = commentLabelId
            };
            await context.ClientCommentLabel.AddAsync(entity);
            await context.SaveChangesAsync();
            var label = await context.CommentLabel.FindAsync(commentLabelId);
            return new CommentLabelModel
            {
                Id = label.Id,
                Handle = label.Handle,
                Name = label.Name
            };
        }

        public async Task<ClientCommentModel> CreateClientComment(ClientCommentModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = new ClientComment
            {
                Idclient = data.ClientId,
                Title = data.Title,
                IsArchived = data.Archive,
                Description = data.Description,
                CreatedDateTime = data.Created ?? DateTime.UtcNow,
                CreatedBy = data.Username
            };
            await context.ClientComment.AddAsync(entity);
            await context.SaveChangesAsync();

            foreach (var label in data.Labels)
            {
                await context.ClientCommentLabel.AddAsync(new ClientCommentLabel
                {
                    IdclientComment = entity.Id,
                    IdcommentLabel = label.Id
                });
            }
            await context.SaveChangesAsync();

            data.Id = entity.Id;
            data.Created = entity.CreatedDateTime;
            return data;
        }

        public async Task DeleteClientComment(int clientCommentId, string deletedBy)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ClientComment.FindAsync(clientCommentId);
            entity.IsDeleted = true;
            entity.DeletedDateTime = DateTime.UtcNow;
            entity.DeletedBy = deletedBy;
            context.ClientComment.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ClientCommentModel>> GetClientComments(int clientId)
        {
            using var context = _dbContextProvider.GetContext();
            var comments = await context.ClientComment
                .Where(cc => cc.Idclient == clientId && 
                       !cc.IsDeleted)
                .Select(cc => new ClientCommentModel
                {
                    Id = cc.Id,
                    ClientId = cc.Idclient,
                    Title = cc.Title,
                    Description = cc.Description,
                    Created = cc.CreatedDateTime,
                    Username = cc.CreatedBy,
                    Archive = cc.IsArchived,
                    Modified = cc.ModifiedDateTime
                }).ToListAsync();
            foreach (var comment in comments)
            {
                var labelIds = await context.ClientCommentLabel
                    .Where(ccl => ccl.IdclientComment == comment.Id)
                    .Select(ccl => ccl.IdcommentLabel)
                    .ToListAsync();
                comment.Labels = await context.CommentLabel
                    .Where(cl => labelIds.Contains(cl.Id))
                    .Select(cl => new CommentLabelModel
                    {
                        Id = cl.Id,
                        Handle = cl.Handle,
                        Name = cl.Name
                    }).ToListAsync();
            }
            return comments;
        }

        public async Task<IEnumerable<CommentLabelModel>> GetCommentLabels()
        {
            using var context = _dbContextProvider.GetContext();
            return await context.CommentLabel.Select(cl => new CommentLabelModel
            {
                Id = cl.Id,
                Handle = cl.Handle,
                Name = cl.Name
            }).ToListAsync();
        }

        public async Task RemoveLabelFromClientComment(int clientCommentId, int commentLabelId)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ClientCommentLabel.SingleOrDefaultAsync(ccl =>
                ccl.IdclientComment == clientCommentId &&
                ccl.IdcommentLabel == commentLabelId);
            context.ClientCommentLabel.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task<ClientCommentModel> UpdateClientComment(ClientCommentModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ClientComment.FindAsync(data.Id);
            entity.Title = data.Title;
            entity.Description = data.Description;
            entity.IsArchived = data.Archive;
            entity.ModifiedDateTime = DateTime.UtcNow;
            context.ClientComment.Update(entity);
            await context.SaveChangesAsync();
            data.Modified = entity.ModifiedDateTime;
            return data;
        }
    }
}