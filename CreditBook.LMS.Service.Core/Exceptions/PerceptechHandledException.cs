using System;

namespace CreditBook.LMS.Service.Core.Exceptions
{
    public class PerceptechHandledException : Exception
    {
        public PerceptechHandledException(string message) : base(message) { }
    }
}