using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class LoanConfigurationService : ILoanConfigurationService
    {
        private readonly IDbContextProvider _dbContextProvider;

        public LoanConfigurationService(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task<LoanAmount> CreateLoanAmount(LoanAmount data)
        {
            using var context = _dbContextProvider.GetContext();
            context.LoanAmount.Add(data);
            await context.SaveChangesAsync();
            return data;
        }

        public async Task<LoanChargeConfigurationModel> CreateLoanChargeConfiguration(LoanChargeConfigurationModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var chargeId = (await context.LoanChargeType.SingleAsync(c => c.Code == data.Code)).Id;
            var entity = new LoanChargeConfiguration
            {
                IdloanChargeType = chargeId,
                AmountCharged = data.AmountCharged,
                EffectiveDate = data.EffectiveDate
            };
            await context.LoanChargeConfiguration.AddAsync(entity);
            await context.SaveChangesAsync();
            data.Id = entity.Id;
            return data;
        }

        public async Task<LoanConfigurationModel> CreateLoanConfiguration(LoanConfigurationModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var loanFrequencyId = (await context.LoanFrequency.SingleAsync(lf => lf.Code == data.LoanFrequencyCode)).Id;
            var loanAmountId = (await context.LoanAmount.SingleAsync(la => la.Amount == data.LoanAmount)).Id;
            var entity = new LoanConfiguration
            {
                IdloanFrequency = loanFrequencyId,
                IdloanAmount = loanAmountId,
                TotalPayments = data.TotalPayments.Value
            };
            context.LoanConfiguration.Add(entity);
            await context.SaveChangesAsync();
            data.Id = entity.Id;
            return data;
        }

        public async Task DeleteLoanAmount(int loanAmountId)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanAmount.FindAsync(loanAmountId);
            context.LoanAmount.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteLoanChargeConfiguration(int loanChargeConfigurationId)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanChargeConfiguration.FindAsync(loanChargeConfigurationId);
            context.LoanChargeConfiguration.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteLoanConfiguration(int loanConfigurationId)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanConfiguration.FindAsync(loanConfigurationId);
            context.LoanConfiguration.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<LoanAmount>> GetLoanAmounts()
        {
            using var context = _dbContextProvider.GetContext();
            return await context.LoanAmount.ToListAsync();
        }

        public async Task<IEnumerable<LoanChargeConfigurationModel>> GetLoanChargeConfigurations()
        {
            using var context = _dbContextProvider.GetContext();
            return await (from conf in context.LoanChargeConfiguration
                          join type in context.LoanChargeType on conf.IdloanChargeType equals type.Id
                          select new LoanChargeConfigurationModel
                          {
                              Id = conf.Id,
                              Code = type.Code,
                              AmountCharged = conf.AmountCharged,
                              EffectiveDate = conf.EffectiveDate
                          }).ToListAsync();
        }

        public async Task<IEnumerable<LoanConfiguration>> GetLoanConfigurations()
        {
            using var context = _dbContextProvider.GetContext();
            return await context.LoanConfiguration.ToListAsync();
        }

        public async Task<IEnumerable<LoanFrequencyModel>> GetLoanFrequencies()
        {
            using var context = _dbContextProvider.GetContext();
            var result = await context.LoanFrequency.ToListAsync();
            return result.Select(freq => new LoanFrequencyModel
            {
                Id = freq.Id,
                Code = freq.Code,
                MinimumDaysBeforeDeposit = freq.MinimumDaysBeforeDeposit
            });
        }

        public async Task<LoanChargeConfigurationModel> UpdateLoanChargeConfiguration(LoanChargeConfigurationModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanChargeConfiguration.FindAsync(data.Id);
            entity.EffectiveDate = data.EffectiveDate;
            entity.AmountCharged = data.AmountCharged;
            context.LoanChargeConfiguration.Update(entity);
            await context.SaveChangesAsync();
            return data;
        }

        public async Task<LoanConfigurationModel> UpdateLoanConfiguration(LoanConfigurationModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanConfiguration.FindAsync(data.Id);
            entity.TotalPayments = data.TotalPayments.Value;
            context.LoanConfiguration.Update(entity);
            await context.SaveChangesAsync();
            return data;
        }

        public async Task<LoanFrequencyModel> UpdateLoanFrequency(LoanFrequencyModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.LoanFrequency.FindAsync(data.Id);
            entity.MinimumDaysBeforeDeposit = data.MinimumDaysBeforeDeposit;
            context.Update(entity);
            await context.SaveChangesAsync();
            return data;
        }
    }
}