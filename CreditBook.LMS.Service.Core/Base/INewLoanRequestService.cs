﻿using System;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models.External;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IExternalLoanRequestService
    {
        Task<bool> CreateLoanRequest(NewLoanFormRequestModel model);
        Task<string> NonCompletedRequest(NonCompletedRequestModel model);
        Task<bool> AutoDecline(NewLoanFormRequestModel model);
    }
}
