using System;
using System.Threading.Tasks;

namespace CreditBook.LMS.External.Core
{
    public interface IErrorService
    {
        void Logger(Exception ex, string model);
        void LoanRequestLogger(string model);
    }
}