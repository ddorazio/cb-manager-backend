﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IContractService
    {
        Task<bool> VerifyContractSigned(string contractPath, string digitalUrl, int loanId, string emailAddress);
        Task<ContractModel> GenerateLoanContract(string contractPath, string baseUrl, string digitalUrl, string contractFilePath, int loanId);
    }
}
