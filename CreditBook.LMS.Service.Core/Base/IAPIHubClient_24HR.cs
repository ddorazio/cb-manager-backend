using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IAPIHubClient_24HR
    {
        Task PostAsync(string path, object payload);
    }
}