using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface ILoanConfigurationService
    {
        Task<IEnumerable<LoanAmount>> GetLoanAmounts();
        Task<LoanAmount> CreateLoanAmount(LoanAmount data);
        Task DeleteLoanAmount(int loanAmountId);
        Task<IEnumerable<LoanFrequencyModel>> GetLoanFrequencies();
        Task<IEnumerable<LoanConfiguration>> GetLoanConfigurations();
        Task<LoanConfigurationModel> CreateLoanConfiguration(LoanConfigurationModel data);
        Task<LoanConfigurationModel> UpdateLoanConfiguration(LoanConfigurationModel data);
        Task<IEnumerable<LoanChargeConfigurationModel>> GetLoanChargeConfigurations();
        Task<LoanChargeConfigurationModel> CreateLoanChargeConfiguration(LoanChargeConfigurationModel data);
        Task DeleteLoanChargeConfiguration(int loanChargeConfigurationId);
        Task<LoanChargeConfigurationModel> UpdateLoanChargeConfiguration(LoanChargeConfigurationModel data);
        Task<LoanFrequencyModel> UpdateLoanFrequency(LoanFrequencyModel data);
        Task DeleteLoanConfiguration(int loanConfigurationId);
    }
}