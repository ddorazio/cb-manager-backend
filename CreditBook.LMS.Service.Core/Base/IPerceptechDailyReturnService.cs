﻿using System;
using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IPerceptechDailyReturnService
    {
        Task UpdateLoanPayments(string dailyRejectionReportFolder, string data, string language, string lmsEndpoint);
        public Task SendDailyRejectionReporErrorEmail(string apiKey);
        public Task<bool> ValidateReportReceived(DateTime rejectionDate);
        public Task UpdatePerecptechRejectionLog(DateTime rejectionDate, bool received, bool empty);
        public string GetLanguage();
    }
}
