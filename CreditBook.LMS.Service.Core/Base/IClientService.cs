using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.CreditBookCheck;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IClientService
    {
        IEnumerable<ClientSummaryModel> GetClientSummaryList(string language);
        Task<ClientModel> GetClientInfo(int clientId);
        ClientModel NewClientData(string language);
        ClientModel SaveNewClient(ClientModel model);
        Task<ClientModel> GetClient(int clientId, string language, string digitalBaseUrl);

        Task<IEnumerable<ClientReferenceModel>> GetClientReferences(int clientId);
        Task<ClientReferenceModel> UpdateClientReference(ClientReferenceModel reference);
        Task<ClientReferenceModel> CreateClientReference(ClientReferenceModel reference);
        Task DeleteClientReference(int refId);

        ClientIBVModel SaveNewIBV(int clientId, string requestId, string Url, string language);
        Task<bool> UpdateIBVStatusStart(string requestId);
        Task<bool> UpdateIBVStatusCompleted(string requestId);

        Task<ClientModel> UpdateClient(ClientModel data);

        bool DeleteClient(int id);
        Task<IEnumerable<ClientLoanModel>> GetClientLoans(int clientId);

        Task<IBVModel> GetIBV(int ibvId);

        Task<List<IBVModel>> GetAllIBV();
        Task<List<ReturnSearchModel>> FormatCreditBookSearchResults(ResponseSearchModel response, string language);
        Task<int> MergeClientProfile(int oldClientId, int newClientId);
    }
}