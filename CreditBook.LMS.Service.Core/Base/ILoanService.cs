﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.EmailTemplates;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface ILoanService
    {
        ClientLoanModel NewLoan(NewLoanModel model, string language);
        ClientLoanModel LoanManualNSFPayment(LoanGenericPaymentModel model, string language, bool perceptech);
        ClientLoanModel LoanRemoveManualNSFPayment(LoanGenericPaymentModel model, string language);

        ClientLoanModel LoanAddRebate(LoanRebateModel model, string language);
        ClientLoanModel LoanManualPayment(LoanPaymentModel model, string language);

        ClientLoanModel LoanRemoveRebateAndManualPayment(LoanGenericPaymentModel model, string language);

        ClientLoanModel LoanModifyPayment(LoanModifyPaymentModel model, bool saveToDatabse, string language);
        ClientLoanModel ModifyLoan(LoanModifyModel model, string language);
        ClientLoanModel RestartLoan(LoanRestartModel model, string language);
        decimal CalculateTotalAmountRemaining(LoanModifyPaymentModel model, string language);
        Task<ClientLoanModel> DeferPayment(LoanDelayPaymentModel model, string language);
        Task<List<LoanRequestModel>> DeleteLoan(int loanId, string language);

        Task<LoanPDFModel> GetAccountBalance(int loanId, string language);

        EmailTemplateModel SendLoanPDF(PDFEmailModel model, string pdfPath, string serialNumber);

        Task<ClientLoanModel> StoppedLoanPayment(StoppedLoanPaymentModel model, string language);

        // This is not used but commented in case client changes their mind
        //ClientLoanModel UpdateLoanStatus(int loanId, string language);
    }
}
