using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Domain.Core.Models.External;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IPaymentAgreement
    {
        Task<List<InteracPaymentAgreementModel>> InteracPaymentAgreement();
        Task<InteracPaymentAgreementModel> UpdateAgreement(InteracPaymentAgreementModel model);
    }
}
