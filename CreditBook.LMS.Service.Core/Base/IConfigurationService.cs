using System;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.External.Core
{
    public interface IConfigurationService
    {

        Task<LenderLoanConfigurationModel> LoadLenderLoanConfiguration();
        bool ValidateMixedLoans();
        bool ValidatePaymentProvider();
        DateTime ValidateMixedLoanDate();
        bool ValidateMembershipFee();
        bool ValidateWebhooksStatus_24HR();
        bool ValidateLenderIs_24HR();
        decimal DeferredFees();
        decimal NsfFees();
        decimal WithdrawalFees();
    }
}