﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IPerceptechService
    {
        public Task ValidateLoanPayments(string apiKey, string dailyReportUploadFolder, string dailyReportRemovedLinesFolder, string clientNumber, string company, string fileVersion, string processingCenter);
        public PerceptechUploadModel ProgramLoanPayments(string apiKey, string dailyReportUploadFolder, string dailyReportRemovedLinesFolder, string clientNumber, string company, string fileVersion, string processingCenter);
        //public void UpdateLoanPayments(string dailyRejectionReportFolder, string data, string language);
        //public string GetLanguage();
        public Task LogUploadFailedUpload(string apiKey, string dailyReportUploadErrorFolder);
        public Task LogUploadErrors(string result, string uploadFileName, string apiKey, string dailyReportUploadErrorFolder);
        public Task LogSubmittedDate();
    }
}
