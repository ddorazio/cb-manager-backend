using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core.Base
{
    public interface IWebhookService_24HR
    {
        Task NewClientAdded_24HR(ExternalLoanWebRequest model);
        Task IBVStatusChange_24HR(ClientIbv model);
        Task RequestStatusChange_24HR(LoanRequestModel model);
    }
}