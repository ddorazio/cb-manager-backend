using System;
using System.IO;
using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core
{
    // Implements stream I/O with Amazon's S3 service
    public interface IS3StreamService
    {
        Task<Stream> GetDownloadStreamAsync(string key);
        Task UploadAsync(string key, Stream stream);
    }
}