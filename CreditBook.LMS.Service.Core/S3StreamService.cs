using System;
using System.IO;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace CreditBook.LMS.Service.Core
{
    public class S3StreamService : IS3StreamService
    {
        private readonly IS3ClientProvider _s3ClientProvider;
        private readonly string _bucketName;

        public S3StreamService(IS3ClientProvider s3ClientProvider, string bucketName)
        {
            _s3ClientProvider = s3ClientProvider;
            _bucketName = bucketName;
        }

        public async Task<Stream> GetDownloadStreamAsync(string key)
        {
            using var client = _s3ClientProvider.GetClient();
            var response = await client.GetObjectAsync(_bucketName, key);
            return response.ResponseStream;
        }

        public async Task UploadAsync(string key, Stream stream)
        {
            using var client = _s3ClientProvider.GetClient();
            using var transferUtility = new TransferUtility(client);

            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = stream,
                Key = key,
                BucketName = _bucketName,
                CannedACL = S3CannedACL.AuthenticatedRead
            };

            await transferUtility.UploadAsync(uploadRequest);
        }
    }
}