﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CreditBook.LMS.Service.Core.Hubs
{
    public class NotificationsHub : Hub
    {
        public async Task SendMessage(string type)
        {
            await Clients.All.SendAsync("NewNotification", type);
        }
    }
}
