using Amazon;
using Amazon.S3;

namespace CreditBook.LMS.Service.Core
{
    public interface IS3ClientProvider
    {
        IAmazonS3 GetClient();
    }

    public class S3ClientProvider : IS3ClientProvider
    {
        private readonly string _accessKeyId;
        private readonly string _secretKey;

        public S3ClientProvider(string awsAccessKeyId, string awsSecretAccessKey)
        {
            _accessKeyId = awsAccessKeyId;
            _secretKey = awsSecretAccessKey;
        }

        public IAmazonS3 GetClient()
        {
            return new AmazonS3Client(_accessKeyId, _secretKey, RegionEndpoint.USEast1);
        }
    }
}