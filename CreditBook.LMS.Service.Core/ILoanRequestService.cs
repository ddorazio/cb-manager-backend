using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface ILoanRequestService
    {
        Task<IEnumerable<LoanRequestModel>> GetLoanRequestList(int? clientId = null, string statusId = null);
        Task<IEnumerable<LoanRequestModel>> GetLoanRequestClientList(int clientId, LanguageEnum language);
        Task<LoanRequestModel> UpdateRequestChecklist(LoanRequestModel model);
        Task<LoanRequestModel> GetLoanRequest(int requestId);
        Task<LoanRequestModel> UpdateLoanRequest(LoanRequestModel model);
    }
}