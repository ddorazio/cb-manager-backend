using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Enums;
using CreditBook.LMS.Domain.Core.Models;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Logic;
using Microsoft.EntityFrameworkCore;

namespace CreditBook.LMS.Service.Core
{
    public class CollectionService : ICollectionService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IMessageService _messageService;

        public CollectionService(IDbContextProvider dbContextProvider, IMessageService messageService)
        {
            _dbContextProvider = dbContextProvider;
            _messageService = messageService;
        }

        public async Task DeleteComment(int collectionCommentId)
        {
            var context = _dbContextProvider.GetContext();
            var entity = await context.ClientLoanCollectionComment.FindAsync(collectionCommentId);
            entity.IsDeleted = true;
            context.ClientLoanCollectionComment.Update(entity);
            await context.SaveChangesAsync();
        }

        public async Task<CollectionModel> GetCollection(int collectionId)
        {
            using var context = _dbContextProvider.GetContext();
            // Note : using EF navigation properties seems to not work sometimes
            // So this slower approach is used just to be safe
            var collection = await context.ClientLoanCollection
                .SingleOrDefaultAsync(clc => clc.Id == collectionId);
            if (collection == null) return null;
            var loan = await context.ClientLoan
                .SingleOrDefaultAsync(l => l.Id == collection.IdclientLoan);
            var loanStatus = await context.LoanStatus
                .SingleOrDefaultAsync(ls => ls.Id == loan.IdloanStatus);
            var collectionStatus = await context.CollectionStatus
                .SingleOrDefaultAsync(cs => cs.Id == collection.IdcollectionStatus);
            var degree = await context.CollectionDegree
                .SingleOrDefaultAsync(cd => cd.Id == collection.IdcollectionDegree);
            var nonPayment = await context.NonPaymentReason
                .SingleOrDefaultAsync(np => np.Id == collection.IdnonPaymentReason);
            var agreement = await context.CollectionPaymentAgreement
                .SingleOrDefaultAsync(cpa => cpa.Id == collection.IdpaymentAgreement);
            var procedure = await (
                                from proc in context.CollectionProcedureStep
                                from step in context.ClientLoanCollectionProcedureStep
                                    .Where(s => s.IdclientLoanCollection == collection.Id)
                                    .Where(s => s.IdcollectionProcedureStep == proc.Id)
                                    .DefaultIfEmpty()
                                select new CollectionProcedureStepModel
                                {
                                    Id = step != null ? step.Id : (int?)null,
                                    CollectionId = collection.Id,
                                    ProcedureStepId = proc.Id,
                                    Order = proc.Order.Value,
                                    Description = proc.DescriptionFr,
                                    Checked = step.Checked
                                }
                            ).ToListAsync();
            var lastPayment = await context.ClientLoanPayment
                    .Where(p => p.IdclientLoan == loan.Id
                             && p.IdpaymentStatus != (int)PaymentStatusEnum.Pending
                             && p.IdpaymentStatus != (int)PaymentStatusEnum.Nsf
                             && p.IdpaymentStatus != (int)PaymentStatusEnum.StopPayment)
                    .OrderByDescending(p => p.PaymentDate)
                    .FirstOrDefaultAsync();
            return new CollectionModel
            {
                Id = collection.Id,
                LoanNumber = loan.LoanNumber.Value,
                LoanStatusCode = loanStatus.Code,
                LastPaymentDate = lastPayment?.PaymentDate,
                CollectionStatusCode = collectionStatus.Code,
                CollectionDegreeCode = degree.Code,
                NonPaymentReasonCode = nonPayment?.Code,
                PaymentAgreementCode = agreement?.Code,
                Procedure = procedure,
                FollowUpDate = collection.FollowUpDate,
                InteracPlannedPaymentDate = collection.InteracPlannedPaymentDate,
                InteracRealPaymentDate = collection.InteracRealPaymentDate,
                InteracConfirmationEmail = collection.InteracConfirmationEmail,
                InteracSmsReminderOnPaymentDate = collection.InteracSmsReminderOnPaymentDate
            };
        }

        public async Task<IEnumerable<CollectionSummaryModel>> GetCollectionList(CollectionStatusEnum? status = null)
        {
            using var context = _dbContextProvider.GetContext();

            var collectionsId = await context.ClientLoanCollection.ToListAsync();
            var loans = await context.ClientLoan.ToListAsync();

            var collections = await (from collection in context.ClientLoanCollection
                                     join loan in context.ClientLoan on collection.IdclientLoan equals loan.Id
                                     join client in context.Client on loan.Idclient equals client.Id
                                     select new CollectionSummaryModel
                                     {

                                         Id = collection.Id,
                                         ClientId = client.Id,

                                         CollectionDegreeCode = (
                                                 from degree in context.CollectionDegree
                                                 where degree.Id == collection.IdcollectionDegree
                                                 select degree.Code).FirstOrDefault(),
                                         FollowUpDate = collection.FollowUpDate,
                                         InteracPaymentDate = collection.InteracPlannedPaymentDate,
                                         NonPaymentReasonCode = (
                                                 from nonPayment in context.NonPaymentReason
                                                 where nonPayment.Id == collection.IdnonPaymentReason
                                                 select nonPayment.Code).FirstOrDefault(),
                                         ClientFirstName = client.FirstName,
                                         ClientLastName = client.LastName,
                                         ClientCellPhoneNumber = String.Format("{0:(###) ###-####}", Convert.ToInt64(client.CellPhoneNumber.Replace(" ", ""))),
                                         ClientEmailAddress = (
                                                 from email in context.ClientEmailAddress
                                                 where email.Idclient == client.Id && email.IsPrimary == true
                                                 select email.EmailAddress).FirstOrDefault(),
                                         LastComment = (
                                                 from clcc in context.ClientLoanCollectionComment
                                                 where clcc.IdclientLoanCollection == collection.Id
                                                 orderby clcc.CreatedDateTime descending
                                                 select clcc.Description)
                                                 .FirstOrDefault(),

                                         LastPaymentDate = (
                                                 from payment in context.ClientLoanPayment
                                                 where loan.Id == payment.IdclientLoan
                                                 && payment.IdpaymentStatus == (int)PaymentStatusEnum.Paid
                                                 orderby payment.PaymentDate descending
                                                 select payment.PaymentDate).FirstOrDefault(),

                                     }).ToListAsync();


            if (status != null)
            {

                collections = await (from collection in context.ClientLoanCollection
                                     where collection.IdcollectionStatus == (int)status.Value
                                     join loan in context.ClientLoan on collection.IdclientLoan equals loan.Id
                                     join client in context.Client on loan.Idclient equals client.Id
                                     select new CollectionSummaryModel
                                     {

                                         Id = collection.Id,
                                         ClientId = client.Id,
                                         CollectionDegreeCode = (
                                                  from degree in context.CollectionDegree
                                                  where degree.Id == collection.IdcollectionDegree
                                                  select degree.Code).FirstOrDefault(),
                                         FollowUpDate = collection.FollowUpDate,
                                         InteracPaymentDate = collection.InteracPlannedPaymentDate,
                                         NonPaymentReasonCode = (
                                                  from nonPayment in context.NonPaymentReason
                                                  where nonPayment.Id == collection.IdnonPaymentReason
                                                  select nonPayment.Code).FirstOrDefault(),
                                         ClientFirstName = client.FirstName,
                                         ClientLastName = client.LastName,
                                         ClientCellPhoneNumber = String.Format("{0:(###) ###-####}", Convert.ToInt64(client.CellPhoneNumber.Replace(" ", ""))),
                                         ClientEmailAddress = (
                                                  from email in context.ClientEmailAddress
                                                  where email.Idclient == client.Id && email.IsPrimary == true
                                                  select email.EmailAddress).FirstOrDefault(),
                                         LastComment = (
                                                  from clcc in context.ClientLoanCollectionComment
                                                  where clcc.IdclientLoanCollection == collection.Id
                                                  orderby clcc.CreatedDateTime descending
                                                  select clcc.Description)
                                                  .FirstOrDefault(),

                                         LastPaymentDate = (
                                                  from payment in context.ClientLoanPayment
                                                  where loan.Id == payment.IdclientLoan
                                                  && payment.IdpaymentStatus == (int)PaymentStatusEnum.Paid
                                                  orderby payment.PaymentDate descending
                                                  select payment.PaymentDate).FirstOrDefault(),

                                     }).ToListAsync();
            }

            //collections = collections.Where(x => x.LastPaymentDate != null).ToList();

            foreach (var collection in collections)
            {
                var collectId = (from collect in collectionsId where collect.Id == collection.Id select collect.IdclientLoan).FirstOrDefault();
                var id = (from loan in loans where collectId == loan.Id select loan.Id).FirstOrDefault();
                collection.Amount = await CalculateBalance(id);
            }
            return collections;
        }

        public async Task<IEnumerable<CollectionCommentModel>> GetComments(int collectionId)
        {
            var context = _dbContextProvider.GetContext();
            var comments = await context.ClientLoanCollectionComment
                .Where(c => c.IdclientLoanCollection == collectionId && !c.IsDeleted)
                .ToListAsync();
            return comments.Select(c => new CollectionCommentModel
            {
                Id = c.Id,
                CollectionId = c.IdclientLoanCollection,
                Created = c.CreatedDateTime,
                Username = c.CreatedBy,
                Modified = c.ModifiedDateTime,
                Description = c.Description,
                Title = c.Title,
                Archive = c.IsArchived
            });
        }

        public async Task<CollectionCommentModel> SaveComment(CollectionCommentModel data)
        {
            var context = _dbContextProvider.GetContext();
            ClientLoanCollectionComment comment;

            if (data.Id != null)
            {
                comment = await context.ClientLoanCollectionComment.FindAsync(data.Id);
                comment.ModifiedDateTime = DateTime.UtcNow;
                comment.Description = data.Description;
                comment.Title = data.Title;
                comment.IsArchived = data.Archive;
            }
            else
            {
                comment = new ClientLoanCollectionComment
                {
                    IdclientLoanCollection = data.CollectionId,
                    CreatedBy = data.Username,
                    Description = data.Description,
                    Title = data.Title,
                    IsArchived = data.Archive
                };
            }

            context.ClientLoanCollectionComment.Update(comment);
            await context.SaveChangesAsync();
            data.Id = comment.Id;
            data.Created = comment.CreatedDateTime;
            data.Modified = comment.ModifiedDateTime;
            return data;
        }

        public async Task SendCollectionReminders(IEnumerable<int> collectionIds)
        {
            using var context = _dbContextProvider.GetContext();
            foreach (var collectionId in collectionIds)
            {
                var result = await (from collection in context.ClientLoanCollection
                                    join loan in context.ClientLoan on collection.IdclientLoan equals loan.Id
                                    join client in context.Client on loan.Idclient equals client.Id
                                    join email in context.ClientEmailAddress on client.Id equals email.Idclient
                                    where collection.Id == collectionId
                                    select new
                                    {
                                        LoanId = loan.Id,
                                        LanguageId = (LanguageEnum)client.Idlanguage,
                                        EmailAddress = collection.InteracConfirmationEmail ?? email.EmailAddress,
                                        DueDate = collection.InteracPlannedPaymentDate
                                    }).SingleOrDefaultAsync();
                if (result == null || result.DueDate == null) continue;
                var amountDue = await CalculateBalance(result.LoanId);
                await _messageService.SendInteracPaymentReminder(result.LanguageId, result.EmailAddress, result.DueDate.Value, amountDue);
            }
        }

        public async Task<CollectionProcedureStepModel> ToggleProcedureStep(CollectionProcedureStepModel step)
        {
            using var context = _dbContextProvider.GetContext();

            ClientLoanCollectionProcedureStep entity;
            if (step.Id != null)
            {
                entity = await context.ClientLoanCollectionProcedureStep.FindAsync(step.Id);
            }
            else
            {
                entity = new ClientLoanCollectionProcedureStep
                {
                    IdclientLoanCollection = step.CollectionId,
                    IdcollectionProcedureStep = step.ProcedureStepId
                };
            }
            entity.Checked = !step.Checked;
            step.Checked = !step.Checked;

            context.ClientLoanCollectionProcedureStep.Update(entity);
            await context.SaveChangesAsync();

            return step;
        }

        public async Task<CollectionModel> UpdateCollection(CollectionModel data)
        {
            using var context = _dbContextProvider.GetContext();
            var entity = await context.ClientLoanCollection.SingleAsync(c => c.Id == data.Id);

            if (!string.IsNullOrEmpty(data.CollectionStatusCode))
            {
                var degree = await context.CollectionStatus.SingleAsync(cd => cd.Code == data.CollectionStatusCode);
                entity.IdcollectionStatus = degree.Id;

                // Set Client back to In Progress once Collections has been resolved
                if (data.CollectionStatusCode == "SETTLED")
                {
                    var clientLoan = await context.ClientLoan.SingleOrDefaultAsync(cl => cl.Id == entity.IdclientLoan);
                    var client = await context.Client.SingleOrDefaultAsync(c => c.Id == Convert.ToInt32(clientLoan.Idclient));
                    var clientStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Code == "IN_PROGRESS");
                    client.IdclientStatus = clientStatus.Id;
                    context.Client.Update(client);
                    await context.SaveChangesAsync();
                }
            }
            if (!string.IsNullOrEmpty(data.NonPaymentReasonCode))
            {
                var npr = await context.NonPaymentReason.SingleAsync(n => n.Code == data.NonPaymentReasonCode);
                entity.IdnonPaymentReason = npr.Id;
            }
            if (!string.IsNullOrEmpty(data.PaymentAgreementCode))
            {
                var agreement = await context.CollectionPaymentAgreement.SingleAsync(pa => pa.Code == data.PaymentAgreementCode);
                entity.IdpaymentAgreement = agreement.Id;

                // Set Client Status to Collections Payment Agreement
                var clientLoan = await context.ClientLoan.SingleOrDefaultAsync(cl => cl.Id == entity.IdclientLoan);
                var client = await context.Client.SingleOrDefaultAsync(c => c.Id == Convert.ToInt32(clientLoan.Idclient));
                var clientStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Code == "COLLECTIONS_AGREEMENT");
                client.IdclientStatus = clientStatus.Id;
                context.Client.Update(client);
                await context.SaveChangesAsync();
            }

            entity.FollowUpDate = data.FollowUpDate;
            entity.InteracPlannedPaymentDate = data.InteracPlannedPaymentDate;
            entity.InteracRealPaymentDate = data.InteracRealPaymentDate;
            entity.InteracConfirmationEmail = data.InteracConfirmationEmail;
            entity.InteracSmsReminderOnPaymentDate = data.InteracSmsReminderOnPaymentDate;

            context.ClientLoanCollection.Update(entity);
            await context.SaveChangesAsync();

            return data;
        }

        private async Task<decimal> CalculateBalance(int loanId)
        {
            using var context = _dbContextProvider.GetContext();
            var loan = await context.ClientLoan.FindAsync(loanId);
            var frequency = await context.LoanFrequency.FindAsync(loan.IdloanFrequency);
            var payments = await context.ClientLoanPayment
                .Where(p => p.IdclientLoan == loan.Id)
                .ToListAsync();

            // Take InterestRate from ClientLoan once Loan is created
            var interestRate = (decimal)loan.InterestRate;
            // var interestRate = decimal.Parse(
            //     (await context.LoanerConfiguration.SingleAsync(lc => lc.Key == "INTEREST_RATE")).Value);

            decimal result = 0;
            var firstPaymentDate = payments.OrderBy(p => p.PaymentDate).First().PaymentDate;

            decimal interestRateToDecimal = interestRate / 100;
            decimal convertedInterest = interestRateToDecimal / frequency.Frequency.Value;
            decimal dailyInterest = interestRateToDecimal / 365;

            if (payments.Where(x => x.PaymentDate.Value.Date <= DateTime.Today).Count() > 0)
            {
                DateTime? previousDate = null;
                decimal previousBalance = 0;
                foreach (var payment in payments.Where(x => x.IsDeleted == false))
                {
                    if (payment.PaymentDate.Value.Date <= DateTime.Today)
                    {
                        previousDate = payment.PaymentDate;
                        previousBalance = payment.Balance.Value;
                    }
                    else
                    {
                        break;
                    }
                }

                var intervalDays = (previousDate != null) ? (DateTime.Today - previousDate.Value.Date).Days : 14;
                var intervalInterest = (dailyInterest * intervalDays) * previousBalance;

                result = previousBalance + intervalInterest;
            }
            else if (DateTime.Today <= firstPaymentDate)
            {
                var intervalDays = (DateTime.Today - (DateTime)loan.CreatedDate.Value.Date).Days;
                var intervalInterest = (dailyInterest * intervalDays) * loan.Amount.GetValueOrDefault();

                result = loan.Amount.Value + intervalInterest;
            }
            return result;
        }
    }
}