﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditBook.LMS.Data.Core.Models;
using CreditBook.LMS.Domain.Core.Models.External;
using CreditBook.LMS.External.Core;
using CreditBook.LMS.Repository.Core.Base;
using CreditBook.LMS.Service.Core.Base;
using CreditBook.LMS.Service.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CreditBook.LMS.Service.Core.External
{
    public class ExternalLoanRequestService : IExternalLoanRequestService
    {
        private readonly IDbContextProvider _dbContextProvider;
        private readonly IClientHistoryService _clientHistoryService;
        private readonly IErrorService _errorService;
        private readonly IWebhookService_24HR _webhookService24HR;

        public ExternalLoanRequestService(IDbContextProvider dbContextFactory, IClientHistoryService clientHistoryService, IErrorService errorService, IWebhookService_24HR webhookService24HR)
        {
            _dbContextProvider = dbContextFactory;
            _clientHistoryService = clientHistoryService;
            _errorService = errorService;
            _webhookService24HR = webhookService24HR;
        }

        public async Task<bool> CreateLoanRequest(NewLoanFormRequestModel model)
        {
            var newClient = new Client();

            using (var context = _dbContextProvider.GetContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        // Get Language
                        var language = context.Language
                                                .Where(x => x.Code == model.LanguageCode)
                                                .SingleOrDefault();

                        // Get Income Type
                        var incomeType = context.IncomeType
                                                .Where(x => x.Code == model.IncomeTypeCode)
                                                .SingleOrDefault();

                        // Get Pay Frequency
                        var payFrequency = context.PayFrequency
                                            .Where(x => x.Code == model.PayFrequencyCode)
                                            .SingleOrDefault();

                        // Get Province
                        var province = context.Province
                                            .Where(x => x.Code == model.Province)
                                            .SingleOrDefault();

                        var housingStatus = context.HousingStatus.ToList();

                        // Always create new client and detect if matches exist for potential match
                        var clientStatus = await context.ClientStatus.SingleOrDefaultAsync(cs => cs.Code == "NEW");
                        newClient.IdclientStatus = clientStatus.Id;
                        newClient.IsActive = true;
                        newClient.FirstName = model.FirstName;
                        newClient.LastName = model.LastName;
                        newClient.CellPhoneNumber = model.CellPhoneNumber;
                        newClient.DateOfBirth = model.Birthday;
                        newClient.Idlanguage = language == null ? (int?)null : language.Id;
                        newClient.ProfileMerged = false;

                        newClient.ClientAddress.Add(new ClientAddress
                        {
                            CivicNumber = model.CivicNumber,
                            StreetName = model.StreetName,
                            PostalCode = model.PostalCode,
                            City = model.City,
                            Idprovince = province == null ? (int?)null : province.Id,
                            Country = "CA",
                            IsPrimary = true,
                            StartOfResidencyDate = model.ResidenceStartDate
                        });

                        newClient.ClientEmailAddress.Add(new ClientEmailAddress
                        {
                            EmailAddress = model.PrimaryEmail,
                            IsPrimary = true
                        });

                        newClient.ClientEmploymentStatus.Add(new ClientEmploymentStatus
                        {
                            EmployerName = model.EmployerName,
                            SupervisorName = model.SupervisorName,
                            PhoneNumber = model.EmployerPhoneNumber,
                            Extension = model.EmployerExtension,
                            Occupation = model.Occupation,
                            HiringDate = model.HiringDate,
                            IdpayFrequency = payFrequency == null ? (int?)null : payFrequency.Id,
                            NextPayDate = model.NextPayDate
                        });

                        newClient.ClientReference.Add(new ClientReference
                        {
                            FirstName = model.Ref1FirstName,
                            LastName = model.Ref1LastName,
                            PhoneNumber = model.Ref1Telephone,
                            Relation = model.Ref1Relationship
                        });

                        newClient.ClientReference.Add(new ClientReference
                        {
                            FirstName = model.Ref2FirstName,
                            LastName = model.Ref2LastName,
                            PhoneNumber = model.Ref2Telephone,
                            Relation = model.Ref2Relationship
                        });

                        // Add Payment amounts
                        if (model.IsRenter != null)
                        {
                            newClient.ClientFinancialStatus = new ClientFinancialStatus()
                            {
                                HousingStatusId = (bool)model.IsRenter ? housingStatus.Where(x => x.Code == "TENANT").SingleOrDefault().Id
                                                                    : housingStatus.Where(x => x.Code == "OWNER").SingleOrDefault().Id,
                                GrossAnnualRevenue = Convert.ToDecimal(model.GrossAnnualSalary),
                                MonthlyCarPayment = Convert.ToDecimal(model.MonthlyCarPayment),
                                MonthlyHousingCost = Convert.ToDecimal(model.MonthlyPaymenet),
                                MonthlyUtilityCost = Convert.ToDecimal(model.MonthlyElectricalPayment),
                                MonthlyOtherLoanPayment = Convert.ToDecimal(model.MontlyAppliancePayment)
                            };
                        }

                        // Add Client Social
                        if (!string.IsNullOrEmpty(model.FacebookId) || !string.IsNullOrEmpty(model.GoogleId))
                        {
                            newClient.ClientSocial.Add(new ClientSocial()
                            {
                                GoogleId = model.GoogleId,
                                FacebookId = model.FacebookId,
                                GooglePhoto = model.GooglePhoto,
                                FacebookPhoto = model.FacebookPhoto
                            });
                        }

                        await context.Client.AddAsync(newClient);
                        await context.SaveChangesAsync();

                        // Add Client Revenue Source
                        await context.ClientRevenueSource.AddAsync(new ClientRevenueSource()
                        {
                            ClientId = newClient.Id,
                            IncomeTypeId = incomeType != null ? incomeType.Id : (int?)null,
                            NextPayDate = model.NextPayDate,
                            NextDepositDate = model.NextDepositDate,
                            EmploymentInsuranceNextPayDate = model.EmploymentInsuranceNextPayDate,
                            EmploymentInsuranceStart = model.EmploymentInsuranceStartDate,
                            EmploymentNextPay = incomeType.Code == "SELF_EMPLOYED" ? model.NextDepositDate : model.EmploymentInsuranceNextPayDate,
                            SelfEmployedDirectDeposit = incomeType.Code == "SELF_EMPLOYED" ? model.DirectDeposit : (bool?)null,
                            SelfEmployedPhoneNumber = model.SelfEmployedWorkTelephone,
                            SelfEmployedPayFrequencyId = incomeType.Code == "SELF_EMPLOYED"
                                                                             ? payFrequency != null ? payFrequency.Id : (int?)null
                                                                             : null,
                            SelfEmployedStartDate = model.SelfEmployedStartDate
                        });
                        await context.SaveChangesAsync();

                        // Validate if the client already exixts
                        List<Client> clientFound = null;
                        bool potentialMatch = false;

                        // Check for perfect match
                        clientFound = (from c in context.Client
                                       join ca in context.ClientAddress on c.Id equals ca.Idclient
                                       join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                       where c.FirstName.Trim().Replace(" ", "").ToUpper() == model.FirstName.Trim().Replace(" ", "").ToUpper()
                                       && c.LastName.Trim().Replace(" ", "").ToUpper() == model.LastName.Trim().Replace(" ", "").ToUpper()
                                       && c.CellPhoneNumber == model.CellPhoneNumber
                                       && cea.EmailAddress.Trim().Replace(" ", "").ToUpper() == model.PrimaryEmail.Trim().Replace(" ", "").ToUpper()
                                       && cea.IsPrimary == true
                                       && c.Id != newClient.Id
                                       && c.ProfileMerged == false
                                       && c.IsDeleted == false
                                       select c)
                                          .ToList();
                        potentialMatch = clientFound.Count == 0 ? false : true;

                        // 1: Check for Phone Number match
                        if (clientFound.Count == 0)
                        {
                            clientFound = (from c in context.Client
                                           where c.CellPhoneNumber == model.CellPhoneNumber
                                           && c.Id != newClient.Id
                                           && c.ProfileMerged == false
                                           && c.IsDeleted == false
                                           select c)
                                          .ToList();
                            potentialMatch = clientFound.Count == 0 ? false : true;
                        }

                        // 2: Check for email match
                        if (clientFound.Count == 0)
                        {
                            clientFound = (from c in context.Client
                                           join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                           where cea.EmailAddress.Trim().Replace(" ", "").ToUpper() == model.PrimaryEmail.Trim().Replace(" ", "").ToUpper()
                                           && cea.IsPrimary == true
                                           && c.Id != newClient.Id
                                           && c.ProfileMerged == false
                                           && c.IsDeleted == false
                                           select c)
                                          .ToList();
                            potentialMatch = clientFound.Count == 0 ? false : true;
                        }

                        // 3: Check for First Name, Last Name and Date Of Birth match
                        if (clientFound.Count == 0)
                        {
                            clientFound = (from c in context.Client
                                           join ca in context.ClientAddress on c.Id equals ca.Idclient
                                           join cea in context.ClientEmailAddress on c.Id equals cea.Idclient
                                           where c.FirstName.Trim().Replace(" ", "").ToUpper() == model.FirstName.Trim().Replace(" ", "").ToUpper()
                                                 && c.LastName.Trim().Replace(" ", "").ToUpper() == model.LastName.Trim().Replace(" ", "").ToUpper()
                                                 && c.DateOfBirth == model.Birthday
                                                 && c.Id != newClient.Id
                                                 && c.ProfileMerged == false
                                                 && c.IsDeleted == false
                                           select c)
                                          .ToList();
                            potentialMatch = clientFound.Count == 0 ? false : true;
                        }

                        // Update newly created Client with Potential Match
                        var savedClient = await context.Client.SingleOrDefaultAsync(c => c.Id == newClient.Id);
                        savedClient.PotentialMatch = potentialMatch;
                        await context.SaveChangesAsync();

                        // Save new loan request
                        LoanRequestStatus requestStatus = null;

                        if (model.AutoDecline)
                        {
                            requestStatus = context.LoanRequestStatus
                                            .Where(x => x.Code == "AUTODECLINED")
                                            .SingleOrDefault();
                        }
                        else
                        {
                            requestStatus = context.LoanRequestStatus
                                            .Where(x => x.Code == "NEW")
                                            .SingleOrDefault();
                        }

                        // Calculate Debt Ratio
                        var totalExpenses = Convert.ToDecimal(model.MonthlyPaymenet) + Convert.ToDecimal(model.MonthlyElectricalPayment)
                                                    + Convert.ToDecimal(model.MonthlyCarPayment) + Convert.ToDecimal(model.MontlyAppliancePayment);

                        var monthlyRevenu = Convert.ToDecimal(model.GrossAnnualSalary) / 12;
                        var debtRatio = monthlyRevenu != 0 ? (totalExpenses / monthlyRevenu) * 100 : 0;

                        var externalLoanWebRequest = new Data.Core.Models.ExternalLoanWebRequest()
                        {
                            Idclient = newClient.Id,
                            Idlanguage = language == null ? (int?)null : language.Id,
                            IdincomeType = incomeType == null ? (int?)null : incomeType.Id,
                            IdpayFrequency = payFrequency == null ? (int?)null : payFrequency.Id,
                            StatusId = requestStatus == null ? (int?)null : requestStatus.Id,
                            Amount = Convert.ToDecimal(model.Amount.Replace("$", "")),
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Birthday = model.Birthday,
                            CivicNumber = model.CivicNumber,
                            StreetName = model.StreetName,
                            AptNo = model.AptNo,
                            Province = model.Province,
                            City = model.City,
                            Country = "CA",
                            ResidenceStartDate = model.ResidenceStartDate,
                            PostalCode = model.PostalCode,
                            //PhoneNumber = model.PhoneNumber,
                            CellPhoneNumber = model.CellPhoneNumber,
                            PrimaryEmail = model.PrimaryEmail,
                            //SecondaryEmail = model.SecondaryEmail,

                            Ref1FirstName = model.Ref1FirstName,
                            Ref1LastName = model.Ref1LastName,
                            Ref1Telephone = model.Ref1Telephone,
                            //Ref1Email = model.Ref1Email,
                            Ref1Relationship = model.Ref1Relationship,

                            Ref2FirstName = model.Ref2FirstName,
                            Ref2LastName = model.Ref2LastName,
                            Ref2Telephone = model.Ref2Telephone,
                            //Ref2Email = model.Ref2Email,
                            Ref2Relationship = model.Ref2Relationship,

                            Bankrupcy = "",// Validate with the webform         

                            IsRenter = model.IsRenter,
                            MonthlyPaymenet = model.MonthlyPaymenet != null ? Convert.ToDecimal(model.MonthlyPaymenet) : (decimal?)null,
                            MonthlyElectricalPayment = model.MonthlyElectricalPayment != null ? Convert.ToDecimal(model.MonthlyElectricalPayment) : (decimal?)null,
                            MonthlyCarPayment = model.MonthlyCarPayment != null ? Convert.ToDecimal(model.MonthlyCarPayment) : (decimal?)null,
                            MontlyAppliancePayment = model.MontlyAppliancePayment != null ? Convert.ToDecimal(model.MontlyAppliancePayment) : (decimal?)null,
                            GrossAnnualSalary = model.GrossAnnualSalary != null ? Convert.ToDecimal(model.GrossAnnualSalary) : (decimal?)null,

                            EmployerName = model.EmployerName,
                            EmployerPhoneNumber = model.EmployerPhoneNumber,
                            EmployerExtension = model.EmployerExtension,
                            SupervisorName = model.SupervisorName,
                            Occupation = model.Occupation,
                            NextPayDate = model.NextPayDate,
                            HiringDate = model.HiringDate,

                            EmploymentInsuranceStartDate = model.EmploymentInsuranceStartDate,
                            EmploymentInsuranceNextPayDate = model.EmploymentInsuranceNextPayDate,
                            DirectDeposit = model.DirectDeposit,
                            SelfEmployedStartDate = model.SelfEmployedStartDate,
                            SelfEmployedWorkTelephone = model.SelfEmployedWorkTelephone,
                            NextDepositDate = model.NextDepositDate,
                            LoanWithoutDocuments = model.LoanWithoutDocuments,

                            Ipaddress = model.IpAddress,
                            RequestedDate = DateTime.Now,
                            CreditBookCheckSuccessfull = model.CreditBookCheckSuccessfull,
                            CreditBookCheckRequestId = model.CreditBookCheckRequestId,

                            DebtRatio = debtRatio, // Need to calculate

                            GoogleId = model.GoogleId,
                            GoogleEmail = model.GoogleEmail,
                            GoogleFisrtName = model.GoogleFirstName,
                            GoogleLastName = model.GoogleLastName,
                            GooglePhoto = model.GooglePhoto,

                            DeclaringBankrupcy = model.DeclaringBankrupcy,
                            IbvrequestId = model.IBVRequestId,

                            FacebookId = model.FacebookId,
                            FacebookEmail = model.FacebookEmail,
                            FacebookFirstName = model.FacebookFirstName,
                            FacebookLastName = model.FacebookLastName,
                            FacebookPhoto = model.FacebookPhoto,

                            ClientMatched = potentialMatch,

                            AutoDeclined = model.AutoDecline
                        };
                        await context.ExternalLoanWebRequest.AddAsync(externalLoanWebRequest);
                        await context.SaveChangesAsync();

                        // Add ExternalLoanWebRequestChecklist
                        var loanRequestChecklist = await context.LoanRequestChecklist
                                                            .Select(x => x)
                                                            .ToListAsync();

                        foreach (var check in loanRequestChecklist)
                        {
                            await context.ExternalLoanWebRequestChecklist.AddAsync(new ExternalLoanWebRequestChecklist()
                            {
                                IdexternalLoanWebRequest = externalLoanWebRequest.Id,
                                IdloanRequestChecklist = check.Id,
                                Confirmed = false
                            });
                        }
                        await context.SaveChangesAsync();

                        // Since a request was completed, we can remove the non completed request
                        var nonCompleted = context.ExternaNonCompletedRequest
                                            .Where(x => x.ReferenceNo == model.ReferenceNo)
                                            .SingleOrDefault();

                        if (nonCompleted != null)
                        {
                            nonCompleted.IsActive = false;
                            await context.SaveChangesAsync();
                        }

                        // Save to Client IBV table only if Autodecline is False
                        if (!model.AutoDecline)
                        {
                            var ibvStatus = context.ClientIbvStatus
                            .Where(x => x.Code == "RS")
                            .SingleOrDefault();

                            await context.ClientIbv.AddAsync(new ClientIbv()
                            {
                                Idclient = newClient.Id,
                                IdclientIbvstatus = ibvStatus != null ? (int?)ibvStatus.Id : null,
                                CreatedDate = DateTime.Now,
                                Url = model.IBVReportUrl,
                                Guid = model.IBVRequestId
                            });
                            await context.SaveChangesAsync();
                        }

                        try
                        {
                            await _webhookService24HR.NewClientAdded_24HR(externalLoanWebRequest);
                        }
                        catch { }

                        // Commit at the end
                        await transaction.CommitAsync();
                    }
                    catch (Exception ex)
                    {
                        var t = JsonConvert.SerializeObject(model).ToString();
                        _errorService.Logger(ex, JsonConvert.SerializeObject(model).ToString());
                        await transaction.RollbackAsync();
                        return false;
                    }
                }
            }

            try
            {
                // Add "client created" history entry
                await _clientHistoryService.AddClientHistoryEntry(newClient.Id, ClientHistoryTypeEnum.NewLoanRequest);
                return true;
            }
            catch (Exception ex)
            {
                _errorService.Logger(ex, JsonConvert.SerializeObject(model));
                return false;
            }
        }

        public async Task<bool> AutoDecline(NewLoanFormRequestModel model)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                try
                {
                    var autoDelines = await context.ExternalLoanWebRequestAutoDecline
                            .Where(x => x.IsActive == true)
                            .ToListAsync();

                    foreach (var reason in autoDelines)
                    {

                        if (reason.Code == "NOT_18")
                        {
                            if (model.Birthday.AddYears(18) > DateTime.Today)
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "LESS_THAN_A_YEAR")
                        {
                            // Leap year
                            if (model.ResidenceStartDate.Year % 4 == 0)
                            {
                                if (DateTime.Today < model.ResidenceStartDate.AddDays(366))
                                {
                                    return true;
                                }
                            }
                            else
                            {
                                if (DateTime.Today < model.ResidenceStartDate.AddDays(365))
                                {
                                    return true;
                                }
                            }
                        }

                        if (reason.Code == "LESS_THAN_3_MONTHS")
                        {
                            if (DateTime.Today < model.ResidenceStartDate.AddMonths(3))
                            {
                                return true;
                            }
                        }


                        if (reason.Code == "WITHIN_QUEBEC")
                        {
                            if (model.Province == "QC")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "EMPLOYEE")
                        {
                            if (model.IncomeTypeCode == "EMPLOYEE")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "EMPLOYMENT_INSURANCE")
                        {
                            if (model.IncomeTypeCode == "EMPLOYMENT_INSURANCE")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "RETIREMENT_PLAN")
                        {
                            if (model.IncomeTypeCode == "RETIREMENT_PLAN")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "DISABILITY")
                        {
                            if (model.IncomeTypeCode == "DISABILITY")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "SELF_EMPLOYED")
                        {
                            if (model.IncomeTypeCode == "SELF_EMPLOYED")
                            {
                                return true;
                            }
                        }

                        if (reason.Code == "PARENTAL_INSURANCE")
                        {
                            if (model.IncomeTypeCode == "PARENTAL_INSURANCE")
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public async Task<string> NonCompletedRequest(NonCompletedRequestModel model)
        {
            using (var context = _dbContextProvider.GetContext())
            {
                //try
                //{
                // Get Language
                var language = context.Language
                                        .Where(x => x.Code == model.LanguageCode)
                                        .SingleOrDefault();

                var referenceNo = Guid.NewGuid().ToString().ToUpper();

                await context.ExternaNonCompletedRequest.AddAsync(new ExternaNonCompletedRequest()
                {
                    Idlanguage = language == null ? (int?)null : language.Id,

                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Birthday = model.Birthday,
                    PhoneNumber = model.PhoneNumber,
                    CellPhoneNumber = model.CellPhoneNumber,
                    PrimaryEmail = model.PrimaryEmail,
                    SecondaryEmail = model.SecondaryEmail,
                    CreatedDate = DateTime.Now,
                    IsActive = true,
                    ReferenceNo = referenceNo,

                    GoogleId = model.GoogleId,
                    GoogleEmail = model.GoogleEmail,
                    GoogleFisrtName = model.GoogleFirstName,
                    GoogleLastName = model.GoogleLastName,
                    GooglePhoto = model.GooglePhoto,

                    FacebookId = model.FacebookId,
                    FacebookPhoto = model.FacebookPhoto,
                    FacebookEmail = model.FacebookEmail,
                    FacebookFirstName = model.FacebookFirstName,
                    FacebookLastName = model.FacebookLastName
                });
                await context.SaveChangesAsync();

                return referenceNo;
                //}
                //catch (Exception ex)
                //{
                //    return null;
                //}      
            }
        }
    }
}