using System.Collections.Generic;
using System.Threading.Tasks;
using CreditBook.LMS.Domain.Core.Models;

namespace CreditBook.LMS.Service.Core
{
    public interface IMessageTemplateService
    {
        Task<MessageTemplateModel> GetMessageTemplate(int messageTemplateId);
        Task<MessageTemplateModel> UpdateMessageTemplate(MessageTemplateModel data);
        Task<IEnumerable<MessageTemplateModel>> GetMessageTemplateList();
        Task SaveEmailConfiguration(EmailConfigurationModel data);
        Task<EmailConfigurationModel> GetEmailConfiguration();
    }
}